<?php

namespace App\Imports;

use App\Models\Inventory;
use App\Models\Pricing;
use App\Models\Wholesale\Wholesaler;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PricingImport implements ToCollection
{

    public function __construct($category,$url)
    {
        $this->category = $category;
        $this->url=$url;
    }
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $array = $collection->toArray();
        $title_row = array_shift($array);
        $title_row_without_bizname_slug = array_values(array_slice($title_row, 2));

        $finalarray = array_slice($array, 0);
        // dd($finalarray);
        $pricing_data = collect($finalarray);

        foreach ($pricing_data as $wholesaler_row) {

            $wholesaler_category_pricing = collect($wholesaler_row);

            $wholesaler_business_no = $wholesaler_category_pricing[1];
            $wholesaler = Wholesaler::findByRegNo($wholesaler_business_no);

            $wholesaler_category_pricing->forget([0, 1]);
            $sku_pricing = $wholesaler_category_pricing->all();
            if ($wholesaler) {
                $i = 0;
                foreach ($sku_pricing as $price) {

                    $inventory = Inventory::findBySku($title_row_without_bizname_slug[$i]);
                    if ($inventory) {

                        Pricing::where('inventory_id', $inventory->id)->where('wholesaler_id', $wholesaler->id)->forceDelete();

                        Pricing::create([
                            'inventory_id' => $inventory->id,
                            'wholesaler_id' => $wholesaler->id,
                            'price' => $price
                        ]);
                    }
                    $i++;
                }
            }
        }

        unlink($this->url);

    }
}
