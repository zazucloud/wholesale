<?php

namespace App\Imports;

use App\Models\Inventory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class InventoryPricingImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $key => $row) {
            $inventory = Inventory::findBySku($row[1]);
            if (!is_null($inventory)) {
                $inventory->price = $row[3];
                $inventory->crossed_price = $row[4];
                $inventory->update();
            }
        }
    }
}
