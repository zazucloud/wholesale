<?php

namespace App\Imports;

use App\Models\Inventory;
use App\Models\ZoneDiscounts;
use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Wholesale\WholesalerItemDiscount;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
class ZoneDiscountImport extends DefaultValueBinder implements ToCollection, WithHeadingRow, WithCustomValueBinder
{

    public function __construct($zone)
    {
        $this->zone = $zone;
    }
    /**
     * @param Collection $collection
     */

    public function bindValue(Cell $cell, $value)
    {

        if ($cell->getColumn()=="D" && $cell->getCoordinate() !== "D1") {
            $cell->setValueExplicit((float) $value, DataType::TYPE_NUMERIC);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function collection(Collection $collection)
    {

        $wholesalers = $this->zone->wholesalers;

        ZoneDiscounts::where('zone_id', $this->zone->id)->forceDelete();

        foreach ($wholesalers as $key => $value) {
            WholesalerItemDiscount::where('wholesaler_id', $value->id)->forceDelete();
        }

        foreach ($collection as $key => $row) {

                $inventory = Inventory::find($row["rcd_id"]);
                if (
                    !is_null($inventory) &&
                    (float)$inventory->price !== $row["regional_price"] &&
                    $row["regional_price"] < (float)$inventory->price
                ) {
                    $zone_discount = new ZoneDiscounts();
                    $zone_discount->zone_id = $this->zone->id;
                    $zone_discount->inventory_id = $row["rcd_id"];
                    $zone_discount->custom_discount = $row["regional_price"];
                    $zone_discount->save();

                }

        }

        foreach ($wholesalers as $key => $value) {
            $value->price_type = 1;
            $value->update();
        }
    }

    public function computePercentage($original, $custom)
    {
        return (($original - $custom) / $original) * 100;
    }
}
