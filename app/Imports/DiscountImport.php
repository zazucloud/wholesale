<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Wholesale\WholesalerItemDiscount;

class DiscountImport implements ToCollection, WithHeadingRow
{

    private $wholesaler_id;

    public function __construct($wholesaler_id)
    {
        $this->wholesaler_id = $wholesaler_id;
    }
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {

        foreach ($collection as $key => $row) {
          
            if ($key !== 0) {

                $custom_discount = ($row["discount"] === null) ? 0 : ($row["discount"]);

                $item_id = (int)$row["rcd_id"];


                if (!is_null($item_id) || $item_id !== 0) {

                    if ($custom_discount > 0) {
                        $wh_discount = new WholesalerItemDiscount();
                        $wh_discount->wholesaler_id = $this->wholesaler_id;
                        $wh_discount->inventory_id = $item_id;
                        $wh_discount->custom_discount = $custom_discount;
                        $wh_discount->save();
                    }
                }
            }
        }
    }
}
