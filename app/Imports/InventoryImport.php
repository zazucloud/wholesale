<?php

namespace App\Imports;

use App\Models\Brand;
use App\Models\Inventory;
use Illuminate\Http\Request;
use App\Models\InventoryCategory;
use App\Models\InventoryAttribute;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Models\InventoryItemCategory;
use App\Models\InventoryItemAttribute;
use App\Models\InventoryAttributeOption;
use App\Models\Warehouses;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class InventoryImport implements ToCollection, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure, WithChunkReading, WithStartRow
{
    use Importable, SkipsErrors, SkipsFailures;

    private $setStartRow = 2;

    private $first_row;

    public function __construct(Request $request = null, $start)
    {
        if (!is_null($request)) {
            $this->fields = $request->fields;
        }

        if (!is_null($start)) {
            $this->setStartRow = $start;
        }
    }

    public function startRow(): int
    {
        return $this->setStartRow;
    }

    public function collection(Collection $rows)
    {
        //extract keys from second object in the list
        $this->first_row=$rows[1]->keys();

        //if color attribute doesn't exist creatde new one
        $color_attribute = InventoryAttribute::firstOrCreate([
            'label' => 'Color',
            'slug' => 'color',
            'type' => 'fixed'
        ]);
        $color_attribute_id = $color_attribute->id;

        //find warehouse


        //group by brand
        $group_by_brand = $rows->values()->groupBy($this->returnPosition('brand'));


        foreach ($group_by_brand as $key => $value) {
            if ($key !== "") {

                //fetch brand and if it doesnt exist create new brand
                $find_brand = Brand::where('slug', str_slug($key))->first();

                if (is_null($find_brand)) {
                    $brand = Brand::Create([
                        'name' => $key,
                        'slug' => str_slug($key),
                        'narration' => $key
                    ]);
                } else {
                    $brand = $find_brand;
                }

                //groub by inventory category , if it doesnt exist create new category
                $group_by_subcategory = $value->groupBy($this->returnPosition('sub_category'));

                foreach ($group_by_subcategory as $key1 => $value2) {
                    $category_slug = str_slug($key1);
                    $inventory_category = InventoryCategory::where('slug', $category_slug)->first();

                    if (is_null($inventory_category)) {
                        $inventory_category = InventoryCategory::create([
                            'name' => $key1,
                            'parent' => 0,
                            'brand_id' => $brand->id,
                            'slug' => $category_slug,
                            'status' => 1
                        ]);
                    }

                    foreach ($value2 as $row) {
                        $warehouse = Warehouses::where('name', $row[$this->returnPosition('warehouse')])->first();
                        // dd((((int)$row[$this->returnPosition('productionitem')] == 1) ? 1 : 0));

                        // create sku from a combination of item number and color provided in excell sheet
                        $sku = $row[$this->returnPosition('item_number')] . '_' . str_replace(' ', '', str_replace('/', '',  $row[$this->returnPosition('color')]));
                        $inventory = Inventory::where('sku', $sku)->get()->first();

                        //check if inventory item exits, if it does update existing record else create new record
                        if (is_null($inventory)) {
                            $inventory = Inventory::create([
                                'name' => $row[$this->returnPosition('product_name')],
                                'sku' => $sku,
                                'itemnumber' => $row[$this->returnPosition('item_number')],
                                'color' => $row[$this->returnPosition('color')],
                                'description' => $row[$this->returnPosition('description')] ?? null,
                                'price' => $row[$this->returnPosition('price')],
                                'quantity' => $row[$this->returnPosition('quantity')] ?? null,
                                'brand_id' => ((is_null($brand)) ? null : $brand->id),
                                'warehouse_id' => ((is_null($warehouse)) ? null : $warehouse->id),
                                'productionitem' => (($row[$this->returnPosition('productionitem')] == 1) ? 1 : 0)
                            ]);

                            //create link between inventory and category if it doesnt exist
                            InventoryItemCategory::firstOrCreate([
                                'inventory_id' => $inventory->id,
                                'category_id' => $inventory_category->id,
                            ]);

                            // check if color exist in aatributes table ,if it doesnt create new collor attribute then associate that color attribute to the inventory
                            if (($row[$this->returnPosition('color')] != 'NA') || ($row[$this->returnPosition('color')] != 'N/A')) {
                                $color_attribute_option = InventoryAttributeOption::firstOrCreate([
                                    'attribute_id' => $color_attribute_id,
                                    'option' => $row[$this->returnPosition('color')]
                                ]);

                                InventoryItemAttribute::firstOrCreate([
                                    'inventory_id' => $inventory->id,
                                    'attribute_id' => $color_attribute_id,
                                    'attribute_type' => $color_attribute->type,
                                    'value' => $color_attribute_option->id
                                ]);
                            }
                        } else {
                            $inventory->price = $row[$this->returnPosition('price')];
                            $inventory->quantity = $row[$this->returnPosition('quantity')] ?? null;
                            $inventory->warehouse_id = ((is_null($warehouse)) ? null : $warehouse->id);
                            $inventory->productionitem = (($row[$this->returnPosition('productionitem')] == 1) ? 1 : 0);
                            $inventory->update();
                        }
                    }
                }
            }
        }
    }


    public function rules(): array
    {
        return [

            // '*.name' => ['unique:inventory,productname'],
            '*.sku' => ['unique:inventory,sku'],
        ];
    }

    public function chunkSize(): int
    {
        return 100;
    }

    //used to perfoma collumn mapping and return correct index postion
    public function returnPosition($name){
        return $this->first_row[array_search($name, $this->fields)];
    }
}
