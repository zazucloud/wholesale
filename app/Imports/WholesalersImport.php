<?php

namespace App\Imports;

use App\Models\User;
use App\Models\County;
use Maatwebsite\Excel\Row;
use Illuminate\Http\Request;
use App\Models\StaffWholesaler;
use App\Models\Wholesale\Wholesaler;
use Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\Importable;
// use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class WholesalersImport implements
    // ToModel,
    WithStartRow,
    SkipsOnError,
    WithValidation,
    SkipsOnFailure,
    WithChunkReading,
    OnEachRow
{
    use Importable, SkipsErrors, SkipsFailures;

    private $setStartRow = 2;

    public function __construct(Request $request = null, $start)
    {
        if (!is_null($request)) {
            $this->fields = $request->fields;
        }

        if (!is_null($start)) {
            $this->setStartRow = $start;
        }
    }

    public function startRow(): int
    {
        return $this->setStartRow;
    }


    public function  onRow(Row $row)
    {
        $county = County::where('name', 'LIKE', '%' . $row[array_search('county', $this->fields)] . '%')->first();

        $wholesaler = Wholesaler::where('business_name',$row[array_search('business_name', $this->fields)])
        ->orWhere('slug', str_replace(" ", "-", $row[array_search('business_name', $this->fields)]))->first();

        if (is_null($wholesaler)) {
            $wholesaler = Wholesaler::create([
                'business_name' => $row[array_search('business_name', $this->fields)],
                'slug' => str_replace(" ", "-", $row[array_search('business_name', $this->fields)]),
                'email' => $row[array_search('email', $this->fields)],
                'telephone' => $row[array_search('telephone', $this->fields)],
                'registration_number' => $row[array_search('customer_code', $this->fields)],
                'physical_address' => $row[array_search('physical_address', $this->fields)],
                'postal_address' => $row[array_search('postal_address', $this->fields)],
                'pin' => $row[array_search('pin', $this->fields)],
                'contact_name' => $row[array_search('contact_name', $this->fields)],
                'contact_position' => $row[array_search('contact_position', $this->fields)],
                'county_id' => $county->id ?? null,
                'credit_mode' => $row[array_search('credit_mode', $this->fields)] == 'credit' ? 0 : 1


            ]);
        } else {
            $wholesaler->update([
                'email' => $row[array_search('email', $this->fields)],
                'telephone' => $row[array_search('telephone', $this->fields)],
                'registration_number' => $row[array_search('customer_code', $this->fields)],
                'physical_address' => $row[array_search('physical_address', $this->fields)],
                'postal_address' => $row[array_search('postal_address', $this->fields)],
                'pin' => $row[array_search('pin', $this->fields)],
                'contact_name' => $row[array_search('contact_name', $this->fields)],
                'contact_position' => $row[array_search('contact_position', $this->fields)],
                'county_id' => $county->id ?? null,
                'credit_mode' => $row[array_search('credit_mode', $this->fields)] == 'credit' ? 0 : 1
            ]);
        }

        // $user = User::where('email', $row[array_search('staffemail', $this->fields)])->first();



        // if (!is_null($user)) {

        //     $staff_check = StaffWholesaler::where('user_id', $user->id)
        //         ->where('wholesaler_id', $wholesaler->id)
        //         ->first();

        //     if (is_null($staff_check)) {
        //         StaffWholesaler::create([
        //             'user_id' => $user->id,
        //             'wholesaler_id' => $wholesaler->id,
        //         ]);
        //     }
        // }
    }

    public function rules(): array
    {
        return [
            '*.business_name' => ['unique:wholesalers,business_name'],
            '*.telephone' => ['unique:wholesalers,telephone'],
            '*.email' => ['unique:wholesalers,email'],
        ];
    }

    public function chunkSize(): int
    {
        return 100;
    }
}
