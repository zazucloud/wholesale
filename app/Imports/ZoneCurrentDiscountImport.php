<?php

namespace App\Imports;

use App\Models\Inventory;
use App\Models\ZoneDiscounts;
use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Wholesale\WholesalerItemDiscount;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
class ZoneCurrentDiscountImport extends DefaultValueBinder implements ToCollection, WithHeadingRow, WithCustomValueBinder
{

    public function __construct($zone)
    {
        $this->zone = $zone;
    }
    /**
     * @param Collection $collection
     */

    public function bindValue(Cell $cell, $value)
    {

        if ($cell->getColumn()=="C" && $cell->getCoordinate() !== "C1") {
            $cell->setValueExplicit((float) $value, DataType::TYPE_NUMERIC);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function collection(Collection $collection)
    {
        ZoneDiscounts::where('zone_id', $this->zone)->forceDelete();

        foreach ($collection as $key => $row) {
                $inventory = Inventory::find($row["rcd_id"]);
                if (
                    !is_null($inventory) &&
                    (float)$inventory->price !== $row["baseprice"] &&
                    $row["baseprice"] < (float)$inventory->price
                ) {
                    $zone_discount = new ZoneDiscounts();
                    $zone_discount->zone_id = $this->zone;
                    $zone_discount->inventory_id = $row["rcd_id"];
                    $zone_discount->custom_discount = $row["baseprice"];
                    $zone_discount->save();

                }

        }


    }
}
