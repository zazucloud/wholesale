<?php

namespace App\Imports;

use App\Models\Wholesale\WholesalerCategoryDiscount;
use App\Models\Wholesale\WholesalerCategoryVisibility;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CategoryVisibilityImport implements ToCollection
{

    public function __construct($url)
    {
        $this->url = $url;
    }
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {

        foreach ($collection as $key => $row) {

            if ($key !== 0) {

                $row[2] = strtolower(trim($row[2]));

                $rcd_id = $row[4];
                $visibility = (($row[2] == 'hidden') || ($row[2] == 'visible')) ? $row[2] : 'hidden';
                $discont_rate = ($row[3] === null) ? 0 : ($row[3]);

                $rcd = explode('.', $rcd_id);

                if (count($rcd) === 2) {
                    $wholesaler_id = $rcd[0];
                    $category_id = $rcd[1];

                    //Handle Visibility
                    $current_visibility = WholesalerCategoryVisibility::check($wholesaler_id, $category_id);

                    if (($current_visibility != null) && ($visibility == 'hidden')) {
                        $current_visibility->forceDelete();
                    }

                    if (($current_visibility === null) && ($visibility == 'visible')) {

                        WholesalerCategoryVisibility::create([
                            'wholesaler_id' => $wholesaler_id,
                            'category_id' => $category_id
                        ]);
                    }


                    //Handle Discount
                    $discount_data = [
                        'wholesaler_id' => $wholesaler_id,
                        'category_id' => $category_id,
                        'discount_rate' => $discont_rate
                    ];

                    WholesalerCategoryDiscount::where('wholesaler_id', $wholesaler_id)->where('category_id', $category_id)->forceDelete();

                    if ($discont_rate > 0) {
                        WholesalerCategoryDiscount::create($discount_data);
                    }
                }
            }
        }

        unlink($this->url);
    }
}
