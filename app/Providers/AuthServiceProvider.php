<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Route;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Route::post('oauth/token', [
            'uses' => 'App\Http\Controllers\API\AuthController@auth',
            'as' => 'passport.token',
            'middleware' => 'throttle'
        ]);

        Route::post('default/oauth/token', [
            'uses' => 'Laravel\Passport\Http\Controllers\AccessTokenController@issueToken',
            'as' => 'passport.original.token',
            'middleware' => 'throttle'
        ]);

        //token passport expires
        Passport::tokensExpireIn(Carbon::now()->addHours(1));
        //passport refresh token
        Passport::refreshTokensExpireIn(Carbon::now()->addHours(3));
    }
}
