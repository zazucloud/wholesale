<?php

use App\Models\Company;
use App\Models\User;
use App\Models\Wholesale\WholesalerCategoryDiscount;
use App\Models\Wholesale\WholesalerItemDiscount;
use App\Models\Wholesaler;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


if (!function_exists('error_class')) {
    function error_class($errors, $key)
    {
        return $errors->has($key) ? ' has-error' : '';
    }
}

if (!function_exists('error_tag')) {
    function error_tag($errors, $key)
    {
        return $errors->has($key) ? "<span class='help-block'><strong>{$errors->first($key)}</strong></span>" : "";
    }
}

if (!function_exists('user')) {

    function user($key = null)
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        if (!is_null($key)) return $user->$key;

        return $user;
    }
}

if (!function_exists('current_user_avatar')) {

    function current_user_avatar($user_id = null)
    {

        if ($user_id == null) {
            $avatar = optional(user())->avatar;
        } else {
            $avatar = \App\Models\User::findOrFail($user_id)->avatar;
        }

        if ($avatar == null) {

            return asset('avatars/default_avatar.png');
        }


        return asset("avatars/$avatar");
    }
}

if (!function_exists('now')) {

    function now()
    {
        return Carbon::now()->toDateTimeString();
    }
}


function stamp_date_time($timestamp)
{
    if ($timestamp == null) return '-';

    $timed = strtotime($timestamp);
    return date('d /m /Y', $timed);
}

// previous date time

function stamp_date_time_cust()
{
    $timed = time();
    return date('F jS, Y', $timed);
}


function stamp_date($timestamp)
{
    if ($timestamp == null) return '-';
    $timed = strtotime($timestamp);
    return date('d /m /Y', $timed);
}

function stamp_date_clean($timestamp)
{
    $timed = strtotime($timestamp);
    return date('d /m /Y', $timed);
}

function stamp_date_time_now()
{

    $timed = time();
    return date('d /m /Y', $timed);
}

function stamp_date_time_now_full()
{
    $timed = time();
    return date('d /m /Y h:i A', $timed);
}


function stamp_date_now_full()
{
    $timed = time();
    return date('d /m /Y', $timed);
}

function stamp_date_time_simple($timestamp)
{
    $timed = strtotime($timestamp);
    return date('d /m /Y', $timed);
}

if (!function_exists('ip')) {

    function ip()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
        }
        $ip = filter_var($ip, FILTER_VALIDATE_IP);
        $ip = ($ip == false) ? '0.0.0.0' : $ip;
        return $ip;
    }
}
if (!function_exists('first_phrase')) {

    function first_phrase($string)
    {

        $split = explode(' ', $string);

        return $split[0];
    }
}

function intval_v2($string)
{
    // $string = self::remove_characters($string);
    $string = preg_replace("/[^0-9\.]/", "", $string);

    $string = floatval($string);
    return $string;
}

function remove_characters($string)
{
    $string = preg_replace("/[^0-9\.]/", "", $string);
    return $string;
}

if (!function_exists('ago')) {

    function ago($timestamp)
    {
        // return Carbon::createFromTimeStamp(strtotime($timestamp))->diffForHumans();
        return $timestamp->diffForHumans();

        // return Carbon::createFromTimeStamp(strtotime($timestamp))->diffForHumans();
    }
}


if (!function_exists('flash_message')) {

    function flash_message($flash_type, $flash_message)
    {
        Session::flash('flash_message', $flash_message);
        Session::flash('flash_type', $flash_type);
    }
}

if (!function_exists('mac_address')) {

    function mac_address()
    {
        $macAddr = false;
        $arp = `arp -n`;
        $lines = explode("\n", $arp);

        foreach ($lines as $line) {
            $cols = preg_split('/\s+/', trim($line));

            if ($cols[0] == $_SERVER['REMOTE_ADDR']) {
                $macAddr = $cols[2];
            }
        }

        return $macAddr;
    }
}


if (!function_exists('passwordGenerator')) {

    function passwordGenerator($length = 9)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789?!-@%&_";

        $string = substr(str_shuffle($chars), 0, $length);

        return $string;
    }
}


if (!function_exists('media_directory')) {

    function media_directory($file_name = '')
    {
        return base_path() . '/public/media/' . $file_name;
    }
}

if (!function_exists('avatar_directory')) {

    function avatar_directory($file_name = '')
    {
        return base_path() . '/public/avatars/' . $file_name;
    }
}

if (!function_exists('avatar_uploader')) {

    function avatar_uploader($file)
    {
        if ($file == null) return null;
        $image_name = 'a-' . random_unique() . '.' . $file->getClientOriginalExtension();
        if ($file->move(avatar_directory(), $image_name)) return $image_name;
        return null;
    }
}


if (!function_exists('user_avatar')) {

    function user_avatar($user_id = null)
    {
        $user = \App\Models\User::find($user_id);

        if (($user != null) && ($user_id != null) && ($user->avatar != null) && (file_exists(avatar_directory() . $user->avatar))) return asset("avatars") . '/' . $user->avatar;
        return asset('avatars/default_avatar.png');
    }
}


if (!function_exists('media_file')) {

    function media_file($media_file = null)
    {
        if (($media_file != null) && (file_exists(media_directory() . $media_file))) return asset("media") . '/' . $media_file;
        return asset('media/default.png');
    }
}

if (!function_exists('random_unique')) {

    function random_unique($length = 32)
    {
        return str_random($length) . time();
    }
}


if (!function_exists('user_permissions')) {

    function user_permissions($user_id)
    {

        return User::findOrFail($user_id)->permissions;
    }
}


if (!function_exists('last_login')) {

    function last_login($user_id)
    {

        $last_login = \Spatie\Activitylog\Models\Activity::where('causer_id', $user_id)->where('log_name', 'auth')->orderBy('created_at', 'DESC')->first();

        return $last_login != null ? stamp_date_time_simple($last_login->created_at) : '-';
    }
}


if (!function_exists('convert_number_to_words')) {
    function convert_number_to_words($number, $suffix = '')
    {
        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int)($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string)$fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        $string = $string . ' ' . $suffix;
        $string = ucwords($string);
        $string = str_replace('And', 'and', $string);
        return trim($string);
    }
}

function array_group_by(array $arr, $key): array
{
    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
        trigger_error('array_group_by(): The key should be a string, an integer, a float, or a function', E_USER_ERROR);
    }
    $isFunction = !is_string($key) && is_callable($key);
    // Load the new array, splitting by the target key
    $grouped = [];
    foreach ($arr as $value) {
        $groupKey = null;
        if ($isFunction) {
            $groupKey = $key($value);
        } else if (is_object($value)) {
            $groupKey = $value->{$key};
        } else {
            $groupKey = $value[$key];
        }
        $grouped[$groupKey][] = $value;
    }
    // Recursively build a nested grouping if more parameters are supplied
    // Each grouped array value is grouped according to the next sequential key
    if (func_num_args() > 2) {
        $args = func_get_args();
        foreach ($grouped as $groupKey => $value) {
            $params = array_merge([$value], array_slice($args, 2, func_num_args()));
            $grouped[$groupKey] = call_user_func_array('array_group_by', $params);
        }
    }
    return $grouped;
}

if (!function_exists('flatten')) {

    function flatten($array, $prefix = '')
    {
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = $result + flatten($value, $prefix . $key . '.');
            } else {
                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }
}


function wholesale_price($wholesaler_id, $inventory_id, $category_id = null)
{

    if (is_object($inventory_id)) {
        $inventory = $inventory_id;
    } else {
        $inventory = \App\Models\Inventory::findOrFail($inventory_id);
    }

    $original_price = $inventory->price;

    $discount_rate = wholesaler_item_discount($wholesaler_id, $inventory);

    return calculate_discounted_price($original_price, $discount_rate);
}

function wholesaler_name($wholesaler_id)
{


    $wholesaler = \App\Models\Wholesale\Wholesaler::findOrFail($wholesaler_id);


    $wholesaler_name = $wholesaler->business_name;


    return $wholesaler_name;
}


function wholesaler_item_discount($wholesaler_id, \App\Models\Inventory $inventory)
{

    $discount_rate = WholesalerItemDiscount::check($wholesaler_id, $inventory->id);

    if (!$discount_rate) {

        $categories = $inventory->categories()->pluck('category_id')->toArray();

        if (count($categories) === 1) {
            $discount_rate = WholesalerCategoryDiscount::discount($wholesaler_id, $categories);
        } else {

            $parent_categories = [];
            foreach ($categories as $category) {
                $children = \App\Models\InventoryCategory::child($category);
                if (count($children) == 0) {
                    $parent_categories[] = $category;
                }
            }

            $child_categories = array_diff($categories, $parent_categories);

            if (count($child_categories) === 0) {
                $discount_rate = WholesalerCategoryDiscount::discount($wholesaler_id, $parent_categories);
            } else {
                $discount_rate = WholesalerCategoryDiscount::discount($wholesaler_id, $child_categories);
            }
        }
    }

    return $discount_rate;
}


function wholesaler_item_price($wholesaler_id, $inventory)
{

    $price = wholesaler_item_discount($wholesaler_id, $inventory);
    $vat_amount = vat_amount($price, config('app.default_vat_rate'));

    return $price - $vat_amount;
}


function calculate_discounted_price($price, $discount_rate)
{

    $price_level = (100 - $discount_rate) / 100;

    return round($price * $price_level, 2);
}

function vat_amount($amount, $vat_rate)
{
    if (!$vat_rate) return 0;
    return round(($vat_rate / 100) * $amount, 2);
}

if (!function_exists('create_totals_plus_vat')) {

    function create_totals_plus_vat(Company $company, $price, $quantity)
    {

        if (!is_null($company)) {
            $vat_rate = $company->vat;
        } else {
            $vat_rate = 0;
        }

        $item_total = $price * $quantity;

       return $item_total;


    }
}

if (!function_exists('production_priority_level')) {

    function production_priority_level($level)
    {

        if ($level == 1) {
           echo 'Low';
        } elseif($level == 2){
            echo 'Medium';
        }
        else {
            echo 'High';
        }



    }
}

function inc_vat(int $amount)
{
    $vat_rate = config('app.default_vat_rate');
    return $amount + vat_amount($amount, $vat_rate);
}

function currency($amount, $prefix = 'Ksh')
{

    return $prefix . ' ' . number_format($amount, 2);
}

function amount($amount)
{

    return number_format($amount, 2);
}

function str_slug_reverse($string)
{

    return ucwords(str_replace('-', ' ', $string));
}



function percentage_discount_given($selling_price, $original_price)
{
    return round(100 - (($selling_price / $original_price) * 100), 2);
}


if (!function_exists('get_mime_type')) {

    function get_mime_type($filename)
    {
        $idx = explode('.', $filename);
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode - 1]);

        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset($mimet[$idx])) {
            return $mimet[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}


function ellipsis($string, $length, $stopanywhere = false)
{
    //truncates a string to a certain char length, stopping on a word if not specified otherwise.
    if (strlen($string) > $length) {
        //limit hit!
        $string = substr($string, 0, ($length - 3));
        if ($stopanywhere) {
            //stop anywhere
            $string .= '...';
        } else {
            //stop on a word.
            $string = substr($string, 0, strrpos($string, ' ')) . '...';
        }
    }
    return $string;
}


function version100()
{

    $permission = Permission::whereName('delete_wholesalers')->first();
    if (!$permission) {
        $permission_details = [
            'name' => 'delete_wholesalers',
            'label' => 'Delete Wholesalers',
            'group' => 'Wholesalers',
            'guard_name' => 'web'
        ];

        $permission = \App\Models\Permission::create($permission_details);

        // $role = Role::findByName('Administrator');
        //$role->givePermissionTo($permission);
    }

    return 'done';
}


function newPermissions()
{

    $permission = Permission::whereName('view_all_wholesalers')->first();
    if (!$permission) {
        $permission_details = [
            'name' => 'view_all_wholesalers',
            'label' => 'View All Wholesalers',
            'group' => 'Wholesalers',
            'guard_name' => 'web'
        ];

        $permission = \App\Models\Permission::create($permission_details);
    }


    return 'done';
}

function resetAllPasswords()
{
    if ((env('APP_ENV') == 'local') && (env('APP_DEBUG') == true))
        return User::where('status', 1)->update([
            'password' => bcrypt('password')
        ]);
}

function floatvalue($val)
{
    $val = str_replace(",", ".", $val);
    $val = preg_replace('/\.(?=.*\.)/', '', $val);
    return floatval($val);
}
function clearTestOrders()
{

    \App\Models\OrderProduct::truncate();
    \App\Models\OrderComment::truncate();
    \App\Models\Order::truncate();

    return 'done';
}

function populate_pricing()
{

    return (new \App\Models\Pricing())->populate();
}


function item_price(\App\Models\Wholesaler $wholesaler, \App\Models\Inventory $inventory)
{
    $cache = Cache::get('p.' . $wholesaler->id . '.' . $inventory->id);
    return ($cache) ? $cache : _item_price_from_db($wholesaler, $inventory);
}


function item_price_via_ids($wholesaler_id, $inventory_id)
{
    $cache = Cache::get('p.' . $wholesaler_id . '.' . $inventory_id);

    return ($cache) ? $cache : _item_price_from_db_via_ids($wholesaler_id, $inventory_id);
}

function _item_price_from_db_via_ids($wholesaler_id, $inventory_id)
{
    $record = \App\Models\Pricing::where('wholesaler_id', $wholesaler_id)->where('inventory_id', $inventory_id)->first();

    return ($record) ? $record->price : \App\Models\Inventory::find($inventory_id)->price;
}


function _item_price_from_db(\App\Models\Wholesaler $wholesaler, \App\Models\Inventory $inventory)
{

    $record = \App\Models\Pricing::where('wholesaler_id', $wholesaler->id)->where('inventory_id', $inventory->id)->first();

    return ($record) ? $record->price : $inventory->price;
}
