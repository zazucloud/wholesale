<?php

namespace App\Jobs;

use App\Mail\QuotationMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use stdClass;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class QuotationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $inquiry, $customer_email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inquiry, $customer_email)
    {
        $this->inquiry = $inquiry;
        $this->customer_email = $customer_email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $inquiry = $this->inquiry;
        $mail_data = new stdClass();
        $mail_data->wholesaler_name = $this->inquiry->clientable->business_name;
        $inquiry_no = $this->inquiry->inquiry_no;


        $pdf = PDF::loadView('exports.pdf.quotation', compact('inquiry'));
        $attachment_filename = 'Quotation' . $this->inquiry->clientable->business_name . '.pdf';
        Storage::disk('tempordersheets')->put($attachment_filename, $pdf->output());

        Mail::to($this->customer_email)->send(new QuotationMail($mail_data, $attachment_filename, $inquiry_no));

        unlink(storage_path('app/tempordersheets/' . $attachment_filename));
    }
}
