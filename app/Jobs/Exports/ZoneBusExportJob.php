<?php

namespace App\Jobs\Exports;

use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use App\Models\InventoryCategory;
use App\Exports\ZoneDiscountExport;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ZoneBusExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;
    public $timeout = 3000;
    private $file_name,$categories,$zone;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($categories,$zone,$name)
    {
        $this->file_name = $name;
        $this->categories = $categories;
        $this->zone = $zone;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $categories = InventoryCategory::whereIn('id', $this->categories)->get();
        (new ZoneDiscountExport($categories, $this->zone))->store($this->file_name, 'localexcels');
    }
}
