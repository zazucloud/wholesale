<?php

namespace App\Jobs;

use App\Mail\CreateWholesalerAccountMail;
use App\Models\User;
use App\Models\Wholesale\Wholesaler;
use App\Models\Wholesale\WholesalerUser;
use App\Models\WholesalerAccountManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use stdClass;

class CreateWholesalerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->data->username)->queue(new CreateWholesalerAccountMail($this->data));

        activity('wholesaler_account_creation')->performedOn($wholesaler)->withProperties(['ip' => ip()])->log("Created Wholesaler Account for ".$this->data['business_name'].' with login username: '.$username)->causedBy(user());

    }
}
