<?php

namespace App\Jobs;

use App\Models\InventoryManufacturer;
use App\Models\ProductionRequest;
use App\Models\ProductionRequestItem;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderProductionRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $client,$items,$delivery_date,$order_id,$type, $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($client,$items,$delivery_date,$order_id,$type,$user)
    {
        $this->client=$client;
        $this->items=$items;
        $this->delivery_date=$delivery_date;
        $this->order_id=$order_id;
        $this->type=$type;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            DB::beginTransaction();
            $request_tag = self::request_tag();

            if($this->type=="App\Models\Wholesale\Wholesaler" && !is_null($this->client->staff_custom)){
                $staff=$this->client->staff_custom()->first()->user_id ?? null;
            }else{
                $staff=null;
            }

            $new_production_request = $this->client->request()->create([
                'request_tag' => $request_tag,
                'staff_id' => $staff,
                'initiator' => !is_null($this->user) ? $this->user->id : null ,
                'delivery_date' => $this->delivery_date,
                'priority' => 3,
                'draft_stage' => 0,
                'notes' => "required for an order",
                'status' => 1,
                'order_id'=>$this->order_id
            ]);
            $producedItems = [];
            for ($i = 0; $i < sizeof($this->items); $i++) {
                $quantity = $this->items[$i]["leftover_quantity"];

                $inventory_id = $this->items[$i]["inventory_id"];
                $priority =3;

                $manufacturer_id = InventoryManufacturer::get_item_manufacturer($inventory_id);

                $itemsrequested=ProductionRequestItem::create([
                    'inventory_id' => $inventory_id,
                    'production_request_id' => $new_production_request->id,
                    'requested_quantity' => $quantity,
                    'production_status' => 0,
                    'manufacturer_id' => $manufacturer_id,
                    'item_priority' => $priority,
                ]);

                array_push($producedItems, $itemsrequested);
            }
            DB::commit();
            Log::info("commited");

            $groupedManufactures = collect($producedItems)->unique('manufacturer_id');


            foreach($groupedManufactures as $groupmanufacturer){
               $manufacturer = $groupmanufacturer->manufacturer;

               $listItems = collect($producedItems)->where('manufacturer_id', $manufacturer->id);

              $inventory =  $listItems->map(function ($item){
                    return   $item->inventory;
                });
                ManufactureProductionItemsJob::dispatch($manufacturer->email,$manufacturer->name, $listItems->toArray());
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info("order production request error: ".$th->getMessage());
            throw new \Exception($th);
        }

    }

    public static function request_tag($len = 8)
    {

        $shuffled_string = substr(str_shuffle("23ab34567c1de12893456fghjk4mnpq5rs7t6v5456w9xyz5"), 0, $len);
        $string = strtoupper($shuffled_string);

        $check = ProductionRequest::tag_is_unique($string);

        if ($check === false) return self::request_tag();

        return $string;
    }
}
