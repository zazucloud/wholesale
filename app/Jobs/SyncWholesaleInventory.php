<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Inventory;
use Illuminate\Support\Facades\Log;

class SyncWholesaleInventory implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $sku;
    protected $quantity;
    public function __construct($sku, $quantity)
    {
        $this->sku = $sku;
        $this->quantity = $quantity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {




        $inventoryItem = Inventory::where('sku', $this->sku)->first();

        // Log::info('Job started ');

        if ($inventoryItem ) {

            
        //  Log::info('Before update: SKU Check - ' . $inventoryItem->sku_check . ', Quantity - ' . $inventoryItem->quantity);

            if ($inventoryItem->sku_check === 0) {
                Log::info('SKU Check is 0 plus the quantity' . $inventoryItem->sku);
                $inventoryItem->quantity = $this->quantity;
                $inventoryItem->sku_check = 1;
                // Log::info('SKU Check ' . $inventoryItem->sku_check);
            } else {
                Log::info('SKU Check is 1 and quantity is ' . $inventoryItem->sku);
                $inventoryItem->quantity += $this->quantity;
                // Log::info('Quantity ' . $inventoryItem->quantity);

            }


            $inventoryItem->save();

            // Log::info('Job ended ');


        }

    }
}
