<?php

namespace App\Jobs;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Httpful\Request as HttpfulRequest;
use Illuminate\Support\Facades\Log;

class SendOrdersDnoteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = 'http://3.141.34.13/api/orders';

        $approvedOrder = Order::with('items', 'clientable', 'location')->find($this->id);


        $response = HttpfulRequest::post($url)->addOnCurlOption(CURLOPT_HEADER, true)
            ->body($approvedOrder)->expectsJson()->sends('application/json')
            ->send();


        // $response = HttpfulRequest::post($url)->addOnCurlOption(CURLOPT_HEADER, true)
        //     ->body(json_encode($this->order))
        //     ->send();




    }
}
