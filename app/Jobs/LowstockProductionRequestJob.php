<?php

namespace App\Jobs;

use App\Models\Inventory;
use App\Models\InventoryManufacturer;
use App\Models\ProductionRequest;
use App\Models\ProductionRequestItem;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LowstockProductionRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
        DB::beginTransaction();
        $low_stock_inventory=Inventory::where('productionitem',1)->whereColumn('quantity','<','threshold')->get();

        $request_tag = self::request_tag();

        $new_production_request = ProductionRequest::create([
            'request_tag' => $request_tag,
            'customer_id' => null,
            'staff_id' => null,
            'initiator' => null,
            'delivery_date' => Carbon::parse(Carbon::now()->addDays(2))->format('Y-m-d'),
            'priority' => 1,
            'draft_stage' => 0,
            'notes' => null,
            'status' => 1,
            'if_stock_update'=>1
        ]);

        foreach ($low_stock_inventory as $key => $value) {


            $quantity = $value->minimumstock;
            $inventory_id = $value->id;
            $priority = 1;

            $manufacturer_id = InventoryManufacturer::get_item_manufacturer($inventory_id);


            ProductionRequestItem::create([
                'inventory_id' => $inventory_id,
                'production_request_id' => $new_production_request->id,
                'requested_quantity' => $quantity,
                'item_notes' => "Production request is for stock update",
                'production_status' => 0,
                'manufacturer_id' => $manufacturer_id,
                'item_priority' => $priority,
            ]);
        }
        DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("lowstock production request error: ".$th->getMessage());
        }

    }

    public static function request_tag($len = 8)
    {

        $shuffled_string = substr(str_shuffle("23ab34567c1de12893456fghjk4mnpq5rs7t6v5456w9xyz5"), 0, $len);
        $string = strtoupper($shuffled_string);

        $check = ProductionRequest::tag_is_unique($string);

        if ($check === false) return self::request_tag();

        return $string;
    }
}
