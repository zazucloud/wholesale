<?php

namespace App\Jobs\Imports;

use App\Imports\ZoneDiscountImport;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Maatwebsite\Excel\Facades\Excel;

class ZoneDiscountImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;
    private $url, $zone;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url,$zone)
    {
        $this->url = $url;
        $this->zone= $zone;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Excel::import(new ZoneDiscountImport($this->zone), $this->url);
    }
}
