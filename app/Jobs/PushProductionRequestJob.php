<?php

namespace App\Jobs;

use App\Http\Traits\ManufacturerTrait;
use App\Mail\ProductionRequestDispatchMail;
use App\Models\Manufacturers;
use App\Models\ProductionRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use stdClass;

class PushProductionRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ManufacturerTrait;

    public $production_request;

    /**
     * Create a new job instance.
     *
     * @param $production_request
     */
    public function __construct($production_request)
    {
        $this->production_request = $production_request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $concerned_manufacturers = ProductionRequest::concerned_manufacturers($this->production_request->id);

        if (count($concerned_manufacturers)) {

            foreach ($concerned_manufacturers as $concerned_manufacturer) {

                $manufacturer = Manufacturers::find($concerned_manufacturer);

                if ($manufacturer) {

                    $manufacturer_notification_email = $manufacturer->email;

                    $mail_data = new stdClass();
                    $mail_data->name = first_phrase($manufacturer->name);
                    $mail_data->production_request = $this->production_request;
                    $mail_data->manufacturer_id = $concerned_manufacturer;
                    $pdf_stream = $this->production_request_pdf($this->production_request, $manufacturer->id, 'stream');

                    Mail::to($manufacturer_notification_email)->queue(new ProductionRequestDispatchMail($mail_data,$pdf_stream));
                    unlink(storage_path('app/tempdfs/' . $pdf_stream));

                }

            }

        }

    }
}
