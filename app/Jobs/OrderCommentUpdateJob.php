<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use stdClass;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailOrder as MailOrderPDF;
use App\Mail\OrderCommentUpdateMail;

class OrderCommentUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $order;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $order = $this->order;
        $mail_data = new stdClass();
        $mail_data->wholesaler_name = $this->order->clientable->business_name;


        $pdf = PDF::loadView('exports.pdf.order_sheet', compact('order'));
        $attachment_filename = 'Your Order' . $this->order->clientable->business_name . '.pdf';
        Storage::disk('tempordersheets')->put($attachment_filename, $pdf->output());

        Mail::to($this->order->clientable->email)->send(new OrderCommentUpdateMail($mail_data, $attachment_filename));

        unlink(storage_path('app/tempordersheets/' . $attachment_filename));
    }
}
