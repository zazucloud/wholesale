<?php

namespace App\Jobs;

use App\Mail\MailOrder as MailOrderPDF;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use stdClass;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class MailOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $order,$email;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order,$email)
    {
        $this->order = $order;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $order = $this->order;
        $mail_data = new stdClass();
        $mail_data->wholesaler_name = $this->order->clientable->business_name;
        $mail_data->ordertag = $this->order->order_tag;


        $pdf = PDF::loadView('exports.pdf.order_sheet', compact('order'));
        $attachment_filename = 'Your Order' . $this->order->clientable->business_name . '.pdf';
        Storage::disk('tempordersheets')->put($attachment_filename, $pdf->output());

        Mail::to($this->email)->send(new MailOrderPDF($mail_data, $attachment_filename));

        unlink(storage_path('app/tempordersheets/' . $attachment_filename));
    }
}
