<?php

namespace App\Jobs;

use App\Mail\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use stdClass;

class AccountSignupRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $user_notification_data = new StdClass();
        $user_notification_data->message_content = "Your request has been received - we'll get back.";
        $user_notification_data->subject = 'Account Request Received';

        /*TODO:: Notify user that request has been sent --- Notify Admin of Request*/
        Mail::to($this->data->email_address)->queue(new Notification($user_notification_data));
    }
}
