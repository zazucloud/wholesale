<?php

namespace App\Jobs;

use App\Imports\PricingImport;
use App\Models\Inventory;
use App\Models\Pricing;
use App\Models\Wholesaler;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class CategoryPricingImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $category;
    public $url;

    /**
     * Create a new job instance.
     *
     * @param $data
     * @param $category
     */
    public function __construct($category, $url)
    {


        $this->category = $category;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Excel::import(new PricingImport($this->category,$this->url), $this->url);
    }
}
