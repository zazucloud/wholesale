<?php

namespace App\Jobs;

use App\Mail\CancelOrderMail;
use App\Mail\NewOrderNotificationWholesaler;
use App\Mail\NewOrderNotificationWholesalerStaff;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use stdClass;

class CancelOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $details;


    /**
     * CancelOrderJob constructor.
     * @param $details
     */
    public function __construct($details)
    {
        //
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $mail_data = new StdClass();
        $mail_data->wholesaler_name = $this->details->wholesaler->business_name;
        $mail_data->order_id = $this->details->order_id;
        $mail_data->order_no = $this->details->order_no;
        $mail_data->order_total = $this->details->order_total;

        Mail::to($this->details->wholesaler->email)->queue(new NewOrderNotificationWholesaler($mail_data));

        $wholesaler_staff = $this->details->wholesaler->staff;

        foreach ($wholesaler_staff as $staff) {
            $mail_data->name = first_phrase($staff->name);
            Mail::to($staff->email_address)->queue(new NewOrderNotificationWholesalerStaff($mail_data));
        }

    }
}
