<?php

namespace App\Jobs;

use App\Mail\ManufacturerProductionItemasMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ManufactureProductionItemsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $manufacturerEmail, $manufacturerName ,$items;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($manufacturerEmail, $manufacturerName,$items)
    {
        $this->manufacturerEmail = $manufacturerEmail;
        $this->manufacturerName = $manufacturerName;
        $this->items = $items;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->manufacturerEmail;
        $name = $this->manufacturerName;
        $Items = $this->items;

        Mail::to($email)->send(new ManufacturerProductionItemasMail($name,$Items));
    }
}
