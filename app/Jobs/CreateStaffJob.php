<?php

namespace App\Jobs;

use App\Mail\CreatedStaffAccountMail;
use App\Models\Staff;
use App\Models\StaffPermission;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use stdClass;

class CreateStaffJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $name, $identification_no, $employee_id, $gender, $phone_number, $email_address, $department, $permissions, $avatar_file, $created_by;

    /**
     * Create a new job instance.
     *
     * @param $name
     * @param $identification_no
     * @param $employee_id
     * @param $gender
     * @param $phone_number
     * @param $email_address
     * @param $department
     * @param $permissions
     * @param $avatar_file
     * @param $created_by
     */
    public function __construct($name, $identification_no, $employee_id, $gender, $phone_number, $email_address, $department, $permissions, $avatar_file, $created_by)
    {
        $this->name = $name;
        $this->identification_no = $identification_no;
        $this->employee_id = $employee_id;
        $this->gender = $gender;
        $this->phone_number = $phone_number;
        $this->email_address = $email_address;
        $this->department = $department;
        $this->permissions = $permissions;
        $this->avatar_file = $avatar_file;
        $this->created_by = $created_by;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $plain_password = passwordGenerator(9);
        $db_password = bcrypt($plain_password);
        $user_account = User::create_account($this->name, $this->email_address, $db_password, 2, $this->avatar_file, $this->permissions);
        $user_id = $user_account->id;

        $staff = Staff::create([
            'name' => $this->name,
            'employee_id' => $this->employee_id,
            'national_id' => $this->identification_no,
            'email_address' => $this->email_address,
            'phone_number' => $this->phone_number,
            'gender' => $this->gender,
            'user_id' => $user_id,
            'department_id' => $this->department,
            'avatar' => $this->avatar_file, // Duplicate as in users table to cater for staff not having accounts
            'status' => 0,
        ]);

        $staff_id = $staff->id;

        User::set_account_id($user_id, $staff_id);

        #old permission allocator
        //StaffPermission::set($staff_id, $this->permissions);

        $mail_data = new StdClass();
        $mail_data->name = first_phrase($this->name);
        $mail_data->username = $this->email_address;
        $mail_data->password = $plain_password;
        $mail_data->created_by = $this->created_by;
        Mail::to($this->email_address)->queue(new CreatedStaffAccountMail($mail_data));
    }
}
