<?php

namespace App\Jobs;

use App\Mail\CreateWholesalerAccountMail;
use App\Mail\NewOrderNotificationWholesaler;
use App\Mail\NewOrderNotificationWholesalerStaff;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderComment;
use App\Models\OrderProduct;
use App\Models\Wholesaler;
use App\Models\WholesalerCategoryDiscount;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\Storage;
use stdClass;

class CreateOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order, $client, $type;

    /**
     * Create a new job instance.
     *
     * @param $order_request
     */
    public function __construct($client, $order, $type)
    {
        $this->client = $client;
        $this->order = $order;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;
        $type = $this->type;

        $mail_data = new stdClass();
        $mail_data->wholesaler_name = $this->client->business_name;
        $mail_data->order_id = $this->order->id;
        $mail_data->order_no = $this->order->face_id;
        $mail_data->order_total = $this->order->total;

        if(!is_null($this->order)){

            $footerHtml = view()->make('exports.pdf.footer_order', compact('order'))->render();
            $headerHtml = view()->make('exports.pdf.header_order', compact('order'))->render();

            $pdf = PDF::loadView('exports.pdf.order_sheet', compact('order', 'type'))
                ->setOption('header-html', $headerHtml)
                ->setOption('footer-html', $footerHtml);

            $attachment_filename = 'OrderSheet_' . $this->order->face_id . '.pdf';
            
            Storage::disk('tempordersheets')->put($attachment_filename, $pdf->output());

            Mail::to($this->client->email)->send(new NewOrderNotificationWholesaler($mail_data, $attachment_filename));

            if ($type == "wholesaler") {
                $wholesaler_staff = $this->client->staff;
                foreach ($wholesaler_staff as $staff) {
                    $mail_data->name = first_phrase($staff->name);
                    Mail::to($staff->email)->send(new NewOrderNotificationWholesalerStaff($mail_data, $attachment_filename));
                }
            }

            unlink(storage_path('app/tempordersheets/' . $attachment_filename));
        }
    }
}
