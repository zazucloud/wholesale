<?php

namespace App\Jobs;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreatePaymentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $wholesaler;
    private $amount;
    private $channel;
    private $reference;
    private $creator;
    private $comment;
    private $attachment;
    private $receipt_no;

    public function __construct($wholesaler, $amount, $channel, $reference, $creator, $comment, $attachment, $receipt_no)
    {
        $this->wholesaler = $wholesaler;
        $this->amount = $amount;
        $this->channel = $channel;
        $this->reference = $reference;
        $this->creator = $creator;
        $this->comment = $comment;
        $this->attachment = $attachment;
        $this->receipt_no = $receipt_no;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Payment::create([
            'wholesaler_id' => $this->wholesaler,
            'order_id' => null,// For future use,
            'amount' => $this->amount,
            'reference' => $this->reference,
            'comment' => $this->comment,
            'attachment' => $this->attachment,
            'created_by' => $this->creator,
            'channel' => $this->channel,
            'status' => 'paid',
            'confirmed' => 1,
            'receipt_no' => $this->receipt_no
        ]);

    }
}
