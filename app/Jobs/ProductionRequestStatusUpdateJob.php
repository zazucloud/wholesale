<?php

namespace App\Jobs;

use App\Mail\ProductionRequestStatusUpdate as ProductionRequestStatusUpdateMail;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProductionRequestStatusUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public  $staff, $production_request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($staff, $production_request)
    {
        $this->staff = $staff;
        $this->production_request = $production_request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::to($this->staff->email)
            ->send(new ProductionRequestStatusUpdateMail($this->staff, $this->production_request));
    }
}
