<?php

namespace App\Listeners;

use App\Events\WelcomeUserEvent;
use App\Mail\WelcomeUserMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendWelcomeUserNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  WelcomeUserEvent  $event
     * @return void
     */
    public function handle(WelcomeUserEvent $event)
    {
        // Log::info(json_encode($event->userInfo));
        $userInfo = $event->userInfo;

        Mail::to($userInfo['user']->email)->send(new WelcomeUserMail($userInfo));
    }
}
