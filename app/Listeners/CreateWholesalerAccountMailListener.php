<?php

namespace App\Listeners;

use App\Events\CreateWholesalerAccountMailEvent;
use App\Mail\CreateWholesalerAccountMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class CreateWholesalerAccountMailListener implements ShouldQueue
{
  

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CreateWholesalerAccountMailEvent $event)
    {

        $newData = $event->maildata;

        Mail::to($newData->username)->send(new CreateWholesalerAccountMail($newData));
    }
}
