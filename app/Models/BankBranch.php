<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankBranch extends Model
{
    //


    public static function name($branch_id)
    {

        $branch = BankBranch::find($branch_id);

        return ($branch ? $branch->name : '');
     
    }

}
