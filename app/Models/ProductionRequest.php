<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed request_tag
 * @property mixed delivery_date
 */
class ProductionRequest extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'production_requests';

    public static function tag_is_unique($tag)
    {
        $check = ProductionRequest::where('request_tag', $tag)->first();

        return ($check === null) ? true : false;
    }


    public function getRouteKeyName()
    {
        return 'request_tag';
    }

    public static function find_by_tag($tag)
    {
        return ProductionRequest::where('request_tag', $tag)->first();
    }

    public static function id_by_tag($tag)
    {
        $record = ProductionRequest::where('request_tag', $tag)->first();

        return ($record == null) ? null : $record->id;
    }
    public function clientable()
    {
        return $this->morphTo();
    }
    public function items()
    {
        return $this->hasMany('App\Models\ProductionRequestItem', 'production_request_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\ProductionRequestComment', 'production_request_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'initiator');
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id');
    }

    public static function request_priority_options()
    {
        $data = [1 => 'Low', 2 => 'Normal', 3 => 'High'];

        return $data;
    }

    

    public static function request_priority_label($priority_id)
    {
        return self::request_priority_options()[$priority_id];
    }

    public function scopePushed($query)
    {
        return $query->where('draft_stage', 0);
    }

    public function scopeDraft($query)
    {
        return $query->where('draft_stage', '!=', 0)->where('status', 1);
    }

    public static function concerned_manufacturers($production_request_id)
    {

        $list = ProductionRequestItem::select('manufacturer_id')->where('production_request_id', $production_request_id)->get()->pluck('manufacturer_id')->toArray();

        return array_unique($list);
    }

    public function manufacturer_requests()
    {

        /*  $fetch = DB::table('production_request_items')
                        ->join('production_requests','')*/
    }

    public function wholersaler()
    {
        return $this->belongsTo(Wholesaler::class, 'customer_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
