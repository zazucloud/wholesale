<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Zone extends Model
{
    use HasFactory, HasRelationships;

    protected $fillable = [
        'name',
    ];


    public function counties()
    {
        return $this->belongsToMany(County::class);
    }


    public function wholesalers()
    {
        return $this->hasManyDeep(
            Wholesaler::class,
            [
                CountyZones::class,
                County::class
            ],
            [
                'zone_id', // Foreign key on the "users" table.
                'id',    // Foreign key on the "posts" table.
                'county_id'     // Foreign key on the "comments" table.
            ],
            [
                'id', // Local key on the "countries" table.
                'county_id', // Local key on the "users" table.
                'id'  // Local key on the "posts" table.
            ]
        );
    }

    public function discounts(){
        return $this->hasMany(ZoneDiscounts::class, 'zone_id');
    }
}
