<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryAttribute extends Model
{
    protected $fillable = ['label', 'slug', 'type'];

    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function options()
    {

        return $this->hasMany(InventoryAttributeOption::class, 'attribute_id')->where('status', 1);
    }

    public function scopeActive()
    {
        return $this->where('status', 1);
    }


    public static function narrate_options($attribute_id)
    {

        $options = InventoryAttribute::find($attribute_id)->options()->get()->pluck('option')->toArray();

        return implode(', ', $options);
    }

    public function attributeitems(){
        return $this->hasMany(InventoryItemAttribute::class,'attribute_id');
    }

}
