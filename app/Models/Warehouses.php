<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouses extends Model
{
    protected $guarded = [];

    public function scopeActive($query)
    {
        return $query->where('status', '=', 1);
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }
}
