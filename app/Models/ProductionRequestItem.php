<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionRequestItem extends Model
{
    protected $guarded = [];

    protected $with = ['inventory'];

    public function production_request()
    {
        return $this->belongsTo('App\Models\ProductionRequestItem', 'id');
    }

      protected $casts = [
          'attributes' => 'array',
      ];


    public function getAttributesAttribute($value)
    {
        return json_decode($value, JSON_UNESCAPED_SLASHES);
    }

    public function setAttributesAttribute($value)
    {
        return $this->attributes['attributes'] = str_replace('\"', '', $value);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturers::class, 'manufacturer_id')->where('status', 1);
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
