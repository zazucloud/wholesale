<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_product';
    protected $guarded = [];

    public function inventory()
    {
        return $this->belongsTo('App\Models\Inventory', 'inventory_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getItemTotalAttribute()
    {

        $total = ($this->price * $this->quantity);

        $vat_amount = vat_amount($total, env('DEFAULT_VAT_RATE'));

        return $total + $vat_amount;
    }

    public static $order_stages = [
        1 => 'Pending',
        2 => 'Completed',
    ];

    public static $order_stages_colors = [
        1 => 'danger',
        2 => 'success',
    ];

}
