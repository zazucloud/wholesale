<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Applications extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'wholesaler_account_requests';
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public static $application_status = [
        1 => 'Pending',
        2 => 'Approved',
        3 => 'Declined'
    ];

    public static function ribbon_color($status_id)
    {
        $ribbon_color_key = [
            1 => 'warning',
            2 => 'success',
            3 => 'danger'
        ];

        return $ribbon_color_key[$status_id];
    }

    public static function status_text($status_id)
    {
        $ribbon_color_key = self::$application_status;

        return $ribbon_color_key[$status_id];
    }

    public function scopeOfStatus($query, $status)
    {
        return $query->whereStatus($status);
    }
}
