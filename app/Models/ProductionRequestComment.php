<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionRequestComment extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $primaryKey = 'comment_id';

    protected $table = 'production_request_comments';
    protected $dates = ['deleted_at'];

    public static function comment_owner($comment_id)
    {
        return ProductionRequestComment::select('user_id')->findOrFail($comment_id)->user_id;
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

}
