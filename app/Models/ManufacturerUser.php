<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ManufacturerUser extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $guard = 'manufacturerUser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'manufacturer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function manufacturer()
    {
        return $this->hasMany(Manufacturers::class, 'manufacturer_id');
    }

    public static function create_account($name, $email, $password, $manufacturer_id,$phone,$status)
    {

        $user = self::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'manufacturer_id' => $manufacturer_id,
            'phone_no' => $phone,
            'status' => $status,
        ]);

        return $user;

    }

    public function wholesaler(){
        return $this->belongsTo(Wholesaler::class,'wholesaler_id');
    }
}
