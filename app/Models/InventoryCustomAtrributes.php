<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryCustomAtrributes extends Model
{
    use HasFactory;
    protected $table="inventory_custom_attributes";
    protected $fillable =['inventory_id','name','value'];
}
