<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use App\Models\Wholesale\WholesalerLocation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @property mixed delivery_location
 */
class Order extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia;

    protected $table = 'orders';
    protected $guarded = [];

    public $order_id_start = 1234;
    protected $orderDirection = 'DESC';

    protected $appends = ['status_text', 'face_id'];

    protected static function boot()
    {
        parent::boot();

    }


    public static function order_tag_is_unique($tag)
    {
        $check = Order::where('order_tag', $tag)->first();

        return ($check === null) ? true : false;
    }



    public static $order_stages = [
        1 => 'Pending',
        2 => 'Approved',
        3 => 'Processing',
        4 => 'Processed',
        5 => 'Partially Shipped',
        6 => 'Fully Shipped',
        7 => 'Paid',
    ];

    public static $order_stages_colors = [
        1 => 'danger',
        2 => 'success',
        3 => 'success',
        4 => 'success',
        5 => 'warning',
        6 => 'success',
        7 => 'info',
    ];

    public static $order_stages_icons = [
        1 => 'icon-bulb',
        2 => 'fa fa-check',
        3 => 'fa fa-anchor',
        4 => 'icon-check',
        5 => 'fa fa-money',
    ];

    public static $order_status_label = [
        1 => '<span class="label label-sm label-danger"> Pending </span>',
        2 => '<span class="label label-sm label-primary"> Approved </span>',
        3 => '<span class="label label-sm label-warning"> Partially Shipped </span>',
        4 => '<span class="label label-sm label-success"> Fully Shipped </span>',
        5 => '<span class="label label-sm label-info"> Paid </span>',
    ];

    public function getRouteKey()
    {
        return 'order';
    }

    function getStatusTextAttribute()
    {
        return self::$order_stages[$this->stage];
    }

    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class, 'wholesaler_id');
    }

    public function productionRequest(){
        return $this->hasOne(ProductionRequest::class,'order_id');
    }

    public function clientable(){
        return $this->morphTo();
    }

    public function inventory()
    {
        return $this->hasManyThrough(
            'App\Models\Inventory',
            'App\Models\OrderProduct',
            'order_id',
            'id',
            'id',
            'inventory_id'
        );
    }

    public function items()
    {
        return $this->hasMany(OrderProduct::class)->with('inventory');
    }

    public function comments()
    {
        return $this->hasMany(OrderComment::class);
    }
    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function getFaceIDAttribute()
    {
        return $this->order_tag;
    }

    public function getLocationAttribute()
    {
        $delivery_location = $this->delivery_location;
        return WholesalerLocation::find($delivery_location);
    }


    public static function determine_order_status($order_id)
    {
        $order_items_status = Order::find($order_id)->items->pluck('status')->toArray();

        $min_status = min($order_items_status);
        $max_status = max($order_items_status);

        if ($min_status === $max_status) return $max_status;

        return $min_status;
    }

    public function scopeOfStage($query, $stage)
    {
        return $query->whereStage($stage);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', $this->orderDirection);
    }

    public function scopeCreated($query)
    {
        return $query->where('is_draft', 0);
    }

    public function scopeDraft($query, $is_draft = 1)
    {
        return $query->where('is_draft', $is_draft);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function location()
    {
        return $this->belongsTo(WholesalerLocation::class, 'delivery_location');
    }

    public function timelines(){
        return $this->hasMany(OrderTimeline::class, 'order_id');
    }
}
