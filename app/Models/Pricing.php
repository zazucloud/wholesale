<?php

namespace App\Models;

use App\Http\Controllers\CacheController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Pricing extends Model
{
    //
    protected $table = 'pricing';
    protected $guarded = [];


    public function populate()
    {

        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $wholesalers = CacheController::wholesalers();

        foreach ($wholesalers as $wholesaler) {
            $wholesaler_categories = $wholesaler->categories;

            Pricing::where('wholesaler_id', $wholesaler->id)->forceDelete();

            if (!!$wholesaler_categories->count()) {

                foreach ($wholesaler_categories as $wholesaler_category) {

                    $category_items = $wholesaler_category->items()->get()->pluck('inventory_id');
                    $inventory_list = Inventory::whereIn('id', $category_items)->get();

                    foreach ($inventory_list as $item) {

                        $wholesaler_item_price = WholesalerCategoryDiscount::price($wholesaler->id, $item->id);
                        $price = inc_vat($wholesaler_item_price);

                        $price = round($price, 2);

                        Pricing::create([
                            'wholesaler_id' => $wholesaler->id,
                            'inventory_id' => $item->id,
                            'price' => $price
                        ]);

                        $cache_key = 'p.' . $wholesaler->id . '.' . $item->id;
                        Cache::forget($cache_key);
                        Cache::forever($cache_key, $price);

                    }

                }

            }

        }


    }

}
