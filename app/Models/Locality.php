<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'name', 'county_id'
    ];

    public function county()
    {
        return $this->belongsTo(County::class, 'county_id');
    }

    public static function findOrCreate($value)
    {


        $locality = Locality::where('id', $value->LocalityId)
        ->where('county_id', $value->CountyId)
        ->where('name', $value->LocalityName)
        ->first();

        if (!$locality) {
            $locality = Locality::create([
                 "id" => strtoupper($value->LocalityId),
                "county_id" => strtoupper($value->CountyId),
                "name" => strtoupper($value->LocalityName),
            ]);
        }

        return $locality;
    }

}
