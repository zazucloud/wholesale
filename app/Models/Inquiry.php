<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }

    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class, 'wholesaler_id');
    }

    public function retailer()
    {
        return $this->belongsTo(Retailer::class, 'retailer_id');
    }

    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function clientable()
    {
        return $this->morphTo();
    }

    public function items(){
        return $this->hasMany(Quotation::class,'inquiry_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model){

            $model->number = $model->max('number') +1;

            $model->inquiry_no = 'Q'. str_pad($model->number,7, '0',STR_PAD_LEFT);

        });



    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
