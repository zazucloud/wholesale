<?php

namespace App\Models;

use App\Models\Wholesale\WholesalerCategoryDiscount;
use App\Models\Wholesale\WholesalerCategoryVisibility;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class InventoryCategory extends Model implements HasMedia
{

    use InteractsWithMedia;
    protected $table = 'inventory_categories';

    protected $guarded = [];


    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeActive($query)
    {
        return $query->where('status', '=', 1);
    }

    public function scopeParent($query)
    {
        return $query->where('parent', '=', 0);
    }

    public function scopeOrderByName($query)
    {
        return $query->orderBy('name');
    }


    public static function name($category_id)
    {
        if ($category_id == 0) return '';

        return optional(InventoryCategory::find($category_id))->name;
    }

    public static function child($category_id)
    {
        return InventoryCategory::active()->where('parent', $category_id)->get();
    }

    public function childinventory()
    {
        return $this->hasMany(InventoryCategory::class, 'parent')->active();
    }

    public function api_inventory()
    {
        return $this->belongsToMany(Inventory::class, 'inventory_item_categories', 'category_id', 'inventory_id')->orderByPivot('inventory_id','asc');
    }


    public static function name_multiple(array $category_ids)
    {
        $names = [];
        foreach ($category_ids as $category_id)
            $names[] = self::name($category_id);

        return implode(',', $names);
    }

    public function items()
    {
        return $this->hasManyThrough(
            InventoryItemCategory::class,
            InventoryCategory::class, 'id', 'category_id', 'id', 'id'
        );
    }

    public function items2()
    {
        return $this->hasMany(InventoryItemCategory::class, 'category_id');
    }

    // public function getImageUrlAttribute()
    // {
    //     return media_file($this->image);
    // }

    public function discount()
    {
        return $this->hasMany(WholesalerCategoryDiscount::class, 'category_id');
    }

    public function catalogue()
    {
        return $this->hasMany(WholesalerCategoryDiscount::class, 'category_id');
    }

    public function brand(){
        return $this->belongsTo(Brand::class,'brand_id');
    }

    public function wholesalers(){
        return $this->hasMany(WholesalerCategoryVisibility::class, 'category_id');
    }
}
