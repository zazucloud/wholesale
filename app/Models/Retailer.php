<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Retailer extends Model
{
    protected $guarded = [];

    use SoftDeletes,HasFactory;


    public function orders()
    {
        return $this->morphMany(Order::class,'clientable');
    }

    public function request()
    {
        return $this->morphMany(ProductionRequest::class,'clientable');
    }

    public function inquiries()
    {
        return $this->morphMany(Inquiry::class, 'clientable');
    }
}
