<?php

namespace App\Models\Wholesale;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class WholesalerUser extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $guard = 'wholesalerUser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_no',
        'wholesaler_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];




    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class);
    }

    public static function create_account($name, $email, $password, $wholesaler_id)
    {

        $user = self::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'wholesaler_id' => $wholesaler_id,
        ]);

        return $user;

    }
}
