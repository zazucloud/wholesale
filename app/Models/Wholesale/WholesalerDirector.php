<?php

namespace App\Models\Wholesale;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WholesalerDirector extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'wholesaler_directors';

    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class);
    }
}
