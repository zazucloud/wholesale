<?php

namespace App\Models\Wholesale;

use Illuminate\Database\Eloquent\Model;

class WholesalerLocation extends Model
{
    protected $guarded = [];

    public function getRouteKey()
    {
        return 'id';
    }

    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class);
    }

    public function getcoordinateAttribute()
    {
        return "{$this->latitude},{$this->longitude}";
    }

}
