<?php

namespace App\Models\Wholesale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WholesalerItemDiscount extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }

    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class);
    }

    public static function check($wholesaler_id, $inventory_id)
    {
        $check = self::where('wholesaler_id',$wholesaler_id)->where('inventory_id',$inventory_id)->first();

        if($check) return $check->custom_discount;

        return null;
    }

}
