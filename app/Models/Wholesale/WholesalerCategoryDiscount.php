<?php

namespace App\Models\Wholesale;

use App\Models\Inventory;
use App\Models\InventoryCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WholesalerCategoryDiscount extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'wholesaler_category_discounts';

    public static function discount($wholesaler_id, $category_id)
    {
        $category_id = (array)$category_id;
        $discount_record = WholesalerCategoryDiscount::where('wholesaler_id', $wholesaler_id)->whereIn('category_id', $category_id)->orderBy('discount_rate', 'DESC')->first();

        if ($discount_record == null) return 0;

        return ($discount_record->discount_rate);
    }

    public static function price($wholesaler_id, $product_id)
    {
        $inventory = Inventory::findorFail($product_id);
        return wholesale_price($wholesaler_id, $inventory);

        /*$inventory = Inventory::findorFail($product_id);
        $original_price = $inventory->price;



        $inventory_categories = $inventory->categories()->pluck('category_id')->toArray();
        $product_category = end($inventory_categories);
        $discount_rate = self::discount($wholesaler_id,$product_category);
        $rate = (100-$discount_rate)/100;

        return round($original_price*$rate,2);*/
    }

    public static function discounts_narration($discounts)
    {
        $content = [];
        foreach ($discounts as $discount) {
            $category = InventoryCategory::name($discount->category_id);
            $discount_rate = $discount->discount_rate . '%';
            $content[] = $category . " ($discount_rate)";
        }

        return implode(', ', $content);
    }


}
