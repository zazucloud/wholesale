<?php

namespace App\Models\Wholesale;

use Illuminate\Database\Eloquent\Model;

class WholesalerAccountManager extends Model
{
    protected $guarded = [];

    public static function set($wholesaler_id,$staff_id)
    {

        WholesalerAccountManager::where('wholesaler_id',$wholesaler_id)->forceDelete();

        return WholesalerAccountManager::create([
            'wholesaler_id' => $wholesaler_id,
            'staff_id' => $staff_id
        ]);

    }

}
