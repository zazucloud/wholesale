<?php

namespace App\Models\Wholesale;

use App\Models\Cart;
use App\Models\County;
use App\Models\Inquiry;
use App\Models\InventoryCategory;
use App\Models\Order;
use App\Models\User;
use App\Models\Payment;
use App\Models\ProductionRequest;
use App\Models\Staff;
use App\Models\StaffWholesaler;
use App\Models\WholesalerContact;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property mixed visible_categories
 * @property mixed given_discounts
 * @property mixed accounts
 */
class Wholesaler extends Model
{
    use SoftDeletes, HasFactory;

    protected $guarded = [];


    public function wholesaleUsers()
    {
        return $this->hasMany(WholesalerUser::class);
    }

    public function inquiries()
    {
        return $this->morphMany(Inquiry::class, 'clientable');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function orders()
    {
        return $this->morphMany(Order::class, 'clientable');
    }

    public function request()
    {
        return $this->morphMany(ProductionRequest::class, 'clientable');
    }

    public function accounts()
    {
        return $this->hasMany(WholesalerUser::class, 'wholesaler_id');
    }

    public function account_by_email($email)
    {
        return $this->accounts()->where('email', $email)->first();
    }

    public function wholesalerDirectors()
    {
        return $this->hasMany(WholesalerDirector::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', '!=', 9);
    }

    public function visible_categories()
    {
        return $this->hasMany(WholesalerCategoryVisibility::class);
    }

    public function categories()
    {
        return $this->belongsToMany(InventoryCategory::class, 'wholesaler_category_visibility', 'wholesaler_id', 'category_id');
    }

    public function itemDiscounts()
    {
        return $this->hasMany(WholesalerItemDiscount::class, 'wholesaler_id');
    }

    public function visible_products()
    {

        $visible_categories = $this->visible_categories()->get()->pluck('category_id')->toArray();

        $visible_item_categories = InventoryItemCategory::whereIn('category_id', $visible_categories)->get()->pluck('inventory_id')->toArray();
        $inventory = Inventory::whereIn('id', $visible_item_categories)->get();

        return $inventory;
    }

    public function discounts()
    {
        return $this->hasMany(WholesalerCategoryDiscount::class);
    }

    public function given_discounts()
    {
        return $this->discounts()->where('discount_rate', '>', 0);
    }

    public static function ownership_text($ownership_type, $ownership_type_extra)
    {

        if ($ownership_type === 1) {
            $label = 'Limited Partnership';
        } elseif ($ownership_type === 2) {
            $label = 'Partnership';
        } elseif ($ownership_type === 3) {
            $label = 'Sole Proprietorship';
        } else {
            $label = $ownership_type_extra;
        }

        return $label;
    }

    public function locations()
    {
        return $this->hasMany(WholesalerLocation::class);
    }


    public static function order_count_3_months($wholesaler_id)
    {
        return Order::where('wholesaler_id', $wholesaler_id)->where('is_draft', 0)->where('created_at', '>=', Carbon::now()->subMonth())->get()->count();
    }

    public static function last_order($wholesaler_id)
    {

        return Order::where('wholesaler_id', $wholesaler_id)->where('is_draft', 0)->latest()->first();
    }

    public static function last_order_total($wholesaler_id)
    {

        $last_order = self::last_order($wholesaler_id);

        if ($last_order === null) return 0;

        return $last_order->total;
    }


    public function staff()
    {
        return $this->belongsToMany(User::class, 'staff_wholesaler')->using(StaffWholesaler::class);
    }

    public function staff_custom()
    {
        return $this->hasMany(StaffWholesaler::class, 'wholesaler_id');
    }


    public static function findByRegNo($registration_number)
    {
        return self::where('registration_number', $registration_number)->first();
    }

    public function county()
    {
        return $this->belongsTo(County::class, 'county_id');
    }

    public function contacts()
    {
        return $this->hasMany(WholesalerContact::class, 'wholesaler_contact_id');
    }
}
