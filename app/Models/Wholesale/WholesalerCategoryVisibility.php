<?php

namespace App\Models\Wholesale;

use App\Models\InventoryCategory;
use Illuminate\Database\Eloquent\Model;

class WholesalerCategoryVisibility extends Model
{
    protected $guarded = [];
    protected $table = 'wholesaler_category_visibility';


    public static function check($wholesaler_id, $category_id)
    {
        return self::where('wholesaler_id',$wholesaler_id)->where('category_id',$category_id)->first();
    }

    public function categories(){
        return $this->belongsTo(InventoryCategory::class,'category_id');
    }
}
