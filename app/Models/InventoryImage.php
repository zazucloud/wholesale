<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryImage extends Model
{
    protected $table = 'inventory_images';
    protected $guarded = [];

    public static function set($inventory_id, $image_name, $is_main = null)
    {
        return InventoryImage::create([
            'image' => $image_name,
            'inventory_id' => $inventory_id,
            'is_main' =>  $is_main
        ]);

    }

    public function getImageAttribute($value)
    {
        return media_file($value);
    }
}
