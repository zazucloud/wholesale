<?php

namespace App\Models;

class ThemeManager
{
    // Theme Config
    protected $sidebar_skin         = 'dark';
    protected $header_skin          = 'light';
    protected $navbar_brand_skin    = 'dark';
    protected $sidebar_state        = 'expand';
    protected $container_class      = 'container';
    protected $logo_file_expand     = '/images/zazulogo_expanded.png';
    protected $logo_file_collapse   = '/images/zazulogo_collapsed.png';
    // protected $logo_file_expand   = '/images/verus.png';
    // protected $logo_file_collapse   = '/images/verus.png';
    protected $body_classes         = '';
    protected $fonts                = array(
        'Karla' => array(400, '400i', 700),
    );

    // Paths for resources
    protected $js       = 'assets/js';
    protected $pjs      = 'js';
    protected $css      = 'assets/css';
    protected $pcss      = 'css';
    protected $vendors  = 'assets/vendors';
    protected $node     = '../node_modules';

    // For Registering styles and scripts
    protected $uni_styles           = array();
    protected $uni_scripts          = array();

    // For Enqueueing styles and script
    // Files in these arrays are display on pages
    protected $uni_styles_output    = array();
    protected $uni_scripts_output   = array();

    public function __construct()
    {
        $this->main_enqueue_and_register();
    }

    // return all output styles
    public function styles()
    {
        ksort($this->uni_styles_output);
        return $this->uni_styles_output;
    }

    // return all output scripts
    public function scripts()
    {
        ksort($this->uni_scripts_output);
        return $this->uni_scripts_output;
    }

    // return all output scripts
    public function enqueue_files($styles = null, $scripts = null)
    {
        $styles = $styles == null ? array() : $styles;
        $scripts = $scripts == null ? array() : $scripts;

        foreach ($styles as $style) {
            $this->enqueue_style($style);
        }
        foreach ($scripts as $script) {
            $this->enqueue_script($script);
        }
        return $this;
    }

    // Append To Body Classes
    public function append_to_body_class($classes)
    {
        $this->body_classes = $this->body_classes . ' ' . $classes;
        return $this;
    }

    public function replace_body_class($classes)
    {
        $this->body_classes = $classes;
        return $this;
    }

    public function get_container_class()
    {
        return $this->container_class;
    }
    public function get_header_skin()
    {
        return $this->header_skin;
    }
    public function get_sidebar_skin()
    {
        return $this->sidebar_skin;
    }
    public function get_navbar_brand_skin()
    {
        return $this->navbar_brand_skin;
    }
    public function get_sidebar_state()
    {
        return $this->sidebar_state;
    }
    public function get_logo_expand()
    {
        return $this->logo_file_expand;
    }
    public function get_logo_collapse()
    {
        return $this->logo_file_collapse;
    }


    /*
   * =============================
   * Register and Enqueue Styles
   * =============================
   */
    protected function register_style($handle, $src = '', $priority = 0)
    {
        $this->uni_styles[$priority][$handle] = $src;
        return $this;
    }

    protected function enqueue_style($handle, $src = '', $priority = 0)
    {
        if (isset($this->uni_styles[$priority][$handle])) {
            $this->uni_styles_output[$priority][$handle] = $this->uni_styles[$priority][$handle];
        } else {
            $this->uni_styles_output[$priority][$handle] = $src;
        }
        return $this;
    }

    /*
   * =============================
   * Register and Enqueue Scripts
   * =============================
   */
    protected function register_script($handle, $src = '', $priority = 0)
    {
        $this->uni_scripts[$priority][$handle] = $src;
        return $this;
    }

    protected function enqueue_script($handle, $src = '', $priority = 0)
    {
        if (isset($this->uni_scripts[$priority][$handle])) {
            $this->uni_scripts_output[$priority][$handle] = $this->uni_scripts[$priority][$handle];
        } else {
            $this->uni_scripts_output[$priority][$handle] = $src;
        }

        return $this;
    }

    protected function enqueue_google_font_files($arr)
    {
        $font_files = '';
        foreach ($arr as $key => $val) {
            if (array_search($key, array_keys($arr)) != 0)
                $font_files = $font_files . '%7C';
            $font_files = $font_files . $key . ':' . implode(',', $val);
        }
        $font_files =  'https://fonts.googleapis.com/css?family=' . $font_files;
        $this->enqueue_style('google-font-files', $font_files);
        return $this;
    }

    public function body_classes($append = '')
    {
        $classes = $this->body_classes;
        if (strlen($append))
            $classes = $classes . ' ' . $append;
        if (strlen($classes) > 1)
            echo ' class="' . $this->body_classes . '"';
        echo '';
    }

    public function register_enqueue_from_json($file)
    {
        $file = json_decode(file_get_contents($file), true);
        $register = $file['register'];
        $enqueue = $file['enqueue'];

        $prefix = '';

        if ($file['type'] == 'npm')
            $prefix = $this->node . '/';

        foreach ($register['js'] as $key => $file) {
            if (gettype($file) == 'string')
                $this->register_script($key, $prefix . $file);
            else if (gettype($file) == 'array')
                $this->register_script($key, $prefix . $file['url'], $file['priority']);
        }

        foreach ($register['css'] as $key => $file) {
            if (gettype($file) == 'string')
                $this->register_style($key, $prefix . $file);
            else if (gettype($file) == 'array')
                $this->register_style($key, $prefix . $file['url'], $file['priority']);
        }


        foreach ($enqueue['js'] as $key => $file) {
            if (gettype($file) == 'string')
                $this->enqueue_script($key, $prefix . $file);
            else if (gettype($file) == 'array')
                $this->enqueue_script($key, $prefix . $file['url'], $file['priority']);
        }

        foreach ($enqueue['css'] as $key => $file) {
            if (gettype($file) == 'string')
                $this->enqueue_style($key, $prefix . $file);
            else if (gettype($file) == 'array')
                $this->enqueue_style($key, $prefix . $file['url'], $file['priority']);
        }
        return $this;
    }

    public function main_enqueue_and_register()
    {
        // Vendors
        $js = $this->js;
        $css = $this->css;
        $vendors = $this->vendors;
        $node = $this->node;
        $pjs = $this->pjs;
        $pcss = $this->pcss;
        // CSS
        $this->enqueue_google_font_files($this->fonts);

        $this->register_style('material-icons', asset($vendors) . '/material-icons/material-icons.css');
        $this->register_style('feather-icons', asset($vendors) . '/feather-icons/feather.css');
        $this->enqueue_style('linearicons', asset($vendors) . '/linear-icons/linearicons.css');
        $this->register_style('social-icons', asset($vendors) . '/mono-social-icons/monosocialiconsfont.css');
        $this->register_style('tablesaw', asset($node) . '/tablesaw/dist/tablesaw.css');
        $this->register_style('jquery-spinner', asset($vendors) . '/jquery-spinner/css/bootstrap-spinner.min.css');
        $this->register_style('weather-icons', asset($vendors) . '/weather-icons-master/weather-icons.min.css');
        $this->register_style('weather-icons-wind', asset($vendors) . '/weather-icons-master/weather-icons-wind.min.css');
        $this->register_style('footable', asset($vendors) . '/footable/css/footable.bootstrap.min.css');
        $this->register_style('switchery', asset($vendors) . '/switchery/switchery.min.css');
        $this->register_style('tagsinput', asset($vendors) . '/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->register_style('dropzone-basic', asset($vendors) . '/dropzone/min/basic.min.css');
        $this->enqueue_style('dropzone', asset($vendors) . '/dropzone/min/dropzone.min.css');
        $this->register_style('nestable', asset($node) . '/nestable2/jquery.nestable.css');

        $this->enqueue_style('base', asset($css) . '/style.css', 99);
        $this->enqueue_style('font-awesome', asset($pcss) . '/font-awesome.min.css', 99);
        $this->enqueue_style('jquery.floatingscroll', asset($vendors) . '/floatingbar/css/jquery.floatingscroll.css', 99);
        $this->enqueue_style('app', mix($pcss . '/app.css'), 99);
        $this->enqueue_style('tailwind', mix($pcss . '/tailwind.css'), 99);



        $this->enqueue_script('jquery.min', asset($pjs) . '/core/jquery.min.js');
        $this->enqueue_script('app', mix($pjs . '/app.js'));
        $this->register_script('toastrs', asset($js) . '/toastrs.js');
        $this->register_script('sigwebtablet', asset($pjs) . '/sigwebtablet.js');
        $this->register_script('tablesaw', asset($node) . '/tablesaw/dist/tablesaw.jquery.js');
        $this->register_script('tablesaw-init', asset($node) . '/tablesaw/dist/tablesaw-init.js');
        $this->register_script('tabledit', asset($node) . '/jquery-tabledit/jquery.tabledit.min.js');
        $this->register_script('nestable', asset($node) . '/nestable2/jquery.nestable.js');
        $this->register_script('chart-js-utility', asset($vendors) . '/charts/utils.js');
        $this->register_script('knob-excanvas', asset($vendors) . '/charts/excanvas.js');
        $this->register_script('jquery-spinner', asset($vendors) . '/jquery-spinner/js/jquery.spinner.min.js');
        $this->register_script('template-widgets', asset($vendors) . '/template-widgets/widgets.js');
        $this->register_script('footable', asset($vendors) . '/footable/js/footable.min.js');
        $this->register_script('switchery', asset($vendors) . '/switchery/switchery.min.js');
        $this->register_script('tagsinput', asset($vendors) . '/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->register_script('flot-charts', asset($vendors) . '/flot/jquery.flot.min.js');
        $this->register_script('flot-time-plugin', asset($vendors) . '/flot/jquery.flot.time.min.js');
        $this->enqueue_script('dropzone', asset($vendors) . '/dropzone/min/dropzone.min.js');

        $this->enqueue_script('jquery.floatingscroll.min', asset($vendors) . '/floatingbar/js/jquery.floatingscroll.min.js', 99);

        $this->enqueue_script('template-js', asset($js) . '/template.js', 99);
        // $this->register_script( 'custom', asset($js) . '/custom.js', 99 );

        $cdn_file = public_path() . '/' . $vendors . '/template-includes/cdn-includes.json';

        $this->register_enqueue_from_json($cdn_file);

        $this->enqueue_script('bootstrap', asset($js) . '/bootstrap.min.js', 0);
    }
}
