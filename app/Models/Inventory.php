<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @property mixed categories
 * @property mixed fixed_attributes
 * @property mixed flexible_attributes
 */
class Inventory extends Model implements HasMedia
{

    use SoftDeletes, InteractsWithMedia;

    protected $table = 'inventory';

    protected $guarded = [];

    // protected $appends = ['images'];

    public function scopeActive($query)
    {
        return $query->where('status', '!=', 9);
    }

    public function scopeFeatured($query)
    {
        return $query->where('is_featured', 1);
    }


    public function scopeVisible($query)
    {
        return $query->where('visibility', '=', 1);
    }

    public function categories()
    {
        return $this->hasMany('App\Models\InventoryItemCategory');
    }


    public function attributes()
    {
        return $this->hasMany(InventoryItemAttribute::class);
    }

    public function fixed_attributes()
    {
        return $this->hasMany(InventoryItemAttribute::class)->where('attribute_type', 'fixed');
    }

    public function flexible_attributes()
    {
        return $this->hasMany(InventoryItemAttribute::class)->where('attribute_type', 'flexible');
    }

    public function custom_attributes()
    {
        return $this->hasMany(InventoryCustomAtrributes::class);
    }

    public function getRouteKeyName()
    {
        return 'sku';
    }


    public static function show_images($inventory_id)
    {
        $inventory = Inventory::find($inventory_id);
        $inventory_images = $inventory->images();

        $get_inventory_images = $inventory_images->get()->pluck('image')->toArray();

        if (count($get_inventory_images) == 0) {

            $inventory_category = $inventory->categories()->select('category_id')->get()->pluck('category_id')->toArray();;
            $get_inventory_categories = InventoryCategory::whereIn('id', $inventory_category)->where('image', '!=', null)->first();

            return [media_file($get_inventory_categories['image'])];
        }

        return array_map("media_file", $get_inventory_images);
    }

    public function images()
    {
        return $this->hasMany(InventoryImage::class)->orderBy('is_main', 'ASC');
    }

    public function getBrandAttribute()
    {
        return Brand::find($this->brand_id);
    }

    public function getImagesAttribute()
    {
        $images = $this->images()->get();

        if ($images->count() == 0) {

            $category_image = optional(InventoryCategory::find($this->categories()->first())->first())->image;

            if ($category_image) {
                return media_file($category_image);
            }

            return asset('assets/logos/top_mfg.png');
        }

        return $images;
    }


    public static function findBySku($sku)
    {
        $item =  Inventory::where('sku', 'LIKE', $sku)->first();
        $item = ($item == null) ? Inventory::whereRaw('lower(sku) = ?', [strtolower($sku)])->first() : $item;

        return $item;
        // return Inventory::whereRaw('lower(sku) = ?', [strtolower($sku)])->first();

    }

    public function wholesalers()
    {
        return $this->hasMany(InventoryManufacturer::class, 'inventory_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouses::class, 'warehouse_id');
    }

    public function manufacturers()
    {
        return $this->belongsToMany(Manufacturers::class, 'manufacturer_inventory', 'inventory_id', 'manufacturer_id');
    }

    public function production_request_items()
    {
        return $this->hasMany(ProductionRequestItem::class);
    }

    public function picture()
    {
        return $this->morphMany(Media::class, 'model');
    }
}
