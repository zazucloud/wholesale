<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    public static function active()
    {
        return Department::where('status',1)->get();
    }

    public static function permissions($department_id){

        return Permission::where('department_id',$department_id)->where('status',1)->get();
    }

}
