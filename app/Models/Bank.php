<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{


    public static function active()
    {

        return Bank::where('status',1)->get();

    }


    public static function branches($bank_id){

       /*TODO:: Add status check for all active bank branch listing*/

        return BankBranch::where('bank_id',$bank_id)->get();

    }


    public static function name($bank_id)
    {
        $bank = Bank::find($bank_id);

        return ($bank ? $bank->name : '');

    }

}
