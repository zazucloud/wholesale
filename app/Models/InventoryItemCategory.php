<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class InventoryItemCategory extends Pivot
{
    protected $table = 'inventory_item_categories';
    protected $guarded = [];


    public static function set($inventory_id, $category_ids = [])
    {
        InventoryItemCategory::where('inventory_id',$inventory_id)->forceDelete();
        foreach ($category_ids as $category_id) {
            InventoryItemCategory::create([
                'inventory_id' => $inventory_id,
                'category_id' => $category_id
            ]);
        }

        return true;
    }


    public function inventory()
    {
        return $this->belongsTo(Inventory::class,'inventory_id');
    }


}
