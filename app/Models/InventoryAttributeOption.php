<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryAttributeOption extends Model
{
    protected $fillable = ['attribute_id', 'option', 'status'];

    public function attribute()
    {
        return $this->belongsTo(InventoryAttribute::class);
    }


    public static function validate_unique($attribute_id, $option)
    {
        $count = InventoryAttributeOption::where('attribute_id', $attribute_id)->where('option', strtolower($option))->get()->count();

        if ($count == 0) return true;

        return false;
    }


    public static function fetch_option_label($option_id)
    {
        return InventoryAttributeOption::findorfail($option_id)->option;
    }
}
