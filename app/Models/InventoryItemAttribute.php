<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryItemAttribute extends Model
{
    protected $table = 'inventory_item_attributes';
    protected $guarded = [];


    public static function set_flexible($inventory_id, $attribute_id, $value)
    {
        /*TODO:: Add Sync Feature*/
        InventoryItemAttribute::create([
            'inventory_id' => $inventory_id,
            'attribute_id' => $attribute_id,
            'attribute_type' => 'flexible',
            'value' => $value
        ]);
    }

    public static function set_fixed($inventory_id, $attribute_id, $selected_options)
    {
        foreach ($selected_options as $selected_option) {
            InventoryItemAttribute::create([
                'inventory_id' => $inventory_id,
                'attribute_id' => $attribute_id,
                'attribute_type' => 'fixed',
                'value' => $selected_option
            ]);
        }
    }


    public static function attributes($inventory_id)
    {
        return InventoryItemAttribute::select('attribute_id')->where('inventory_id', $inventory_id)->distinct('attribute_id')->get()->pluck('attribute_id')->toArray();
    }

    public function attributeOptionData()
    {
        return $this->belongsTo(InventoryAttributeOption::class, 'value');
    }
}
