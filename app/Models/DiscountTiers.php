<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountTiers extends Model
{
    protected $table = 'discount_tiers';
    protected $guarded = [];

    public function scopeActive($query)
    {
        return $query->where('status', '!=', 9);
    }
}
