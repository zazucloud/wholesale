<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed channel
 * @property mixed attachment
 * @property mixed receipt_no
 */
class Payment extends Model
{
    const receipt_no_start_from = 4000;
    const receipt_no_increment = 1;

    public static $channels = [
        1 => 'Bank Transfer', 2 => 'Cheque', 3 => 'Ipay', 4 => 'Mpesa', 5 => 'Airtel',
        6 => 'T-Kash'
    ];

    protected $guarded = [];
    private $orderDirection = 'DESC';

    public function getRouteKeyName()
    {
        return 'receipt_no';
    }

    public static function receipt_number()
    {
        $payment_count = Payment::count();

        return self::receipt_no_start_from + $payment_count + self::receipt_no_increment;
    }

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function wholesaler()
    {
        return $this->hasOne(Wholesaler::class, 'id', 'wholesaler_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', $this->orderDirection);
    }

    public function getChannelNameAttribute()
    {
        return self::$channels[$this->channel];
    }

}
