<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed pending_production_requests
 */
class Manufacturers extends Model  implements HasMedia
{
    use InteractsWithMedia;

    protected $table = 'manufacturers';
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public static function alert_emails($manufacturer_id)
    {
        /*TODO:: fetch multiple from manufacturer notification emails --- add to profile*/
        return Manufacturers::find($manufacturer_id)->first()->email;
    }


    public function produces()
    {
        return $this->hasMany(InventoryManufacturer::class, 'manufacturer_id', 'id')->where('status', 1);
    }

    public function manufacturerUsers()
    {
        return $this->belongsTo(ManufacturerUser::class, 'manufacturer_id');
    }

    public function producible_inventory()
    {
        return $this->produces()->select('inventory_id')->get()->pluck('inventory_id')->toArray();
    }

    public function manufacturable()
    {
        return $this->hasMany(InventoryManufacturer::class, 'manufacturer_id', 'id')->where('status', 1)->with('inventory');
    }

    public function produces_inventory()
    {
        return $this->hasManyThrough(Inventory::class, InventoryManufacturer::class, 'manufacturer_id', 'id');
    }


    public function production_requests()
    {
        return $this->hasManyThrough(
            'App\Models\ProductionRequest',
            'App\Models\ProductionRequestItem',
            'manufacturer_id',
            'id',
            'id',
            'production_request_id'
        )->distinct();
    }

    /*TODO:: Convert to a filter --- do array key for requests status*/

    public function pending_production_requests()
    {
        return $this->production_requests()->where('draft_stage', 0)->where('production_requests.status', 1);
    }

    public function delivered_production_requests()
    {
        return $this->production_requests()->where('draft_stage', 0)->where('production_requests.status', 4);
    }

    public function in_production_requests()
    {
        return $this->production_requests()->where('draft_stage', 0)->where('production_requests.status', 2);
    }

    public function completed_requests()
    {
        return $this->production_requests()->where('draft_stage', 0)->where('production_requests.status', 3);
    }

    public function recent_requests($count = 10)
    {
        return $this->production_requests()->orderBy('production_requests.id', 'DESC')->take($count)->get();
    }

    public function production_request_items()
    {
        return $this->hasMany(ProductionRequestItem::class);
    }
}
