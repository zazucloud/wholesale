<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = [];

    public static function seeds()
    {
        return [

            'Manufacturers' => [
                'create_manufacturers' => 'Create Manufacturers',
                'list_manufacturers' => 'List Manufacturers',
                'view_manufacturer_details' => 'View Manufacturer Details',
                'edit_manufacturer_details' => 'Edit Manufacturer Details',
                'manage_production_list' => 'Manage Manufacturers Production List',
                'delete_manufacturers' => 'Delete Manufacturers',
                'create_manufacturing_staff' => 'Create Manufacturing Staff',
                'edit_manufacturing_staff' => 'Edit Manufacturing Staff',
                'view_manufacturing_staff' => 'View Manufacturing Staff Details',
                'list_manufacturing_staff' => 'List Manufacturing Staff'
            ],

            'Manufacturer Staff' => [
                'list_respective_production_requests' => 'List Respective Production Requests',
                'view_respective_production_requests' => 'View Respective Production Requests',
                'manage_respective_production_requests' => 'Manage Respective Production Requests',
                'cancel_respective_production_requests' => 'Cancel Respective Production Requests'
            ],

            'Production Requests' => [
                'create_production_requests' => 'Create Production Requests',
                'list_production_requests' => 'List Production Requests',
                'view_production_request_details' => 'View Production Request Details',
                'edit_production_request_details' => 'Edit Production Request Details',
                'delete_production_requests' => 'Delete Production Requests',
                'cancel_production_requests' => 'Cancel Production Requests',
                'manage_production_requests' => 'Manage Production Requests',
                'approve_production_requests' => 'Approve Production Requests',
                'direct_push_production_requests' => 'Direct Push Production Requests'
            ],

            'Staff' => [
                'create_staff' => 'Create Staff',
                'list_staff' => 'List Staff',
                'view_staff_details' => 'View Staff Details',
                'edit_staff_details' => 'Edit Staff Details',
                'delete_staff' => 'Delete Staff'
            ],

            'Orders' => [
                'create_orders' => 'Create Orders',
                'list_orders' => 'List Orders',
                'view_order_details' => 'View Order Details',
                'cancel_orders' => 'Cancel Orders',
                'manage_order_status' => 'Manage Order Status'
            ],

            'Wholesalers' => [
                'create_wholesaler_accounts' => 'Create Wholesaler Accounts',
                'list_wholesalers' => 'List Wholesalers',
                'view_wholesaler_details' => 'View Wholesaler Details',
                'edit_wholesaler_details' => 'Edit Wholesaler Details',
                'view_wholesaler_applications' => 'View Wholesaler Applications',
                'manage_wholesaler_applications' => 'Manage Wholesaler Applications',
                'delete_wholesaler_applications' => 'Delete Wholesaler Applications',
                'view_wholesaler_catalog_visibility' => 'View Wholesaler Catalog Visibility',
                'manage_wholesaler_catalog_visibility' => 'Manage Wholesaler Catalog Visibility',
                'view_wholesaler_discounts' => 'View Wholesaler Discounts',
                'manage_wholesaler_discounts' => 'Manage Wholesaler Discounts'
            ],

            'Payments' => [
                'create_manual_payments' => 'Create Manual Payments',
                'list_all_payments' => 'List Payments',
                'list_associated_accounts_payments' => 'List Associated Accounts Payments',
                'cancel_payments' => 'Cancel Payments'
            ],

            'Inventory' => [
                'create_inventory' => 'Create Inventory',
                'list_inventory' => 'List Inventory',
                'view_inventory_profile' => 'View Inventory Profile',
                'edit_inventory_details' => 'Edit Inventory Details',
                'delete_inventory_details' => 'Delete Inventory Details',
            ],

            'Settings' => [
                'view_primary_config' => 'View Primary Settings',
                'manage_primary_config' => 'Manage Primary Setting',
                'view_inventory_attributes' => 'View Inventory Attributes',
                'manage_inventory_attributes' => 'Manage Inventory Attributes',
                'view_inventory_categories' => 'View Inventory Categories',
                'manage_inventory_categories' => 'Manage Inventory Categories',
                'view_discount_tiers' => 'View Discount Tiers',
                'manage_discount_tiers' => 'Manage Discount Tiers',
                'view_credit_settings' => 'View Credit Settings',
                'manage_credit_settings' => 'Manage Credit Settings',
                'view_warehouses' => 'View Warehouses',
                'manage_warehouses' => 'Manage Warehouses',
                'view_visibility_groups' => 'View Visibility Groups',
                'manage_visibility_groups' => 'Manage Visibility Groups'
            ]

        ];
    }


    public function dynamic_seeds()
    {
        $categories = InventoryCategory::all();

        $group = 'Pricing';

        foreach ($categories as $category) {

            $category_name = $category->name;
            $category_id = $category->id;
            $permission_name = 'manage_prices_for_' . $category_id;
            $permission_label = 'Manage Prices for ' . $category_name;

            Permission::query()->updateOrCreate([
                'name' => $permission_name,
                'group' => $group,
                'label' => $permission_label,
                'entity_id' => $category_id,
                //'meta' => '',
                'meta_model' => 'App/Models/InventoryCategory'
            ]);
        }

        return null;

    }

}
