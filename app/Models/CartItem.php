<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
