<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Brand extends Model implements HasMedia
{
    use SoftDeletes,InteractsWithMedia;

    protected $guarded = [];
    protected $table = 'brands';
    protected $dates = ['deleted_at'];


    public function scopeActive($query)
    {
        return $query->where('status','=',1);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

}
