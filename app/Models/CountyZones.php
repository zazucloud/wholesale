<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountyZones extends Model
{
    use HasFactory;
    protected $table = 'county_zone';
    protected $guarded=[];

}
