<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WholesalerContact extends Model
{
    use HasFactory;

    public function wholesaler()
    {
        return $this->belongsTo(Wholesaler::class);
    }
}
