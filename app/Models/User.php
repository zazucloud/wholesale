<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'smcode',
        'password',
        'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function wholesalers()
    {
        return $this->belongsToMany(Wholesaler::class, 'staff_wholesaler')->using(StaffWholesaler::class);
    }

    public static function create_account($name, $email, $password, $account_type, $avatar, $permissions = [])
    {

        $user = self::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'avatar' => $avatar,
            'account_type' => $account_type
        ]);

        return $user;
    }

    public function zones()
    {
        return $this->hasMany(Zone::class);
    }
}
