<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryManufacturer extends Model
{
    protected $guarded = [];
    protected $table = 'manufacturer_inventory';

    /*TODO:: Move this to Job*/
    public static function set($manufacturer_id, $new_items, $category)
    {
        $category_items =
            InventoryItemCategory::with('inventory')->where('category_id', $category)->get()->pluck('inventory_id');

        InventoryManufacturer::where('manufacturer_id', $manufacturer_id)->whereIn('inventory_id', $category_items)->forceDelete();

        foreach ($new_items as $new_item) {
            InventoryManufacturer::create([
                'manufacturer_id' => $manufacturer_id,
                'inventory_id' => $new_item,
                'status' => 1
            ]);
        }
    }

    /*TODO:: implement relation*/
    public static function get_item_manufacturer($item_id)
    {

        $check = InventoryManufacturer::where('inventory_id', $item_id)->where('status', 1)->first();

        if ($check == null) return null;
        return $check->manufacturer_id;
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
