<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Template extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['name'];

    public function inquiries()
    {
        return $this->hasMany(Inquiry::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function registerMediaConversions(?Media $media = null): void
    {
        $this->addMediaConversion('templateImage')
            ->width('931')->height('144');
    }

    public function getHeaderAttribute()
    {
        return $this->getMedia('templateHeader')->last();
    }
    public function getFooterAttribute()
    {
        return $this->getMedia('templateFooter')->first();
    }
}
