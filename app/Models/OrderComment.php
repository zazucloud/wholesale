<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    protected $guarded = [];
    protected $table = 'order_comments';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function create_comment($comment,$order_id,$created_by)
    {
        if($comment!==null)
        OrderComment::create([
            'order_id' => $order_id,
            'comment' => $comment,
            'user_id' => $created_by,
        ]);
    }

}
