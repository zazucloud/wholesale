<?php

namespace App\Models;

use App\Models\Wholesale\Wholesaler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'name',
    ];


    public static function findOrCreate($value)
    {
        $county = County::where('name', $value->CountyName)->first();

        if (!$county) {
            $county = County::create([
                "id" => strtoupper($value->CountyId),
                "name" => strtoupper($value->CountyName),
            ]);
        }

        return $county;
    }

    public function zones()
    {
        return $this->belongsToMany(Zone::class);
    }

    public function zoneAllocation()
    {
        return $this->hasOne(CountyZones::class,'county_id');
    }

    public function wholesalers(){
        return $this->hasMany(Wholesaler::class,'county_id');
    }
}
