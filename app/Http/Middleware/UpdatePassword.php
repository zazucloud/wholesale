<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UpdatePassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->first_login == 0) {
            return redirect()->route('home.first.login');
        } else if (auth()->check()) {
            return $next($request);
        } else {
            return redirect()->route('login');
        }
    }
}
