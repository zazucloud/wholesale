<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DisabledMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       if (auth()->check() && auth()->user()->disabled == 1) {
            Auth::logout();
            return redirect()->route('login')->withErrors('You Account Has Been Disabled. Please Contact Your Administrator For Assistance.');
        } else {
            return $next($request);
        }
    }
}
