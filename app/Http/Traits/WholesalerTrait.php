<?php

namespace App\Http\Traits;


use App\Models\InventoryCategory;
use App\Models\Wholesaler;

trait WholesalerTrait
{

    public function can_access_category(Wholesaler $wholesaler, InventoryCategory $inventoryCategory)
    {
        $wholesaler_visible_category = $wholesaler->categories->pluck('id')->toArray();

        if(in_array($inventoryCategory->id, $wholesaler_visible_category)) return true;
        return false;
    }


}