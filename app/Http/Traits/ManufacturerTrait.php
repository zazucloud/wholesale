<?php

namespace App\Http\Traits;


use App\Models\Inventory;
use App\Models\Manufacturers;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Support\Carbon;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\Storage;

trait ManufacturerTrait
{

    /*Returns ids of inventory which a manufacturer can produce*/
    public function producible_inventory($manufacturer_id)
    {

        $manufacturer = Manufacturers::find($manufacturer_id);

        if ($manufacturer === null) {
            return Inventory::all()->pluck('id')->toArray();
        } else {
            return $manufacturer->producible_inventory();
        }
    }

    public function production_request_pdf($production_request, $manufacturer_id, $export_type = 'stream')
    {

        $production_items = $production_request->items()
            ->join('inventory', 'inventory_id', '=', 'inventory.id')
            ->whereIn('inventory_id', $this->producible_inventory($manufacturer_id))
            ->orderBy('production_request_items.item_delivery_date', 'DESC')->get();

        $request_tag = $production_request->request_tag;

        $sales_person = User::find($production_request->staff_id);

        $date_difference = Carbon::parse(Carbon::now())->diffInDays($production_request->delivery_date);

        if ($date_difference === 0) {
            $date_difference_label = 'Today';
        } elseif ($date_difference === 1) {
            $date_difference_label = 'Tomorrow';
        } else {
            $date_difference_label = $date_difference . ' days from now';
        }

        $manufacturer_name = '';
        if (Manufacturers::find($manufacturer_id)) {
            $manufacturer_name = 'For ' . Manufacturers::find($manufacturer_id)->name;
        }

        $pdf = PDF::loadView('exports.pdf.production_request', compact('manufacturer_name', 'production_items', 'date_difference_label', 'sales_person', 'production_request', 'request_tag'));

        $export_file_label = 'Production_Request-' . $request_tag . '-' . env('APP_NAME');


        $filePath = $export_file_label . '.' . 'pdf';
        Storage::disk('temppdfs')->put($filePath, $pdf->output());
        return $filePath;
    }


}
