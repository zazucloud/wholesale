<?php

namespace App\Http\Traits;

use App\Imports\InventoryImport;
use App\Models\CsvData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

trait InventoryImportTrait
{

    public function custom_import_Inventory(Request $request)
    {

        $path = $request->file('csv_file')->getRealPath();


        $data = Excel::toArray(new InventoryImport(null, 1), $request->file('csv_file'));
        $data = $data[0];

        $upload = $request->file('csv_file');
        $filename = 'im-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );


        if (count($data) > 0) {

            if ($request->has('header')) {

                $csv_header_fields = [];

                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $value;
                }

                $csv_data = array_slice($data, 1, 2);
            } else {
                // get the first two rows of the csv file with no header
                $csv_data = array_slice($data, 0, 2);
            }


            //store the temporary data into the csvdata table
            $csv_data_file = CsvData::create([
                'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode(array_slice($data, 0, 3))
            ]);
        } else {
            return redirect()->back();
        }


        if ($csv_data_file->csv_header == 1) {
            return view('admin.config.inventory.import_parse', compact('csv_header_fields', 'csv_data', 'csv_data_file', 'filename'));
        }
        return view('admin.config.inventory.import_parse', compact('csv_data', 'csv_data_file', 'filename'));
    }


    public function custom_import_Inventory_process(Request $request)
    {
        ini_set('memory_limit', '1024M');
        $csvdata = CsvData::find($request->csv_data_file_id);
        $start = 1;
        if ($csvdata->csv_header == 1) {
            $start = null;
        }
        try {
            $import = new InventoryImport($request, $start);

            $import->import(base_path('storage/app/uploads/' . $request->filename));

            //delete the the csv file from the storage
            Storage::delete('uploads/' . $request->filename);
            //delete the the csv data from the csvdata table
            $csvdata->delete();

            return redirect()->route('admin.config.inventory_import')->with(['success' => "Inventories Imported  Successfully"]);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return redirect()->route('admin.config.inventory_import')->withErrors(["Something went wrong. Please download excel template to countercheck column Mappings are correct."]);
        }
    }



}
