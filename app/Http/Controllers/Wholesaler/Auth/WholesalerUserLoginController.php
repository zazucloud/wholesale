<?php

namespace App\Http\Controllers\Wholesaler\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseTemplateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

class WholesalerUserLoginController extends BaseTemplateController
{

    public function __construct()
    {
        parent::__construct();
        // $this->middleware('guest');
        $this->middleware('guest:wholesalerUser',['except' => ['wholesalerLogout']]);
    }

    public function showLoginForm()
    {

        return view('wholesaler.auth.login');
    }

    public function login(Request $request)
    {
        // validate the form data
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);
        //attempt to log the user in

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::guard('wholesalerUser')->attempt($credentials, $request->remember)) {
            //if successfull then redirect to their intended view
            return redirect()->intended(route('wholesaler.dashboard'));
        }
        //if unsuccessfull then redirect back to the login with the form data
        return back()->withInput($request->only('email', 'remember'));
    }

    public function wholesalerLogout(Request $request)
    {
        Auth::guard('wholesalerUser')->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();

        // if ($response = $this->loggedOut($request)) {
        //     return $response;
        // }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect()->route('wholesale_user.loginForm');
    }
}
