<?php

namespace App\Http\Controllers\Wholesaler;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:wholesalerUser');
    }

    public function index()
    {
        $products = Inventory::all();

        return view('wholesaler.products.index', compact('products'));
    }

    public function retrieveCart()
    {
        $cart=Cart::where('wholesaler_id', auth()->user()->wholesaler_id)->where('status', 0)->first();
        if(is_null($cart)){
            $cartItems = 0;

        }else{
            $cartItems = $cart->cartItems->count();

        }
        return response()->json(['counter' => $cartItems]);
    }

    public function show($id)
    {

        $product = Inventory::find($id);

        return view('wholesaler.products.show', compact('product'));
    }

    public function addToCart(Request $request)
    {

        $wholesalerId = auth()->user()->wholesaler->id;

        $wholesalerStatus = Cart::where('wholesaler_id', $wholesalerId)->where('status', 0)->orderBy('id', 'asc')->first();

        if (!$wholesalerStatus) {
            $cart = new Cart();
            $cart->wholesaler_id = $wholesalerId;
            $cart->save();

            //add cart items
            $cartItem = new CartItem();
            $cartItem->cart_id = $cart->id;
            $inventory = $request->input('inventory_id');

            $existsingInventory = Cart::find($cart->id)->with('cartItems')->where('wholesaler_id', $wholesalerId)
                ->where('status', 0)->whereHas('cartItems', function ($query) use ($inventory) {
                    $query->where('inventory_id', $inventory);
                });



            if ($existsingInventory->count() > 0) {

                $ei =   $existsingInventory->cartItems->first();

                $ei->quantity = $ei->quantity + $request->input('quantity');
                $ei->total_price = $ei->quantity * $request->input('price');
                $ei->update();
            } else {
                $cartItem->inventory_id = $inventory;
                $cartItem->quantity = $request->input('quantity');
                $quantity =  $cartItem->quantity;
                $cartItem->price = $request->input('price');
                $price =  $cartItem->price;
                $cartItem->total_price = $quantity * $price;

                $cartItem->save();
            }
        } else {

            $cartItem = new CartItem();
            $cartItem->cart_id = $wholesalerStatus->id;

            $inventory = $request->input('inventory_id');

            $existsingInventory = CartItem::where('cart_id', $cartItem->cart_id)
                ->where(function ($query) use ($inventory) {
                    $query->where('inventory_id', $inventory);
                });

            if ($existsingInventory->count() > 0) {

                $ei =  $existsingInventory->first();

                $ei->quantity = $ei->quantity + $request->input('quantity');
                $ei->total_price = $ei->quantity * $request->input('price');
                $ei->update();
            } else {
                $cartItem->inventory_id = $inventory;
                $cartItem->quantity = $request->input('quantity');
                $quantity =  $cartItem->quantity;
                $cartItem->price = $request->input('price');
                $price =  $cartItem->price;
                $cartItem->total_price = $quantity * $price;

                $cartItem->save();
            }
        }



        return response()->json(['message' => 'successfully added to cart'], 200);
    }

    public function cartSummary(Request $request)
    {
        $cartItems = Cart::where('wholesaler_id', auth()->user()->wholesaler_id)->where('status', 0)->first()->cartItems;

        $grandTotal = Cart::where('wholesaler_id', auth()->user()->wholesaler_id)->where('status', 0)->first()->cartItems->sum('total_price');

        return view('wholesaler.products.cartSummary', compact('cartItems', 'grandTotal'));
    }

    public function completeOrder(Request $request)
    {

        $cart=Cart::where('wholesaler_id', auth()->user()->wholesaler_id)->where('status', 0)->first();
        $cartItems = $cart->cartItems;

        $grandTotal = Cart::where('wholesaler_id', auth()->user()->wholesaler_id)->where('status', 0)->first()->cartItems->sum('total_price');

        $order = Order::create([
            'wholesaler_id' => auth()->user()->wholesaler_id,
            'delivery_location' => $request->location,
            'delivery_date' => $request->delivery_date,
            'total' => $grandTotal,
            'is_draft' => 1,
            'stage' => 1,
        ]);

        foreach ($cartItems as $key => $value) {

            OrderProduct::create([
                'order_id' => $order->id,
                'inventory_id' => $value->inventory_id,
                'quantity' => $value->quantity,
                'price' => $value->price,
                'status' => 1,
                'attributes' => null
            ]);
        }
        $cart->status=1;
        $cart->update();

        return redirect()->route('wholesale.products.index')->with(['success' => 'Created New Order']);
    }

    public function cartQuantity(Request $request)
    {

        // dd($request->all());

        for ($i = 0; $i < sizeof($request->cartitemid); $i++) {
            $cartItem = CartItem::find($request->cartitemid[$i]);
            $cartItem->quantity = $request->quantity[$i];
            $cartItem->price = $request->price[$i];


            $quantity = $cartItem->quantity;
            $price = $cartItem->price;

            $total_price = $quantity * $price;
            $cartItem->total_price = $total_price;
            $cartItem->update();
        }

        return redirect()->route('wholesale.products.checkout');
        // return back();
    }

    public function checkout(Request $request)
    {

        $cartItems = Cart::where('wholesaler_id', auth()->user()->wholesaler_id)->where('status', 0)->first()->cartItems;

        $locations = auth()->user()->wholesaler->locations;


        return view('wholesaler.products.checkout', compact('cartItems', 'locations'));
    }

    public function destroy(Request $request)
    {
        $cartItem = CartItem::findOrFail($request->cart_item_id);

        $cartItem->delete();

        return back();
    }
}
