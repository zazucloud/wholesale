<?php

namespace App\Http\Controllers\Wholesaler;


use App\Http\Controllers\BaseTemplateController;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\Wholesale\Wholesaler;
use App\Models\WholesalerCategoryDiscount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WholesalerController extends BaseTemplateController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:wholesalerUser');
    }

    public function dashboard()
    {

        return view('wholesaler.dashboard');
    }

    public function orders()
    {

        $orders = $this->wholesaler()->orders()->ordered()->draft(0)->paginate(10);

        $filter_label = ' Orders';

        return view('wholesaler.orders')->with(['orders' => $orders, 'filter_label' => $filter_label]);
    }

    public function wholesaler()
    {
        return Wholesaler::find(user()->account_id);
    }

    public function order()
    {
        $products = Inventory::all();
        $wholesaler = $this->wholesaler();

        return view('wholesaler.create_order', compact('products', 'wholesaler'));
    }

    public function order_process(Request $request)
    {

        return $request;
    }

    public function shop()
    {
        return view('shop.shop');
    }


    public function wholesaler_product_selection(Request $request)
    {
        $wholesaler_id = $request->get('wholesaler_id');

        $wholesaler = new Wholesaler();
        $products = $wholesaler->visible_products($wholesaler_id);

        $options = '';
        foreach ($products as $product) {
            $price = WholesalerCategoryDiscount::price($wholesaler_id, $product->id);
            $options .= "<option data-unit_price='$price' value='$product->id'>$product->name</option>";
        }

        return "<select class='select2 item_product form-control' data-id='1' name='product[]'
                                               id='product_1'>
                                                <option value='0'>Product Selection</option>
                                                $options
                                        </select>" . "<script>$(\".item_product\").on('change', function () {

            var id = $(this).data('id');
            var selected = $(this).select2().find(\":selected\");
            var unit_price = selected.data(\"unit_price\");

            $(\"#unit_price_\" + id).val(unit_price);

            $(\"#quantity_\" + id).val(1);
            compute();
        }); </script>";
    }

}