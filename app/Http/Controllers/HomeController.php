<?php

namespace App\Http\Controllers;

use App\Models\Applications;
use App\Models\Manufacturers;
use App\Models\Order;
use App\Models\Staff;
use App\Models\Wholesaler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HomeController extends BaseTemplateController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $finalarray = $this->adminDashboardData();

        return view('home')->with($finalarray);
    }

    public function adminDashboardData()
    {
        $wholesaler_count = Str::plural(Wholesaler::active()->count());

        $staff_accounts_count = Str::plural(Staff::active()->count());

        $manufacturers = new Manufacturers();
        $pending_production_requests_count = $manufacturers->pending_production_requests->count();
        $pending_orders_count = Order::ofStage(1)->count();

        $recent_pending_orders = Order::ofStage(1)->latest('created_at')->take(5)->get();
        $recent_applications = Applications::ofStatus(1)->latest('created_at')->take(5)->get();
        $results = [
            'wholesaler_count' => $wholesaler_count,
            'pending_production_requests_count' => $pending_production_requests_count,
            'pending_orders_count' => $pending_orders_count,
            'recent_pending_orders' => $recent_pending_orders,
            'recent_applications' => $recent_applications,
            'staff_accounts_count' => $staff_accounts_count
        ];
        return $results;
    }

    public function manufacturerDashboardData()
    {
        $pending_requests_count = Manufacturers::findOrFail(auth()->user()->account_id)->pending_production_requests->count();
        $completed_requests_count = Manufacturers::findOrFail(auth()->user()->account_id)->completed_requests->count();
        $delivered_requests_count = Manufacturers::findOrFail(auth()->user()->account_id)->delivered_production_requests->count();
        $in_production_requests_count = Manufacturers::findOrFail(auth()->user()->account_id)->in_production_requests->count();

        $recent_requests = Manufacturers::findOrFail(auth()->user()->account_id)->recent_requests();
        $results = [
            'pending_requests_count' => $pending_requests_count,
            'completed_requests_count' => $completed_requests_count,
            'delivered_requests_count' => $delivered_requests_count,
            'in_production_requests_count' => $in_production_requests_count,
            'recent_requests' => $recent_requests
        ];
        return $results;
    }

    public function WholesalerDashboardData()
    {
        $results = [];
        return $results;
    }

    public function firstLoginReset()
    {
        return view('auth.passwords.firsttime');
    }

    public function firstLoginUpdate(Request $request)
    {
        // dd($request->all());

        $this->validate($request, [
            'password' => 'required|same:password_confirmation'
        ]);

        $useracc = auth()->user();
        // dd($useracc);

        $useracc->password = Hash::make($request->password);
        $useracc->first_login = 1;
        $useracc->update();
        // dd($useracc);

        return redirect()->route('admin.dashboard');
    }
}
