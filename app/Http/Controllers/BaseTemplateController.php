<?php

namespace App\Http\Controllers;

use App\Models\ThemeManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BaseTemplateController extends Controller
{
  public $theme_manager = null;

  //load theme configurations to blade files
  public function __construct()
  {
    $theme_manager = new ThemeManager();
    View::share('theme_manager', $theme_manager);
  }
}
