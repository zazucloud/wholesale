<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;

class BusBatchController extends Controller
{
    public function checkBatchStatus($batchId){
        $batchstatus=Bus::findBatch($batchId)->finished();
        return response()->json(['batchstatus'=>$batchstatus],200);
    }


    public function downloadFile($filename){
        return response()->download(Storage::disk('localexcels')->path($filename))->deleteFileAfterSend(true);
    }
}
