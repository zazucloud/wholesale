<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoriesMobileResource;
use App\Http\Resources\InventoryMobileResource;
use App\Models\Inventory;
use App\Models\InventoryCategory;
use App\Models\InventoryItemCategory;
use App\Models\Wholesale\Wholesaler;
use Illuminate\Http\Request;

class InventoryController extends Controller
{

    public function categories($wholesaler_id){
        $categories=Wholesaler::find($wholesaler_id)
        ->categories;

        return response()->json([
            "success" => true,
            "categories" => CategoriesMobileResource::collection($categories),
            "message" => "Categories fetched successfully."
        ]);

    }

    public function wholesaleInventory($categoryid)
    {
        $inventory = InventoryCategory::find($categoryid)->api_inventory;

        return response()->json([
            "success" => true,
            "inventory" => InventoryMobileResource::collection($inventory),
            "message" => "Inventory For Wholesaler fetched successfully."
        ]);
    }
}
