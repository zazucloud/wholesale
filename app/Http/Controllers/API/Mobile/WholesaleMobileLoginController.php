<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Route;
class WholesaleMobileLoginController extends Controller
{
    private $client;

    public function login(Request $request)
    {

        // query oauth_client password encription key.
        $this->client = Client::find(env('PASSPORT_CLIENT_ID'));

        //create payload and post to laravel passport auth route
        $params = [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->email,
            'password' => $request->password,
            'scope' => '*'
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
    }

    public function userRefreshToken(Request $request)
    {
        $this->client = Client::find(env('PASSPORT_CLIENT_ID'));

        $data = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => ''
        ];
        $request = Request::create('/default/oauth/token', 'POST', $data);
        $content = json_decode(app()->handle($request)->getContent());
        return response()->json([
            'error' => false,
            'token' => $content->access_token,
            'refresh_token' => $content->refresh_token
        ], 200);
    }
}
