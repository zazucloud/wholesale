<?php

namespace App\Http\Controllers\API;


use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wholesale\WholesalerUser;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Http\Controllers\AccessTokenController;

class AuthController extends AccessTokenController
{
    public function auth(ServerRequestInterface $request)
    {
        // Then we just add the user to the response before returning it.
        $username = $request->getParsedBody()['username'];

        $user = WholesalerUser::where("email", $username)->first();
        $tokenResponse = parent::issueToken($request);
        // dd($request);

        $token = $tokenResponse->getContent();

        // $tokenInfo will contain the usual Laravel Passort token response.
        $tokenInfo = json_decode($token, true);

        // dd($user);
        $tokenInfo = collect($tokenInfo);
        $tokenInfo->put('wholesaler_id', $user->wholesaler_id);
        $tokenInfo->put('name', $user->name);
        $tokenInfo->put('email', $user->email);
        $tokenInfo->put('phone', $user->phone_no);
        $tokenInfo->put('wholesaler_user_id', $user->id);

        return $tokenInfo;
    }
}
