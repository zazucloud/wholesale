<?php

namespace App\Http\Controllers\API\Fieldsales;

use App\Models\Template;
use Illuminate\Http\Request;
use App\Jobs\CreateWholesalerJob;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Wholesale\Wholesaler;
use Illuminate\Support\Facades\Validator;

class WholesalerController extends Controller
{
    public function index()
    {
        $wholesaler = Wholesaler::all();

        return response()->json([
            "success" => true,
            "wholesaler" => $wholesaler,
            "message" => "Wholesalers fetched successfully."
        ]);
    }


    public function location_ajax_specific($id)
    {
        $locations = Wholesaler::find($id)->locations;
        return response()->json(['locations' => $locations], 200);
    }

    public function templates()
    {
        $templates =  Template::all();
        return response()->json(['templates' => $templates], 200);
    }

    public function sync_clients_to_fieldsales(Request $request)
    {
        // Log::info($request);
        $this->validate(
            $request,
            [
                'company_name' => 'required',
                'cust_code' => 'required',
            ]
        );

        $email = $request->get('email');
        $name = $request->get('company_name');

        $plain_password = passwordGenerator(9);
        $db_password = bcrypt($plain_password);

        $wholesaler = Wholesaler::create([
            'business_name' => $name,
            'slug' => str_slug($request['cust_code']),
            'postal_address' => $request['company_address'],
            'physical_address' => $request['address'],
            'telephone' => $request['phone'],
            'email' => $request['email_address'],
            'pin' => $request['kra_pin'],
            'registration_number' => $request['cust_code'],
            'contact_name' => $request['firstcontact']['name'],
            'contact_position' => $request['firstcontact']['designation'],
            'status' => 1,
            'synced' => 1,
        ]);

        Log::info($wholesaler,"Synced succesfully");

        $inputs = $wholesaler;

        CreateWholesalerJob::dispatch($inputs);

        return response()->json([
            "success" => true,
            "wholesaler" => $wholesaler,
            "message" => "Wholesalers created successfully."
        ]);
    }
}
