<?php

namespace App\Http\Controllers\API\Fieldsales;

use App\Models\Order;
use App\Models\Retailer;
use App\Models\Inventory;
use App\Jobs\CreateOrderJob;
use App\Models\OrderComment;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Wholesale\Wholesaler;
use App\Jobs\OrderProductionRequestJob;
use Illuminate\Support\Facades\Validator;
use App\Models\Wholesale\WholesalerCategoryDiscount;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    public static function order_tag($len = 8)
    {

        $shuffled_string = substr(str_shuffle("23ab34567c1de12893456fghjk4mnpq5rs7t6v5456w9xyz5"), 0, $len);
        $string = strtoupper($shuffled_string);

        $check = Order::order_tag_is_unique($string);

        if ($check === false) {
            return self::order_tag();
        }

        return $string;
    }

    public function checkExcess(Request $request)
    {
        $boolean = $this->checkExcessCommon($request);
        return $boolean;
    }

    public function checkExcessApi(Request $request)
    {
        $boolean = $this->checkExcessCommon($request);

        return response()->json(['status' => $boolean], 200);
    }

    public function checkExcessCommon(Request $request)
    {
        $excess_items = [];


        if (is_string($request->inventory)) {
            $items = json_decode($request->inventory, true);
        } else {
            $items = $request->inventory;
        }

        if ($items !== null) {
            for ($i = 0; $i < sizeof($items); $i++) {
                $find_inventory = Inventory::find($items[$i]["inventory_id"]);
                $item_quantity = intval($items[$i]["quantity"]);

                if ($item_quantity > $find_inventory->quantity) {
                    $difference = $item_quantity - intval($find_inventory->quantity);
                    $update_quantity = 0;
                    $level_array = [
                        'inventory_id' => $items[$i]["inventory_id"],
                        'name' => $find_inventory->name,
                        'sku' => $find_inventory->sku,
                        'leftover_quantity' => $difference
                    ];
                    array_push($excess_items, $level_array);
                }
            }
        }
        if (sizeof($excess_items)) {
            return [
                'status' => true,
                'excess_items' => $excess_items
            ];
        } else {
            return [
                'status' => false,
                'excess_items' => []
            ];
        }
    }

    public function store(Request $request)
    {
        //check request type
        $validator = Validator::make($request->all(), [
            'wholesaler' => 'required',
            'delivery_date' => 'required',
            'inventory' => 'required',
            'location' => 'required',
            'generate_production' => 'required',
            'template' => 'required'
        ]);



        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        try {


            $wholesaler_id = $request['wholesaler'];
            $template_id = $request['template'];


            //run validations
            if ($request['generate_production'] == "false") {
                $check_excess = $this->checkExcess($request);
                if ($check_excess['status'] == true) {
                    return response()->json([
                        "success" => false,
                        "items" => $check_excess['excess_items'],
                        "generate_production" => false,
                        "message" => "Excess Quantity Detected,Please Remove Excess Quanity or Accept Production Request Creation When Popup Appears",
                    ]);
                }
            }

            $delivery_date = $request['delivery_date'];

            $order_tag = self::order_tag();

            $wholesaler = Wholesaler::find($wholesaler_id);


            $order = $wholesaler->orders()->create([
                'order_tag' => $order_tag,
                'delivery_location' => $request['locations'],
                'delivery_date' => $delivery_date,
                'template_id' => $template_id,
                'total' => 0,
                'is_draft' => 0,
                'stage' => 1,
                'created_by' => 1
                // 'created_by' => auth()->user()->id
            ]);
            $item_total = [];
            $excess_items = [];
            $items = $request['inventory'];

            for ($i = 0; $i < sizeof($items); $i++) {

                $find_inventory = Inventory::find($items[$i]["inventory_id"]);
                $price = WholesalerCategoryDiscount::price($wholesaler_id, $items[$i]["inventory_id"]);
                $item_quantity = intval($items[$i]["quantity"]);

                $discount_percentage = percentage_discount_given($price, Inventory::findOrFail($items[$i]["inventory_id"])->price);
                $vat_rate = env('DEFAULT_VAT_RATE');


                OrderProduct::create([
                    'order_id' => $order->id,
                    'inventory_id' => $items[$i]["inventory_id"],
                    'quantity' => $item_quantity,
                    'price' => $price,
                    'vat_percent' => $vat_rate,
                    'discount_percent' => $discount_percentage,
                    'status' => 1,
                    'attributes' => null
                ]);

                $vat_amount = vat_amount($price, $vat_rate);
                $item_total[] = ($vat_amount + $price) * $item_quantity;

                if ($item_quantity > $find_inventory->quantity) {
                    $difference = $item_quantity - intval($find_inventory->quantity);
                    $update_quantity = 0;
                    $level_array = [
                        'inventory_id' => $items[$i]["inventory_id"],
                        'leftover_quantity' => $difference
                    ];
                    array_push($excess_items, $level_array);
                } else {
                    $update_quantity = intval($find_inventory->quantity) - $item_quantity;
                }
                $find_inventory->quantity = $update_quantity;
                $find_inventory->update();
            }


            $total = array_sum($item_total);

            $order->update([
                'total' => $total
            ]);

            if ($request->hasFile('attachment')) {

                $orderAttchment = $request->file('attachment');
                $extension = $orderAttchment->getClientOriginalExtension();

                if ($extension == "pdf") {
                    $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                    $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');
                } else {

                    $height = Image::make($request->attachment)->height();
                    $width = Image::make($request->attachment)->width();


                    if ($height > 0 || $width > 0) {
                        $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                        $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');
                    }
                }
            }

            if ($request->has('comment')) {
                OrderComment::create_comment($request->comment, $order->id, auth()->user()->id);
            }

            // CreateOrderJob::dispatch($wholesaler, $order, $request->type);

            if (sizeof($excess_items) > 0 && $request->generate_production == "true") {
                OrderProductionRequestJob::dispatch($wholesaler, $excess_items, $delivery_date, $order->id, $order->clientable_type, auth()->user());
            }

            return response()->json([
                "success" => true,
                "order" => $order,
                "message" => "Order placed succesfully.",
            ]);
        } catch (\Throwable $th) {
            Log::info($th);

            return response()->json([
                "success" => false,
                "message" => "Something went wrong.",
            ]);
        }
    }

    public function wholesalerOrders(Request $request, $id)
    {
        $orders = Order::where('clientable_type', 'App\Models\Wholesale\Wholesaler')
            ->where('clientable_id', $id)
            ->latest()
            ->paginate(30);

        return response()->json($orders, 200);
    }

    public function wholesalerOrderItems($id)
    {
        $orders = OrderProduct::select('order_product.*', 'inventory.itemnumber', 'inventory.name', 'inventory.description', 'inventory.sku')
            ->leftjoin('inventory', 'inventory.id', '=', 'order_product.inventory_id')
            ->where('order_id', $id)
            ->latest()
            ->get();

        return response()->json(['items' => $orders], 200);
    }
}
