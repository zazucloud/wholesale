<?php

namespace App\Http\Controllers\API\Fieldsales;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    public function index()
    {
        $inventory = Inventory::all();

        return response()->json([
            "success" => true,
            "inventory" => $inventory,
            "message" => "Inventory fetched successfully."
        ]);
    }
}
