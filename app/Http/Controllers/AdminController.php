<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseTemplateController;
use App\Models\Applications;
use App\Models\Bank;
use App\Models\Manufacturers;
use App\Models\Order;
use App\Models\Staff;
use App\Models\Wholesale\Wholesaler;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function dashboard()
    {

        $wholesaler_count = Str::plural(Wholesaler::active()->count()) ;

        // dd(auth()->user()->getRoleNames()->where());

        $manufacturers = new Manufacturers();
        $pending_production_requests_count = $manufacturers->pending_production_requests->count();
        $pending_orders_count = Order::ofStage(1)->count();

        $recent_pending_orders = Order::ofStage(1)->latest('created_at')->take(5)->get();

        return view('admin.dashboard',compact('recent_pending_orders','wholesaler_count','pending_production_requests_count','pending_orders_count'));
        // return view('admin.dashboard');

    }

    public function ajax_fetch_bank_branches(Request $request)
    {
        $this->validate($request, [
            'bank_id' => 'required'
        ]);

        $bank_id = $request->get('bank_id');

        $branches = Bank::branches($bank_id);

        $options = '';
        foreach ($branches as $branch) {

            $branch_id = $branch->id;
            $name = $branch->name;

            $options .= "<option value='$branch_id'>$name</option>";
        }

        return $options;

    }


}
