<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Inquiry;
use Illuminate\Http\Request;

class StartController extends BaseTemplateController
{
    /**__construct()
     * parent::__construct() initiate template settings from baseTemplateController
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * index()
     * return login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('auth.login');
    }


    /**
     * nopermission()
     * return no permissions blade view.
     *
     * @return \Illuminate\Http\Response
     */
    public function nopermission(){
        return view('permissions.nopermissions');
    }

    public function testviews(){
        //  $order=Order::find(72);
        // return view('exports.pdf.order_sheet',compact('order'));

        $inquiry=Inquiry::find(10);
        return view('exports.pdf.quotation', compact('inquiry'));
    }
}
