<?php

namespace App\Http\Controllers\Manufacturer;

use App\Http\Controllers\BaseTemplateController;
use Illuminate\Http\Request;
use App\Http\Traits\ManufacturerTrait;
use App\Models\Manufacturers;
use App\Models\ProductionRequest;
use App\Models\ProductionRequestComment;
use App\Models\ProductionRequestItem;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Support\Carbon;

class ManufacturerController extends BaseTemplateController
{

    use ManufacturerTrait;


    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:manufacturerUser');
    }

    /*TODO:: implement middleware to allow production request to only be accessed by concerned users*/


    public function my_inventory_list()
    {
        return Manufacturers::findOrFail(user()->account_id)->produces_inventory()->get();
    }


    public function dashboard()
    {

        // $pending_requests_count = Manufacturers::findOrFail(user()->account_id)->pending_production_requests->count();
        // $completed_requests_count = Manufacturers::findOrFail(user()->account_id)->completed_requests->count();
        // $delivered_requests_count = Manufacturers::findOrFail(user()->account_id)->delivered_production_requests->count();
        // $in_production_requests_count = Manufacturers::findOrFail(user()->account_id)->in_production_requests->count();

        // $recent_requests = Manufacturers::findOrFail(user()->account_id)->recent_requests();

        return view('manufacturer.dashboard');
    }


    public function pending_requests()
    {

        $production_requests = Manufacturers::findOrFail(user()->account_id)->pending_production_requests()->paginate(10);

        $filter_title = 'Pending Production Requests';

        return view('manufacturer.production-requests-listing', compact('filter_title', 'production_requests'));
    }

    public function in_production_requests()
    {

        $production_requests = Manufacturers::findOrFail(user()->account_id)->in_production_requests()->paginate(10);

        $filter_title = 'In-Production Requests';

        return view('manufacturer.production-requests-listing', compact('filter_title', 'production_requests'));
    }

    public function completed_requests()
    {

        $production_requests = Manufacturers::findOrFail(user()->account_id)->completed_requests()->paginate(10);

        $filter_title = 'Completed Requests';

        return view('manufacturer.production-requests-listing', compact('filter_title', 'production_requests'));
    }

    public function delivered_requests()
    {

        $production_requests = Manufacturers::findOrFail(user()->account_id)->delivered_production_requests()->paginate(10);

        $filter_title = 'Delivered Requests';

        return view('manufacturer.production-requests-listing', compact('filter_title', 'production_requests'));
    }


    public function request_view(ProductionRequest $production_request)
    {

        $items_producible = $this->producible_inventory(user()->account_id);

        /*TODO:: refactor via migration -- primary id to item_id*/
        $items_query = $production_request->items()->select(
            'inventory.id as inventory_id',
            'production_status',
            'production_request_items.id as item_id',
            'name',
            'sku',
            'item_notes',
            'quantity',
            'item_delivery_date',
            'requested_quantity',
            'item_priority',
            'attributes'
        )
            ->join('inventory', 'inventory_id', '=', 'inventory.id')
            ->whereIn('inventory_id', $items_producible)
            ->orderBy('production_request_items.item_delivery_date', 'DESC');


        $items = $items_query->get();

        $pending_items_count = $items_query->where('production_request_items.production_request_id', $production_request->id)->whereIn('production_status', [0, 1])->count();

        $sales_person = Staff::find($production_request->staff_id);

        $comments = $production_request->comments;

        $comment_count = count($comments);

        $date_difference = Carbon::parse(Carbon::now())->diffInDays($production_request->delivery_date);

        return view('manufacturer.production_request', compact('date_difference', 'pending_items_count', 'comments', 'production_request', 'items', 'sales_person', 'comments', 'comment_count'));
    }


    public function request_export(ProductionRequest $production_request)
    {
        return $this->production_request_pdf($production_request, user()->account_id, 'download');
    }


    public function print_preview(ProductionRequest $production_request)
    {
        return $this->production_request_pdf($production_request, user()->account_id, 'stream');
    }


    public function requests()
    {

        $production_requests = Manufacturers::findOrFail(user()->account_id)->production_requests()->paginate(10);

        $filter_title = 'All Requests';

        return view('manufacturer.production-requests-listing', compact('filter_title', 'production_requests'))->with('my_inventory_list', $this->my_inventory_list());
    }


    public function request_item_update(Request $request)
    {
        /*TODO:: perm. can-update status*/

        $this->validate($request, [
            'item_id' => 'required',
            'status' => 'required'
        ]);

        ProductionRequestItem::find($request->get('item_id'))->update([
            'production_status' => $request->get('status')
        ]);

        return 1;
    }

    public function production_timeline()
    {
        /*Add report viewing permission*/
    }


    public function production_timeline_filtered(Request $request)
    {
    }
}
