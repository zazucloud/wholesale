<?php

namespace App\Http\Controllers\Manufacturer\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseTemplateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

class ManufacturerLoginController extends BaseTemplateController
{
    

    public function __construct()
    {
        parent::__construct();
        // $this->middleware('guest');
        $this->middleware('guest:manufacturerUser',['except' => ['ManufacturerLogout']]);
    }

    public function showLoginForm()
    {

        return view('manufacturer.auth.login');
    }

    public function login(Request $request)
    {
        // validate the form data
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);
        //attempt to log the user in

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::guard('manufacturerUser')->attempt($credentials, $request->remember)) {
            //if successfull then redirect to their intended view
            return redirect()->intended(route('manufacturer.dashboard'));
        }
        //if unsuccessfull then redirect back to the login with the form data
        return back()->withInput($request->only('email', 'remember'));
    }

    public function ManufacturerLogout(Request $request)
    {
        Auth::guard('manufacturerUser')->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();

        // if ($response = $this->loggedOut($request)) {
        //     return $response;
        // }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect()->route('manufacturer_user.loginForm');
    }

}
