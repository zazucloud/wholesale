<?php

namespace App\Http\Controllers\Admin;

use App\Models\Zone;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseTemplateController;
use App\Models\County;
use PHPUnit\Framework\Constraint\Count;

class CountyController extends BaseTemplateController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $counties = County::orderBy('id', 'ASC')->paginate(30);

        return view('counties.index', compact('counties'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('counties.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, County $county)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $county = $request->all();
        // dd($county);

        County::create($county);

        return redirect()->route('counties.index')->with('success', 'County created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(County $county)
    {
        $county = County::find($county->id);

        return view('counties.edit', compact('county'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, County $county)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $input = $request->all();

        $county = County::find($county->id);
        $county->update($input);

        return redirect()->route('counties.index')->with('success', 'County updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $county = County::find($id);
        $county->delete();

        return redirect()->route('counties.index')->with('success', 'County deleted successfully');
    }

    public function search(Request $request)
    {

        $counties = County::where('name', 'LIKE', '%' . $request->search . '%')
            ->paginate(30);

        return  view('counties.index', compact('counties'))->with('i', ($request->input('page', 1) - 1) * 30);;
    }
}
