<?php

namespace App\Http\Controllers\Admin;

use App\Exports\InventoryNoImageExport;
use App\Http\Controllers\BaseTemplateController;
use App\Http\Controllers\CacheController;
use App\Http\Controllers\Controller;
use App\Http\Middleware\AdminMiddleware;
use App\Jobs\CacheBuildJob;
use App\Models\Brand;
use App\Models\Inventory;
use App\Models\InventoryAttribute;
use App\Models\InventoryAttributeOption;
use App\Models\InventoryCategory;
use App\Models\InventoryCustomAtrributes;
use App\Models\InventoryImage;
use App\Models\InventoryItemAttribute;
use App\Models\InventoryItemCategory;
use App\Models\Warehouses;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class InventoryController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:inventory-list', ['only' => ['index']]);
        $this->middleware('permission:inventory-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:inventory-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:inventory-delete', ['only' => ['delete']]);
    }

    public function index(Request $request)
    {
        $search = $request->search_name;
        $color = $request->color;
        $warehouse_id = $request->warehouse_id;
        $checkImage = $request->has('checkImage');
        session(['_old_input' => $request->all()]);

        $warehouses = Warehouses::all()->pluck('name', 'id');
        // $inv = Inventory::doesntHave('picture')->get();

        // dd($inv->count());

        $toreturn = $request->except('token');

        $inventory = Inventory::when($warehouse_id, function ($query) use ($warehouse_id) {
            return $query->where('warehouse_id', $warehouse_id);
        })
        ->when($search, function ($query) use ($search) {
            return  $query->where('name', 'LIKE', '%' . $search . '%')
                // ->orwhere('itemnumber', 'LIKE', '%' . $search . '%');
                ->orwhere('sku', 'LIKE', '%' . $search . '%');
        })
        ->when($color, function ($query) use ($color) {
            return  $query->where('color', 'LIKE', '%' . $color . '%');
        })->when($checkImage, function ($query) use ($checkImage){
            return $query->doesntHave('picture');
        })
            ->latest()
            ->paginate(30);

            // dd($inventory);

        return view('admin.inventory.index', compact('inventory', 'warehouses','toreturn'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function inventory_view_gallery($id)
    {
        $inventory = Inventory::find($id);
        return view('admin.inventory.images', compact('inventory'));
    }

    public function create()
    {

        $parent = InventoryCategory::parent()->active()->get();
        $attributes = InventoryAttribute::all();
        $brands = Brand::active()->pluck('name', 'id');
        $selected_categories = [];
        $selected_fixed_attributes = [];
        $selected_flexible_attributes = [];
        $warehouses = Warehouses::all()->pluck('name', 'id');
        $route = ['admin.inventory.create'];

        $current_brand = null;

        return view('admin.inventory.create', compact('current_brand', 'route', 'warehouses', 'selected_flexible_attributes', 'selected_fixed_attributes', 'selected_categories', 'parent', 'attributes', 'brands'));
    }

    public function edit(Inventory $inventory)
    {

        $selected_categories = $inventory->categories->pluck('category_id')->toArray();
        $selected_fixed_attributes = $inventory->fixed_attributes()
            ->select("inventory_item_attributes.*", "inventory_attribute_options.option", 'inventory_attributes.label')
            ->leftjoin("inventory_attribute_options", "inventory_attribute_options.id", "=", "inventory_item_attributes.value")
            ->leftjoin("inventory_attributes", "inventory_attributes.id", "=", "inventory_item_attributes.attribute_id")
            ->get()
            ->groupBy("attribute_id")
            ->toArray();

           if($inventory->productionitem==null){
            $inventory->productionitem=0;
           }
        //    dd($inventory);

        $selected_flexible_attributes = $inventory->custom_attributes;

        $parent = InventoryCategory::with('childinventory')->parent()->active()->get();
        $attributes = InventoryAttribute::with("options")->get();
        $brands = Brand::active()->pluck('name', 'id');
        $route = ['inventory.edit', $inventory->sku];
        $warehouses = Warehouses::all()->pluck('name', 'id');
        $current_brand = $inventory->brand_id;

        return view('admin.inventory.edit', compact('route', 'inventory', 'brands', 'warehouses', 'current_brand', 'parent', 'attributes', 'selected_categories', 'selected_fixed_attributes', 'selected_flexible_attributes'));
    }


    public function store(Request $request)
    {
        // dump(explode(',',$request->attribute_value[0]));
        // dd($request->all());
        $this->validate($request, [
            'sku' => 'required|unique:inventory,sku',
            'name' => 'required|min:3',
            'price' => 'required',
            'visibility' => 'required',
            'image' => 'image|mimes:jpeg,bmp,png|max:5000',
            'image_gallery[]' => 'image|mimes:jpeg,bmp,png|max:5000'
        ]);

        $flexible_attribute_options = $request->get('custom_name', []);
        $fixed_attribute_options = $request->get('attribute_id', []);


        $product = Inventory::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'sku' => $request->get('sku'),
            'brand_id' => $request->get('brand', null),
            'warehouse_id' => $request->get('warehouse', null),
            'quantity' => $request->get('quantity', null),
            'outofstock' => $request->get('outofstock', null),
            'minimumstock' => $request->get('minimumstock', null),
            'threshold' => $request->get('threshold', null),
            'price' => $request->get('price'),
            'crossed_price' => $request->get('crossed_price', null),
            'status' => 1,
            'is_featured' => $request->get('featured', 0),
            'productionitem' => $request->get('productionitem'),
            'visibility' => $request->get('visibility'),
            'created_by' => auth()->user()->id
        ]);

        $inventory_id = $product->id;

        $product_name = $product->name;
        $sku = $product->sku;

        $product_categories = $request->get('categories', []);

        if (count($product_categories) > 0) {
            InventoryItemCategory::set($product->id, $product_categories);
        }


        if ($request->hasFile('image')) {
            $product->addMedia($request->image)->toMediaCollection('inventory');
        }

        if ($request->hasFile('image_gallery')) {
            foreach ($request->image_gallery as $key => $value) {
                $product->addMedia($value)->toMediaCollection('inventorygallery');
            }
        }


        if (count($fixed_attribute_options) > 0) {

            foreach ($fixed_attribute_options as $key => $fixed_attribute_option) {


                InventoryItemAttribute::set_fixed($inventory_id, $fixed_attribute_option, explode(',', $request->attribute_value[$key]));
            }
        }

        if (count($flexible_attribute_options) > 0) {
            foreach ($flexible_attribute_options as $key => $flexible_attribute_option_value) {

                InventoryCustomAtrributes::create([
                    'inventory_id' => $inventory_id,
                    'name' => $flexible_attribute_option_value,
                    'value' => $request->custom_value[$key],
                ]);
            }
        }

        // CacheController::cache();

        /*TODO:: Consider making thumbnails for images*/

        return redirect()->route('admin.inventory.index')->with(['success' => "$product_name (SKU: $sku) has Created Successfully"]);
    }

    public function update(Inventory $inventory, Request $request)
    {
        // dd($request->all());

        $this->validate($request, [
            'sku' => 'required',
            'name' => 'required|min:3',
            'price' => 'required',
            'visibility' => 'required',
            'image' => 'image|mimes:jpeg,bmp,png|max:5000',
            'image_gallery[]' => 'image|mimes:jpeg,bmp,png|max:5000'

        ]);

        $flexible_attribute_options = $request->get('custom_name', []);
        $fixed_attribute_options = $request->get('attribute_id', []);

        $inventory->update([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'sku' => $request->get('sku'),
            'brand_id' => $request->get('brand', null),
            'warehouse_id' => $request->get('warehouse_id', null),
            'quantity' => $request->get('quantity', null),
            'outofstock' => $request->get('outofstock', null),
            'minimumstock' => $request->get('minimumstock', null),
            'threshold' => $request->get('threshold', null),
            'price' => $request->get('price'),
            'crossed_price' => $request->get('crossed_price', null),
            'visibility' => $request->get('visibility'),
            'productionitem' => $request->get('productionitem'),
            'is_featured' => $request->get('is_featured'),
        ]);

        $product = $inventory;
        $inventory_id = $product->id;

        $product_name = $product->name;
        $sku = $product->sku;

        $product_categories = $request->get('categories', []);

        if (count($product_categories) > 0) {
            InventoryItemCategory::set($product->id, $product_categories);
        }

        if ($request->hasFile('image')) {
            foreach ($inventory->getMedia('inventory')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }
            $product->addMedia($request->image)->toMediaCollection('inventory');
        }

        if ($request->hasFile('image_gallery')) {
            foreach ($inventory->getMedia('inventorygallery')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }
            foreach ($request->image_gallery as $key => $value) {
                $product->addMedia($value)->toMediaCollection('inventorygallery');
            }
        }


        InventoryItemAttribute::where('inventory_id', $inventory_id)->forceDelete();
        InventoryCustomAtrributes::where('inventory_id', $inventory_id)->forceDelete();

        if (count($fixed_attribute_options) > 0) {

            foreach ($fixed_attribute_options as $key => $fixed_attribute_option) {


                InventoryItemAttribute::set_fixed($inventory_id, $fixed_attribute_option, explode(',', $request->attribute_value[$key]));
            }
        }

        if (count($flexible_attribute_options) > 0) {
            foreach ($flexible_attribute_options as $key => $flexible_attribute_option_value) {

                InventoryCustomAtrributes::create([
                    'inventory_id' => $inventory_id,
                    'name' => $flexible_attribute_option_value,
                    'value' => $request->custom_value[$key],
                ]);
            }
        }

        // CacheController::cache();

        return redirect()->route('admin.inventory.index')->with(['success' => "$product_name (SKU: $sku) has Updated Successfully"]);
    }


    public function view(Inventory $inventory)
    {

        return view('admin.inventory.view', compact('inventory'));
    }

    public function delete(Request $request, Inventory $inventory)
    {

        $inventory->forceDelete();
        // CacheController::cache();

        return redirect()->route('admin.inventory.index')->with(['success' => "Item has deleted"]);
    }

    public static function inventory_options(Inventory $inventory)
    {

        $attributes = collect($inventory->attributes)->toArray();
        $grouped_attributes = array_group_by($attributes, 'attribute_id');

        $attributes = [];
        foreach ($grouped_attributes as $key => $grouped_attribute) {

            $attribute = InventoryAttribute::find($key);

            $attribute_name = $attribute->label;

            $item_attribute_options = InventoryItemAttribute::where('inventory_item_attributes.inventory_id', $inventory->id)
                ->where('inventory_item_attributes.attribute_id', $attribute->id)->get()->pluck('value')->toArray();

            $options = InventoryAttributeOption::whereIn('id', $item_attribute_options)->where('status', 1)->get();

            if ($options->count()) $attributes[] = ['attribute_name' => $attribute_name, 'attribute_id' => $key, 'options' => $options];
        }

        return $attributes;
    }

    public function inventory_ajax_dropdown(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $page = $request->page;
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;
            if (is_null($request->term)) {
                $inventory = Inventory::select('name', 'id', 'sku')->whereHas("wholesalers")->orderBy('name')->skip($offset)->take($resultCount)->get();
            } else {
                $inventory = Inventory::select('name', 'id', 'sku')
                    ->whereHas("wholesalers")
                    ->where(function ($query) use ($request) {
                        return $query->where('name', 'LIKE',  '%' . $request->term . '%')
                            ->orwhere('sku', 'LIKE',  '%' . $request->term . '%');
                    })->orderBy('name')->skip($offset)->take($resultCount)->get();
            }

            $count = Inventory::count();
            $endCount = $offset + $resultCount;
            $morePages = $endCount < $count;


            $results = array(
                "results" => $inventory,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }
    }

    public function inventory_ajax_specific(Request $request)
    {

        if ($request->ajax()) {
            $inventory = Inventory::find($request->id);
            return response()->json(['inventory' => $inventory]);
        }
    }

    public function inventory_ajax_attributes(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->id;
            $inventory = Inventory::find($id);

            $attributes = InventoryAttribute::with(["attributeitems" => function ($query) use ($id) {
                $query->with("attributeOptionData")->where('inventory_id', $id);
            }])->whereHas("attributeitems", function ($query) use ($id) {
                $query->where('inventory_id', $id);
            })->get();
            return response()->json(['attributes' => $attributes, 'inventory' => $inventory]);
        }
    }

    public function exportInvetory(Request $request){




        return Excel::download(new InventoryNoImageExport($request), 'inventories.xlsx');
    }
}
