<?php

namespace App\Http\Controllers\Admin;

use App\Models\Retailer;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseTemplateController;

class RetailerController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:retailers-list', ['only' => ['index']]);
        $this->middleware('permission:retailers-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:retailers-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:retailers-delete', ['only' => ['destroy']]);
    }


    public function index(Request $request)
    {
        $retailers = Retailer::latest()->paginate(30);
        return view('admin.retailers.index', compact('retailers'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function create()
    {
        return view('admin.retailers.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'business_name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'telephone' => 'required|unique:retailers,telephone',
            'email' => 'required|email',
            'kra_pin' => [
                'required',
                'regex:/^a\d{9}[a-z]$/i',
                'unique:retailers,kra_pin'
            ],
        ]);

        Retailer::create([
            'business_name' => $request->business_name,
            'slug' => str_slug($request->business_name),
            'address' => $request->address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'kra_pin' => $request->kra_pin,

        ]);

        return redirect()->route('admin.retailers.index')->with(['success' => "Retailer Created Successfully"]);
    }

    public function edit($id)
    {
        $retailer = Retailer::find($id);
        return view('admin.retailers.edit', compact('retailer'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'business_name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'telephone' => 'required',
            'email' => 'required|email',
            'kra_pin' => [
                'required',
                'regex:/^a\d{9}[a-z]$/i',
                'unique:retailers,kra_pin'
            ],
        ]);

        $retailer = Retailer::find($id);

        $retailer->business_name = $request->business_name;
        $retailer->slug = str_slug($request->business_name);
        $retailer->address = $request->address;
        $retailer->latitude = $request->latitude;
        $retailer->longitude = $request->longitude;
        $retailer->telephone = $request->telephone;
        $retailer->email = $request->email;
        $retailer->kra_pin = $request->kra_pin;
        $retailer->update();

        return redirect()->route('admin.retailers.index')->with(['success' => "Retailer Updated Successfully"]);
    }

    public function destroy($id)
    {
        $retailer = Retailer::find($id);
        $retailer->delete();
        return redirect()->route('admin.retailers.index')->with(['success' => "Retailer Deleted Successfully"]);
    }

    public function search(Request $request)
    {
        //cache search inputs to persist data in search form
        session(['_old-input' => $request->all()]);

        $search = $request->search;

        $toreturn = $request->except('token');

        $return2 = [
            'search' => $request->search,
        ];

        $retailers = Retailer::when($search, function ($query) use ($search) {
            return $query->where('business_name', 'LIKE', '%' . $search . '%')
                ->orwhere('email', 'LIKE', '%' . $search . '%')
                ->orwhere('telephone', 'LIKE', '%' . $search . '%')
                ->orwhere('kra_pin', 'LIKE', '%' . $search . '%');
        })->paginate(30);

        return view('admin.retailers.index', compact('retailers'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function retailers_ajax_dropdown(Request $request)
    {

        if ($request->ajax()) {
            $page = $request->page;
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;
            if (is_null($request->term)) {
                $clients = Retailer::select('business_name', 'id')->orderBy('business_name', 'ASC')->skip($offset)->take($resultCount)->get();
            } else {
                $clients = Retailer::select('business_name', 'id')
                    ->where(function ($query) use ($request) {
                        return $query->where('business_name', 'LIKE',  '%' . $request->term . '%');
                    })->orderBy('business_name', 'ASC')->skip($offset)->take($resultCount)->get();
            }

            $count = Retailer::count();
            $endCount = $offset + $resultCount;
            $morePages = $endCount < $count;


            $results = array(
                "results" => $clients,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }
    }
}
