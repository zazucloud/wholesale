<?php

namespace App\Http\Controllers\Admin;

use PDF;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Retailer;
use App\Models\Inventory;
use Illuminate\Http\Request;
use App\Models\Manufacturers;
use App\Models\StaffWholesaler;
use App\Models\ProductionRequest;
use Illuminate\Support\Facades\URL;
use App\Models\Wholesale\Wholesaler;
use App\Models\InventoryManufacturer;
use App\Models\ProductionRequestItem;
use App\Jobs\PushProductionRequestJob;
use App\Models\InventoryItemAttribute;
use App\Models\ProductionRequestComment;
use Illuminate\Support\Facades\Redirect;
use App\Jobs\ProductionRequestStatusUpdateJob;
use App\Http\Controllers\BaseTemplateController;
use App\Jobs\ManufactureProductionItemsJob;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductionController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:production-list', ['only' => ['index', 'pending', 'inproduction', 'completed', 'delivered', 'drafts']]);
        $this->middleware('permission:production-create', ['only' => ['create', 'store_step_1']]);
        $this->middleware('permission:production-edit', ['only' => ['edit', 'status_update']]);
    }

    public function index(Request $request)
    {
        // dd($request->all());

        $search = $request->request_tag;
        $date = $request->date;
        $contactperson = $request->createdby;
        $production_requests = ProductionRequest::with('createdBy', 'order')
            ->pushed()

            ->when(!is_null($search), function ($query) use ($search) {
                return $query->where('request_tag', 'LIKE', '%' . $search . '%')->orwhere(function ($q) use($search){
                    return $q->whereHas('order',function($q_level1) use ($search){
                        return $q_level1->where('order_tag', 'LIKE', '%' . $search . '%');
                    });
                });
            })

            ->when($date, function ($query) use ($date) {
                return $query
                    ->whereDate('delivery_date', '=', $date)
                    ->orwhere(function ($q) use ($date) {
                        return $q->whereDate('created_at', '>=', $date)->whereDate('created_at', '<=', $date);
                    });
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('initiator', $contactperson);
            })
            ->orderBy('priority', 'DESC')->paginate(30);

        return view('admin.production.index', compact('production_requests'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function pending(Request $request)
    {

        $search = $request->request_tag;
        $date = $request->date;
        $contactperson = $request->createdby;
        $production_requests = ProductionRequest::with('createdBy')->pushed()

            ->when($search, function ($query) use ($search) {
                return $query->where('request_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('delivery_date', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('initiator', $contactperson);
            })
            ->where('status', 1)->latest()->paginate(10);

        return view('admin.production.pending', compact('production_requests'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function inproduction(Request $request)
    {

        $search = $request->request_tag;
        $date = $request->date;
        $contactperson = $request->createdby;
        $production_requests = ProductionRequest::with('createdBy')->pushed()

            ->when($search, function ($query) use ($search) {
                return $query->where('request_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('delivery_date', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('initiator', $contactperson);
            })
            ->where('status', 2)->latest()->paginate(10);

        return view('admin.production.inproduction', compact('production_requests'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function completed(Request $request)
    {

        $search = $request->request_tag;
        $date = $request->date;
        $contactperson = $request->createdby;
        $production_requests = ProductionRequest::with('createdBy')->pushed()

            ->when($search, function ($query) use ($search) {
                return $query->where('request_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('delivery_date', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('initiator', $contactperson);
            })
            ->where('status', 3)->latest()->paginate(10);

        return view('admin.production.completed', compact('production_requests'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function delivered(Request $request)
    {

        $search = $request->request_tag;
        $date = $request->date;
        $contactperson = $request->createdby;
        $production_requests = ProductionRequest::with('createdBy')->pushed()

            ->when($search, function ($query) use ($search) {
                return $query->where('request_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('delivery_date', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('initiator', $contactperson);
            })
            ->where('status', 4)->latest()->paginate(10);

        return view('admin.production.delivered', compact('production_requests'))->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function drafts()
    {

        $draft_production_requests = ProductionRequest::draft()->paginate(10);

        return view('admin.production.drafts', compact('draft_production_requests'));
    }

    public function create()
    {
        $production_items = null;
        $priority_select_options = ProductionRequest::request_priority_options();
        $retailers = Retailer::orderBy('business_name', 'ASC')->get()->pluck('business_name', 'id');

        $wholesalers = Wholesaler::active()->orderBy('business_name', 'ASC')->get()->pluck('business_name', 'id');

        return view('admin.production.create', compact('wholesalers', 'retailers', 'production_items', 'priority_select_options'));
    }

    public function store_step_1(Request $request)
    {

        if ($request->has("ifstockupdate")) {
            $validation_fields = [
                'delivery_date' => 'required',
                'priority_level' => 'required',
                'inventory_id' => 'required',
            ];
        } else {
            if ($request->type == "retailer") {
                $validation_fields = [
                    'retailer' => 'required|min:1',
                    'delivery_date' => 'required',
                    'priority_level' => 'required',
                    'inventory_id' => 'required',
                ];
            } else {
                $validation_fields = [
                    'customer' => 'required|min:1',
                    'sales_person' => 'required|min:1',
                    'delivery_date' => 'required',
                    'priority_level' => 'required',
                    'inventory_id' => 'required',
                ];
            }
        }

        $this->validate($request, $validation_fields);


        try {
            DB::beginTransaction();

            $raw_delivery_date = $request->get('delivery_date');

            $request_tag = self::request_tag();

            if ($request->has("ifstockupdate")) {
                $new_production_request = ProductionRequest::create([
                    'request_tag' => $request_tag,
                    'initiator' => user()->id,
                    'delivery_date' => $raw_delivery_date,
                    'priority' => $request->get('priority_level'),
                    'draft_stage' => 1,
                    'notes' => $request->get('notes'),
                    'status' => 1
                ]);
            } elseif ($request->type == "wholesaler") {
                $wholesaler = Wholesaler::find($request->customer);
                $new_production_request = $wholesaler->request()->create([
                    'request_tag' => $request_tag,
                    'staff_id' => $request->get('sales_person', null),
                    'initiator' => user()->id,
                    'delivery_date' => $raw_delivery_date,
                    'priority' => $request->get('priority_level'),
                    'draft_stage' => 1,
                    'notes' => $request->get('notes'),
                    'status' => 1
                ]);
            } else {
                $retailer = Retailer::find($request->retailer);
                $new_production_request = $retailer->request()->create([
                    'request_tag' => $request_tag,
                    'initiator' => user()->id,
                    'delivery_date' => $raw_delivery_date,
                    'priority' => $request->get('priority_level'),
                    'draft_stage' => 1,
                    'notes' => $request->get('notes'),
                    'status' => 1
                ]);
            }
            $producedItems = [];

            for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                $quantity = $request->inventory_quantity[$i];

                $array = [];



                foreach (json_decode($request->get('attributes')[$i]) as $key => $value) {

                    array_push($array, [$value->parent => $value->child]);
                }
                $item_attributes_json = json_encode($array, true);
                $raw_delivery_date = $request->item_delivery_date[$i];
                $inventory_id = $request->inventory_id[$i];
                $priority = $request->priority_id[$i] ?? $request->get('priority_level');

                $manufacturer_id = InventoryManufacturer::get_item_manufacturer($inventory_id);


                $itemsrequested =  ProductionRequestItem::create([
                    'inventory_id' => $inventory_id,
                    'production_request_id' => $new_production_request->id,
                    'requested_quantity' => $quantity,
                    'item_notes' => $request->item_notes[$i],
                    'production_status' => 0,
                    'attributes' => $item_attributes_json,
                    'manufacturer_id' => $manufacturer_id,
                    'item_delivery_date' => $raw_delivery_date,
                    'item_priority' => $priority,
                ]);

                array_push($producedItems, $itemsrequested);
            }

            $groupedManufactures = collect($producedItems)->unique('manufacturer_id');


            foreach ($groupedManufactures as $groupmanufacturer) {
                $manufacturer = $groupmanufacturer->manufacturer;

                $listItems = collect($producedItems)->where('manufacturer_id', $manufacturer->id);

                $inventory =  $listItems->map(function ($item) {
                    return   $item->inventory;
                });
                // ManufactureProductionItemsJob::dispatch($manufacturer->email, $manufacturer->name ,$listItems->toArray());
            }


            $new_production_request->draft_stage = 0;
            if ($request->has("ifstockupdate")) {
                $new_production_request->if_stock_update = 1;
            }
            $new_production_request->update();


            return redirect()->route('admin.production.index');
            DB::commit();

            return redirect()->route('admin.production.index')->with(['success' => "Production Request Successfully Created"]);
        } catch (\Throwable $th) {

            DB::rollBack();
            return redirect()->route('admin.production.index')->withErrors(['Something Went Wrong. Contact Administrator For Assistance.']);
        }
    }

    public function creation_step_2(ProductionRequest $production_request)
    {
        /*Add permission check --- permission label -- can manage all requests*/
        $products = Inventory::active()->get();

        $request_tag = $production_request->request_tag;

        $production_items = $production_request->items()->join('inventory', 'inventory_id', '=', 'inventory.id')->orderBy('production_request_items.id', 'DESC')->get();

        // dd($production_items);

        $default_production_date = $production_request->delivery_date;
        $parsed_production_date = Carbon::parse($default_production_date)->format('d-M-Y (l)');

        $priority_select_options = ProductionRequest::request_priority_options();


        return view('admin.production.create_request.step-2', compact('priority_select_options', 'parsed_production_date', 'request_tag', 'products', 'production_request', 'production_items'));
    }

    public function store_step_2(Request $request)
    {
    }


    public function step_3(ProductionRequest $production_request)
    {

        $request_tag = $production_request->request_tag;

        $production_items = $production_request->items()->join('inventory', 'inventory_id', '=', 'inventory.id')->orderBy('production_request_items.item_delivery_date', 'DESC')->get();

        $default_production_date = $production_request->delivery_date;
        $parsed_production_date = Carbon::parse($default_production_date)->format('d-M-Y (l)');

        return view('admin.production.create_request.step-3', compact('parsed_production_date', 'request_tag', 'production_request', 'production_items'));
    }

    public function push_production_request(Request $request)
    {
        $this->validate($request, [
            'request_tag' => 'required'
        ]);

        $production_request = ProductionRequest::with("wholersaler", "staff")->where('request_tag', $request->get('request_tag'))->first();

        $production_request->update([
            'draft_stage' => 0
        ]);

        PushProductionRequestJob::dispatch($production_request);

        return redirect()->route('admin.production.index')->with(['success' => "Production request has been dispatched"]);
    }

    /**
     * @param ProductionRequest $production_request
     * @return mixed
     */
    public function pdfExport(ProductionRequest $production_request)
    {
        $production_items = $production_request->items()->join('inventory', 'inventory_id', '=', 'inventory.id')->orderBy('production_request_items.item_delivery_date', 'DESC')->get();

        $request_tag = $production_request->request_tag;

        $sales_person = Staff::find($production_request->staff_id);

        $date_difference = Carbon::parse(Carbon::now())->diffInDays($production_request->delivery_date);

        if ($date_difference === 0) {
            $date_difference_label = 'Today';
        } elseif ($date_difference === 1) {
            $date_difference_label = 'Tomorrow';
        } else {
            $date_difference_label = $date_difference . ' days from now';
        }

        $pdf = PDF::loadView('exports.pdf.production_request', compact('production_items', 'date_difference_label', 'sales_person', 'production_request', 'request_tag'));

        $export_file_label = 'Production Request - ' . $request_tag . ' - ' . env('APP_NAME');

        return $pdf->download($export_file_label . '.pdf');
    }


    public function add_item(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            'request_tag' => 'required',
            'inventory_id' => 'required',
        ]);

        $request_tag = $request->request_tag;
        $production_request_id = ProductionRequest::id_by_tag($request_tag);

        $existing_production_request = ProductionRequest::find_by_tag($request_tag);
        $existing_production_request->items()->delete();


        for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
            $quantity = $request->inventory_quantity[$i];

            $array = [];

            foreach (json_decode($request->get('attributes')[$i]) as $key => $value) {

                array_push($array, [$value->parent => $value->child]);
            }
            $item_attributes_json = json_encode($array, true);
            $raw_delivery_date = $request->item_delivery_date[$i];
            $inventory_id = $request->inventory_id[$i];
            $priority = $request->priority_id[$i] ?? ProductionRequest::find_by_tag($request_tag)->priority;

            $manufacturer_id = InventoryManufacturer::get_item_manufacturer($inventory_id);

            if (is_null($manufacturer_id)) {
                $find_inventory = Inventory::find($inventory_id);
                return redirect()->back()->withErrors("Inventory with SKU : " . $find_inventory->sku . " Has No Manufacturers Configured To It");
            } else {
                ProductionRequestItem::create([
                    'inventory_id' => $inventory_id,
                    'production_request_id' => $production_request_id,
                    'requested_quantity' => $quantity,
                    'item_notes' => $request->item_notes[$i],
                    'production_status' => 0,
                    'attributes' => $item_attributes_json,
                    'manufacturer_id' => $manufacturer_id,
                    'item_delivery_date' => $raw_delivery_date,
                    'item_priority' => $priority,
                ]);
            }
        }

        $existing_production_request->draft_stage = 0;
        $existing_production_request->update();

        return redirect()->route('admin.production.index')->with(['success' => "Successfully Finished Production Request"]);
    }


    public function item_attributes_html(Request $request)
    {

        $this->validate($request, [
            'item_id' => 'required'
        ]);

        $item_id = $request->get('item_id');

        $item_attributes = InventoryItemAttribute::attributes($item_id);

        return view('admin.production.partials.item_attributes', compact('item_attributes'));
    }


    public static function request_tag($len = 8)
    {

        $shuffled_string = substr(str_shuffle("23ab34567c1de12893456fghjk4mnpq5rs7t6v5456w9xyz5"), 0, $len);
        $string = strtoupper($shuffled_string);

        $check = ProductionRequest::tag_is_unique($string);

        if ($check === false) return self::request_tag();

        return $string;
    }


    public function creation_step_1($request_tag)
    {

        return $request_tag;
    }


    public function store_comment(Request $request)
    {

        /*TODO:: implement permission -- can comment own request*/

        $this->validate($request, [
            'comment' => 'required',
            'request_id' => 'required',
            'request_tag' => 'required',
        ]);


        ProductionRequestComment::create([
            'production_request_id' => $request->get('request_id'),
            'user_id' => auth()->user()->id,
            'comment' => $request->get('comment'),
            'status' => 1,
        ]);

        $url = URL::route('admin.productionrequest.view', ['production_request' => $request->get('request_tag')]) . '#comments';

        return Redirect::to($url)->with(['success' => "Your comment has been submitted successfully"]);
    }

    public function status_update(Request $request)
    {
        $this->validate($request, [
            'status_update' => 'required',
            'request_id' => 'required',
            'request_tag' => 'required',
        ]);

        if ($request->status_update >= 3) {
            if ($request->status_update == 3) {
                $product_status = 2;
            } else {
                $product_status = 3;
            }

            $item = ProductionRequestItem::where('production_request_id', $request->request_id)->where('production_status', '<', $product_status)->first();
            if (!is_null($item)) {
                if ($request->status_update == 3) {
                    $message = "Cannot Mark Production Request As Completed Until All Items are marked Done";
                } else {
                    $message = "Cannot Mark Production Request As Delivered Until All Items are Delivered Done";
                }
                return redirect()->route('admin.productionrequest.view', ['production_request' => $request->get('request_tag')])->withErrors([$message]);
            }
        }

        $production_request = ProductionRequest::find($request->get('request_id'));

        $staff = User::find($production_request->staff_id);
        // dd($staff, $production_request);

        if ($request->status_update <= $production_request->status) {
            return redirect()->route('admin.productionrequest.view', ['production_request' => $request->get('request_tag')])->withErrors(["Cannot Update Production Request Status,Production Request Has Aleady Passed That State"]);
        } else {
            $production_request->update(['status' => $request->get('status_update')]);

            $process_status_update = (new ProductionRequestStatusUpdateJob($staff, $production_request))->delay(Carbon::now()->addSeconds(60));
            dispatch($process_status_update);
        }



        if ($production_request->status == 3 && $production_request->if_stock_update == 1) {
            $items = $production_request->items;
            foreach ($items as $key => $value) {
                $inventory = Inventory::find($value->inventory_id);
                $update_quantity = $inventory->quantity + $value->requested_quantity;
                $inventory->quantity = $update_quantity;
                $inventory->update();
            }
        }

        return redirect()->route('admin.productionrequest.view', ['production_request' => $request->get('request_tag')])->with(['success' => "Status has been updated successfully."]);
    }

    public static function can_delete_comment($comment_id)
    {

        $comment_owner = ProductionRequestComment::comment_owner($comment_id);

        if ((user('id') == $comment_owner) || (user('account_type') == 7)) return true;

        return false;
    }

    public function delete_comment(Request $request)
    {

        $this->validate($request, [
            'comment_id' => 'required',
            'request_id' => 'required',
            'request_tag' => 'required',
        ]);

        $comment_id = $request->get('comment_id');

        if (self::can_delete_comment($comment_id)) {

            ProductionRequestComment::find($comment_id)->delete();

            flash_message('success', "Your comment has been deleted");

            /*Uses URL to enable redirect to comments_tab */
            $url = URL::route('manufacturer.request.view', ['production_request' => $request->get('request_tag')]) . '#comments_tab';

            return Redirect::to($url);
        }

        flash_message('danger', "Not Permitted to delete comment");

        return \redirect()->back();
    }


    public function deleteProcess(ProductionRequest $productionRequest, Request $request)
    {
        $this->can('delete_production_requests');

        $draft_stage = $productionRequest->draft_stage;

        $productionRequest->delete();

        flash_message('info', "Production Request Deleted");

        if ($draft_stage) return redirect()->route('admin.production.drafts');

        return redirect()->route('admin.production.index');
    }

    public function request_view(ProductionRequest $production_request)
    {

        // $items_producible = $this->producible_inventory(user()->account_id);

        /*TODO:: refactor via migration -- primary id to item_id*/
        $items_query = $production_request->items()->select(
            'inventory.id as inventory_id',
            'production_status',
            'production_request_items.id as item_id',
            'name',
            'sku',
            'item_notes',
            'quantity',
            'item_delivery_date',
            'requested_quantity',
            'item_priority',
            'attributes'
        )
            ->join('inventory', 'inventory_id', '=', 'inventory.id')
            // ->whereIn('inventory_id', $items_producible)
            ->orderBy('production_request_items.item_delivery_date', 'DESC');


        $items = $items_query->get();

        $pending_items_count = $items_query->where('production_request_items.production_request_id', $production_request->id)->whereIn('production_status', [0, 1])->count();

        $sales_person = User::find($production_request->staff_id);

        $comments = $production_request->comments()->with('user')->get();

        $comment_count = count($comments);

        $date_difference = Carbon::parse(Carbon::now())->diffInDays($production_request->delivery_date);

        return view('admin.production.production_request', compact('date_difference', 'pending_items_count', 'comments', 'production_request', 'items', 'sales_person', 'comments', 'comment_count'));
    }
    public function staff_ajax_specific(Request $request)
    {

        if ($request->ajax()) {

            $staff_wholesaler = StaffWholesaler::with("staff")->where('wholesaler_id', $request->id)->get();

            if ($staff_wholesaler->count() == 0) {
                $html = '<option value="">No Staff Assigned to Wholesaler</option>';
            } else {
                $html = '<option value="">Select Staff</option>';

                foreach ($staff_wholesaler as $item) {
                    $html .= '<option value="' . $item->user_id . '">' . $item->staff->name . '</option>';
                }
            }
            return response()->json(['html' => $html]);
        }
    }

    public function request_item_update(Request $request)
    {

        $this->validate($request, [
            'item_id' => 'required',
            'status' => 'required'
        ]);

        $item = ProductionRequestItem::find($request->get('item_id'));

        if ($request->status > $item->production_status) {
            $item->update([
                'production_status' => $request->get('status')
            ]);
            return response()->json(['status' => 'success', 'ifupdated' => true], 200);
        } else {
            return response()->json(['status' => 'success', 'ifupdated' => false], 200);
        }
    }

    public function update_comment(Request $request)
    {
        $comment = ProductionRequestComment::where('comment_id', $request->comment_id)->first();
        $comment->comment = $request->comment;
        $comment->update();
        return redirect()->back()->with(['success' => 'Comment Has Been Updated']);
    }
}
