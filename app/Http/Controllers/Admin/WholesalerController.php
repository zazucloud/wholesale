<?php

namespace App\Http\Controllers\Admin;

use App\Events\CreateWholesalerAccountMailEvent;
use App\Exports\DiscountsExport;
use App\Http\Controllers\BaseTemplateController;
use App\Http\Controllers\CacheController;
use App\Http\Controllers\Controller;
use App\Http\Middleware\AdminMiddleware;
use App\Imports\DiscountImport;
use App\Jobs\CreateWholesalerJob;
use App\Mail\CreateWholesalerAccountMail;
use App\Models\Bank;
use App\Models\County;
use App\Models\InventoryCategory;
use App\Models\Staff;
use App\Models\StaffWholesaler;
use App\Models\User;
use App\Models\Wholesale\Wholesaler;
use App\Models\Wholesale\WholesalerCategoryDiscount;
use App\Models\Wholesale\WholesalerCategoryVisibility;
use App\Models\Wholesale\WholesalerDirector;
use App\Models\Wholesale\WholesalerItemDiscount;
use App\Models\Wholesale\WholesalerLocation;
use App\Models\Wholesale\WholesalerUser;
use App\Models\WholesalerContact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use stdClass;
use PHPExcel_Style_Protection;


class WholesalerController extends BaseTemplateController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:wholesaler-list', ['only' => ['index']]);
        $this->middleware('permission:wholesaler-create', ['only' => ['create', 'store', 'create_account', 'location_insert', 'store_director', 'account_store']]);
        $this->middleware('permission:wholesaler-edit', ['only' => ['account_update', 'staff_assignment_edit', 'catalog_visibility_update', 'discount_control_update', 'custom_discount_update', 'location_update', 'update_director', 'edit_basic_info', 'update_basic_info']]);
        $this->middleware('permission:wholesaler-delete', ['only' => ['delete_director']]);
        $this->middleware('permission:wholesaler-export', ['only' => ['custom_discount_download']]);
        $this->middleware('permission:wholesaler-product_discount', ['only' => ['custom_discount']]);
        $this->middleware('permission:wholesaler-category_discount', ['only' => ['discount_control']]);
    }


    public function index(Request $request)
    {
        $search = $request->search;
        $wholesalers = Wholesaler::with('county')->when($search, function ($query) use ($search) {
            return  $query->where('business_name', 'LIKE', '%' . $search . '%')
                ->orwhere('registration_number', 'LIKE', '%' . $search . '%')
                ->orwhere('email', 'LIKE', '%' . $search . '%')
                ->orwhere('telephone', 'LIKE', '%' . $search . '%')
                ->orwhere('physical_address', 'LIKE', '%' . $search . '%')
                ->orwhere('pin', 'LIKE', '%' . $search . '%');
        })
            ->latest()
            ->paginate(30);

        return view('admin.wholesalers.index', compact('wholesalers'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function create()
    {

        $categories = InventoryCategory::active()->get();

        $counties = County::all()->pluck('name', 'id');

        $parent_categories = InventoryCategory::with('childinventory')->parent()->active()->get();


        return view('admin.wholesalers.create', compact('categories', 'parent_categories', 'counties'));
    }

    public function ship()
    {
        //test email sending

        //Mail::to('joash_gomba@yaoo.com')->send(new TestAmazonSes('WORKING'));

        $mail_data = new StdClass();
        $mail_data->name = 'TOPMFG';
        $mail_data->username = 'test@mail.com';
        $mail_data->password = 'test';
        $mail_data->created_by = 'Joash Gomba';
        Mail::to('joash_gomba@yahoo.com')->send(new CreateWholesalerAccountMail($mail_data));
    }


    public function create_full()
    {
        $this->can('create_wholesaler_accounts');
        $banks = Bank::active()->pluck('name', 'id');
        $account_managers = Staff::account_managers()->pluck('name', 'id');

        $staff = array_merge([0 => 'Select Sales or Marketing Staff'], $account_managers->toArray());

        return view('admin.wholesalers.create-full', compact('banks', 'staff'));
    }


    public function create_account(Request $request)
    {
        // dd($request->all());

        $this->validate(
            $request,
            [
                // 'business_name' => 'required|unique:wholesalers,business_name',
                'email_address' => 'required|email|unique:wholesalers,email|unique:users,email',
                'name' => 'required|unique:wholesalers,business_name',
                'county_id' => 'required',
                'ownership_type' => 'required',
                'registration_number' => 'required|unique:wholesalers,registration_number',
                'telephone' => "phone:KE"
            ],
            [
                'telephone.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );

        // dd(str_slug($request['registration_number']));

        $email = $request->get('email_address');
        $name = $request->get('name');
        $categories = $request->get('categories', []);

        $plain_password = passwordGenerator(9);
        $db_password = bcrypt($plain_password);

        $wholesaler = Wholesaler::create([
            'business_name' => $name,
            'slug' => str_slug($request['registration_number']),
            'postal_address' => $request['postal_address'],
            'county_id' => $request['county_id'],
            'physical_address' => $request['physical_address'],
            'telephone' => $request['telephone'],
            'email' => $request['email_address'],
            'pin' => $request['pin'],
            'contact_name' => $request['contact_name'],
            'contact_position' => $request['contact_person_position'],
            'ownership_type' => $request['ownership_type'],
            'registration_number' => $request['registration_number'],
            'registration_date' => $request['registration_date'],
            'credit_mode' => $request['credit_mode'],
            'status' => "1",
        ]);

        $wholesaler_id = $wholesaler->id;

        WholesalerUser::create_account($name, $email, $db_password,  $wholesaler_id);


        foreach ($categories as $category) {
            WholesalerCategoryVisibility::create([
                'wholesaler_id' => $wholesaler_id,
                'category_id' => $category
            ]);
        }
        $mail_data = new StdClass();
        $mail_data->name = $request['name'];
        $mail_data->username = $email;
        $mail_data->password = $plain_password;
        $mail_data->created_by = \user()->name;

        //store contact details to contact table
        if($request->contactname){
            for ($i = 0; $i < sizeof($request->contactname); $i++) {
                # code...
                $contact = new WholesalerContact();

                $contact->contactname = $request->contactname[$i];
                $contact->phone = $request->phone[$i];
                $contact->email = $request->email[$i];
                $contact->wholesaler_id = $wholesaler_id;
                $contact->save();
            }
        }


        // CreateWholesalerJob::dispatch($mail_data);

        activity('wholesaler_account_creation')->performedOn($wholesaler)->withProperties(['ip' => ip()])->log("Created Wholesaler Account for " . $name . ' with login username: ' . $email)->causedBy(user());

        // CacheController::wholesalers();

        return redirect()->route('admin.wholesalers.index')->with(['success' => "Success! Wholesaler Account Created Successfully"]);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'business_name' => 'required|unique:wholesalers,business_name',
            'postal_address' => 'required',
            'telephone' => 'required',
            'email' => 'required|email',
            'username' => 'required|email|unique:users,email'
        ]);

        $inputs = Input::all();

        CreateWholesalerJob::dispatch($inputs);

        return redirect()->route('admin.wholesalers.index')->with(['success' => "Success! Wholesaler Account Created Successfully"]);
    }

    public function show(Wholesaler $wholesaler)
    {

        /*TODO:: can view wholesaler for this method*/

        $lastorder = $wholesaler->orders()->latest()->first();
        $lastordervalue = ((is_null($lastorder)) ? 0 : $lastorder->total);
        $last_three_months_orders = $wholesaler->orders()->where('created_at', '>=', Carbon::now()->subDays(90))->get()->count();

        $directors = $wholesaler->wholesalerDirectors;
        $locations = $wholesaler->locations;
        $activity_log = optional(User::find($wholesaler->user_id))->activity_log();

        return view('admin.wholesalers.home', compact('wholesaler', 'directors', 'locations', 'activity_log', 'lastordervalue', 'last_three_months_orders'));
    }

    public function staff_assignment(Wholesaler $wholesaler)
    {
        $wholesaler_staff = $wholesaler->staff()->paginate(10);
        return view('admin.wholesalers.staff-assignment', compact('wholesaler', 'wholesaler_staff'));
    }


    public function staff_assignment_edit(Wholesaler $wholesaler)
    {
        $staff = User::all();

        $wholesaler_staff = $wholesaler->staff()->get()->pluck('id')->toArray();

        return view('admin.wholesalers.staff-assignment-edit', compact('staff', 'wholesaler', 'wholesaler_staff'));
    }


    public function staff_assignment_process(Wholesaler $wholesaler, Request $request)
    {
        $selected_staff = $request->get('staff', []);

        $data = [];

        foreach ($selected_staff as $item) {
            $data[$item] = ['user_id' => $item, 'wholesaler_id' => $wholesaler->id];
        }

        $wholesaler->staff()->sync($data, true);

        return redirect()->route('wholesaler.staff_assignment', $wholesaler->slug)->with(['info' => 'Staff assignment updated successfully']);
    }


    public function activity()
    {
    }

    public function catalog_visibility(Wholesaler $wholesaler)
    {
        $inventory_category = new InventoryCategory();
        $categories = $inventory_category->all()->pluck('name')->toArray();
        $parent_categories = $inventory_category::with("childinventory")->parent()->active()->get();
        $visible_categories = $wholesaler->visible_categories()->pluck('category_id')->toArray();
        $invisible_categories = array_diff($categories, $visible_categories);

        return view('admin.wholesalers.catalogue_visibility', compact('wholesaler', 'parent_categories', 'visible_categories', 'invisible_categories', 'categories'));
    }

    public function catalog_visibility_update(Request $request)
    {

        $this->validate($request, [
            'wholesaler_id' => 'required',
            'wholesaler_slug' => 'required'
        ]);
        $categories = $request->get('categories', []);

        $wholesaler = Wholesaler::findOrFail($request->get('wholesaler_id'));

        $wholesaler->visible_categories()->forceDelete();

        foreach ($categories as $category)
            WholesalerCategoryVisibility::create([
                'wholesaler_id' => $request->get('wholesaler_id'),
                'category_id' => $category
            ]);

        activity('wholesaler_profile_update')->performedOn($wholesaler)->withProperties(['ip' => ip()])->log("Updated Catalog Visibility")->causedBy(user());

        return redirect()->route('wholesaler.catalog_visibility', $request->get('wholesaler_slug'))->with(['success' => 'Catalog Visibility has been updated']);
    }

    public function discount_control(Wholesaler $wholesaler)
    {
        $parent_categories = InventoryCategory::whereHas('wholesalers', function ($query) use ($wholesaler) {
            return $query->where('wholesaler_id', $wholesaler->id);
        })
            ->with(["discount" => function ($query) use ($wholesaler) {
                return $query->where('wholesaler_id', $wholesaler->id);
            }])
            ->with(["childinventory" => function ($q) use ($wholesaler) {
                return $q->with(["discount" => function ($query) use ($wholesaler) {
                    return $query->where('wholesaler_id', $wholesaler->id);
                }]);
            }])
            ->parent()
            ->active()
            ->get();

        $given_discounts = $wholesaler->given_discounts;

        return view('admin.wholesalers.discount_control', compact('wholesaler', 'parent_categories', 'given_discounts'));
    }

    public function discount_control_update(Request $request)
    {


        $this->validate($request, [
            'wholesaler_id' => 'required',
            'wholesaler_slug' => 'required'
        ]);

        $discount_rates = $request->get('discount_rates', []);

        $wholesaler = Wholesaler::findOrFail($request->get('wholesaler_id'));
        $wholesaler->price_type = 3;
        $wholesaler->update();

        $wholesaler->discounts()->forceDelete();
        $wholesaler->itemDiscounts()->forceDelete();

        foreach ($discount_rates as $category_id => $discount_rate) {
            WholesalerCategoryDiscount::create([
                'wholesaler_id' => $request->get('wholesaler_id'),
                'category_id' => $category_id,
                'discount_rate' => $discount_rate
            ]);

            $category_items = InventoryCategory::find($category_id)->items2;

            foreach ($category_items as $items) {
                WholesalerItemDiscount::create([
                    'wholesaler_id' => $request->get('wholesaler_id'),
                    'inventory_id' => $items->inventory_id,
                    'custom_discount' => $discount_rate
                ]);
            }
        }
        activity('wholesaler_profile_update')->performedOn($wholesaler)->withProperties(['ip' => ip()])->log("Updated Category Discounts")->causedBy(user());

        return redirect()->route('wholesaler.discount_control', $request->get('wholesaler_slug'))->with(['success' => 'Discount Control has been updated']);
    }


    public function location_insert(Wholesaler $wholesaler, Request $request)
    {
        $this->validate($request, [
            'label' => 'required|min:2',
            'locality' => 'required|min:4',
            'wholesaler_id' => 'required'
        ]);

        WholesalerLocation::create([
            'wholesaler_id' => $request->get('wholesaler_id'),
            'label' => $request->get('label'),
            'locality' => $request->get('locality'),
            'longitude' => $request->get('longitude', null),
            'latitude' => $request->get('latitude', null),
        ]);

        flash_message('success', 'Location (' . $request->get('label') . ') added successfully');

        return redirect()->route('admin.wholesalers.show', $wholesaler->slug);
    }


    public function location_update(Wholesaler $wholesaler, Request $request)
    {
        $this->validate($request, [
            'label' => 'required|min:2',
            'locality' => 'required|min:4',
            'wholesaler_id' => 'required',
            'location_id' => 'required'
        ]);

        WholesalerLocation::where('id', $request->get('location_id'))->where('wholesaler_id', $wholesaler->id)->first()->update([
            'label' => $request->get('label'),
            'locality' => $request->get('locality'),
            'longitude' => $request->get('longitude', null),
            'latitude' => $request->get('latitude', null)
        ]);


        flash_message('info', 'Location (' . $request->get('label') . ') updated successfully');

        return redirect()->route('admin.wholesalers.show', $wholesaler->slug);
    }

    public function financial_info(Request $request)
    {

        $wholesaler = Wholesaler::findOrFail($request->wholesaler_id);

        $wholesaler->bank_account_no = $request->input('bank_account_no');
        $wholesaler->financial_controller = $request->input('financial_controller');
        $wholesaler->financial_contact_person = $request->input('financial_contact_person');
        $wholesaler->financial_contact_phone = $request->input('financial_contact_phone');

        $wholesaler->update();

        flash_message('info', 'Financial Information updated successfully');

        return back();
    }

    public function store_director(Request $request)
    {

        $this->validate(
            $request,
            [
                'phone' => 'phone:KE',
            ],
            [
                'phone.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );
        WholesalerDirector::create($request->all());
        return redirect()->back()->with(["success", 'Director Information stored successfully']);
    }

    public function update_director(Request $request)
    {
        $this->validate(
            $request,
            [
                'phone' => 'phone:KE',
            ],
            [
                'phone.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );

        $director = WholesalerDirector::findOrFail($request->director_id);

        $director->name = $request->input('name');
        $director->email = $request->input('email');
        $director->phone = $request->input('phone');

        $director->update();

        return redirect()->back()->with(["success", 'Director Information updated successfully']);
    }

    public function delete_director(Request $request)
    {

        $director = WholesalerDirector::findOrFail($request->director_id);

        $director->delete();

        flash_message('info', 'Director Deleted successfully');

        return back();
    }

    public function orders(Request $request, Wholesaler $wholesaler)
    {
        $location = $request->location;
        $orders = $wholesaler
            ->orders()
            ->with('location')
            ->when($location, function ($query) use ($location) {
                return $query->where('delivery_location', $location);
            })
            ->ordered()
            ->paginate(5);

        return view('admin.wholesalers.orders', compact('wholesaler', 'orders'));
    }


    public function payments(Wholesaler $wholesaler)
    {

        $payments = $wholesaler->payments()->ordered()->paginate(10);

        return view('admin.wholesalers.payments', compact('wholesaler', 'payments'));
    }

    public function accounts(Wholesaler $wholesaler)
    {

        $accounts = $wholesaler->accounts;

        return view('admin.wholesalers.accounts', compact('wholesaler', 'accounts'));
    }

    public function account_manage_edit(Wholesaler $wholesaler, $email)
    {

        $account = WholesalerUser::where("wholesaler_id", $wholesaler->id)->where("email", $email)->first();
        if (!$this->is_valid_wholesaler_account($wholesaler, $account)) return redirect()->back();

        return view('admin.wholesalers.account_edit', compact('wholesaler', 'account'));
    }


    public function account_update(Wholesaler $wholesaler, Request $request)
    {

        $this->validate(
            $request,
            [
                'account' => 'required',
                'name' => 'required',
                'email' => 'required',
                'phone_number' => 'required|phone:KE',
                'status' => 'required'
            ],
            [
                'phone_number.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );

        $user = WholesalerUser::where('email', $request->get('account'))->first();

        if (!$this->is_valid_wholesaler_account($wholesaler, $user)) return redirect()->back();

        $new_email = $request->get('email');

        /*TODO:: email change notifications*/
        /* if ($new_email != $user->email) {

         }*/

        $user->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'status' => $request->get('status'),
            'phone_no' => $request->get('phone_number')
        ]);


        return redirect()->route('wholesalers.accounts', ['wholesaler' => $wholesaler])->with(['success' => "Account updated successfully - " . $user->email]);
    }


    public function account_create(Wholesaler $wholesaler)
    {

        return view('admin.wholesalers.account_create', compact('wholesaler'));
    }


    public function account_store(Wholesaler $wholesaler, Request $request)
    {

        $this->validate(
            $request,
            [
                // 'wholesaler' => 'required',
                'name' => 'required',
                'email' => 'required|unique:wholesaler_users',
                'phone' => 'required|phone:KE',
                'active' => 'required'
            ],
            [
                'phone.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );

        /*Convert to Event*/

        $plain_password = passwordGenerator(9);
        $db_password = bcrypt($plain_password);

        $user = WholesalerUser::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'status' => $request->get('active'),
            'password' => $db_password,
            'wholesaler_id' => $wholesaler->id,
            'phone_no' => $request->get('phone')
        ]);

        Log::info($plain_password);
        $mail_data = new StdClass();
        $mail_data->name = $wholesaler->business_name;
        $mail_data->username = $user->email;
        $mail_data->password = $plain_password;
        $mail_data->created_by = \user()->name;

        activity('wholesaler_account_creation')->performedOn($user)->withProperties(['ip' => ip()])->log("Created Wholesaler Account for " . $wholesaler->business_name . ' with login username: ' . $user->email)->causedBy(user());

        // CreateWholesalerJob::dispatch($mail_data);


        return back()->with(['success' => "Account created successfully - " . $user->email]);
    }


    public function is_valid_wholesaler_account(Wholesaler $wholesaler, WholesalerUser $account)
    {
        $wholesaler_accounts = $wholesaler->accounts()->get();
        return $wholesaler_accounts->contains('email', $account->email);
    }


    public function edit_basic_info(Wholesaler $wholesaler)
    {

        // dd($wholesaler->account_by_email('considinebergnaumandblanda@gmail.com')->first());

        $categories = InventoryCategory::active()->get();

        $parent_categories = InventoryCategory::parent()->active()->get();

        $wholesaler_categories_array = $wholesaler->categories->pluck('category_id')->toArray();

        $counties = County::all();

        return view('admin.wholesalers.edit.basic-details', compact('wholesaler', 'categories', 'counties', 'parent_categories', 'wholesaler_categories_array'));
    }

    public function update_basic_info(Wholesaler $wholesaler, Request $request)
    {

        $this->validate(
            $request,
            [
                'name' => 'required',
                'telephone' => 'required|phone:KE',
                'email_address' => 'required|email',
            ],
            [
                'telephone.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );

        $current_email = $wholesaler->email;

        $wholesaler->update([
            'business_name' => $request->name,
            'postal_address' => $request->postal_address,
            'ownership_type' => $request->ownership_type,
            'registration_number' => $request->registration_number,
            'physical_address' => $request->physical_address,
            'pin' => $request->pin,
            'contact_name' => $request->contact_person,
            'contact_position' => $request->contact_person_position,
            'telephone' => $request->telephone,
            'email' => $request->email_address,
            'county_id' => $request->county_id,
            'credit_mode' => $request->credit_mode,
        ]);

        if ($current_email != $request->email_address) {
            // dd($current_email);
            $account = $wholesaler->accounts()->where('email', $current_email)->first();

            if (is_null($account)) {
                WholesalerUser::create([
                    'email' => $request->email_address,
                    'name' => $request->name,
                    'wholesaler_id' => $wholesaler->id,
                    'password' => bcrypt($request->email_address),
                ]);
            } else {
                $account->email = $request->email_address;
                $account->update();
            }
        }


        activity('wholesaler_account_edit')->performedOn($wholesaler)->withProperties(['ip' => ip()])->log("Edited Wholesaler Account for " . $wholesaler->business_name)->causedBy(user());

        return redirect()->route('admin.wholesalers.show', $wholesaler->slug)->with(['success' => 'Details updated successfully']);
    }

    public function custom_discount(Wholesaler $wholesaler)
    {

        return view('admin.wholesalers.custom_discounts', compact('wholesaler'));
    }


    public function custom_discount_download(Wholesaler $wholesaler)
    {

        ini_set("max_execution_time", 99600);
        ini_set('memory_limit', '-1');
        $visible_categories = $wholesaler->visible_categories()->with('categories.items2.inventory')->get();

        if (count($visible_categories) == 0) {

            flash_message('danger', "Alert! The wholesaler is not allocated any categories");

            return redirect()->back()->withErrors("No Categories Are Available");
        } else {
            return Excel::download(new DiscountsExport($visible_categories, $wholesaler), $wholesaler->slug . '-CustomDiscounts-' . date('d-m-y h:i') . '.xlsx');
        }
    }

    public function custom_discount_update(Wholesaler $wholesaler, Request $request)
    {
        //ini_set("max_execution_time", 3600);


        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'cwd-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);

        //DB::connection()->disableQueryLog();

        WholesalerItemDiscount::where('wholesaler_id', $wholesaler->id)->forceDelete();

        //  Excel::filter('chunk')->selectSheetsByIndex(0)->load($url)->chunk(300, function ($data) {
        $wholesaler->price_type = 2;
        $wholesaler->update();

        Excel::import(new DiscountImport($wholesaler->id), $url);
        // });

        // CacheController::cache();

        return redirect()->route('wholesaler.custom_discount_update', $wholesaler->slug)->with(['success' => 'Pricing data has been updated - Changes will reflect in 5 minutes']);
    }

    public function location_view(Wholesaler $wholesaler, WholesalerLocation $wholesaler_location)
    {

        return view('admin.wholesalers.location_view', compact('wholesaler', 'wholesaler_location'));
    }

    public function destroy($id, Wholesaler $wholesaler)
    {
        $wholesaler = Wholesaler::find($wholesaler->id);
        $wholesaler->delete();

        redirect()->route('admin.wholesalers.index')->with('success', 'Wholesaler deleted successfully');
    }

    public function deleteProcess(Wholesaler $wholesaler, Request $request)
    {

        $this->validate($request, [
            'password' => 'required'
        ]);

        $db_password = user()->password;

        if (Hash::check($request->password, $db_password)) {

            User::where('account_type', 1)->where('account_id', $wholesaler->id)->delete();
            StaffWholesaler::where('wholesaler_id', $wholesaler->id)->delete();
            $wholesaler->delete();

            flash_message('info', "Wholesaler Deleted Successfully!");
            return view('admin.wholesalers.index');
        }

        flash_message('info', "Wrong Password - Could not delete wholesaler!");

        return view('admin.wholesalers.delete', compact('wholesaler'));
    }

    public function location_ajax_specific(Request $request)
    {
        if (!$request->id) {
            $html = '<option value="">No Delivery Location Found.</option>';
        } else {
            $html = '<option value="">Select Delivery Location</option>';
            $locations = Wholesaler::find($request->id)->locations;
            if ($locations->count() == 0) {
                $html = '<option value="">No Delivery Location Found.</option>';
            } else {


                if ($request->selected) {
                    foreach ($locations as $item) {
                        $html .= '<option value="' . $item->id . '" ' . (($request->selected == $item->id) ? 'selected' : '') . '>' . $item->label . '</option>';
                    }
                } else {
                    foreach ($locations as $item) {
                        $html .= '<option value="' . $item->id . '">' . $item->label . '</option>';
                    }
                }
            }
        }

        return response()->json(['html' => $html]);
    }

    public function updateCreditInfo(Request $request)
    {
        $wholesaler = Wholesaler::find($request->wholesaler_id);
        $wholesaler->credit_amount = $request->credit_amount;
        $wholesaler->update();

        return redirect()->route('admin.wholesalers.show', ['wholesaler' => $wholesaler->slug])->with(['success' => "Credit Information Successfully Updated"]);
    }

    public function wholesalers_ajax_dropdown(Request $request)
    {

        if ($request->ajax()) {
            $page = $request->page;
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;
            if (is_null($request->term)) {
                $clients = Wholesaler::select('business_name', 'id')->orderBy('business_name', 'ASC')->skip($offset)->take($resultCount)->get();
            } else {
                $clients = Wholesaler::select('business_name', 'id')
                    ->where(function ($query) use ($request) {
                        return $query->where('business_name', 'LIKE',  '%' . $request->term . '%');
                    })->orderBy('business_name', 'ASC')->skip($offset)->take($resultCount)->get();
            }

            $count = Wholesaler::count();
            $endCount = $offset + $resultCount;
            $morePages = $endCount < $count;


            $results = array(
                "results" => $clients,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }
    }
}
