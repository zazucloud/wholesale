<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User as ModelsUser;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseTemplateController;

class SettingsController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:users-view', ['only' => ['getusers']]);
        $this->middleware('permission:roles-view', ['only' => ['getroles']]);
    }

    public function fetchUsers(Request $request)
    {
        $data = ModelsUser::orderBy('id', 'DESC')->paginate(6);
        return view('users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function fetchRoles(Request $request)
    {
        $roles = Role::orderBy('id', 'DESC')->paginate(6);
        return view('roles.index', compact('roles'))

            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
}
