<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Inquiry;
use App\Models\Retailer;
use App\Models\Template;
use App\Models\Inventory;
use App\Models\Quotation;
use App\Jobs\QuotationJob;
use App\Jobs\CreateOrderJob;
use App\Models\OrderComment;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Models\ZoneDiscounts;
use App\Models\Wholesale\Wholesaler;
use App\Jobs\OrderProductionRequestJob;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\Http\Controllers\BaseTemplateController;
use App\Models\Company;
use App\Models\Wholesale\WholesalerCategoryDiscount;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

use function GuzzleHttp\Promise\all;

class InquiryController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:quotation-download|quotation-download|quotation-download|quotation-download', ['only' => ['index']]);
    }

    public function index(Request $request)
    {
        $inquiries = Inquiry::latest()->with('quotations.inventory')->paginate(30);
        $templates =  Template::all();
        $date = $request->date;
        $contactperson = $request->contactperson;
        $retailers = Retailer::with('inquiries')->orderBy('business_name', 'ASC')->pluck('business_name', 'id')->all();
        $wholesaler = Wholesaler::with('inquiries')->orderBy('business_name', 'ASC')->pluck('business_name', 'id')->all();

        // $clients = $retailers + $wholesaler;

        // dd($wholesaler);


        return view('inquiry.index', compact('inquiries', 'templates', 'retailers', 'wholesaler', 'date', 'contactperson'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $wholesalers = Wholesaler::orderBy('business_name', 'ASC')->get()->pluck('business_name', 'id');
        $retailers = Retailer::orderBy('business_name', 'ASC')->get()->pluck('business_name', 'id');
        $templates =  Template::all();
        return view('inquiry.create', compact('wholesalers', 'templates', 'retailers'));
    }

    public function edit($id)
    {
        $wholesalers = Wholesaler::all();
        $templates =  Template::all()->pluck('name', 'id');
        $inquiry = Inquiry::with('quotations.inventory')->find($id);
        $retailers = Retailer::all();

        return view('inquiry.edit', compact('inquiry', 'wholesalers', 'templates', 'retailers'));
    }

    public function store(Request $request)
    {


        $validation_array = [];
        if ($request->type == "wholesaler") {
            $validation_array = [
                'wholesaler' => 'required',
                'inventory_id' => 'required',
            ];
            $wholesaler_id = $request->wholesaler;
            // $template_id = $request->wholesaler_template_id;
        } else {
            $validation_array = [
                'retailers' => 'required',
                'inventory_id' => 'required',
            ];
            $retailer_id = $request->retailers;
            // $template_id = $request->retailer_template_id;
        }

        $this->validate(
            $request,
            $validation_array,
            [
                'inventory_id.required' => 'Select Atleast One Item'
            ]
        );
        $company = Company::latest()->first();
        // dd($company->vat);

        $inquiry = new Inquiry();

        if ($request->type == "retailer") {
            $inquiry->clientable_id = $retailer_id;
            $inquiry->clientable_type = 'App\Models\Retailer';
        } elseif ($request->type == "wholesaler") {
            $inquiry->clientable_id = $wholesaler_id;
            $inquiry->clientable_type = 'App\Models\Wholesale\Wholesaler';
        }

        $inquiry->template_id = $request->input('template_id');
        $inquiry->created_by = auth()->user()->id;
        $inquiry->customer_reference = $request->input('customer_reference');
        $inquiry->note = $request->input('note');
        $inquiry->validity = $request->input('validity');
        $inquiry->terms = $request->input('terms');
        $inquiry->delivery = $request->input('delivery');

        $inquiry->save();

        if ($request->type == "wholesaler") {
            $wholesaler = Wholesaler::with('county.zoneAllocation')->find($wholesaler_id);

            for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                if ($wholesaler->price_type == 1 && !is_null($wholesaler->county_id)) {

                    $zone_price_discount = ZoneDiscounts::where('zone_id', $wholesaler->county->zoneAllocation->zone_id)->where('inventory_id', $request->inventory_id[$i])->first();
                    if (is_null($zone_price_discount)) {
                        $price = WholesalerCategoryDiscount::price($wholesaler_id, $request->inventory_id[$i]);
                    } else {
                        $price = $zone_price_discount->custom_discount;
                    }
                } else {
                    $price = WholesalerCategoryDiscount::price($wholesaler_id, $request->inventory_id[$i]);
                }
                $discount_percentage = percentage_discount_given($price, $request->inventory_price[$i]);

                $inquiry->quotations()->create([
                    'inventory_id' => $request->inventory_id[$i],
                    'quantity' => $request->inventory_quantity[$i],
                    'total_price' => $request->inventory_price[$i] * $request->inventory_quantity[$i],
                    'price' => $request->inventory_price[$i],
                    'disc_price' => $price,
                    'total_disc_price' => $price * $request->inventory_quantity[$i],
                    'discount_percent' => $discount_percentage,
                    'vat_percent' => !is_null($company) ? $company->vat : 0,
                    'note' => $request->item_notes[$i]
                ]);



                $item_total[] =  create_totals_plus_vat($company, $price, $request->inventory_quantity[$i]);

                $total = array_sum($item_total);
            }
        } else {
            for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                $inquiry->quotations()->create([
                    'inventory_id' => $request->inventory_id[$i],
                    'quantity' => $request->inventory_quantity[$i],
                    'price' => $request->inventory_price[$i],
                    'disc_price' => $request->inventory_price[$i],
                    'total_price' => $request->inventory_price[$i] * $request->inventory_quantity[$i],
                    'total_disc_price' => $request->inventory_price[$i] * $request->inventory_quantity[$i],
                    'vat_percent' => !is_null($company) ? $company->vat : 0,
                    'note' => $request->item_notes[$i]
                ]);
                $item_total[] =  create_totals_plus_vat($company, $request->inventory_price[$i], $request->inventory_quantity[$i]);

                $total = array_sum($item_total);
            }
        }

        $inquiry->update([
            'total_price' => $total
        ]);

        return redirect()->route('admin.inquiry.index')->with(['success' => 'Successfully Created New Quotation']);
    }


    public function update(Request $request)
    {
        $validation_array = [];
        if ($request->type == "wholesaler") {
            $validation_array = [
                'wholesaler' => 'required',
                'inventory_id' => 'required',
            ];
            $wholesaler_id = $request->wholesaler;
            // $template_id = $request->wholesaler_template_id;
        } else {
            $validation_array = [
                'retailers' => 'required',
                'inventory_id' => 'required',
            ];
            $retailer_id = $request->retailers;
            // $template_id = $request->retailer_template_id;
        }

        $this->validate(
            $request,
            $validation_array,
            [
                'inventory_id.required' => 'Select Atleast One Item'
            ]
        );
        $company = Company::latest()->first();

        $inquiry = Inquiry::find($request->id);
        $inquiry->quotations()->delete();
        if ($request->type == "retailer") {
            $inquiry->clientable_id = $retailer_id;
            $inquiry->clientable_type = 'App\Models\Retailer';
        } elseif ($request->type == "wholesaler") {
            $inquiry->clientable_id = $wholesaler_id;
            $inquiry->clientable_type = 'App\Models\Wholesale\Wholesaler';
        }

        $inquiry->template_id = $request->input('template_id');
        $inquiry->customer_reference = $request->input('customer_reference');
        $inquiry->note = $request->input('note');
        $inquiry->validity = $request->input('validity');
        $inquiry->terms = $request->input('terms');
        $inquiry->delivery = $request->input('delivery');


        if ($request->type == "wholesaler") {
            $wholesaler = Wholesaler::with('county.zoneAllocation')->find($wholesaler_id);

            for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                if ($wholesaler->price_type == 1) {
                    $zone_price_discount = ZoneDiscounts::where('zone_id', $wholesaler->county->zoneAllocation->zone_id)->where('inventory_id', $request->inventory_id[$i])->first();


                    if (is_null($zone_price_discount)) {
                        $price = WholesalerCategoryDiscount::price($wholesaler_id, $request->inventory_id[$i]);
                    } else {
                        $price = $zone_price_discount->custom_discount;
                    }
                } else {
                    $price = WholesalerCategoryDiscount::price($wholesaler_id, $request->inventory_id[$i]);
                }
                $discount_percentage = percentage_discount_given($price, $request->inventory_price[$i]);
                $inquiry->quotations()->create([
                    'inventory_id' => $request->inventory_id[$i],
                    'quantity' => $request->inventory_quantity[$i],
                    'total_price' => $request->inventory_price[$i] * $request->inventory_quantity[$i],
                    'price' => $request->inventory_price[$i],
                    'disc_price' => $price,
                    'total_disc_price' => $price * $request->inventory_quantity[$i],
                    'discount_percent' => $discount_percentage,
                    'note' => $request->item_notes[$i],
                    'vat_percent' => !is_null($company) ? $company->vat : 0
                ]);

                $item_total[] =  create_totals_plus_vat($company, $price, $request->inventory_quantity[$i]);

                $total = array_sum($item_total);
            }
        } else {
            for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                $inquiry->quotations()->create([
                    'inventory_id' => $request->inventory_id[$i],
                    'quantity' => $request->inventory_quantity[$i],
                    'price' => $request->inventory_price[$i],
                    'disc_price' => $request->inventory_price[$i],
                    'total_price' => $request->inventory_price[$i] * $request->inventory_quantity[$i],
                    'total_disc_price' => $request->inventory_price[$i] * $request->inventory_quantity[$i],
                    'note' => $request->item_notes[$i],
                    'vat_percent' => !is_null($company) ? $company->vat : 0
                ]);

                $item_total[] =  create_totals_plus_vat($company, $request->inventory_price[$i], $request->inventory_quantity[$i]);

                $total = array_sum($item_total);
            }
        }

        $inquiry->total_price = $total;

        $inquiry->update();

        return redirect()->route('admin.inquiry.index')->with(['success' => 'Successfully Updated Quotation']);
    }

    public function show($id)
    {
        $inquiry = Inquiry::findOrFail($id);

        return view('inquiry.show', compact('inquiry'));
    }

    public function quotation_download(Inquiry $inquiry)
    {
        $footerHtml = view()->make('exports.pdf.footer', compact('inquiry'))->render();
        $headerHtml = view()->make('exports.pdf.header', compact('inquiry'))->render();

        $pdf = PDF::loadView('exports.pdf.quotation', compact('inquiry'))
            ->setOption('header-html', $headerHtml)
            ->setOption('footer-html', $footerHtml);

        $export_file_label = 'Quotation -' . $inquiry->inquiry_no . '-' . env('APP_NAME');

        return $pdf->download($export_file_label . '.pdf');
    }

    public function mail_quotation(Request $request)
    {

        $inquiry = Inquiry::findOrFail($request->inquiry_id);

        $inquiry->template_id = $request->template_id;
        $inquiry->mailed = 1;

        $inquiry->update();

        $customer_email = $request->customer_email;

        QuotationJob::dispatch($inquiry, $customer_email);

        return redirect()->route('admin.inquiry.index')->with(['success' => 'Quotation Sent Succesfully']);
    }

    public function expired_quotation(Request $request)
    {
        // dd($request->all());
        $this->validate(
            $request,
            [
                'reason' => 'required',
            ]
        );
        $inquiry = Inquiry::findOrFail($request->inquiry_id);
        $inquiry->reason = $request->input('reason');
        $inquiry->expired = 1;
        // dd($request->inquiry);
        $inquiry->update();

        return redirect()->route('admin.inquiry.index')->with(['success' => 'Quotation Set To Expired']);
    }

    public function quotation_transfer(Request $request)
    {
        $inquiry = Inquiry::find($request->inquiry_id);
        $client = $inquiry->clientable;
        $order_tag = self::order_tag();
        $company = Company::latest()->first();

        if ($inquiry->clientable_type == "App\Models\Wholesale\Wholesaler") {
            $wholesaler_location = Wholesaler::findOrFail($request->wholesaler_id);
        }

        if ($inquiry->clientable_type == "App\Models\Wholesale\Wholesaler" && $wholesaler_location->locations->isEmpty()) {

            return back()->with(['success' => 'Wholesaler does not have a delivery location please add delivery location first']);
        } else {

            $order_array = [
                'order_tag' => $order_tag,
                'delivery_location' => $request->locations,
                'delivery_date' => $request->delivery_date,
                'template_id' => $request->template_id,
                // 'total' => $request->total,
                'total' => $inquiry->quotations->sum('total_price'),
                'is_draft' => 0,
                'stage' => 1,
                'created_by' => auth()->user()->id
            ];



            if ($inquiry->clientable_type == "App\Models\Wholesale\Wholesaler") {
                array_merge($order_array, ['delivery_location' => $wholesaler_location->locations->first()->id]);
            }

            $order = $client->orders()->create($order_array);
            // dd($order);

            $item_total = [];
            $excess_items = [];
            $excess_items_for_raising_lpo = [];

            foreach ($inquiry->quotations as $quote) {
                $find_inventory = $quote->inventory;
                if ($inquiry->clientable_type == "App\Models\Wholesale\Wholesaler") {

                    $price = WholesalerCategoryDiscount::price($order->clientable_id, $find_inventory->id);
                    // dd($price);

                } else {
                    $price = $find_inventory->price;
                }
                $item_quantity = intval($quote->quantity);

                //  $discount_percentage = percentage_discount_given($price, Inventory::findOrFail($quote->inventory_id)->price);
                if (!is_null($company)) {
                    $vat_rate = $company->vat;
                } else {
                    $vat_rate = 0;
                }
                // Log::info($quote->discount_percent);
                OrderProduct::create([
                    'order_id' => $order->id,
                    'inventory_id' => $quote->inventory_id,
                    'quantity' => $item_quantity,
                    'price' => $quote->disc_price,
                    'vat_percent' => $vat_rate,
                    'discount_percent' => $quote->discount_percent,
                    'status' => 1,
                    'attributes' => null,
                    'original_price' => $quote->price,
                ]);

                // $vat_amount = vat_amount($price, $vat_rate);
                $item_total[] = $quote->disc_price * $item_quantity;

                if ($item_quantity > $find_inventory->quantity) {
                    if ($find_inventory->productionitem == 1) {
                        $difference = $item_quantity - intval($find_inventory->quantity);
                        $update_quantity = 0;
                        $level_array = [
                            'inventory_id' => $quote->inventory_id,
                            'leftover_quantity' => $difference
                        ];
                        array_push($excess_items, $level_array);
                    } else {
                        $difference = $item_quantity - intval($find_inventory->quantity);
                        $update_quantity = 0;
                        $level_array = [
                            'inventory_id' => $quote->inventory_id,
                            'leftover_quantity' => $difference,
                            'name' => $find_inventory->name,
                            'sku' => $find_inventory->sku,

                        ];
                        array_push($excess_items_for_raising_lpo, $level_array);
                    }
                } else {
                    $update_quantity = intval($find_inventory->quantity) - $item_quantity;
                }
                $find_inventory->quantity = $update_quantity;
                $find_inventory->update();
            }

            $inquiry->transfered = 1;
            $inquiry->update();

            $total = array_sum($item_total);

            $order->update([
                'total' => $total
            ]);

            if ($request->hasFile('attachment')) {
                $orderAttchment = $request->file('attachment');
                $extension = $orderAttchment->getClientOriginalExtension();

                if ($extension == "pdf") {
                    $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                    $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');
                } else {

                    $height = Image::make($request->attachment)->height();
                    $width = Image::make($request->attachment)->width();


                    if($height > 0 || $width > 0) {
                        $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                        $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');

                    }
                }
            }

            if ($request->has('comment')) {
                OrderComment::create_comment($request->comment, $order->id, auth()->user()->id);
            }

            if ($inquiry->clientable_type == "App\Models\Wholesale\Wholesaler") {
                $type = "wholesaler";
            } else {
                $type = "retailer";
            }
            // CreateOrderJob::dispatch($client, $order, $type);

            if (sizeof($excess_items) > 0 && $request->generate_production == "true") {

                OrderProductionRequestJob::dispatch($client, $excess_items, $order->delivery_date, $order->id, $inquiry->clientable_type, auth()->user());
            }

            $message = null;

            if (sizeof($excess_items_for_raising_lpo) > 0) {
                $lpo = true;
                $message = "LPO Needs To Be Raised For: <br>";

                foreach ($excess_items_for_raising_lpo as $key => $value) {
                    $message = $message . "<br>" . ($key + 1) . ". " . $value["name"] . " : " . $value["sku"] . " -> Quantity : " . $value["leftover_quantity"];
                }
            }

            if (!is_null($message)) {
                $order->lporequest = $message;
            }
            $order->update();

            return back()->with(['success' => 'Quotation Transfered To an Order']);
        }
    }

    public function retrieveItems($id)
    {
        $items = Inquiry::find($id)->items;
        return response()->json(['items' => $items], 200);
    }

    public static function order_tag($len = 8)
    {

        $shuffled_string = substr(str_shuffle("23ab34567c1de12893456fghjk4mnpq5rs7t6v5456w9xyz5"), 0, $len);
        $string = strtoupper($shuffled_string);

        $check = Order::order_tag_is_unique($string);

        if ($check === false) {
            return self::order_tag();
        }

        return $string;
    }

    public function search(Request $request)
    {


        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }

        $name = $request->inquiryno;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $templates =  Template::all();

        $retailers = Retailer::with('inquiries')->pluck('business_name', 'id')->all();
        $wholesaler = Wholesaler::with('inquiries')->pluck('business_name', 'id')->all();



        $inquiries = Inquiry::with('quotations.inventory')->where('clientable_type', $clientable_type)
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })->when($name, function ($query) use ($name) {
                return $query->where('inquiry_no', 'LIKE', '%' . $name . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })->paginate(30);



        return view('inquiry.index', compact('inquiries', 'templates', 'retailers', 'wholesaler', 'contactperson', 'date'))->with('i', ($request->input('page', 1) - 1) * 5);
    }
}
