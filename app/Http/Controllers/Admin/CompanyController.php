<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseTemplateController;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class CompanyController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:companyinfo-view|companyinfo-create|companyinfo-edit|companyinfo-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:companyinfo-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:companyinfo-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:companyinfo-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $company = Company::latest()->get();

        return view('admin.company.index', compact('company'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:companies,email',
            'phone' => 'required|unique:companies,phone',
            'company_logo' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input = $request->all();
        $company = Company::create($input);

        if ($request->hasFile('company_logo')) {

            $height = Image::make($request->company_logo)->height();
            $width = Image::make($request->company_logo)->width();

             if($height > 0 || $width > 0){
                $newFileName = Str::random(20) . '.' . $request->file('company_logo')->extension();
                $company->addMediaFromRequest('company_logo')->usingFileName($newFileName)->toMediaCollection('companylogos');

             }

            
        }


        return redirect()->route('companies.index')->with('success', 'Company created successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $company = Company::latest()->first();

        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|phone:KE',
            ],
            [
                'phone.phone' => 'Invalid Phone number format. Also Make Sure its a kenyan number',
            ]
        );
        $input = $request->all();

        if ($id == 0) {

            $company = Company::create($input);
        } else {
            $company = Company::find($id);
            $company->update($input);
        }

        if ($request->hasFile('company_logo')) {

            foreach ($company->getMedia('companylogos')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }
            $company->addMedia($request->company_logo)->toMediaCollection('companylogos');
        }

        return redirect()->back()->with('success', 'Company updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::find($id)->delete();
        return redirect()->route('companies.index')
            ->with('success', 'Company deleted successfully');
    }
}
