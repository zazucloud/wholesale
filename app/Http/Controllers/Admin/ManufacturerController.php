<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Manufacturers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseTemplateController;
use App\Jobs\CreateManufacturerJob;
use App\Models\InventoryCategory;
use App\Models\InventoryItemCategory;
use App\Models\InventoryManufacturer;
use App\Models\ManufacturerUser;
use App\Models\User;
use stdClass;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class ManufacturerController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:manufacturers-view|manufacturers-create|manufacturers-edit|manufacturers-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:manufacturers-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:manufacturers-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:manufacturers-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {

        $manufacturers = Manufacturers::latest()->paginate(30);

        return view('admin.manufacturers.index', compact('manufacturers'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function create()
    {
        return view('admin.manufacturers.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required|unique:manufacturers,name',
            'email' => 'required|unique:users,email',
            'phone' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg'
        ]);


        $initial_password = passwordGenerator();
        $set_password = bcrypt($initial_password);

        $manufacturer = Manufacturers::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'notes' => $request->get('notes'),
            'status' => 1
        ]);

        if ($request->hasFile('logo')) {

            $height = Image::make($request->logo)->height();
            $width = Image::make($request->logo)->width();


            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('logo')->extension();
                $manufacturer->addMediaFromRequest('logo')->usingFileName($newFileName)->toMediaCollection('manulogos');

            }

        }
        $user = ManufacturerUser::create_account($request->get('name'), $request->get('email'), $set_password, $manufacturer->id, $request->get('phone'), 1);


        $mail_data = new stdClass();
        $mail_data->name = $manufacturer->name;
        $mail_data->username = $user->email;
        $mail_data->password = $initial_password;
        $mail_data->created_by = auth()->user()->name;

        CreateManufacturerJob::dispatch($mail_data);

        /*TODO:: Refactor Separate Mail Job notifications to single for account details */
        // Mail::to($user->email)->queue(new NewAccountCreatedMail($mail_data));

        activity('wholesaler_account_creation')->withProperties(['ip' => ip()])->log("Created Manufacturer Account for " . $manufacturer->name . ' with login username: ' . $user->email)->causedBy(user());

        return redirect()->route('admin.manufacturers.index')->with(['success' => "Manufacturer Added Successfully - Account details have been emailed out"]);
    }

    public function update($id, Request $request)
    {
        $manufacturer = Manufacturers::find($id);

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        if ($request->hasFile('logo')) {
            $manufacturer->getFirstMedia('manulogos');
            foreach ($manufacturer->getMedia('manulogos')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }


            $height = Image::make($request->logo)->height();
            $width = Image::make($request->logo)->width();


            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('logo')->extension();
                $manufacturer->addMediaFromRequest('logo')->usingFileName($newFileName)->toMediaCollection('manulogos');

            }


        }

        $manufacturer->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'notes' => $request->get('notes'),
            'status' => 1
        ]);

        return redirect()->route('admin.manufacturers.index')->with(['success' => "Manufacturer updated Successfully"]);
    }

    public function production(Manufacturers $manufacturer)
    {
        $categories = InventoryCategory::active()->get();
        $active_items = InventoryManufacturer::select('inventory_id')->where('manufacturer_id', $manufacturer->id)->where('status', 1)->get()->pluck('inventory_id')->toArray();


        return view('admin.manufacturers.managelist', compact('categories', 'active_items', 'manufacturer'));
    }

    public function loadInventoryItems(Request $request)
    {
        if ($request->has("id")) {
            $category_items = InventoryItemCategory::with('inventory')->where('category_id', $request->id)->get();

            return response()->json(
                [
                    "items" => $category_items,
                ]
            );
        }
    }

    public function production_list(Manufacturers $manufacturer)
    {
        $production_list = InventoryManufacturer::where('manufacturer_id', $manufacturer->id)->get();

        $categories = InventoryCategory::active()->get();

        $active_manufacturer_production_items = InventoryManufacturer::select('inventory_id')->where('manufacturer_id', $manufacturer->id)->where('status', 1)->get()->pluck('inventory_id')->toArray();

        return view('admin.manufacturers.manage_inventory_production_list', compact('categories', 'active_manufacturer_production_items', 'manufacturer', 'production_list'));
    }

    public function update_production_list(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'manufacturer_id' => 'required',
            'category' => 'required',
            'manufacturer_name' => 'required'
        ]);

        $items = $request->get('item', []);

        $item_count = count($items);

        InventoryManufacturer::set($request->get('manufacturer_id'), $items, $request->category);

        return redirect()->route('admin.manufacturers.index')->with(['success' => "Production List for " . $request->get('manufacturer_name') . " has been updated - Production Item Count: $item_count"]);
    }

    public function search(Request $request)
    {

        $manufacturers = Manufacturers::where('name', 'LIKE', '%' . $request->search . '%')
            ->orwhere('email', 'LIKE', '%' . $request->search . '%')
            ->orwhere('phone', 'LIKE', '%' . $request->search . '%')
            ->paginate(30);

        return view('admin.manufacturers.index', compact('manufacturers'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function delete(Request $request, $id)
    {
        $manufacturer = Manufacturers::find($id);
        $manufacturer->delete();

        return redirect()->route('admin.manufacturers.index')->with('success', 'Manufacturer deleted successfully');
    }
}
