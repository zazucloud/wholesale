<?php

namespace App\Http\Controllers\Admin;

use stdClass;
use Mpdf\Tag\Dd;
use App\Models\Order;
use App\Jobs\MailOrder;
use App\Models\Company;
use App\Models\Retailer;
use App\Models\Template;
use App\Models\Inventory;
use App\Jobs\CancelOrderJob;
use App\Jobs\CreateOrderJob;
use App\Models\OrderComment;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Models\OrderTimeline;
use App\Models\ZoneDiscounts;
use App\Jobs\SendOrdersDnoteJob;
use App\Models\ProductionRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Wholesale\Wholesaler;
use App\Models\ProductionRequestItem;
use Illuminate\Support\Facades\Input;
use App\Jobs\OrderProductionRequestJob;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Wholesale\WholesalerLocation;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\Http\Controllers\BaseTemplateController;
use App\Jobs\OrderCommentUpdateJob;
use App\Models\Wholesale\WholesalerCategoryDiscount;
use PhpOffice\PhpSpreadsheet\Calculation\Financial\Securities\Price;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

use function GuzzleHttp\Promise\all;

class OrderController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:orders-list', ['only' => ['index', 'pendingOrders', 'approvedOrders', 'partiallyShippedOrders', 'fullyShippedOrders', 'paidShippedOrders']]);
        $this->middleware('permission:orders-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:orders-edit', ['only' => ['update_order_status']]);
        $this->middleware('permission:orders-delete', ['only' => ['cancel']]);
    }

    public function index(Request $request)
    {

        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $status = $request->status;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }

        $orders = Order::with("createdBy")
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->when($status, function ($query) use ($status) {
                return $query->where('stage', $status);
            })
            ->ordered()->draft(0)->paginate(30);
        $templates =  Template::all();

        return view('admin.orders.index')->with(['orders' => $orders, 'templates' => $templates])->with('i', ($request->input('page', 1) - 1) * 30);
    }

    public function pendingOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }
        $orders = Order::ofStage(1)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.pending')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function approvedOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }
        $orders = Order::ofStage(2)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.approved')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function processingOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }
        $orders = Order::ofStage(3)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.processing')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function processedOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }
        $orders = Order::ofStage(4)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.processed')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function partiallyShippedOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }

        $orders = Order::ofStage(5)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.partiallyshipped')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function fullyShippedOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }

        $orders = Order::ofStage(6)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.fullyshipped')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function paidShippedOrders(Request $request)
    {
        $search = $request->ordertag;
        $date = $request->date;
        $contactperson = $request->contactperson;
        $clientable_id = null;
        $clientable_type = null;

        if ($request->type == "wholesaler") {
            $clientable_id = $request->wholesaler;
            $clientable_type = 'App\Models\Wholesale\Wholesaler';
        } elseif ($request->type == "retailer") {
            $clientable_id = $request->retailer;
            $clientable_type = 'App\Models\Retailer';
        }
        $orders = Order::ofStage(7)
            ->when($clientable_type, function ($query) use ($clientable_type) {
                return $query->where('clientable_type', $clientable_type);
            })
            ->when($clientable_id, function ($query) use ($clientable_id) {
                return $query->where('clientable_id', $clientable_id);
            })
            ->when($search, function ($query) use ($search) {
                return $query->where('order_tag', 'LIKE', '%' . $search . '%');
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereDate('created_at', '=', $date);
            })
            ->when($contactperson, function ($query) use ($contactperson) {
                return $query->where('created_by', $contactperson);
            })
            ->ordered()->draft(0)->paginate(10);
        return view('admin.orders.paid')->with(['orders' => $orders])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $wholesalers = Wholesaler::orderBy('business_name', 'ASC')->select('id', 'business_name', 'registration_number')->get();
        $retailers = Retailer::orderBy('business_name', 'ASC')->get()->pluck('business_name', 'id');
        $templates =  Template::all();

        return view('admin.orders.create', compact('wholesalers', 'templates', 'retailers'));
    }


    public function store(Request $request)
    {
        // dd(auth()->user());

        $validation_array = [];
        if ($request->type == "wholesaler") {
            $validation_array = [
                'wholesaler' => 'required',
                'delivery_date' => 'required',
                'inventory_id' => 'required',
                'locations' => 'required',
                'generate_production' => 'required'
            ];
            $wholesaler_id = $request->wholesaler;
            $template_id = $request->wholesaler_template_id;
        } else {
            $validation_array = [
                'retailers' => 'required',
                'delivery_date' => 'required',
                'inventory_id' => 'required',
                'generate_production' => 'required'
            ];
            $wholesaler_id = $request->retailers;
            $template_id = $request->retailer_template_id;
        }

        $this->validate(
            $request,
            $validation_array,
            [
                'inventory_id.required' => 'Select Atleast One Item'
            ]
        );

        if ($request->generate_production == "false") {
            $check_excess = $this->checkExcess($request);
            if ($check_excess["hadExcessProduction"] == true) {
                return redirect()->route('orders.create')->withErrors(['Excess Quantity Detected,Please Remove Excess Quanity or Accept Production Request Creation When Popup Appears']);
            }
        }

        $lpo = false;

        try {
            DB::beginTransaction();


            $delivery_date = $request->delivery_date;

            $order_tag = self::order_tag();
            $company = Company::latest()->first();

            if ($request->type == "wholesaler") {
                $wholesaler = Wholesaler::with('county.zoneAllocation')->find($wholesaler_id);

                $order = $wholesaler->orders()->create([
                    'order_tag' => $order_tag,
                    'delivery_location' => $request->locations,
                    'delivery_date' => $delivery_date,
                    'template_id' => $template_id,
                    'total' => 0,
                    'is_draft' => 0,
                    'stage' => 1,
                    'created_by' => auth()->user()->id
                ]);

                $branch = WholesalerLocation::find($request->locations);
                $sum = $branch->counter + 1;
                $branch->counter = $sum;
                $branch->update();

                $item_total = [];
                $excess_items = [];
                $excess_items_for_raising_lpo = [];

                for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                    $find_inventory = Inventory::find($request->inventory_id[$i]);

                    if ($wholesaler->price_type == 1) {
                        if (!is_null($wholesaler->county_id)) {
                            $zone_price_discount = ZoneDiscounts::where('zone_id', $wholesaler->county->zoneAllocation->zone_id)->where('inventory_id', $request->inventory_id[$i])->first();
                        } else {
                            $zone_price_discount = null;
                        }

                        if (is_null($zone_price_discount)) {
                            $price = WholesalerCategoryDiscount::price($wholesaler_id, $request->inventory_id[$i]);
                        } else {
                            $price = $zone_price_discount->custom_discount;
                        }
                    } else {
                        $price = WholesalerCategoryDiscount::price($wholesaler_id, $request->inventory_id[$i]);
                    }


                    $item_quantity = intval($request->inventory_quantity[$i]);

                    if ($item_quantity < 0) {
                        return redirect()->route('orders.index')->withErrors('Please put correct quantities, Negative quantities are not Accepted');
                    }

                    $discount_percentage = percentage_discount_given($price, $find_inventory->price);


                    if (!is_null($company)) {
                        $vat_rate = $company->vat;
                    } else {
                        $vat_rate = 0;
                    }



                    OrderProduct::create([
                        'order_id' => $order->id,
                        'inventory_id' => $request->inventory_id[$i],
                        'quantity' => $item_quantity,
                        'price' => $price,
                        'vat_percent' => $vat_rate,
                        'discount_percent' => $discount_percentage,
                        'status' => 1,
                        'attributes' => null,
                        'note' => $request->item_notes[$i],
                        'original_price' => $find_inventory->price
                    ]);

                    $item_total[] = $price * $item_quantity;

                    if ($item_quantity > $find_inventory->quantity) {
                        if ($find_inventory->productionitem == 1) {
                            $difference = $item_quantity - intval($find_inventory->quantity);
                            $update_quantity = 0;
                            $level_array = [
                                'inventory_id' => $request->inventory_id[$i],
                                'leftover_quantity' => $difference
                            ];
                            array_push($excess_items, $level_array);
                        } else {
                            $difference = $item_quantity - intval($find_inventory->quantity);
                            $update_quantity = 0;
                            $level_array = [
                                'inventory_id' => $request->inventory_id[$i],
                                'leftover_quantity' => $difference,
                                'name' => $find_inventory->name,
                                'sku' => $find_inventory->sku,

                            ];
                            array_push($excess_items_for_raising_lpo, $level_array);
                        }
                    } else {
                        $update_quantity = intval($find_inventory->quantity) - $item_quantity;
                    }
                    $find_inventory->quantity = $update_quantity;
                    $find_inventory->update();
                }


                $total = array_sum($item_total);

                $order->update([
                    'total' => $total
                ]);

                if ($request->hasFile('attachment')) {

                    $orderAttchment = $request->file('attachment');
                    $extension = $orderAttchment->getClientOriginalExtension();

                    if ($extension == "pdf") {
                        $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                        $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');
                    } else {

                        $height = Image::make($request->attachment)->height();
                        $width = Image::make($request->attachment)->width();


                        if($height > 0 || $width > 0) {
                            $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                            $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');

                        }
                    }

                }

                // if ($request->has('comment')) {
                //     OrderComment::create_comment($request->comment, $order->id, auth()->user()->id);
                // }

                // CreateOrderJob::dispatch($wholesaler, $order, $request->type);

                // if (sizeof($excess_items) > 0 && $request->generate_production == "true") {
                //     OrderProductionRequestJob::dispatch($wholesaler, $excess_items, $delivery_date, $order->id, $order->clientable_type, auth()->user());
                // }

                $message = null;

                if (sizeof($excess_items_for_raising_lpo) > 0) {
                    $lpo = true;
                    $message = "LPO Needs To Be Raised For: <br>";

                    foreach ($excess_items_for_raising_lpo as $key => $value) {
                        $message = $message . "<br>" . ($key + 1) . ". " . $value["name"] . " : " . $value["sku"] . " -> Quantity : " . $value["leftover_quantity"];
                    }
                }

                if (!is_null($message)) {
                    $order->lporequest = $message;
                }
                $order->update();
            } else {
                $retailer = Retailer::find($wholesaler_id);

                $order = $retailer->orders()->create([
                    'order_tag' => $order_tag,
                    'delivery_date' => $delivery_date,
                    'template_id' => $template_id,
                    'total' => 0,
                    'is_draft' => 0,
                    'stage' => 1,
                    'created_by' => auth()->user()->id
                ]);
                $item_total = [];
                $excess_items = [];
                $excess_items_for_raising_lpo = [];

                for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
                    $find_inventory = Inventory::find($request->inventory_id[$i]);
                    $price = $find_inventory->price;
                    $item_quantity = intval($request->inventory_quantity[$i]);

                    $discount_percentage = percentage_discount_given($price, Inventory::findOrFail($request->inventory_id[$i])->price);

                    if (!is_null($company)) {
                        $vat_rate = $company->vat;
                    } else {
                        $vat_rate = 0;
                    }

                    OrderProduct::create([
                        'order_id' => $order->id,
                        'inventory_id' => $request->inventory_id[$i],
                        'quantity' => $item_quantity,
                        'price' => $price,
                        'vat_percent' => $vat_rate,
                        'discount_percent' => $discount_percentage,
                        'status' => 1,
                        'attributes' => null,
                        'note' => $request->item_notes[$i],
                        'original_price' => $find_inventory->price
                    ]);


                    if (!is_null($company)) {
                        $vat_rate = $company->vat;
                    } else {
                        $vat_rate = 0;
                    }

                    $item_total[] = $price * $item_quantity;

                    if ($item_quantity > $find_inventory->quantity) {
                        if ($find_inventory->productionitem == 1) {
                            $difference = $item_quantity - intval($find_inventory->quantity);
                            $update_quantity = 0;
                            $level_array = [
                                'inventory_id' => $request->inventory_id[$i],
                                'leftover_quantity' => $difference
                            ];
                            array_push($excess_items, $level_array);
                        } else {
                            $difference = $item_quantity - intval($find_inventory->quantity);
                            $update_quantity = 0;
                            $level_array = [
                                'inventory_id' => $request->inventory_id[$i],
                                'leftover_quantity' => $difference,
                                'name' => $find_inventory->name,
                                'sku' => $find_inventory->sku,

                            ];
                            array_push($excess_items_for_raising_lpo, $level_array);
                        }
                    } else {
                        $update_quantity = intval($find_inventory->quantity) - $item_quantity;
                    }
                    $find_inventory->quantity = $update_quantity;
                    $find_inventory->update();
                }


                $total = array_sum($item_total);

                $order->update([
                    'total' => $total
                ]);



                if ($request->hasFile('attachment')) {

                    $orderAttchment = $request->file('attachment');
                    $extension = $orderAttchment->getClientOriginalExtension();

                    if ($extension == "pdf") {
                        $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                        $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');
                    } else {

                        $height = Image::make($request->attachment)->height();
                        $width = Image::make($request->attachment)->width();


                        if($height > 0 || $width > 0) {
                            $newFileName = Str::random(20) . '.' . $request->file('attachment')->extension();
                            $order->addMediaFromRequest('attachment')->usingFileName($newFileName)->toMediaCollection('docs');

                        }
                    }

                }

                // if ($request->has('comment')) {
                //     OrderComment::create_comment($request->comment, $order->id, auth()->user()->id);
                // }

                // CreateOrderJob::dispatch($retailer, $order, $request->type);

                // if (sizeof($excess_items) > 0 && $request->generate_production == "true") {
                //     OrderProductionRequestJob::dispatch($retailer, $excess_items, $delivery_date, $order->id, $order->clientable_type, auth()->user());
                // }
                $message = null;
                if (sizeof($excess_items_for_raising_lpo) > 0) {
                    $lpo = true;
                    $message = "LPO Needs TO Be Raised For:<br>";

                    foreach ($excess_items_for_raising_lpo as $key => $value) {
                        $message = $message . "<br>" . ($key + 1) . ". " . $value["name"] . " : " . $value["sku"] . " -> Quantity : " . $value["leftover_quantity"];
                    }
                }

                if (!is_null($message)) {
                    $order->lporequest = $message;
                }
                $order->update();
            }
            $success_message = 'Successfully Created New Order';

            if ($lpo) {
                $success_message = 'Successfully Created New Order. However LPO Needs To Be Created for Trade Items in the Order.View Order to check on more details';
            }
            DB::commit();
            return redirect()->route('orders.index')->with(['success' => $success_message]);
        } catch (\Throwable $th) {
            Log::error($th);
            DB::rollBack();
            return redirect()->route('orders.index')->withErrors(['Something Went Wrong. Contact Administrator For Assistance.']);
        }
    }


    public function checkstock(Request $request)
    {
        // dd($request->all());

        $this->validate(
            $request,
            [
                'inventory_id' => 'required'
            ],
            [
                'inventory_id.required' => 'Select Atleast One Item'
            ]
        );


        $excess_items = [];
        $hadExcessProduction = false;
        $hadExcessTrade = false;

        for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
            $find_inventory = Inventory::find($request->inventory_id[$i]);
            $item_quantity = intval($request->inventory_quantity[$i]);


            if ($item_quantity > $find_inventory->quantity) {

                if ($find_inventory->productionitem == 0 || is_null($find_inventory->productionitem)) {
                    $hadExcessTrade = true;
                }

                if ($find_inventory->productionitem == 1) {
                    $hadExcessProduction = true;
                    $difference = $item_quantity - intval($find_inventory->quantity);
                    $level_array = [
                        'inventory_id' => $request->inventory_id[$i],
                        'name' => $find_inventory->name,
                        'sku' => $find_inventory->sku,
                        'leftover_quantity' => $difference
                    ];
                    array_push($excess_items, $level_array);
                }
            }
        }

        if (sizeof($excess_items)) {
            return response()->json([
                'hadExcessProduction' => $hadExcessProduction,
                'hadExcessTrade' => $hadExcessTrade,
                'items' => $excess_items
            ]);
        } else {
            return response()->json([
                'hadExcessProduction' => $hadExcessProduction,
                'hadExcessTrade' => $hadExcessTrade,
                'items' => []
            ]);
        }
    }

    public function checkExcess(Request $request)
    {
        $hadExcessProduction = false;
        $hadExcessTrade = false;
        for ($i = 0; $i < sizeof($request->inventory_id); $i++) {
            $find_inventory = Inventory::find($request->inventory_id[$i]);
            $item_quantity = intval($request->inventory_quantity[$i]);


            if ($item_quantity > $find_inventory->quantity) {
                if ($find_inventory->productionitem == 0 || is_null($find_inventory->productionitem)) {
                    $hadExcessTrade = true;
                }

                if ($find_inventory->productionitem == 1) {
                    $hadExcessProduction = true;
                }
            }
        }

        return ['hadExcessProduction' => $hadExcessProduction, 'hadExcessTrade' => $hadExcessTrade];
    }


    public function view(Order $order)
    {
        $comments = $order->comments;
        $delivery_location = $order->location;
        $user = $order->createdBy;
        $timelines = $order->timelines()->with('createdBy')->get();

        $approved = $timelines->filter(function ($item) {
            return $item->stage == 2;
        })->first();

        $partially_shipped = $timelines->filter(function ($item) {
            return $item->stage == 3;
        })->first();

        $fully_shipped = $timelines->filter(function ($item) {
            return $item->stage == 4;
        })->first();

        $paid = $timelines->filter(function ($item) {
            return $item->stage == 5;
        })->first();


        return view('admin.orders.view')->with([
            'order' => $order,
            'comments' => $comments,
            'delivery_location' => $delivery_location,
            'user' => $user,
            'approved' => $approved,
            'partially_shipped' => $partially_shipped,
            'fully_shipped' => $fully_shipped,
            'paid' => $paid,
            'timeline_count' => $timelines->count()

        ]);
    }


    public function comment_store(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required|min:3',
            'order_id' => 'required',
        ]);

        $order_id = $request->get('order_id');

        OrderComment::create_comment(
            $request->get('comment'),
            $order_id,
            user()->id
        );

        return redirect()->route('orders.view', ['order' => $order_id])->with(['success', 'Your comment has been made']);
    }


    public function add_order_item_element(Request $request)
    {
        $this->validate($request, [
            'current_id' => 'required'
        ]);

        $current_id = $request->get('current_id');
        $wholesaler_id = $request->get('wholesaler_id');

        $cache_file = asset("cache_22/" . Wholesaler::findOrFail($wholesaler_id)->slug) . '?v=' . date('jHm');

        return view('orders.partials.entry', compact('wholesaler_id', 'current_id', 'cache_file'));
    }


    public function fetch_delivery_locations(Request $request)
    {
        $this->validate($request, [
            'wholesaler_id' => 'required'
        ]);

        $wholesaler = Wholesaler::findOrFail($request->get('wholesaler_id'));
        $locations = $wholesaler->locations;
        $locations_count = count($locations);

        return view('orders.partials.location_selection', compact('locations', 'locations_count'));
    }

    public function status_update(Request $request)
    {


        $this->validate($request, [
            'order_id' => 'required',
            'status_update' => 'required|min:1',
            'selected_order_items' => 'required',
        ]);

        $order_id = $request->get('order_id');
        $status = $request->get('status_update');
        $selected_items = $request->get('selected_order_items', []);

        foreach ($selected_items as $item) {

            OrderProduct::where('id', $item)->where('order_id', $order_id)->update([
                'status' => $status
            ]);
        }

        //Update Order Status
        $order_status = Order::determine_order_status($order_id);

        Order::find($order_id)->update([
            'stage' => $order_status
        ]);

        return redirect()->route('orders.view', ['order' => $order_id])->with(['success', 'Order Status has been updated']);
    }


    /**
     * @param Order $order
     * @return mixed
     */
    public function invoice_download(Request $request, Order $order)
    {
        $footerHtml = view()->make('exports.pdf.footer_order', compact('order'))->render();
        $headerHtml = view()->make('exports.pdf.header_order', compact('order'))->render();

        $order = Order::with('comments')->find($order->id);
        // dd($order);

        $pdf = PDF::loadView('exports.pdf.order_sheet', compact('order'))
        ->setOption('header-html', $headerHtml)
        ->setOption('footer-html', $footerHtml);

        $export_file_label = 'OrderSheet-' . $order->face_id . '-' . env('APP_NAME');

        return $pdf->download($export_file_label . '.pdf');
    }


    public function wholesaler_items_json(Request $request)
    {

        $wholesaler = Wholesaler::findOrFail(6);
        $items = $wholesaler = $wholesaler->visible_products();

        // $items = collect();
        return response()->json($items);


        return response()->json(['data' => $items]);
    }


    public function cancel(Order $order)
    {
        $production_request = $order->productionRequest;
        if (!is_null($order->productionRequest)) {
            $items = $production_request->items;

            foreach ($order->items as $key => $value) {
                $find_inventory = Inventory::find($value->inventory_id);

                if (in_array($value->inventory_id, $items->pluck('inventory_id')->toArray())) {
                    $proditem = ProductionRequestItem::where('production_request_id', $production_request->id)->where('inventory_id', $value->inventory_id)->first();
                    $difference = intval($value->quantity) - intval($proditem->requested_quantity);
                    $update_quantity = intval($find_inventory->quantity) + $difference;
                } else {
                    $update_quantity = intval($find_inventory->quantity) + intval($value->quantity);
                }
                $find_inventory->quantity = $update_quantity;
                $find_inventory->update();
            }
            $production_request->forceDelete();
        } else {
            foreach ($order->items as $key => $value) {
                $find_inventory = Inventory::find($value->inventory_id);
                $update_quantity = intval($find_inventory->quantity) + intval($value->quantity);
                $find_inventory->quantity = $update_quantity;
                $find_inventory->update();
            }
        }
        $branch = WholesalerLocation::find($order->delivery_location);

        if ($branch->counter !== 0) {
            $diff = $branch->counter - 1;
            $branch->counter = $diff;
            $branch->update();
        }


        $order->delete();

        return redirect()->route('orders.index')->with(['success' => 'Order has been deleted']);
    }

    public function mail_order(Request $request)
    {
        // dd($request->all());
        $order = Order::findorFail($request->order_id);
        // dd($order);
        // dd($order->template->getFirstMedia('templateHeader'));

        MailOrder::dispatch($order, $request->customer_email);

        return redirect()->route('orders.index')->with(['success' => 'Mail Successfully Sent']);
    }

    public function update_order_status(Request $request)
    {
        $order = Order::with('items')->find($request->order_id);
        if ($request->status_update <= $order->stage) {
            return redirect()->back()->withErrors(['Cannot Update Order Status,Order Has Aleady Passed That State']);
        } elseif ($request->status_update > 2 && $order->stage < 2) {
            return redirect()->back()->withErrors(['Cannot Update Order Status Until Order Is Approved']);
        } elseif ($request->status_update == 5 && $order->stage < 3) {
            return redirect()->back()->withErrors(['Cannot Update Order Status To Paid Until it Has Either Been updated to Partially or Fully Shipped']);
        } else {
            $order->stage = $request->status_update;
            $order->update();

            $timeline = new OrderTimeline();
            $timeline->order_id = $request->order_id;
            $timeline->stage = $request->status_update;
            $timeline->created_by = auth()->user()->id;
            $timeline->save();

            if ((int)$request->status_update == 2) {

                // dd($request->status_update);

                $order->update([
                    'stage' => 2
                ]);

                SendOrdersDnoteJob::dispatch($order->id);
            }

            if ((int)$request->status_update >= 4) {
                $items = $order->items;

                foreach ($items as $key => $value) {
                    $value->status = 2;
                    $value->update();
                }
            }
        }



        return redirect()->back()->with(['success' => 'Order Status Has Been Updated']);
    }


    public static function order_tag($len = 8)
    {

        $shuffled_string = substr(str_shuffle("23ab34567c1de12893456fghjk4mnpq5rs7t6v5456w9xyz5"), 0, $len);
        $string = strtoupper($shuffled_string);

        $check = Order::order_tag_is_unique($string);

        if ($check === false) {
            return self::order_tag();
        }

        return $string;
    }


    public function update_comment(Request $request)
    {
        $orderProduct = OrderProduct::find($request->order_product_id);

        $orderProduct->note = $request->note;
        $orderProduct->update();
        $order = $orderProduct->order;
        // OrderCommentUpdateJob::dispatch($order);

        return redirect()->back()->with(['success' => 'Comment Has Been Updated']);
    }
}
