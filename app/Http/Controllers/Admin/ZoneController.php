<?php

namespace App\Http\Controllers\Admin;

use App\Models\Zone;
use App\Models\County;
use App\Models\CountyZones;
use Illuminate\Http\Request;
use App\Models\ZoneDiscounts;
use App\Models\InventoryCategory;
use App\Exports\ZoneDiscountExport;
use App\Imports\ZoneDiscountImport;
use Illuminate\Support\Facades\Bus;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Exports\ZoneCurrentDiscountExport;
use App\Imports\ZoneCurrentDiscountImport;
use App\Http\Controllers\BaseTemplateController;
use App\Jobs\Exports\ZoneBusExportJob;
use App\Jobs\Imports\ZoneDiscountImportJob;

class ZoneController extends BaseTemplateController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $zones = Zone::orderBy('id', 'ASC')->paginate(15);

        return view('zones.index', compact('zones'))->with('i', ($request->input('page', 1) - 1) * 15);
    }

    public function retrieveZoneDiscounts(Request $request, $id)
    {
        $search = $request->search;
        $zone = Zone::find($id);
        $discounts = ZoneDiscounts::with('inventory')
            ->where('zone_id', $id)
            ->when($search, function ($query) use ($search) {
                return $query->whereHas('inventory', function ($q) use ($search) {
                    return
                        $q->where('name', 'LIKE', '%' . $search . '%')
                        ->orwhere('itemnumber', 'LIKE', '%' . $search . '%')
                        ->orwhere('sku', 'LIKE', '%' . $search . '%');
                });
            })
            ->paginate(30);
        return view('zones.viewdiscounts', compact('discounts', 'zone'))->with('i', ($request->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('zones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Zone $zone)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $zone = $request->all();

        Zone::create($zone);

        return redirect()->route('zones.index')->with('success', 'Zone created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(Zone $zone)
    {
        $zone = Zone::find($zone->id);

        return view('zones.edit', compact('zone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $input = $request->all();

        $zone = Zone::find($zone->id);
        $zone->update($input);

        return redirect()->route('zones.index')->with('success', 'Zone updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone)
    {
        $zone = Zone::find($zone->id);
        $zone->delete();

        return redirect()->route('zones.index')->with('success', 'Zone deleted successfully');
    }

    public function custom_discount(Zone $zone)
    {
        $categories = InventoryCategory::all();

        return view('zones.custom_discounts', compact('zone', 'categories'));
    }


    public function custom_discount_download(Request $request)
    {

        $zone = Zone::find($request->zoneid);

        $filename = $zone->name . '-ZoneInvetoryCustomDiscounts-' . date('d-m-y h:i') . '.xlsx';

        if (is_null($request->categories)) {

            return response()->json(['message' => 'No Categories Are Available'], 500);
        } else {

            $batch = Bus::batch([
                new ZoneBusExportJob($request->categories, $zone, $filename)
            ])->dispatch();

            return response()->json(['message' => 'Successfully Started Exporting Process', 'batchid' => $batch->id, 'filename' => $filename], 200);
        }
    }

    public function custom_discount_update(Zone $zone, Request $request)
    {
        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'pcwd-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);
        $batch = Bus::batch([
            new ZoneDiscountImportJob($url,$zone)
        ])->dispatch();
        return response()->json(['message' => 'Successfully Started Importing Process', 'batchid' => $batch->id], 200);
    }

    public function allocate(Request $request, Zone $zone)
    {

        $zone = Zone::find($zone->id);
        // $cz = CountyZones::where('zone_id', $zone->id)->get();
        $counties = County::with('zoneAllocation')
            ->orderBy('name', 'ASC')
            ->get();

        return view('zones.allocate', compact('zone', 'counties'));
    }

    public function allocate_counties(Request $request)
    {
        $this->validate($request, [
            'zone_id' => 'required',
            'county_id' => 'required',
        ]);

        $zone = Zone::find($request->zone_id);
        $current_allocations = $zone->counties;

        $current_allocations_array = $current_allocations->pluck('id')->toArray();
        $existing_allocations_to_be_removed = array_diff($current_allocations_array, $request->county_id);
        $allocations_to_be_added = array_diff($request->county_id, $current_allocations_array);


        //reset allocated column to 0 for all current allocations
        foreach ($existing_allocations_to_be_removed as $key => $county) {

            $updated_county = $current_allocations->filter(function ($item) use ($county) {
                return $item->id == $county;
            })->first();

            $updated_county->allocated = 0;
            $updated_county->update();

            $wholesalers = $updated_county->wholesalers;

            foreach ($wholesalers as $key => $wholesaler) {
                if ($wholesaler->price_type == 1) {
                    $wholesaler->price_type = 0;
                    $wholesaler->update();
                }
            }
        }

        //for those chosen set allocated collumn to 1
        foreach ($allocations_to_be_added as $key => $county) {

            $updated_county = County::find($county);
            $updated_county->allocated = 1;
            $updated_county->update();

            $wholesalers = $updated_county->wholesalers;

            foreach ($wholesalers as $key => $wholesaler) {
                if ($wholesaler->price_type == 0) {
                    $wholesaler->price_type = 1;
                    $wholesaler->update();
                }
            }
        }

        $zone->counties()->sync($request->input('county_id'));
        return redirect()->route('zones.index')->with('success', 'County assigned successfully');
    }

    public function search(Request $request)
    {

        $zones = Zone::where('name', 'LIKE', '%' . $request->search . '%')
            ->paginate(15);

        return  view('zones.index', compact('zones'))->with('i', ($request->input('page', 1) - 1) * 15);
    }

    public function download_current_discount($zone, $mode, $wholesaler)
    {
        // dd($zone);
        if ($mode == 0) {
            $mode = null;
        }
        if ($wholesaler == 0) {
            $wholesaler = null;
        }

        ini_set("max_execution_time", 99600);
        return Excel::download(new ZoneCurrentDiscountExport($zone, $mode, $wholesaler), 'ZoneCurrentDiscountExport-' . date('d-m-y h:i') . '.xlsx');
    }

    public function current_zone_discount_update(Request $request)
    {
        $this->validate($request, [
            'excel' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('excel');
        $filename = 'cwd-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);
        Excel::import(new ZoneCurrentDiscountImport($request->zone), $url);

        return redirect()->route('zones.index')->with(['success' => 'Pricing data has been updated']);
    }
}
