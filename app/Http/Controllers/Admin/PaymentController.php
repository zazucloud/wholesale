<?php

namespace App\Http\Controllers\Admin;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Exports\PaymentExport;
use App\Jobs\CreatePaymentJob;
use App\Http\Controllers\Controller;
use App\Models\Wholesale\Wholesaler;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\BaseTemplateController;
use App\Models\Wholesale\Wholesaler as WholesaleWholesaler;

class PaymentController extends BaseTemplateController
{
    public $payment_attachments_folder = 'payment_attachments/';


    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:payments-view|payments-create|payments-edit|payments-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:payments-create', ['only' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payments = Payment::latest()->paginate(30);

        $clients = Wholesaler::orderBy('business_name', 'ASC')->pluck('business_name', 'id')->all();

        return view('admin.payments.index', compact('payments', 'clients'))->with('i', ($request->input('page', 1) - 1) * 30);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $payment_channels = Payment::$channels;

        $wholesalers = WholesaleWholesaler::active()->orderBy('business_name', 'ASC')->get();

        return view('admin.payments.create', compact('payment_channels', 'wholesalers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'channel' => 'required',
            'wholesaler' => 'required|numeric|min:1',
            'amount' => 'required|numeric|min:1',
            //'reference_input' => 'required',
            'attachment' => 'sometimes|max:10000|mimes:jpeg,bmp,png,gif,pdf',
            $request->get('reference_input') => 'sometimes'
        ]);

        $wholesaler = $request->get('wholesaler');
        $amount = $request->get('amount');
        $channel = $request->get('channel');
        $creator = user()->id;

        $receipt_no = Payment::receipt_number();

        $reference = $request->get($request->get('reference_input'));

        $attachment = null;
        if ($request->hasFile('attachment')) {
            $file_extension = $request->file('attachment')->getClientOriginalExtension();
            $attachment = random_unique() . '.' . $file_extension;
            $contents = file_get_contents($request->file('attachment')->getRealPath());
            Storage::put($this->payment_attachments_folder . $attachment, $contents);
        }

        CreatePaymentJob::dispatchNow($wholesaler, $amount, $channel, $reference, $creator, $request->get('comment'), $attachment, $receipt_no);

        flash_message('success', "Record has been added successfully.");

        return redirect()->route('payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        return view('admin.payments.show')->with('payment', $payment);
    }

    public function amount_to_words(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required'
        ]);
        return convert_number_to_words($request->get('amount'), ' Kenyan Shillings');
    }

    public function cancel(Payment $payment, Request $request)
    {
        $this->validate($request, [
            'receipt_no' => 'required'
        ]);

        Payment::where('receipt_no', $request->get('receipt_no'))->where('id', $payment->id)->delete();

        flash_message('warning', 'Payment with receipt no ' . $payment->receipt_no . ' of amount ' . $payment->amount . ' has been cancelled');

        return redirect()->route('payments.index');
    }

    public function attachment_view(Payment $payment)
    {
        $attachment = $payment->attachment;
        $attachment_file = $this->payment_attachments_folder . $attachment;
        if (Storage::exists($attachment_file)) {
            $attachment_contents = Storage::get($attachment_file);

            $response = Response::make($attachment_contents, 200);
            return $response->header("Content-Type", get_mime_type($attachment));
        }

        flash_message('danger', "Error while retrieving attachment - contact system admin.");

        return redirect()->route('payments.view', $payment->receipt_no);
    }

    public function exportPayments(Request $request)
    {
        return Excel::download(new PaymentExport, 'payments.xlsx');
    }

    public function printPayments(Request $request)
    {
        $payments = Payment::all();

        $pdf = SnappyPdf::loadView('admin.payments.pdf.payment', compact('payments'))
            // ->setOption('footer-html', $footerHtml)
            ->setOption('margin-left', '0')
            ->setOption('margin-right', '0')
            ->setOption('margin-top', '8')
            ->setOption('margin-bottom', '0');

        return $pdf->inline();
    }

    public function search(request $request)
    {
        session(['_old-input' => $request->all()]);

        $wholesaler_id = $request->wholesaler_id;
        $channel = $request->channel;
        $search = $request->search;

        $toreturn = $request->except('token');
        $return2 = [
            'wholesaler_id' => $request->wholesaler_id,
            'channel' => $request->channel,
            'search' => $request->search,
        ];

        $payments = Payment::when($search, function ($query) use ($search) {
            return $query->where(function ($q) use ($search) {
                return $q->where('reference', 'LIKE', '%' . $search . '%')
                    ->orwhere('receipt_no', 'LIKE', '%' . $search . '%');
            });
        })->when($wholesaler_id, function ($query) use ($wholesaler_id) {
            return $query->where('wholesaler_id', $wholesaler_id);
        })
            ->when($channel, function ($query) use ($channel) {
                return $query->where('channel', $channel);
            })
            ->paginate(30);

        $clients = Wholesaler::pluck('business_name', 'id')->all();

        return view('admin.payments.index', compact('payments', 'clients'))->with('i', ($request->input('page', 1) - 1) * 30);
    }
}
