<?php

namespace App\Http\Controllers\Admin;

use App\Events\WelcomeUserEvent;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr as SupportArr;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Hash as FacadesHash;
use App\Http\Controllers\BaseTemplateController;


class UsersController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:users-view|users-create|users-edit|users-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id', 'DESC')->paginate(10);

        return view('users.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            // 'password' => 'required|same:confirm-password',
            'phone' => 'required|unique:users,phone',
            'roles' => 'required'
        ]);

        // generate random 10 char password from below chars
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        $password = substr($random, 0, 10);

        $input = $request->all();
        $input['password'] = $password;



        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        $userInfo = [
            'user' => $user,
            'pass' => $password
        ];

        // dd($userInfo['user']->email);
        // Mail::to($userInfo['user']->email)->send(new WelcomeUserMail($userInfo));

        event(new WelcomeUserEvent($userInfo));

        $user->password = FacadesHash::make($password);
        $user->update();


        return redirect()->route('users.index')->with('success', 'User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('users.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = FacadesHash::make($input['password']);
        } else {
            $input = SupportArr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        FacadesDB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->route('users.index')->with('success', 'User deleted successfully');
    }

    public function disableUser(Request $request)
    {
        $user = User::find($request->id);
        $user->disabled = 1;
        $user->update();

        return redirect()->route('users.index')->with(['success' => 'User Account Has Been Disabled']);
    }

    public function enableUser(Request $request)
    {
        $user = User::find($request->id);
        $user->disabled = 0;
        $user->update();

        return redirect()->route('users.index')->with(['success' => 'User Account Has Been Enabled']);
    }

    public function search(Request $request)
    {

        $data = User::where('name', 'LIKE', '%' . $request->search . '%')
            ->orwhere('email', 'LIKE', '%' . $request->search . '%')
            ->paginate(10);

        return  view('users.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 10);;
    }

    public function users_ajax_dropdown(Request $request)
    {

        if ($request->ajax()) {
            $page = $request->page;
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;
            if (is_null($request->term)) {
                $users = User::select('name', 'id')->orderBy('name')->skip($offset)->take($resultCount)->get();
            } else {
                $users = User::select('name', 'id')
                    ->where(function ($query) use ($request) {
                        return $query->where('name', 'LIKE',  '%' . $request->term . '%');
                    })->orderBy('name')->skip($offset)->take($resultCount)->get();
            }

            $count = User::count();
            $endCount = $offset + $resultCount;
            $morePages = $endCount < $count;


            $results = array(
                "results" => $users,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }
    }


}
