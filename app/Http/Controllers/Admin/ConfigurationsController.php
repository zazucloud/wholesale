<?php

namespace App\Http\Controllers\Admin;

use Mpdf\Tag\Dd;
use App\Models\User;
use App\Models\Brand;
use App\Models\CsvData;
use App\Models\Inventory;
use App\Models\Warehouses;
use App\Jobs\CacheBuildJob;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\PricingExport;
use App\Imports\PricingImport;
use PHPExcel_Style_Protection;
use App\Imports\InventoryImport;
use App\Models\InventoryCategory;
use App\Imports\WholesalersImport;
use App\Models\InventoryAttribute;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Jobs\Imports\PriceImportJob;
use App\Models\Wholesale\Wholesaler;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;
use App\Jobs\CategoryPricingImportJob;
use App\Jobs\Exports\PricingExportJob;
use App\Models\InventoryItemAttribute;
use App\Exports\InventoryPricingExport;
use App\Imports\InventoryPricingImport;
use Illuminate\Support\Facades\Storage;
use App\Exports\InventoryTemplateExport;
use App\Models\InventoryAttributeOption;
use Illuminate\Support\Facades\Response;
use App\Exports\CategoryVisibilityExport;
use App\Exports\WholesalerTemplateExport;
use App\Http\Controllers\CacheController;
use App\Http\Traits\InventoryImportTrait;
use App\Imports\CategoryVisibilityImport;
use App\Models\WholesalerCategoryDiscount;
use App\Http\Controllers\BaseTemplateController;
use App\Models\Wholesale\WholesalerCategoryVisibility;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ConfigurationsController extends BaseTemplateController
{
    use InventoryImportTrait;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:wholesaler_config-uploading', ['only' => ['wholesaler_import', 'wholesaler_import_template', 'wholesaler_import_process']]);
    }

    public function brands(Request $request)
    {

        $brands = Brand::latest()->paginate(15);

        return view('admin.config.brands', compact('brands'))->with('i', ($request->input('page', 1) - 1) * 15);
    }

    public function brands_store(Request $request)
    {
        $this->validate($request, [
            'brand_name' => 'required|unique:brands,name',
            'status' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg',
        ]);

        $brand = Brand::create([
            'name' => $request->get('brand_name'),
            'slug' => str_slug($request->get('brand_name')),
            'narration' => $request->get('narration'),
            'status' => $request->get('status')
        ]);

        if ($request->hasFile('logo')) {
            $height = Image::make($request->logo)->height();
            $width = Image::make($request->logo)->width();

            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('logo')->extension();
                $brand->addMediaFromRequest('logo')->usingFileName($newFileName)->toMediaCollection('brandlogos');
            }
        }

        return redirect()->route('admin.config.brands')->with('success', 'Brand created successfully');
    }

    public function brand_update($id, Request $request)
    {
        $brand = Brand::find($id);

        $this->validate($request, [
            'brand_name' => 'required',
            'status' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg',
        ]);

        if ($request->hasFile('logo')) {
            $brand->getFirstMedia('brandlogos');
            foreach ($brand->getMedia('brandlogos')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }


            $height = Image::make($request->logo)->height();
            $width = Image::make($request->logo)->width();

            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('logo')->extension();
                $brand->addMediaFromRequest('logo')->usingFileName($newFileName)->toMediaCollection('brandlogos');
            }

        }

        $brand->update([
            'name' => $request->get('brand_name'),
            'narration' => $request->get('narration'),
            'slug' => str_slug($request->get('brand_name')),
            'status' => $request->get('status')
        ]);

        return redirect()->route('admin.config.brands')->with('success', 'Brand updated successfully');
    }

    public function brand_delete(Request $request, $id)
    {
        $brand = Brand::find($id);
        $brand->delete();

        return redirect()->route('admin.config.brands')->with('success', 'Brand deleted successfully');
    }

    public function search_brands(Request $request)
    {
        $brands = Brand::where('name', 'LIKE', '%' . $request->search . '%')->paginate(15);

        return view('admin.config.brands', compact('brands'))->with('i', ($request->input('page', 1) - 1) * 15);
    }


    public function import_inventory(Request $request)
    {

        return view('admin.config.inventory.import');
    }

    public function template_inventory(Request $request)
    {
        return Excel::download(new InventoryTemplateExport(), 'inventoryTemplate.xlsx');
    }


    public function inventory_import_process()
    {
        ini_set("max_execution_time", 3600);

        $this->can(['create_inventory', 'edit_inventory_details']);

        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'im-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);

        $inserted_count = 0;
        $updated_count = 0;


        $inventory_data = Excel::selectSheetsByIndex(0)->load($url, function ($reader) {
        })->get()->toArray();

        $inventory_data_grouped_sub_category = (array_group_by($inventory_data, 'sub_category'));

        $color_attribute = InventoryAttribute::firstOrCreate([
            'label' => 'Color',
            'slug' => 'color',
            'type' => 'fixed'
        ]);
        $color_attribute_id = $color_attribute->id;

        foreach ($inventory_data_grouped_sub_category as $subcategory => $inventory) {

            $inventory_collection = collect($inventory);
            $inventory_collection_brands = array_unique($inventory_collection->pluck('brand')->toArray());
            $inventory_collection_colors = array_unique($inventory_collection->pluck('color')->toArray());

            $subcategory_brand_name = current($inventory_collection_brands);
            $brand = Brand::firstOrCreate([
                'name' => $subcategory_brand_name
            ]);
            $brand_id = $brand->id;

            $category_slug = str_slug($subcategory);

            /*  InventoryCategory::firstOrCreate([
                  'name' => $subcategory,
                  'parent' => 0,
                  'brand_id' => $brand_id,
                  'slug' => str_slug($subcategory),
                  'status' => 1
              ]);*/

            $inventory_category = InventoryCategory::where('slug', $category_slug)->first();


            if (!$inventory_category) {
                InventoryCategory::create([
                    'name' => $subcategory,
                    'parent' => 0,
                    'brand_id' => $brand_id,
                    'slug' => str_slug($subcategory),
                    'status' => 1
                ]);
            }


            foreach ($inventory_collection_colors as $inventory_collection_color) {
                if (($inventory_collection_color != 'NA') || ($inventory_collection_color != 'N/A')) {
                    InventoryAttributeOption::firstOrCreate([
                        'attribute_id' => $color_attribute_id,
                        'option' => $inventory_collection_color
                    ]);
                }
            }
        }


        $sku_grouped = (array_group_by($inventory_data, 'item_number'));

        foreach ($sku_grouped as $sku => $inventory) {

            // $item_count = count($inventory);

            $inventory_collection = collect($inventory);

            $item_colors = $inventory_collection->pluck('color');
            $inventory_collection_brands = array_unique($inventory_collection->pluck('brand')->toArray());
            $item_brand = current($inventory_collection_brands);
            $inventory_collection_category = current(array_unique($inventory_collection->pluck('sub_category')->toArray()));

            $brand = Brand::firstOrCreate([
                'name' => $item_brand
            ]);
            $brand_id = $brand->id;

            $inventory_category = InventoryCategory::firstOrCreate([
                'name' => $inventory_collection_category,
            ]);

            $inventory_single = current($inventory);
            $item_sku = $inventory_single['sku'] . '_' . $inventory_single['color'];

            $inventory_load = [
                'name' => $inventory_single['product_name'],
                'sku' => $item_sku,
                'itemnumber' => $inventory_single['item_number'],
                'color' => $inventory_single['color'],
                'brand_id' => $brand_id,
                'quantity' => 0,
                'threshold' => 0,
                'crossed_price' => null,
                'visibility' => 1,
                'status' => 1
            ];


            if (isset($inventory_single['price'])) {

                $inventory_load['price'] = $inventory_single['price'];
            } else {
                $inventory_load['price'] = null;
            }


            if (isset($inventory_single['description'])) {
                $inventory_load['description'] = $inventory_single['description'];
            } else {
                $inventory_load['description'] = null;
            }


            $inventory_check = Inventory::where('sku', $item_sku)->get()->count();

            if ($inventory_check === 0) {
                $inventory = Inventory::create($inventory_load);
                $inserted_count++;
            } else {
                //  Log::info("Updated",$inventory_load);

                Inventory::where('sku', $item_sku)->update($inventory_load);
                $inventory = Inventory::where('sku', $item_sku)->first();
                $updated_count++;
            }


            \App\Models\InventoryItemCategory::firstOrCreate([
                'inventory_id' => $inventory->id,
                'category_id' => $inventory_category->id,
            ]);


            foreach ($item_colors as $color) {

                if (($color != 'NA') || ($color != 'N/A')) {
                    $color_attribute_option = InventoryAttributeOption::firstOrCreate([
                        'attribute_id' => $color_attribute_id,
                        'option' => $color
                    ]);

                    InventoryItemAttribute::firstOrCreate([
                        'inventory_id' => $inventory->id,
                        'attribute_id' => $color_attribute_id,
                        'attribute_type' => $color_attribute->type,
                        'value' => $color_attribute_option->id
                    ]);
                }
            }
        }

        self::cache();

        flash_message('success', "Inventory Data has been updated. </br> Newly Inserted Records: $inserted_count </br> Updated Records: $updated_count");

        return view('admin.config.inventory_import');
    }

    public function inventory_categories(Request $request)
    {

        $parent = Brand::active()->get();

        $brands = Brand::pluck('name', 'id')->all();

        $categories = InventoryCategory::with('brand')->active()->paginate(15);
        // dd($parent);

        return view('admin.config.inventory_categories', compact('parent', 'categories', 'brands'))->with('i', ($request->input('page', 1) - 1) * 15);
    }


    public function inventory_categories_store(Request $request)
    {

        $this->validate($request, [
            'category_name' => 'required|unique:inventory_categories,name',
            'parent' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        $parent = $request->get('parent');

        $category_name = $request->get('category_name');

        $category = InventoryCategory::create([
            'name' => $category_name,
            'brand_id' => $parent,
            'parent' => 0,
            'slug' => str_slug($category_name),
            'status' => 1
        ]);

        if ($request->hasFile('image')) {

            $height = Image::make($request->image)->height();
            $width = Image::make($request->image)->width();


            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('image')->extension();
                $category->addMediaFromRequest('image')->usingFileName($newFileName)->toMediaCollection('categorylogos');
            }



        }

        return redirect()->route('admin.config.inventory_categories')->with(['success' => "$category_name Created Successfully"]);
    }


    public function inventory_categories_update($id, Request $request)
    {
        $category = InventoryCategory::find($id);

        $this->validate($request, [
            'category_name' => 'required',
            'status' => 'required'
        ]);

        if ($request->hasFile('image')) {
            $category->getFirstMedia('categorylogos');
            foreach ($category->getMedia('categorylogos')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }


            $height = Image::make($request->image)->height();
            $width = Image::make($request->image)->width();


            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('image')->extension();
                $category->addMediaFromRequest('image')->usingFileName($newFileName)->toMediaCollection('categorylogos');
            }

        }

        $category->update([
            'name' => $request->get('category_name'),
            'parent' => $request->get('parent'),
            'slug' => str_slug($request->get('category_name')),
            'status' => $request->get('status')
        ]);


        flash_message('info', "Category Updated Successfully");

        return redirect()->route('admin.config.inventory_categories');
    }

    public function search_inventory_categories(Request $request)
    {
        session(['_old-input' => $request->all()]);

        $brand_id = $request->brand_id;

        $toreturn = $request->except('token');
        $return2 = [
            'brand_id' => $request->brand_id,
        ];

        $categories = InventoryCategory::where('name', 'LIKE', '%' . $request->search . '%')
            ->when($brand_id, function ($query) use ($brand_id) {
                return $query->where('brand_id', $brand_id);
            })
            ->paginate(15);

        $parent = Brand::active()->get();
        $brands = Brand::pluck('name', 'id')->all();

        return view('admin.config.inventory_categories', compact('parent', 'categories', 'brands'))->with('i', ($request->input('page', 1) - 1) * 15);
    }

    public function inventory_attributes()
    {

        $inventory_attributes = InventoryAttribute::all();

        return view('admin.config.inventory_attributes', compact('inventory_attributes'));
    }

    public function inventory_attributes_store(Request $request)
    {
        // dd($request->all());

        $this->validate($request, [
            'attribute_name' => 'required|unique:inventory_attributes,label',
            // 'attribute_type' => 'required'
        ]);

        InventoryAttribute::create([
            'label' => $request->get('attribute_name'),
            'slug' => str_slug($request->get('attribute_name')),
            // 'type' => $request->get('attribute_type')
        ]);


        flash_message('success', "Attribute Created Successfully");

        return redirect()->route('admin.config.inventory_attributes');
    }

    public function inventory_attribute_manage(InventoryAttribute $attribute)
    {

        $options = $attribute->options()->orderBy('id', 'ASC')->paginate(10);

        return view('admin.config.inventory_attributes_options', compact('attribute', 'options'));
    }

    public function inventory_attribute_option_store(Request $request)
    {

        $this->validate($request, [
            'attribute_id' => 'required',
            'option' => 'required'
        ]);

        $option = $request->get('option');

        $attribute_id = $request->get('attribute_id');

        $attribute_slug = InventoryAttribute::findOrFail($attribute_id)->slug;


        if (!InventoryAttributeOption::validate_unique($attribute_id, $option)) {
            flash_message('danger', "$option already exists as an attribute option");
            return redirect()->route('admin.config.inv-att.manage', ['attribute' => $attribute_slug]);
        }

        InventoryAttributeOption::create([
            'attribute_id' => $attribute_id,
            'option' => $option
        ]);

        flash_message('success', "Attribute Option ($option) Created Successfully");


        return redirect()->route('admin.config.inv-att.manage', ['attribute' => $attribute_slug]);
    }

    public function import_inventory_csv(Request $request)
    {
        //  dd($request->all());
        // ini_set("max_execution_time", 3600);

        $file = $request->file('uploadInventory');
        // dd($file);

        $import = new InventoryImport();
        // dd($import);
        try {
            $import->import($file);
            return redirect()->route('admin.config.inventory_import')->with(['success' => "Inventory Imported  Successfully"]);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return redirect()->route('admin.config.inventory_import')->withErrors(["SOmething went Wrong. Please download template to countercheck column names required."]);
        }
        // dd($import);

    }

    public function warehouses(Request $request)
    {

        $warehouses = Warehouses::latest()->active()->paginate(10);

        return view('admin.config.warehouses', compact('warehouses'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function search_warhouses(Request $request)
    {
        $warehouses = Warehouses::where('name', 'LIKE', '%' . $request->search . '%')
            ->paginate(15);

        return view('admin.config.warehouses', compact('warehouses'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function warehouses_store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'warehouse_name' => 'required|unique:warehouses,name',
            'latitude' => 'required',
            'longitude' => 'required',
            'locality' => 'required',
        ]);

        Warehouses::create([
            'name' => $request->get('warehouse_name'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'locality' => $request->get('locality'),
            'status' => 1
        ]);



        return redirect()->route('admin.config.warehouses')->with(['success' => "Warehouse Added Successfully"]);
    }

    public function warehouses_update(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'warehouse_name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'locality' => 'required',
            'id' => 'required'
        ]);

        Warehouses::find($request->id)->update([
            'name' => $request->get('warehouse_name'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'locality' => $request->get('locality'),
            'status' => 1
        ]);



        return redirect()->route('admin.config.warehouses')->with(['success' => "Warehouse Updated Successfully"]);
    }

    public function pricing(Request $request)
    {
        $categories = InventoryCategory::latest()->paginate(10);

        return view('admin.config.pricing', compact('categories'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function category_pricing(InventoryCategory $category)
    {
        return view('admin.config.category-pricing', compact('category'));
    }


    public function category_pricing_process(InventoryCategory $category, Request $request)
    {
        ini_set("max_execution_time", 3600);

        $this->validate($request, [
            'price' => 'required'
        ]);

        $prices = $request->get('price', []);

        foreach ($prices as $id => $price) {
            Inventory::findOrFail($id)->update(['price' => $price]);
        }
        self::cache();

        flash_message('success', "Prices for <strong>" . $category->name . '</strong> have been updated successfully');

        return redirect()->route('admin.config.category_pricing', $category->slug);
    }



    public function category_discounts_download($id)
    {
        $category = InventoryCategory::find($id);

        ini_set("max_execution_time", 99600);
        ini_set("memory_limit", 999999999);

        $title = 'MasterPricing-' . $category->name;

        $category_items = $category->items()->get()->pluck('inventory_id');
        $inventory_list = Inventory::whereIn('id', $category_items)->get();
        // $wholesalers = CacheController::wholesalers();

        $visible_to = WholesalerCategoryVisibility::where('category_id', $category->id)->get()->pluck('wholesaler_id');

        $wholesalers = Wholesaler::whereIn('id', $visible_to)->get();
        return Excel::download(new PricingExport($wholesalers, $inventory_list), $title . ' As at ' . date('d-m-y h:i') . '.xlsx');
    }

    public function category_discounts_process($id, Request $request)
    {
        $category = InventoryCategory::find($id);

        ini_set("max_execution_time", 3600);


        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'pu-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);

        CategoryPricingImportJob::dispatch($category, $url);

        return redirect()->route('admin.config.pricing')->with(['success' => 'Category pricing updated, changes will take effect shortly']);
    }



    public function custom_discounts_download()
    {

        ini_set("max_execution_time", 3600);

        return Excel::create('Pricing-' . date('d-m-y h:i'), function ($excel) {

            $excel->setTitle('MasterPricing');
            $excel->setCreator(config('app.name'))->setCompany(config('app.name'));

            $excel->sheet('MasterPricing', function ($sheet) {
                $data = [];
                $wholesalers = CacheController::wholesalers();

                $i = 0;
                $category = InventoryCategory::find(37);

                $category_items = $category->items()->get()->pluck('inventory_id');
                $inventory_list = Inventory::whereIn('id', $category_items)->get();

                $inventory_list_count = count($inventory_list);
                foreach ($wholesalers as $key => $wholesaler) {


                    foreach ($inventory_list as $inventory) {

                        $sheet->cells('B2:B' . $inventory_list_count, function ($cells) use ($inventory) {

                            foreach ($cells as $cell) {
                                $cell->setValue($inventory->sku);
                            }
                        });

                        $data[] = [];
                    }
                }


                $sheet->fromArray($data);


                $bold_text = array(
                    'font' => array(
                        'bold' => true,
                    )
                );


                $sheet->getStyle('A1:E1')->applyFromArray($bold_text);

                $sheet->getStyle('A1:A' . $i)->applyFromArray($bold_text);


                $sheet->getProtection()->setPassword('topmfgexcelpassword');
                $sheet->getProtection()->setSheet(true);

                $sheet->getStyle('C1:D' . count($data))->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
            });
        })->download('xlsx');
    }


    public function custom_discounts_process(Request $request)
    {
        $this->can(['manage_wholesaler_discounts']);

        ini_set("max_execution_time", 0);

        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'cd-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);

        DB::connection()->disableQueryLog();


        Excel::filter('chunk')->selectSheetsByIndex(0)->load($url)->chunk(300, function ($data) {

            dd($data);
        });
    }

    public function wholesaler_import()
    {

        return view('admin.config.wholesaler_import');
    }

    public function wholesaler_template()
    {
        return Excel::download(new WholesalerTemplateExport(), 'wholesalerTemplate.xlsx');
    }

    public function import_wholesaler_csv(Request $request)
    {
        
ini_set('max_execution_time', 300);

        // dd($request->all());

        $file = $request->file('uploadWholesaler');
        // dd($file);

        $import = new WholesalersImport();
        try {
            $import->import($file);
            return redirect()->route('admin.config.wholesaler_import')->with(['success' => "Wholesaler's Imported  Successfully"]);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return redirect()->route('admin.config.wholesaler_import')->withErrors(["Something went wrong. Please download excel template to countercheck column names."]);
        }
        // dd($import);

    }


    public function custom_import_wholesaler_csv(Request $request)
    {

        $path = $request->file('csv_file')->getRealPath();


        $data = Excel::toArray(new WholesalersImport(null, 1), $request->file('csv_file'));
        $data = $data[0];

        $upload = $request->file('csv_file');
        $filename = 'im-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );


        if (count($data) > 0) {

            if ($request->has('header')) {

                $csv_header_fields = [];

                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $value;
                }

                $csv_data = array_slice($data, 1, 2);
            } else {
                // get the first two rows of the csv file with no header
                $csv_data = array_slice($data, 0, 2);
            }


            //store the temporary data into the csvdata table
            $csv_data_file = CsvData::create([
                'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode(array_slice($data, 0, 3))
            ]);
        } else {
            return redirect()->back();
        }


        if ($csv_data_file->csv_header == 1) {
            return view('admin.config.wholesaler_import_parse', compact('csv_header_fields', 'csv_data', 'csv_data_file', 'filename'));
        }
        return view('admin.config.wholesaler_import_parse', compact('csv_data', 'csv_data_file', 'filename'));
    }


    public function custom_import_wholesaler_csv_process(Request $request)
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 3600);
        $csvdata = CsvData::find($request->csv_data_file_id);
        $start = 1;
        if ($csvdata->csv_header == 1) {
            $start = null;
        }
        try {
            $import = new WholesalersImport($request, $start);

            $import->import(base_path('storage/app/uploads/' . $request->filename));

            //delete the the csv file from the storage
            Storage::delete('uploads/' . $request->filename);
            //delete the the csv data from the csvdata table
            $csvdata->delete();

            return redirect()->route('admin.config.wholesaler_import')->with(['success' => "Wholesaler's Imported  Successfully"]);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return redirect()->route('admin.config.wholesaler_import')->withErrors(["Something went wrong. Please download excel template to countercheck column Mappings are correct."]);
        }
    }



    public function wholesaler_import_template()
    {
        $file = public_path() . "/$this->wholesaler_import_template_file";

        return Response::download($file, 'WholesalersImportTemplate.xlsx');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wholesaler_import_process(Request $request)
    {
        ini_set("max_execution_time", 3600);

        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'im-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $created_count = 0;

        $url = base_path('storage/app/uploads/' . $filename);
        $wholesalers = Excel::selectSheetsByIndex(0)->load($url, function ($reader) {
        })->get()->toArray();


        foreach ($wholesalers as $key => $wholesaler) {

            $name = current($wholesaler);

            $wholesaler_record = Wholesaler::updateOrCreate(
                [
                    'registration_number' => $wholesaler['business_registration_number']
                ],
                [
                    'business_name' => $name,
                    'slug' => str_slug(trim($wholesaler['business_registration_number'])),
                    'postal_address' => $wholesaler['postal_address'],
                    'physical_address' => $wholesaler['physical_address'],
                    'telephone' => $wholesaler['phone_number'],
                    'email' => $wholesaler['account_email_address'],
                    'pin' => $wholesaler['pin_number'],
                    'contact_name' => $wholesaler['contact_person'],
                    'contact_position' => $wholesaler['contact_person_position'],

                    'financial_controller' => null,
                    'ownership_type' => null,
                    'ownership_type_extra' => null,
                    'registration_number' => $wholesaler['business_registration_number'],
                    'business_nature' => null,
                    'annual_item_value' => null,
                    'credit_amount' => null,
                    'bank_id' => null,
                    'bank_branch_id' => null,
                    'bank_account_name' => null,
                    'bank_account_no' => null,
                    'financial_contact_person' => null,
                    'financial_contact_phone' => null,
                    'status' => 1,
                ]
            );


            $wasCreated = $wholesaler_record->wasRecentlyCreated;

            if ($wasCreated) {

                $created_count++;
            }
        }

        self::cache();

        flash_message('success', "Wholesaler Import Process Success - Check wholesalers listing to confirm changes </br> $created_count " . str_plural('record', $created_count) . ' created');

        return view('admin.config.wholesaler_import');
    }

    public function category_visibility_discount_import()
    {

        return view('admin.config.category_visibility_discount_import');
    }


    public function wholesaler_category_discount_download()
    {
        ini_set("max_execution_time", 3600);
        return Excel::download(new CategoryVisibilityExport(), 'CategoryVisibilityDiscountRates-' . date('d-m-y h:i') . '.xlsx');
    }


    public function category_visibility_discount_process(Request $request)
    {
        ini_set("max_execution_time", 3600);

        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'cvd-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);

        Excel::import(new CategoryVisibilityImport($url), $url);
        // self::cache();

        return redirect()->route('admin.config.category_visibility_discount_import')->with(['success' => "Category Visibility and Discount Data has been updated - Changes will reflect in 5 minutes"]);
    }


    public static function cache()
    {
        CacheBuildJob::dispatch();
    }

    public function inventory_download()
    {
        $filename = 'PricingImport-' . date('d-m-y h:i') . '.xlsx';
        $batch=Bus::batch([
            new PricingExportJob($filename)
        ])->dispatch();

        // dd($batch->id);

        return response()->json(['message'=>'Successfully Started Exporting Process','batchid'=> $batch->id,'filename'=>$filename], 200);
    }

    public function inventroy_pricing_update(Request $request)
    {
        $this->validate($request, [
            'upload' => 'required|mimes:xlsx'
        ]);

        $upload = $request->file('upload');
        $filename = 'pcwd-' . random_unique() . '.' . $upload->getClientOriginalExtension();

        Storage::disk('local')->putFileAs(
            'uploads',
            $upload,
            $filename
        );

        $url = base_path('storage/app/uploads/' . $filename);
        $batch = Bus::batch([
            new PriceImportJob($url)
        ])->dispatch();
        return response()->json(['message' => 'Successfully Started Exporting Process', 'batchid' => $batch->id], 200);
    }

    public function axios_attribute_values(Request $request, $id)
    {
        if ($request->ajax()) {
            $attributes = InventoryAttribute::find($id)->options;
            return response()->json(['values' => $attributes]);
        }
    }
}
