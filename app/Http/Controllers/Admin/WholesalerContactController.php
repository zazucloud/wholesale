<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\WholesalerContact;
use App\Http\Controllers\BaseTemplateController;

class WholesalerContactController extends BaseTemplateController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WholesalerContact  $wholesalerContact
     * @return \Illuminate\Http\Response
     */
    public function show(WholesalerContact $wholesalerContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WholesalerContact  $wholesalerContact
     * @return \Illuminate\Http\Response
     */
    public function edit(WholesalerContact $wholesalerContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WholesalerContact  $wholesalerContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WholesalerContact $wholesalerContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WholesalerContact  $wholesalerContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(WholesalerContact $wholesalerContact)
    {
        //
    }
}
