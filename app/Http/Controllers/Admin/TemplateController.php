<?php

namespace App\Http\Controllers\Admin;

use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseTemplateController;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class TemplateController extends BaseTemplateController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('permission:templates-view|templates-create|templates-edit|templates-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:templates-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:templates-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:templates-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $templates = Template::orderBy('id', 'ASC')->paginate(10);
        return view('admin.templates.index', compact('templates'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.templates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'template_header' => 'required|image|mimes:jpeg,png,jpg',
            'template_footer' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input = $request->all();
        $template = Template::create($input);

        if ($request->hasFile('template_header')) {

            $height = Image::make($request->company_logo)->height();
            $width = Image::make($request->company_logo)->width();


            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('template_header')->extension();
                $template->addMediaFromRequest('template_header')->usingFileName($newFileName)->toMediaCollection('templateHeader');

            }

        }

        if ($request->hasFile('template_footer')) {


            $height = Image::make($request->company_logo)->height();
            $width = Image::make($request->company_logo)->width();


            if($height > 0 || $width > 0) {
                $newFileName = Str::random(20) . '.' . $request->file('template_footer')->extension();
                $template->addMediaFromRequest('template_footer')->usingFileName($newFileName)->toMediaCollection('templateFooter');

            }

        }

        return redirect()->route('templates.index')->with('success', 'Template created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $template = Template::find($id);

        return view('admin.templates.show', compact('template'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        //
        $template = Template::find($template->id);

        return view('admin.templates.edit', compact('template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $input = $request->all();
        $template = Template::find($id);
        $template->update($input);

        if ($request->hasFile('template_header')) {

            foreach ($template->getMedia('templateHeader')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }
            $template->addMedia($request->template_header)->toMediaCollection('templateHeader');
        }

        if ($request->hasFile('template_footer')) {

            foreach ($template->getMedia('templateFooter')->pluck('id') as $key => $value) {
                $media = Media::find($value);
                $model = $media->model_type::find($media->model_id);
                $model->deleteMedia($media->id);
            }
            $template->addMedia($request->template_footer)->toMediaCollection('templateFooter');
        }

        return redirect()->route('templates.index')->with('success', 'Template updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Template::find($id)->delete();
        return redirect()->route('templates.index')
            ->with('success', 'Template deleted successfully');
    }
}
