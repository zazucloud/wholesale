<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryMobileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);
        $getmedia = $this->getFirstMedia("inventory");
        $array['image'] = (($getmedia) ? $getmedia->getUrl() : asset('images/noimage.jpeg'));
        $array['imgstatus'] = (($getmedia) ? true : false);
        return $array;
    }
}
