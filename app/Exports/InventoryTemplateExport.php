<?php

namespace App\Exports;

use App\Models\Inventory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InventoryTemplateExport implements WithHeadings, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    // public function collection()
    // {
    //     return Inventory::all();
    // }

    public function headings(): array
    {
        return [
            'warehouse',
            'product_name',
            'item_number',
            'description',
            'color',
            'brand',
            'sub_category',
            'price',
            'quantity',
            'productionitem',
            // 'crossed_price',
            // 'visibility',
            // 'status',
            // 'is_featured'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
