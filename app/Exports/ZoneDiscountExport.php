<?php

namespace App\Exports;

use App\Models\ZoneDiscounts;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class ZoneDiscountExport extends DefaultValueBinder implements WithProperties, FromView, ShouldAutoSize, WithCustomValueBinder
{
    use Exportable;
    private $categories, $zone;
    /**
     * @return \Illuminate\Support\Collection
     */
    public function properties(): array
    {
        return [
            'creator'        => config('app.name'),
            'title'          => 'Custom Discounts',
            'company'        => config('app.name'),
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        return parent::bindValue($cell, $value);
    }

    public function __construct($categories, $zone)
    {
        $this->categories = $categories;
        $this->zone = $zone;
    }

    public function view(): View
    {
        $current_zonne_prices = $this->zone->discounts;
        $results = [];
        foreach ($this->categories as $category) {

            $items = $category->api_inventory;
            $items_count = $items->count();


            if ($items_count > 0) {

                foreach ($items as $item_loop_key => $item) {

                    $curr_price = $current_zonne_prices->first(function ($value, int $key) use($item){
                            return $value->inventory_id == $item->id;
                        });

                    $category_label = ($item_loop_key == 0) ? $category->name : '';

                    $inventory = $item;

                    $res = [

                        'category' => $category_label,
                        'sku' => $inventory->sku,
                        'itemname' => $inventory->name,
                        'basePrice' => $inventory->price,
                        'regionPrice' => $curr_price->custom_discount ?? null,
                        'rcd_id' => strval($inventory->id)
                    ];
                    array_push($results, $res);
                }
            }
        }

        return view('admin.wholesalers.excel.zonediscounts', [
            'results' => $results,
        ]);
    }
}
