<?php

namespace App\Exports;

use App\Models\Wholesale\Wholesaler;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class WholesalerTemplateExport implements WithHeadings, ShouldAutoSize, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'business_name',
            'postal_address',
            'physical_address',
            'telephone',
            'email',
            'pin',
            'contact_name',
            'contact_position',
            'customer_code',
            'county',
            'staffemail',
            'credit_mode'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
