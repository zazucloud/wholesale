<?php

namespace App\Exports;

use App\Models\InventoryCategory;
use App\Models\Wholesale\Wholesaler;
use App\Models\Wholesale\WholesalerCategoryDiscount;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithProperties;
use Illuminate\Contracts\View\View;

class CategoryVisibilityExport implements WithProperties,FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function properties(): array
    {
        return [
            'creator'        => config('app.name'),
            'title'          => 'CategoryVisibilityDiscountRates',
            'company'        => config('app.name'),
        ];
    }

    public function view(): View
    {
        $data = [];
        $wholesalers = Wholesaler::all();
        $categories = InventoryCategory::active()->get();

        foreach ($wholesalers as $key => $wholesaler) {

            $visible_categories = $wholesaler->categories()->get()->pluck('id')->toArray();

            foreach ($categories as $category_loop_key => $category) {

                $wholesaler_label = ($category_loop_key === 0) ? $wholesaler->business_name : '';

                $build_array = [
                    'wholersaler' => $wholesaler_label,
                    'category' => $category->name,
                    'category_visibility' => in_array($category->id, $visible_categories) ? 'Visible' : 'Hidden',
                    'percentage' => WholesalerCategoryDiscount::discount($wholesaler->id, [$category->id]),
                    'rcd_id' => $wholesaler->id . '.' . $category->id
                ];
                array_push($data,$build_array);

            }
        }
        return view('exports.excel.category_discounts_visibility', [
            'results' => $data,
        ]);
    }
}
