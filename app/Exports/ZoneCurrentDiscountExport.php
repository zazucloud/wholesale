<?php

namespace App\Exports;

use App\Models\ZoneDiscounts;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ZoneCurrentDiscountExport  implements ShouldAutoSize, WithMapping, FromQuery, WithHeadings, WithChunkReading
{
    private $zone, $mode, $wholesaler;
    public function __construct($zone, $mode = null, $wholesaler = null)
    {
        $this->zone = $zone;
        $this->mode = $mode;
        $this->wholesaler = $wholesaler;
    }
    public function query()
    {
        return ZoneDiscounts::query()->with("inventory")->where('zone_id', $this->zone);

    }

    public function map($inventory): array
    {
        if (!is_null($this->mode)) {
            return [

                $inventory->inventory->sku,
                $inventory->inventory->name,
                $inventory->custom_discount,
                $inventory->inventory->price,
                strval($inventory->inventory_id)
            ];
        } else {
            return [
                $inventory->inventory->sku,
                $inventory->inventory->name,
                $inventory->custom_discount,
                strval($inventory->inventory_id)
            ];
        }
    }


    public function headings(): array
    {
        if (!is_null($this->mode)) {
            return [
                'sku',
                'itemname',
                'discount',
                'basePrice',
                'rcd_id'
            ];
        } else {
            return [
                'sku',
                'itemname',
                'basePrice',
                'rcd_id'
            ];
        }
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
