<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithProperties;

class DiscountsExport implements WithProperties,FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function properties(): array
    {
        return [
            'creator'        => config('app.name'),
            'title'          => 'Custom Discounts',
            'company'        => config('app.name'),
        ];
    }

    public function __construct($data,$wholesaler)
    {
        $this->data = $data;
        $this->wholesaler=$wholesaler;
    }

    public function view(): View
    {
        $results=[];

        foreach ($this->data as $category_loop_key => $visible_category) {

            $category=$visible_category->categories;
            $items = $category->items2;
            $items_count = $items->count();

            if ($items_count > 0) {

                foreach ($items as $item_loop_key => $item) {

                    $category_label = ($item_loop_key == 0) ? $category->name : '';

                    $inventory = $item->inventory;

                    $item_discount = wholesaler_item_discount($this->wholesaler->id, $inventory);

                    $res = [
                        'category' => $category_label,
                        'sku' => $inventory->sku,
                        'itemname' => $inventory->name,
                        'percentage' => $item_discount,
                        'basePrice' => $inventory->price,
                        'rcd_id' => $inventory->id
                    ];
                    array_push($results,$res);

                }
            }
        }
        // dd($results);
        return view('admin.wholesalers.excel.discounts', [
            'results' => $results,
        ]);
    }
}
