<?php

namespace App\Exports;

use App\Models\Inventory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Http\Request;
use App\Models\Warehouses;


class InventoryNoImageExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{


    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        // dd($this->request->all());

        $search = $this->request->search_name;
        $color = $this->request->color;
        $warehouse_id = $this->request->warehouse_id;
        $checkImage = $this->request->checkImage;


        // $warehouses = Warehouses::all()->pluck('name', 'id');
        // $inv = Inventory::doesntHave('picture')->get();

        // dd($inv->count());


        $inventory = Inventory::when($warehouse_id, function ($query) use ($warehouse_id) {
            return $query->where('warehouse_id', $warehouse_id);
        })
            ->when($search, function ($query) use ($search) {

                return  $query->where('name', 'LIKE', '%' . $search . '%')
                    // ->orwhere('itemnumber', 'LIKE', '%' . $search . '%')
                    ->orwhere('sku', 'LIKE', '%' . $search . '%');
            })
            ->when($color, function ($query) use ($color) {
                return  $query->where('color', 'LIKE', '%' . $color . '%');
            })
            ->when($checkImage == "on", function ($query) use ($checkImage) {
                return $query->doesntHave('picture');
            })
            ->get();


        return $inventory;
    }

    public function headings(): array
    {
        return [
            'Warehouse',
            'SKU',
            'Name',
        ];
    }

    public function map($inventory): array
    {
        return [
            $inventory->warehouse ? $inventory->warehouse->name : '',
            $inventory->sku,
            $inventory->name,
        ];
    }
}
