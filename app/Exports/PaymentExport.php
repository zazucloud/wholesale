<?php

namespace App\Exports;

use App\Models\Payment;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PaymentExport implements ShouldAutoSize, WithMapping, FromQuery, WithHeadings
{

    public function query()
    {
        return Payment::query()->with([
            "creator",
            "wholesaler"
        ]);
    }

    public function map($payment): array
    {
        return [

            $payment->wholesaler->business_name,
            $payment->order_id,
            $payment->amount,
            $payment->reference,
            $payment->channel,
            $payment->receipt_no,
            $payment->creator->name,
            $payment->comment,
            $payment->status,
            $payment->confirmed,


        ];
    }


    public function headings(): array
    {
        return [
            'Wholesaler',
            'Order No.',
            'Amount',
            'Reference',
            'Channel',
            'Receipt No.',
            'Created By',
            'Comment',
            'Status',
            'Confirmed',
        ];
    }
}
