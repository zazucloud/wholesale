<?php

namespace App\Exports;

use App\Models\Inventory;
use App\Models\Pricing;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithProperties;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PricingExport implements WithProperties, FromView,ShouldAutoSize
{

    public function properties(): array
    {
        return [
            'creator'        => config('app.name'),
            'title'          => 'Custom Pricing',
            'company'        => config('app.name'),
        ];
    }

    public function __construct($wholesalers,$list)
    {
        $this->list = $list;
        $this->wholesalers = $wholesalers;

    }

    public function view(): View
    {
        return view('exports.excel.pricing-state', [
            'wholesalers' => $this->wholesalers,
            'inventory_list' =>$this->list
        ]);
    }
}
