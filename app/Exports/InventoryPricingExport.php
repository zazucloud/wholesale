<?php

namespace App\Exports;

use App\Models\Inventory;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class InventoryPricingExport implements FromQuery,WithHeadings,WithChunkReading
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return Inventory::query()->select('name','sku','itemnumber','price as regular_price','crossed_price as sales_price');
    }

    public function headings() : array
    {
        return ['name','sku','itemnumber','regular_price','sales_price'];
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
