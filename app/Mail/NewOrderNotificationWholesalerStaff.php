<?php

namespace App\Mail;

use App\Models\Order;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewOrderNotificationWholesalerStaff extends Mailable
{
    use Queueable, SerializesModels;
    public $data,$pdf_stream;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data,$pdf_stream)
    {
        //
        $this->data = $data;
        $this->pdf_stream = $pdf_stream;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $order = Order::where('id', $this->data->order_id)->first();



        return $this->subject('Order Notification - ' . $order->face_id . ' - ' . config('app.name'))
            ->view('emails.new_order_wholesaler_staff_notification')->with([
                'order_number' => $order->face_id,
                'wholesaler_name' => $this->data->wholesaler_name,
                'name' => $this->data->name,
            ])->attach(storage_path('app/tempordersheets/' . $this->pdf_stream));
    }
}
