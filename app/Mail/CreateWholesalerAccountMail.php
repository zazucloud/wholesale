<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreateWholesalerAccountMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your '. env('APP_NAME').' Wholesaler Account is Ready!')
            ->view('emails.created_wholesaler_account')->with([
                'login_url' => route('login'),
                'logo' => '', /*TODO:: add logo*/
                'username' => $this->data->username,
                'password' => $this->data->password,
                'name' => $this->data->name
            ]);
    }
}
