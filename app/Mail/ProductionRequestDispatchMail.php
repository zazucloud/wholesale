<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductionRequestDispatchMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data,$pdf_stream;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data,$pdf_stream)
    {
        $this->data = $data;
        $this->pdf_stream = $pdf_stream;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        /*TODO:: implement markdown*/

        return $this->subject('Production Request - ' . $this->data->production_request->request_tag)->view('emails.manufacturer_production_request')->with([
            'name' => $this->data->name,
        ])->attach(storage_path('app/tempdfs/' . $this->pdf_stream));
    }
}
