<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuotationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data, $pdf_stream, $inquiry_no;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $pdf_stream, $inquiry_no)
    {
        $this->data = $data;
        $this->pdf_stream = $pdf_stream;
        $this->inquiry_no = $inquiry_no;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Your Quotation - '  . ' - ' . config('app.name'))
        ->view('emails.new_order_wholesaler_notification')->with([
            'name' => $this->data->wholesaler_name,
            'number' => $this->inquiry_no
        ])->attach(storage_path('app/tempordersheets/' . $this->pdf_stream));
        // return $this->view('view.name');
    }
}
