<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAccountCreatedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your '. env('APP_NAME').' '.$this->data->account_type.' Account is Ready!')
            ->view('emails.new_account_created')->with([
                'login_url' => route('login'),
                'logo' => '', /*TODO:: add logo*/
                'username' => $this->data->username,
                'password' => $this->data->password,
                'name' => $this->data->name
            ]);
    }
}
