<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailOrder extends Mailable
{
    use Queueable, SerializesModels;
    public $data, $pdf_stream;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $pdf_stream)
    {
        $this->data = $data;
        $this->pdf_stream = $pdf_stream;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Order - '  . ' - ' . config('app.name'))
            ->view('emails.new_order_wholesaler_notification')->with([
                'name' => $this->data->wholesaler_name,
                'ordertag' => $this->data->ordertag,
            ])->attach(storage_path('app/tempordersheets/' . $this->pdf_stream));
    }
}
