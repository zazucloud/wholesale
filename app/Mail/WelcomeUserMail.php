<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeUserMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $userInfo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userInfo)
    {
        //  
        $this->userInfo = $userInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Welcome To Zazu Top Manufacturing';

        return $this->view('emails.welcome')->subject($subject)->with(["data" => $this->userInfo]);
    }
}
