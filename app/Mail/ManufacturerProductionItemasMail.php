<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ManufacturerProductionItemasMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name, $Items;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $name,$Items)
    {
        $this->name = $name;
        $this->Items = $Items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $no = 0;
        return $this->subject('Items to be Produced - '  . ' - ' . config('app.name'))
        ->view('emails.manufacturerProductionItems')->with([
            'productionItems' => $this->Items,
            'no' => $no,
            'name' => $this->name,
        ]);

    }
}
