<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductionRequestStatusUpdate extends Mailable
{
    use Queueable, SerializesModels;

    public $staff, $production_request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($staff, $production_request)
    {
        $this->staff = $staff;
        $this->production_request = $production_request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your ' . env('APP_NAME') . ' Production Request with tag -' . $this->production_request->request_tag . ' is Ready!')
            ->view('emails.production_status_update')->with([
                'username' => $this->staff,
                'production request' => $this->production_request,
            ]);
    }
}
