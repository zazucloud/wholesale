<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCommentUpdateMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data, $pdf_stream;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $pdf_stream)
    {
        $this->data = $data;
        $this->pdf_stream = $pdf_stream;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Order With Updated Comments- '  . ' - ' . config('app.name'))
            ->view('emails.order_comment_update')->with([
                'name' => $this->data->wholesaler_name,
            ])->attach(storage_path('app/tempordersheets/' . $this->pdf_stream));
    }
}
