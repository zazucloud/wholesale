<?php

namespace App\Console\Commands;

use App\Jobs\LowstockProductionRequestJob;
use Carbon\Carbon;
use Illuminate\Console\Command;

class lowStockCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lowstock:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check low stock inventory items and create production requests for them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $job = (new LowstockProductionRequestJob())->delay(Carbon::now()->addSeconds(1));
        dispatch($job);
    }
}
