<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetSkuCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:reset-sku-check';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset sku_check column to 0 in the inventory table';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        DB::table('inventory')->update(['sku_check' => 0]);

        $this->info('SKU check values reset successfully.');
        return 0;

    }
}
