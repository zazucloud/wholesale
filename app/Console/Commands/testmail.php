<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class testmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Mail Mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Mail::raw('Text to e-mail', function ($message) {
            $message->to('elviskane390@gmail.com');
            $message->subject('Laravel Testing Mail with Attachment');
            $message->setBody('Hi, welcome user!');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
}
