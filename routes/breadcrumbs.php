<?php
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

Breadcrumbs::for('wholesalers', function (BreadcrumbTrail $trail) {
    $trail->push('Wholesalers', route('admin.wholesalers.index'));
});

Breadcrumbs::for('wholesalersHome', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Home', route('admin.wholesalers.show', $slug));
});

Breadcrumbs::for('wholesalersOrders', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Orders', route('wholesalers.orders', $slug));
});


Breadcrumbs::for('wholesalersAccounts', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Accounts', route('wholesalers.accounts', $slug));
});


Breadcrumbs::for('wholesalersPayments', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Payments', route('wholesalers.payments', $slug));
});

Breadcrumbs::for('wholesalersStaff', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Staff Assignment', route('wholesaler.staff_assignment', $slug));
});


Breadcrumbs::for('wholesalersCatalogue', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Catalogue', route('wholesaler.catalog_visibility', $slug));
});
Breadcrumbs::for('wholesalersPdiscounts', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Product Discounts', route('wholesaler.custom_discount', $slug));
});


Breadcrumbs::for('wholesalersCdiscounts', function (BreadcrumbTrail $trail, $slug) {
    $trail->parent('wholesalers');
    $trail->push('Category Discount', route('wholesaler.discount_control', $slug));
});
