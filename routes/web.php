<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\StartController;
use App\Http\Controllers\AdminHomeController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\ZoneController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\CountyController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\InquiryController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\RetailerController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Admin\TemplateController;
use App\Http\Controllers\Admin\ManufacturerController;
use App\Http\Controllers\Admin\ConfigurationsController;
use App\Http\Controllers\BusBatchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [StartController::class, 'index'])->name('topmfg');

Route::get('/testviews', [StartController::class, 'testviews'])->name('testviews');

Auth::routes();

Route::post('/admin/logout', [LoginController::class, 'adminLogout'])->name('admin.logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/first/login/reset', [HomeController::class, 'firstLoginReset'])->name('home.first.login');
    Route::post('/first/login/update', [HomeController::class, 'firstLoginUpdate'])->name('home.first.update');
});

// Route::post('/uploadtemp', [\App\Http\Controllers\DropzoneController::class, 'DropzoneProcess'])->name('uploadtemp');
Route::get('/nopermissions', [StartController::class, 'nopermission'])->name('nopermissions');

Route::middleware(['auth.disabled', 'auth.first', 'auth'])->prefix('admin')->group(function () {
    Route::get('dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('ajax/batch/status/{batchid}', [BusBatchController::class, 'checkBatchStatus'])->name('bus.batchstatus');
    Route::post('ajax/inventory/pricing/download', [ConfigurationsController::class, 'inventory_download'])->name('admin.config.inventory_download_pricing');
    Route::get('donwload/file/{filename}', [BusBatchController::class, 'downloadFile'])->name('bus.downloadFile');


    Route::prefix('inventory')->group(function () {

        Route::get('', [App\Http\Controllers\Admin\InventoryController::class, 'index'])->name('admin.inventory.index');
        Route::get('create', [App\Http\Controllers\Admin\InventoryController::class, 'create'])->name('admin.inventory.create');
        Route::post('create', [App\Http\Controllers\Admin\InventoryController::class, 'store'])->name('admin.inventory.store');

        Route::get('edit/{inventory}', [App\Http\Controllers\Admin\InventoryController::class, 'edit'])->name('inventory.edit');
        Route::post('edit/{inventory}', [App\Http\Controllers\Admin\InventoryController::class, 'update'])->name('inventory.update');
        Route::post('delete/{inventory}', [App\Http\Controllers\Admin\InventoryController::class, 'delete'])->name('inventory.delete');
        Route::get('inventory/images/{inventory}', [App\Http\Controllers\Admin\InventoryController::class, 'inventory_view_gallery'])->name('inventory.inventory_view_gallery');
        Route::post('inventory/export/no_images', [App\Http\Controllers\Admin\InventoryController::class, 'exportInvetory'] )->name('inventory.export.no_images');

        Route::get('view/{inventory}', [App\Http\Controllers\Admin\InventoryController::class, 'view'])->name('inventory.view');
        Route::get('ajax/getinventory/dropdown', [App\Http\Controllers\Admin\InventoryController::class, 'inventory_ajax_dropdown'])->name('inventory.ajax.dropdown');
        Route::get('ajax/getinventory/specific', [App\Http\Controllers\Admin\InventoryController::class, 'inventory_ajax_specific'])->name('inventory.ajax.specific');
        Route::get('ajax/getinventory/attributes', [App\Http\Controllers\Admin\InventoryController::class, 'inventory_ajax_attributes'])->name('inventory.ajax.attributes');
    });

    Route::prefix('orders')->group(function () {
        Route::get('/', [App\Http\Controllers\Admin\OrderController::class, 'index'])->name('orders.index');
        Route::get('pending', [App\Http\Controllers\Admin\OrderController::class, 'pendingOrders'])->name('orders.pending');
        Route::get('approved', [App\Http\Controllers\Admin\OrderController::class, 'approvedOrders'])->name('orders.approved');
        Route::get('processing', [App\Http\Controllers\Admin\OrderController::class, 'processingOrders'])->name('orders.processing');
        Route::get('processed', [App\Http\Controllers\Admin\OrderController::class, 'processedOrders'])->name('orders.processed');
        Route::get('paid', [App\Http\Controllers\Admin\OrderController::class, 'partiallyShippedOrders'])->name('orders.paid');
        Route::get('fully-shipped', [App\Http\Controllers\Admin\OrderController::class, 'fullyShippedOrders'])->name('orders.fully-shipped');
        Route::get('partially-shipped', [App\Http\Controllers\Admin\OrderController::class, 'paidShippedOrders'])->name('orders.partially-shipped');

        Route::get('create', [App\Http\Controllers\Admin\OrderController::class, 'create'])->name('orders.create');
        Route::post('create', [App\Http\Controllers\Admin\OrderController::class, 'store'])->name('orders.store');
        Route::post('check/stock', [App\Http\Controllers\Admin\OrderController::class, 'checkstock'])->name('orders.checkstock');
        Route::get('view/{order}', [App\Http\Controllers\Admin\OrderController::class, 'view'])->name('orders.view');
        Route::post('comment', [App\Http\Controllers\Admin\OrderController::class, 'comment_store'])->name('orders.comment.store');
        Route::post('updatestatus', [App\Http\Controllers\Admin\OrderController::class, 'update_order_status'])->name('orders.change.status');

        Route::post('{order}/cancel', [App\Http\Controllers\Admin\OrderController::class, 'cancel'])->name('orders.cancel');
        Route::get('ordersheet/{order}', [App\Http\Controllers\Admin\OrderController::class, 'invoice_download'])->name('orders.invoice_download');
        Route::post('order/mail_pdf', [App\Http\Controllers\Admin\OrderController::class, 'mail_order'])->name('order.mail');
        Route::patch('order/comment', [App\Http\Controllers\Admin\OrderController::class, 'update_comment'])->name('order.update.comment');
    });

    Route::prefix('production')->group(function () {
        Route::get('', [App\Http\Controllers\Admin\ProductionController::class, 'index'])->name('admin.production.index');
        Route::get('pending', [App\Http\Controllers\Admin\ProductionController::class, 'pending'])->name('admin.production.pending');
        Route::get('inproduction', [App\Http\Controllers\Admin\ProductionController::class, 'inproduction'])->name('admin.production.inproduction');
        Route::get('completed', [App\Http\Controllers\Admin\ProductionController::class, 'completed'])->name('admin.production.completed');
        Route::get('delivered', [App\Http\Controllers\Admin\ProductionController::class, 'delivered'])->name('admin.production.delivered');
        Route::get('create', [App\Http\Controllers\Admin\ProductionController::class, 'create'])->name('admin.production.create');
        Route::get('request/view/{production_request}', [App\Http\Controllers\Admin\ProductionController::class, 'request_view'])->name('admin.productionrequest.view');
        Route::get('drafts', [App\Http\Controllers\Admin\ProductionController::class, 'drafts'])->name('admin.production.drafts');

        Route::get('{production_request}/step-1', [App\Http\Controllers\Admin\ProductionController::class, 'creation_step_1'])->name('admin.production.creation_step_1');
        Route::get('{production_request}/step-2', [App\Http\Controllers\Admin\ProductionController::class, 'creation_step_2'])->name('admin.production.creation_step_2');
        Route::get('{production_request}/step-3', [App\Http\Controllers\Admin\ProductionController::class, 'step_3'])->name('admin.production.step_3');

        Route::post('create', [App\Http\Controllers\Admin\ProductionController::class, 'store_step_1'])->name('admin.production.store_step_1');
        Route::post('step-2', [App\Http\Controllers\Admin\ProductionController::class, 'store_step_2'])->name('admin.production.store_step_2');
        Route::post('step-3', [App\Http\Controllers\Admin\ProductionController::class, 'push_production_request'])->name('admin.production.push_request');
        Route::post('add-item', [App\Http\Controllers\Admin\ProductionController::class, 'add_item'])->name('admin.production.add_item');


        Route::get('ajax/staff/specific', [App\Http\Controllers\Admin\ProductionController::class, 'staff_ajax_specific'])->name('productionrequest.ajax.specific');

        Route::prefix('production-request')->group(function () {
            Route::post('comment', [App\Http\Controllers\Admin\ProductionController::class, 'store_comment'])->name('production_request.comment.store');
            Route::post('update', [App\Http\Controllers\Admin\ProductionController::class, 'status_update'])->name('production_request.status_update');
            Route::post('delete_comment', [App\Http\Controllers\Admin\ProductionController::class, 'delete_comment'])->name('production_request.delete_comment');
            Route::post('request-item-update', [App\Http\Controllers\Admin\ProductionController::class, 'request_item_update'])->name('production_request.request.item_update');
        });

        Route::patch('update/comment', [App\Http\Controllers\Admin\ProductionController::class, 'update_comment'])->name('production_request.update.comment');
    });

    Route::prefix('retailers')->group(function () {
        Route::get('', [App\Http\Controllers\Admin\RetailerController::class, 'index'])->name('admin.retailers.index');
        Route::get('create', [App\Http\Controllers\Admin\RetailerController::class, 'create'])->name('admin.retailers.create');
        Route::post('save', [App\Http\Controllers\Admin\RetailerController::class, 'store'])->name('admin.retailers.store');
        Route::get('edit/{id}', [App\Http\Controllers\Admin\RetailerController::class, 'edit'])->name('admin.retailers.edit');
        Route::patch('update/{id}', [App\Http\Controllers\Admin\RetailerController::class, 'update'])->name('admin.retailers.update');
        Route::delete('delete/{id}', [App\Http\Controllers\Admin\RetailerController::class, 'destroy'])->name('admin.retailers.destroy');
        Route::get('ajax/dropdown', [App\Http\Controllers\Admin\RetailerController::class, 'retailers_ajax_dropdown'])->name('admin.retailers.ajax.dropdown');
    });

    Route::prefix('configurations')->group(function () {

        /**
         * Brand Configuration Settings
         */
        Route::get('brands', [ConfigurationsController::class, 'brands'])->name('admin.config.brands');
        Route::post('brands', [ConfigurationsController::class, 'brands_store'])->name('admin.config.brands.store');
        Route::post('brands/update/{id}', [ConfigurationsController::class, 'brand_update'])->name('admin.config.brand.update');
        Route::delete('brands/delete/{id}', [ConfigurationsController::class, 'brand_delete'])->name('admin.config.brand.delete');
        Route::get('attributes/fetchvalues/{id}', [ConfigurationsController::class, 'axios_attribute_values'])->name('admin.config.ajax_attribute_values');


        /**
         * Inventory Import Configuration Settings
         */
        Route::get('inventory-import', [ConfigurationsController::class, 'import_inventory'])->name('admin.config.inventory_import');
        Route::get('inventory-template', [ConfigurationsController::class, 'template_inventory'])->name('admin.config.inventory_template');
        Route::post('inventory/upload-csv', [ConfigurationsController::class, 'import_inventory_csv'])->name('admin.config.inventory_import_process');
        Route::post('inventory-custom-import-parse', [ConfigurationsController::class, 'custom_import_Inventory'])->name('admin.config.custom_import_Inventory');
        Route::post('inventory-custom-import-process', [ConfigurationsController::class, 'custom_import_Inventory_process'])->name('admin.config.custom_import_Inventory_process');

        // Route::get('inventory/pricing/download', [ConfigurationsController::class, 'inventory_download'])->name('admin.config.inventory_download_pricing');
        /**
         * Inventory Categories Configurations
         */
        Route::get('inventory-categories', [ConfigurationsController::class, 'inventory_categories'])->name('admin.config.inventory_categories');
        Route::post('inventory-categories', [ConfigurationsController::class, 'inventory_categories_store'])->name('admin.config.inventory_categories_store');
        Route::post('inventory-categories/edit/{id}', [ConfigurationsController::class, 'inventory_categories_update'])->name('admin.config.inventory_categories.edit');
        Route::post('inventory-categories/delete/{category}', [ConfigurationsController::class, 'inventory_categories_delete'])->name('admin.config.inventory_categories.delete');
        // Route::get('inventory-categories/delete/{category}', 'Admin\ConfigController@inventory_categories_delete')->name('config.inventory_categories.delete');

        /*
        * Inventory Attributes Configurations
        */
        Route::get('inventory-attributes', [ConfigurationsController::class, 'inventory_attributes'])->name('admin.config.inventory_attributes');
        Route::post('inventory-attributes', [ConfigurationsController::class, 'inventory_attributes_store'])->name('admin.config.inventory_attributes.store');
        Route::get('inv-attribute-options/{attribute}', [ConfigurationsController::class, 'inventory_attribute_manage'])->name('admin.config.inv-att.manage');
        Route::get('inv-attribute-option', [ConfigurationsController::class, 'inventory_attribute_option_store'])->name('admin.config.inv-att.store');
        // Route::get('credit-settings', [ConfigurationsController::class, 'credit_settings'])->name('admin.config.credit_settings');

        /**
         * Warehouse Configuration
         */

        Route::get('warehouses', [ConfigurationsController::class, 'warehouses'])->name('admin.config.warehouses');
        Route::post('warehouses', [ConfigurationsController::class, 'warehouses_store'])->name('admin.config.warehouses.store');
        Route::post('warehouses/update', [ConfigurationsController::class, 'warehouses_update'])->name('admin.config.warehouses.update');
        Route::get('wholesaler-import', [ConfigurationsController::class, 'wholesaler_import'])->name('admin.config.wholesaler_import');
        Route::get('wholesaler-template', [ConfigurationsController::class, 'wholesaler_template'])->name('admin.config.wholesaler_template');
        Route::post('wholesaler-import', [ConfigurationsController::class, 'import_wholesaler_csv'])->name('admin.config.wholesaler_import_process');
        //custom csv file parse
        Route::post('wholesaler-custom-import-parse', [ConfigurationsController::class, 'custom_import_wholesaler_csv'])->name('admin.config.wholesaler_custom_import_parse');
        Route::post('wholesaler-custom-import-process', [ConfigurationsController::class, 'custom_import_wholesaler_csv_process'])->name('admin.config.wholesaler_custom_import_process');

        Route::post('wholesaler-import-template', [ConfigurationsController::class, 'wholesaler_import_template'])->name('wholesaler_import_template');

        /**
         * Pricing Configuration
         */
        Route::get('pricing', [ConfigurationsController::class, 'pricing'])->name('admin.config.pricing');
        Route::get('pricing/{category}', [ConfigurationsController::class, 'category_pricing'])->name('admin.config.category_pricing');
        Route::post('pricing/{category}', [ConfigurationsController::class, 'category_pricing_process'])->name('admin.config.category_pricing_process');
        Route::post('pricing/update/bulk', [ConfigurationsController::class, 'inventroy_pricing_update'])->name('admin.config.inventroy_pricing_update');
        Route::post('category-pricing-import/{id}', [ConfigurationsController::class, 'category_discounts_process'])->name('admin.config.category-pricing-process');
        Route::get('category-pricing-download/{category}', [ConfigurationsController::class, 'category_discounts_download'])->name('admin.config.category-pricing-download');

        /**
         * Category Visibility & Discount Import
         */
        Route::get('category_visibility_discount_import', [ConfigurationsController::class, 'category_visibility_discount_import'])->name('admin.config.category_visibility_discount_import');
        Route::post('category_visibility_discount_process', [ConfigurationsController::class, 'category_visibility_discount_process'])->name('admin.config.category_visibility_discount_process');
        Route::get('wholesaler_category_discount_download', [ConfigurationsController::class, 'wholesaler_category_discount_download'])->name('admin.config.wholesaler_category_discount_download');
    });

    Route::prefix('manufacturers')->group(function () {

        Route::get('', [ManufacturerController::class, 'index'])->name('admin.manufacturers.index');
        Route::post('create', [ManufacturerController::class, 'store'])->name('admin.manufacturers.create');
        Route::post('update/{id}', [ManufacturerController::class, 'update'])->name('admin.manufacturers.update');
        Route::get('production_list/{manufacturer}', [ManufacturerController::class, 'production'])->name('admin.manufacturers.production_list');
        Route::get('category-items', [ManufacturerController::class, 'loadInventoryItems']);
        Route::post('/sync/productionlist', [ManufacturerController::class, 'update_production_list'])->name('admin.manufacturers.update_production_list');
        Route::delete('{id}', [ManufacturerController::class, 'delete'])->name('admin.manufacturers.delete');
    });

    Route::prefix('payments')->group(function () {
        Route::get('/', [PaymentController::class, 'index'])->name('payments.index');
        Route::get('create', [PaymentController::class, 'create'])->name('payments.create');
        Route::post('create', [PaymentController::class, 'store'])->name('payments.store');
        Route::get('view/{payment}', [PaymentController::class, 'show'])->name('payments.show');
        Route::patch('cancel/{payment}', [PaymentController::class, 'cancel'])->name('payments.cancel');
        Route::get('amount-to-words', [PaymentController::class, 'amount_to_words'])->name('payments.amount_to_words');
        Route::get('view/attachment/{payment}', [PaymentController::class, 'attachment_view'])->name('payments.attachment.view');

        Route::prefix('export')->group(function () {
            Route::get('csv', [PaymentController::class, 'exportPayments'])->name('payments.export.excel');
            Route::get('pdf', [PaymentController::class, 'printPayments'])->name('payments.export.pdf');
        });
    });

    Route::prefix('settings')->group(function () {

        Route::prefix('user')->group(function () {
            Route::post('disable', [UsersController::class, 'disableUser'])->name('users.disable');
            Route::post('enable', [UsersController::class, 'enableUser'])->name('users.enable');
        });

        Route::get('users', [SettingsController::class, 'fetchUsers'])->name('config.users');
        Route::get('roles', [SettingsController::class, 'fetchRoles'])->name('config.roles');
    });

    Route::get('users/ajax/dropdown', [UsersController::class, 'users_ajax_dropdown'])->name('users.ajax.dropdown');

    Route::prefix('quotations')->group(function () {
        Route::get('', [App\Http\Controllers\Admin\InquiryController::class, 'index'])->name('admin.inquiry.index');
        Route::get('/edit/{id}', [App\Http\Controllers\Admin\InquiryController::class, 'edit'])->name('admin.inquiry.edit');
        Route::get('/create', [App\Http\Controllers\Admin\InquiryController::class, 'create'])->name('admin.inquiry.create');
        Route::post('/store', [App\Http\Controllers\Admin\InquiryController::class, 'store'])->name('admin.inquiry.store');
        Route::post('/update', [App\Http\Controllers\Admin\InquiryController::class, 'update'])->name('admin.inquiry.update');
        Route::get('/view/{id}', [InquiryController::class, 'show'])->name('admin.inquiry.show');
        Route::get('/pdf/{inquiry}', [App\Http\Controllers\Admin\InquiryController::class, 'quotation_download'])->name('quotation.download');
        Route::post('/mail_quotation', [App\Http\Controllers\Admin\InquiryController::class, 'mail_quotation'])->name('quotation.mail');
        Route::patch('/expired_quotation', [App\Http\Controllers\Admin\InquiryController::class, 'expired_quotation'])->name('expired.quotation');
        Route::post('/quotation_to_order', [App\Http\Controllers\Admin\InquiryController::class, 'quotation_transfer'])->name('quotation.order');
        Route::get('/retrieve/{id}', [App\Http\Controllers\Admin\InquiryController::class, 'retrieveItems'])->name('quotation.ajax');
        Route::get('/search', [InquiryController::class, 'search'])->name('quotation.search');
    });

    Route::prefix('zones')->group(function () {
        Route::get('allocate/{zone}', [ZoneController::class, 'allocate'])->name('zones.allocate');
        Route::post('assign', [ZoneController::class, 'allocate_counties'])->name('zones.assign');
        Route::get('{zone}/custom_discount', [ZoneController::class, 'custom_discount'])->name('zones.custom_discount');
        Route::get('{id}/view/custom_discount', [ZoneController::class, 'retrieveZoneDiscounts'])->name('zones.view.custom_discount');
        Route::post('custom_discount/download', [ZoneController::class, 'custom_discount_download'])->name('zones.custom_discount_download');
        Route::post('{zone}/custom_discount', [ZoneController::class, 'custom_discount_update'])->name('zones.custom_discount_update');
        Route::get('{zone}/download_current_discount/{mode}/{wholesaler}', [ZoneController::class, 'download_current_discount'])->name('zones.download_current_discount');
        Route::post('current_discount/update', [ZoneController::class, 'current_zone_discount_update'])->name('zones.current_zone_discount_update');


    });


    Route::resource('zones', ZoneController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('users', UsersController::class);
    Route::resource('counties', CountyController::class);
    Route::resource('companies', CompanyController::class);
    Route::resource('templates', TemplateController::class);


    Route::prefix('company')->group(function () {
        Route::get('information', [CompanyController::class, 'edit'])->name('company.edit');
    });

    Route::prefix('search')->group(function () {
        Route::get('user', [UsersController::class, 'search'])->name('users.search');
        Route::get('zones', [ZoneController::class, 'search'])->name('zones.search');
        Route::get('counties', [CountyController::class, 'search'])->name('counties.search');
        Route::get('retailers', [RetailerController::class, 'search'])->name('retailers.search');
        Route::get('manufacturers', [ManufacturerController::class, 'search'])->name('manufacturers.search');
        Route::get('brands', [ConfigurationsController::class, 'search_brands'])->name('brands.search');
        Route::get('categories', [ConfigurationsController::class, 'search_inventory_categories'])->name('categories.search');
        Route::get('warehouses', [ConfigurationsController::class, 'search_warhouses'])->name('warehouses.search');
        Route::get('payments', [PaymentController::class, 'search'])->name('payments.search');
    });
});
