<?php

use App\Http\Controllers\API\Mobile\InventoryController;
use App\Http\Controllers\API\Mobile\WholesaleMobileLoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('guest')->group(function () {
    Route::post('wholesale/login', [WholesaleMobileLoginController::class,'login']);
    Route::post('wholesalerMobile/refreshtoken',    [WholesaleMobileLoginController::class, 'userRefreshToken']);

});

Route::middleware('auth:api')->prefix('wholesalerMobile')->group(function () {

    Route::get('categories/{wholesaler_id}', [InventoryController::class, 'categories']);
    Route::get('inventory/{category_id}', [InventoryController::class, 'wholesaleInventory']);
});



Route::group(['prefix' => 'Fieldsales', 'namespace' => 'App\Http\Controllers'], function ($api) {
    Route::get('wholesaler-listing',    'API\Fieldsales\WholesalerController@index');
    Route::get('inventory-listing',    'API\Fieldsales\InventoryController@index');

    Route::get('fetch/locations/{id}',    'API\Fieldsales\WholesalerController@location_ajax_specific');
    Route::get('fetch/templates',    'API\Fieldsales\WholesalerController@templates');

    Route::post('order-create',        'API\Fieldsales\OrderController@store');
    Route::post('check-excess',        'API\Fieldsales\OrderController@checkExcessApi');
    Route::get('fetch/orders/{id}',    'API\Fieldsales\OrderController@wholesalerOrders');
    Route::get('fetch/orderitems/{id}',    'API\Fieldsales\OrderController@wholesalerOrderItems');


    Route::post('sync', 'API\Fieldsales\WholesalerController@sync_clients_to_fieldsales');
});



Route::post('sync_inventory', [App\Http\Controllers\API\UpdateInventoryController::class, 'store'])->name('sync_inv');
