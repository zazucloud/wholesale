<?php

use App\Http\Controllers\Wholesaler\Auth\WholesalerUserLoginController;
use App\Http\Controllers\Wholesaler\ProductController;
use App\Http\Controllers\Wholesaler\WholesalerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Wholesaler Routes


Auth::routes();

Route::prefix('wholesaler')->group(function () {

    Route::get('/login', [WholesalerUserLoginController::class, 'showLoginForm'])->name('wholesale_user.loginForm');
    Route::post('/login', [WholesalerUserLoginController::class, 'login'])->name('wholesale_user.login');
    Route::get('/dashboard', [WholesalerController::class, 'dashboard'])->name('wholesaler.dashboard');

    Route::get('/products', [ProductController::class, 'index'])->name('wholesale.products.index');
    Route::get('/products/{id}', [ProductController::class, 'show'])->name('wholesale.products.show');
    Route::post('/add_to_cart', [ProductController::class, 'addToCart'])->name('wholesale.products.addToCart');
    Route::get('/cart_summary', [ProductController::class, 'cartSummary'])->name('wholesale.products.cartSummary');
    Route::get('/get/cartdetails', [ProductController::class, 'retrieveCart'])->name('wholesale.products.retrieveCart');
    Route::patch('/cart_quantity', [ProductController::class, 'cartQuantity'])->name('wholesale.products.cartQuantity');
    Route::get('/checkout', [ProductController::class, 'checkout'])->name('wholesale.products.checkout');
    Route::post('/completeorder', [ProductController::class, 'completeOrder'])->name('wholesale.products.completeOrder');
    Route::delete('/cartItemDelete', [ProductController::class, 'destroy'])->name('wholesale.products.delete');



    Route::post('/logout', [WholesalerUserLoginController::class, 'wholesalerLogout'])->name('wholesale_user.logout');
});


Route::middleware(['auth.disabled', 'auth.first', 'auth'])->group(
    function () {

        //admin wholesaler routes
        Route::prefix('wholesalers')->group(function () {
            Route::get('/', [App\Http\Controllers\Admin\WholesalerController::class, 'index'])->name('admin.wholesalers.index');
            Route::get('create', [App\Http\Controllers\Admin\WholesalerController::class, 'create'])->name('admin.wholesalers.create');
            Route::post('create', [App\Http\Controllers\Admin\WholesalerController::class, 'create_account'])->name('admin.wholesalers.create_account');
            Route::get('{wholesaler}', [App\Http\Controllers\Admin\WholesalerController::class, 'show'])->name('admin.wholesalers.show');
            Route::delete('{wholesaler}', [App\Http\Controllers\Admin\WholesalerController::class, 'delete'])->name('admin.wholesalers.delete');


            Route::get('{wholesaler}/orders', [App\Http\Controllers\Admin\WholesalerController::class, 'orders'])->name('wholesalers.orders');
            Route::get('{wholesaler}/payments', [App\Http\Controllers\Admin\WholesalerController::class, 'payments'])->name('wholesalers.payments');
            Route::get('{wholesaler}/accounts', [App\Http\Controllers\Admin\WholesalerController::class, 'accounts'])->name('wholesalers.accounts');

            Route::get('accounts/{wholesaler}/manage/{account}', [App\Http\Controllers\Admin\WholesalerController::class, 'account_manage_edit'])->name('wholesalers.account_manage');
            Route::post('{wholesaler}/accounts', [App\Http\Controllers\Admin\WholesalerController::class, 'account_update'])->name('wholesalers.account_update');

            Route::get('{wholesaler}/staff-assignment', [App\Http\Controllers\Admin\WholesalerController::class, 'staff_assignment'])->name('wholesaler.staff_assignment');
            Route::get('{wholesaler}/staff-assignment/edit', [App\Http\Controllers\Admin\WholesalerController::class, 'staff_assignment_edit'])->name('wholesaler.staff_assignment_edit');
            Route::post('{wholesaler}/staff-assignment', [App\Http\Controllers\Admin\WholesalerController::class, 'staff_assignment_process'])->name('wholesaler.staff_assignment_process');

            Route::get('{wholesaler}/catalogue-visibility', [App\Http\Controllers\Admin\WholesalerController::class, 'catalog_visibility'])->name('wholesaler.catalog_visibility');
            Route::post('{wholesaler}/catalogue-visibility', [App\Http\Controllers\Admin\WholesalerController::class, 'catalog_visibility_update'])->name('wholesaler.catalog_visibility_update');

            Route::get('{wholesaler}/discount_control', [App\Http\Controllers\Admin\WholesalerController::class, 'discount_control'])->name('wholesaler.discount_control');
            Route::post('{wholesaler}/discount_control', [App\Http\Controllers\Admin\WholesalerController::class, 'discount_control_update'])->name('wholesaler.discount_control_update');

            Route::get('{wholesaler}/custom_discount', [App\Http\Controllers\Admin\WholesalerController::class, 'custom_discount'])->name('wholesaler.custom_discount');
            Route::get('{wholesaler}/custom_discount/download', [App\Http\Controllers\Admin\WholesalerController::class, 'custom_discount_download'])->name('wholesaler.custom_discount_download');
            Route::post('{wholesaler}/custom_discount', [App\Http\Controllers\Admin\WholesalerController::class, 'custom_discount_update'])->name('wholesaler.custom_discount_update');

            Route::post('{wholesaler}/locations', [App\Http\Controllers\Admin\WholesalerController::class, 'location_insert'])->name('wholesalers.locations.insert');
            Route::patch('{wholesaler}/locations', [App\Http\Controllers\Admin\WholesalerController::class, 'location_update'])->name('wholesalers.locations.update');

            Route::post('add_directors', [App\Http\Controllers\Admin\WholesalerController::class, 'store_director'])->name('director.store');
            Route::patch('update_director', [App\Http\Controllers\Admin\WholesalerController::class, 'update_director'])->name('director.update');
            Route::delete('delete_director', [App\Http\Controllers\Admin\WholesalerController::class, 'delete_director'])->name('director.destroy');

            Route::post('{wholesaler}/accounts/create', [App\Http\Controllers\Admin\WholesalerController::class, 'account_store'])->name('wholesalers.account_store');
            Route::get('ajax/getwholesaler/locations', [App\Http\Controllers\Admin\WholesalerController::class, 'location_ajax_specific'])->name('wholesalers.location_ajax_specific');

            Route::get('{wholesaler}/edit/basic-info', [App\Http\Controllers\Admin\WholesalerController::class, 'edit_basic_info'])->name('wholesalers.edit_basic_info');
            Route::post('{wholesaler}/edit/basic-info', [App\Http\Controllers\Admin\WholesalerController::class, 'update_basic_info'])->name('wholesalers.update_basic_info');
            Route::post('update_creditinfo', [App\Http\Controllers\Admin\WholesalerController::class, 'updateCreditInfo'])->name('wholesalers.updateCreditInfo');

            Route::get('ajax/dropdown', [App\Http\Controllers\Admin\WholesalerController::class, 'wholesalers_ajax_dropdown'])->name('wholesalers.ajax.dropdown');
        });
    }
);
