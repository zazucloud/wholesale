<?php

use App\Http\Controllers\Manufacturer\Auth\ManufacturerLoginController;
use App\Http\Controllers\Manufacturer\ManufacturerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Auth::routes();

Route::prefix('manufacturer')->group(function () {
    Route::get('/login', [ManufacturerLoginController::class, 'showLoginForm'])->name('manufacturer_user.loginForm');
    Route::post('/login', [ManufacturerLoginController::class, 'login'])->name('manufacturer_user.login');
    Route::get('/dashboard', [ManufacturerController::class, 'dashboard'])->name('manufacturer.dashboard');

    Route::post('/logout', [ManufacturerLoginController::class, 'ManufacturerLogout'])->name('manufacturer_user.logout');
});