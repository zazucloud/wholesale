<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::whereEmail('admin@test.com')->first();
        if (is_null($user)) {
            $user = User::create([
                'name' => 'Super Administrator',
                'email' => 'admin@test.com',
                'password' => bcrypt('password'),
                'first_login'=>1
            ]);
        }

        $role = Role::findOrCreate('Adminstrator');

        $permissions = Permission::pluck('id', 'id')->all();
        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
