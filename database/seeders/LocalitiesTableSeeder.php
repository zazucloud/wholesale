<?php

namespace Database\Seeders;

use File;
use App\Models\County;
use App\Models\Locality;
use Illuminate\Database\Seeder;

class LocalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json = File::get("database/data/counties.json");
        $counties = json_decode($json);

        foreach ($counties as $key => $value) {
            County::findOrCreate($value);
        }

        Locality::truncate();

        $json =  File::get("database/data/localities.json");
        $localities = json_decode($json);

        foreach ($localities as $key => $value) {
            Locality::findOrCreate($value);
        }
    }
}
