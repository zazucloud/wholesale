<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            'wholesaler-list',
            'wholesaler-create',
            'wholesaler-edit',
            'wholesaler-delete',
            'wholesaler-export',
            'wholesaler_config-uploading',
            'wholesaler_config-downloading',
            'wholesaler-credit_info',
            'wholesaler-product_discount',
            'wholesaler-category_discount',
            'retailers-list',
            'retailers-create',
            'retailers-edit',
            'retailers-delete',
            'inventory-list',
            'inventory-create',
            'inventory-edit',
            'inventory-delete',
            'inventory-export',
            'inventory_config-uploading',
            'inventory_config-downloading',
            'quotation-list',
            'quotation-create',
            'quotation-edit',
            'quotation-sendmail',
            'quotation-download',
            'quotation-expired',
            'quotation-email',
            'quotation-order_transfer',
            'payments-list',
            'payments-view',
            'payments-create',
            'orders-list',
            'orders-create',
            'orders-edit',
            'orders-delete',
            'orders-view_pending',
            'production-list',
            'production-create',
            'production-edit',
            'production-delete',
            'manufacturers-list',
            'manufacturers-view',
            'manufacturers-create',
            'manufacturers-edit',
            'manufacturers-delete',
            'manufacturers-config',
            'brands-list',
            'brands-create',
            'brands-edit',
            'brands-delete',
            'pricing-uploading',
            'pricing-downloading',
            'warehouse-list',
            'warehouse-create',
            'warehouse-edit',
            'warehouse-delete',
            'attributes-list',
            'attributes-create',
            'attributes-edit',
            'attributes-delete',
            'companyinfo-view',
            'companyinfo-create',
            'companyinfo-edit',
            'roles-list',
            'roles-create',
            'roles-edit',
            'roles-delete',
            'users-list',
            'users-create',
            'users-edit',
            'users-delete',
            'users-disable',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete',
            'templates-list',
            'templates-create',
            'templates-edit',
            'templates-view',
            'templates-delete',
            'categories-list',
            'categories-create',
            'categories-edit',
            'categories-delete',
            'zones-create',
            'zones-edit',
            'zones-allocate',
            'zones-delete',
            'zones-upload',
            'zones-view',
        ];

        foreach ($permissions as $permission) {
            Permission::findOrCreate($permission);
        }
    }
}
