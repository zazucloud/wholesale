<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory', function (Blueprint $table) {
            $table->foreignId('brand_id')->nullable()->default(null)->after('sku')->constrained()->onDelete('cascade');
            $table->string('price')->nullable()->default(null)->change();
            $table->tinyInteger('is_featured')->default(0)->after('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory', function (Blueprint $table) {
            $table->dropForeign('brand_id');
            $table->dropColumn(['price', 'is_featured']);
        });
    }
}
