<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('wholesaler_id')->nullable()->default(null)->constrained()->onDelete('cascade');
            $table->foreignId('order_id')->nullable()->default(null)->onDelete('cascade');
            $table->decimal('amount');
            $table->string('reference')->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);
            $table->string('attachment')->nullable()->default(null);
            $table->foreignId('created_by')->nullable()->default(null)->constrained('users')->onDelete('cascade');
            $table->tinyInteger('channel');
            $table->enum('status', ['paid', 'unpaid', 'trashed', 'reversed'])->default('unpaid');
            $table->boolean('confirmed')->default(0);
            $table->string('receipt_no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
