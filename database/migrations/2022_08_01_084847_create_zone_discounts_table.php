<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZoneDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_discounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('zone_id')->constrained("zones")->onDelete('cascade');
            $table->foreignId('inventory_id')->constrained("inventory")->onDelete('cascade');
            $table->decimal('custom_discount', 13, 2)->default(null)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zone_discounts');
    }
}
