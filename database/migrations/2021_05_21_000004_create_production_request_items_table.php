<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_request_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('inventory_id')->constrained('inventory')->onDelete('cascade');
            $table->foreignId('manufacturer_id')->constrained('manufacturers')->onDelete('cascade');
            $table->foreignId('production_request_id')->constrained('production_requests')->onDelete('cascade');
            $table->integer('requested_quantity')->default(1)->unsigned();
            $table->text('item_notes')->default(null)->nullable();
            $table->tinyInteger('production_status')->default(0);
            $table->dateTime('item_delivery_date')->default(null)->nullable();
            $table->tinyInteger('item_priority')->default(1);
            $table->json('attributes')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_request_items');
    }
}
