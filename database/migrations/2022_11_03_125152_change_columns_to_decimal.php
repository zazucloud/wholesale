<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsToDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory', function (Blueprint $table) {
            $table->decimal('price', 13, 2)->nullable()->change();
            $table->decimal('crossed_price', 13, 2)->nullable()->default(null)->change();
        });

        Schema::table('order_product', function (Blueprint $table) {
            $table->decimal('price', 13, 2)->change();
        });

        Schema::table('wholesaler_category_discounts', function (Blueprint $table) {
            $table->decimal('discount_rate', 13, 2)->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('total', 13, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
