<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyProductionRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('production_requests', function (Blueprint $table) {
            $table->dropForeign('production_requests_customer_id_foreign');
            $table->dropColumn('customer_id');
            $table->nullableMorphs('clientable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('production_requests', function (Blueprint $table) {
            //
        });
    }
}
