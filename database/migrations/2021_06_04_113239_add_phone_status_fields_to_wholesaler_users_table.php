<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneStatusFieldsToWholesalerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wholesaler_users', function (Blueprint $table) {
            $table->string('phone_no')->nullable()->after('email');
            $table->boolean('status')->nullable()->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wholesaler_users', function (Blueprint $table) {
            $table->dropColumn(['phone_no', 'status']);
        });
    }
}
