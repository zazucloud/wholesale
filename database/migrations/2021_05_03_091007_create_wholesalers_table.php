<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWholesalersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wholesalers', function (Blueprint $table) {
            $table->id();
            $table->string('business_name')->unique()->index();
            $table->string('slug')->unique();
            $table->string('postal_address')->nullable()->default(null);
            $table->string('physical_address')->nullable()->default(null);
            $table->string('telephone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('pin')->nullable()->default(null);
            $table->string('contact_name')->nullable()->default(null);
            $table->string('contact_position')->nullable()->default(null);
            $table->string('financial_controller')->nullable()->default(null);
            $table->tinyInteger('ownership_type')->nullable()->default(null);
            $table->string('ownership_type_extra')->nullable()->default(null);

            $table->string('registration_number')->nullable()->default(null)->index();
            $table->string('registration_date')->nullable()->default(null);

            $table->string('business_nature')->nullable()->default(null);

            $table->string('annual_item_value')->nullable()->default(null);
            $table->string('credit_amount')->nullable()->default(null);

            $table->string('bank_account_name')->nullable()->default(null);
            $table->string('bank_account_no')->nullable()->default(null);
            $table->string('financial_contact_person')->nullable()->default(null);
            $table->string('financial_contact_phone')->nullable()->default(null);


            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wholesalers');
    }
}
