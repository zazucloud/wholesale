<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('wholesaler_id')->constrained()->onDelete('cascade');
            $table->string('total');
            $table->foreignId('delivery_location')->constrained('wholesaler_locations')->nullable()->onDelete('cascade');
            $table->dateTime('delivery_date')->nullable()->default(null);
            $table->string('attachment')->nullable()->default(null);
            $table->enum('made_via', ['app', 'web', 'staff_web', 'other'])->default('other');
            $table->string('stage');
            $table->boolean('is_draft')->default(true);
            $table->foreignId('created_by')->nullable()->constrained('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
