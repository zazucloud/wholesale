<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_requests', function (Blueprint $table) {
            $table->id();
            $table->string('request_tag',32)->unique();
            $table->foreignId('customer_id')->constrained('wholesalers')->onDelete('cascade');
            $table->foreignId('staff_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('initiator')->constrained('users')->onDelete('cascade');
            $table->dateTime('delivery_date')->default(null)->nullable();
            $table->tinyInteger('priority')->default(1);
            $table->tinyInteger('draft_stage')->default(0);
            $table->text('notes')->default(null)->nullable();
            $table->string('status');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_requests');
    }
}
