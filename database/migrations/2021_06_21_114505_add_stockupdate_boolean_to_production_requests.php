<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStockupdateBooleanToProductionRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('production_requests', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->boolean('if_stock_update')->default(0);
            $table->bigInteger('customer_id')->nullable()->unsigned()->change();
            $table->bigInteger('staff_id')->nullable()->unsigned()->change();
            $table->bigInteger('initiator')->nullable()->unsigned()->change();
            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('production_requests', function (Blueprint $table) {
            //
        });
    }
}
