@if (Auth::guard('web')->check())
  <p class="text-success">
    You are Logged In as a <strong>ADMIN</strong>
  </p>
@else
  <p class="text-danger">
    You are Logged Out as a <strong>ADMIN</strong>
  </p>
@endif

@if (Auth::guard('wholesalerUser')->check())
  <p class="text-success">
    You are Logged In as a <strong>WHOLESALER</strong>
  </p>
@else
  <p class="text-danger">
    You are Logged Out as a <strong>WHOLESALER</strong>
  </p>
@endif

@if (Auth::guard('manufacturerUser')->check())
  <p class="text-success">
    You are Logged In as a <strong>manufacturer</strong>
  </p>
@else
  <p class="text-danger">
    You are Logged Out as a <strong>manufacturer</strong>
  </p>
@endif