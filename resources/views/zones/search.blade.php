<div class="row">

    <div class="col-md-10 col-12 ">
        {!! Form::open(['route' => 'zones.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-md-10 col-12">
            <div class="">
                <label for="">Search</label>
                {!! Form::text('search', null, [
                    'class' => 'form-control py-2',
                    'required' => true,
                    'placeholder' => 'Search By Name',
                ]) !!}
            </div>
        </div>
        <div class="col-md-2 mt-md-4 pt-2 col-12">
            <div>
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm ripple']) }}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2  col-12 mt-md-4 pt-md-2 ">
        <a class="btn btn-success btn-sm ripple float-right" href="{{ route('zones.create') }}">
            <span><i class="fa fa-plus mr-2"></i> Create New</span>
        </a>
    </div>


</div>
