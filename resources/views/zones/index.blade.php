@extends('layouts.base')

@section('title')
    Zones
@endsection

@section('content')
    @include('partials.page-title')

    <div class="card mt-3">
        <div class="card-header">
            @include('zones.search')
        </div>
        <div class="card-body">
            <div class="spacious-container">
            <table class="table table-hover center-text">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($zones as $key => $zone)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $zone->name }}</td>
                            <td class="tw-flex">
                                @if(auth()->user()->hasPermissionTo('zones-allocate'))
                                <a href="{{ route('zones.allocate', $zone) }}" data-toggle="tooltip" data-placement="top"
                                    title="Allocate" class="btn btn-sm btn-success text-white mr-2">
                                    <i class="fa fa-map"></i>
                                </a>
                                @else
                                <button data-toggle="tooltip" data-placement="top"
                                    title="Allocate" class="btn btn-sm btn-success text-white mr-2" disabled>
                                    <i class="fa fa-map"></i>
                                </button>
                                @endif

                                @if(auth()->user()->hasPermissionTo('zones-edit'))
                                <a href="{{ route('zones.edit', $zone) }}" data-toggle="tooltip" data-placement="top"
                                    title="Edit" class="btn btn-sm btn-warning text-white mr-2">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @else
                                <button data-toggle="tooltip" data-placement="top"
                                    title="Edit" class="btn btn-sm btn-warning text-white mr-2" disabled>
                                    <i class="fa fa-edit
                                    "></i>
                                </button>
                                @endif

                                @if(auth()->user()->hasPermissionTo('zones-delete'))
                                {!! Form::open(['method' => 'DELETE', 'route' => ['zones.destroy', $zone], 'style' => 'display:inline']) !!}
                                <button type="submit" class="btn btn-sm btn-danger text-white mr-2"
                                    onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                    data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                                {!! Form::close() !!}
                                @else
                                <button data-toggle="tooltip" data-placement="top"
                                    title="Delete" class="btn btn-sm btn-danger text-white mr-2" disabled>
                                    <i class="fa fa-trash"></i>
                                </button>
                                @endif

                                @if(auth()->user()->hasPermissionTo('zones-upload'))
                                <a href="{{ route('zones.custom_discount', $zone) }}" data-toggle="tooltip"
                                    data-placement="top" title="Upload" class="btn btn-sm btn-primary text-white mr-2">
                                    <i class="fa fa-file-excel-o"></i>
                                </a>
                                @else
                                <button data-toggle="tooltip" data-placement="top"
                                    title="Upload" class="btn btn-sm btn-primary text-white mr-2" disabled>
                                    <i class="fa fa-file
                                    "></i>
                                </button>
                                @endif

                                @if(auth()->user()->hasPermissionTo('zones-view'))
                                 <a href="{{ route('zones.view.custom_discount', $zone->id) }}" data-toggle="tooltip"
                                    data-placement="top" title="view" class="btn btn-sm btn-info text-white mr-2">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @else
                                <button data-toggle="tooltip" data-placement="top"
                                    title="View" class="btn btn-sm btn-info text-white mr-2" disabled>
                                    <i class="fa fa-eye"></i>
                                </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
            {!! $zones->appends(Request::except('page'))->render() !!}
        </div>
    </div>

    {{-- <zonesmodal></zonesmodal> --}}
@endsection
