<div class="row">
    {!! Form::open(['route' => null, 'method' => 'get', 'class' => 'row col-sm-10']) !!}

    <div class="col-sm-10">
        <div class="">
            <label>Search</label>
            {!! Form::text('search', null, [
                'class' => 'form-control form-control-inputs',
                // 'required' => true,
                'placeholder' => 'Search By Name,SKU or Item Number',
            ]) !!}
        </div>
    </div>

    <div class="col-sm-2 mt-4 pt-2">
        <div>
            {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

        </div>
    </div>
    {!! Form::close() !!}

    <div class="row col-md-2 col-12">
        <div class="col-sm-6 col-12 mt-4 pt-2">
            <a href="{{route('zones.download_current_discount',['zone'=>$zone->id,'mode'=>0,'wholesaler'=>0])}}" class="btn btn-sm btn-primary">Export</a>
        </div>

        <div class="col-sm-6 col-12 mt-4 pt-2">
            <button @click="openZoneModal()" class="btn btn-sm btn-info">Import</a>
        </div>
    </div>

</div>
