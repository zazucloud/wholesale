@extends('layouts.base')

@section('title')
    Inventory Discounts For <b>{{$zone->name}}</b>
@endsection

@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @include('partials.page-title')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">
                    <div class="card-header">
                        @include('zones.searches.searchform')
                    </div>

                    <div class="card-body override_cbody">

                        <div class="spacious-container">
                            <table class="table table-hover center-text" id="invoicestable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>SKU</th>
                                        <th>ItemNumber</th>
                                        <th>Name</th>
                                        <th>Color</th>
                                        <th>Original Price</th>
                                        <th>Discount Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($discounts as $index => $datum)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>

                                            <td>{{ $datum->inventory->sku }}</td>
                                            <td>{{ $datum->inventory->itemnumber }}</td>
                                            <td>{{ $datum->inventory->name }}</td>
                                            <td>{{ $datum->inventory->color }}</td>
                                            <td>{{ $datum->inventory->price }}</td>
                                            <td>{{ $datum->custom_discount }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div style="padding-left:20px;">
                        {!! $discounts->appends(Request::except('page'))->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <discountuploadmodal :zone="{{$zone->id}}"></discountuploadmodal>
@endsection

@section('custom-scripts')
@endsection

@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="select2"]').select2();
        });
    </script>
@endsection
