@extends('layouts.base')

@section('title')
    Create Zone
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">



                <div class="card mt-2">
                    <div class="card-body override_cbody">
                        <span class="caption-subject font-blue-madison bold uppercase mx-3 my-3"></span>
                        <h5 class="box-title"><b class="mx-3 my-2">Zone Discount Information for <span class="text-info"> {{$zone->name}}</span></b></h5>
                        <div class="tabs tabs-bordered mx-3 my-2">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a href="#profile-tab" data-toggle="tab" aria-expanded="true"
                                        class="nav-link px-3 py-2 active">
                                        <i class="mdi mdi-pencil-box-multiple font-18 d-md-none d-block"></i>
                                        <span class="d-none d-md-block">Export</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#branches" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                                        <i class="mdi mdi-image font-18 d-md-none d-block"></i>
                                        <span class="d-none d-md-block"> Import </span>
                                    </a>
                                </li>

                            </ul> <!-- end nav-->
                        </div>
                        <div class="tab-content">



                            <div class="tab-pane active" id="profile-tab">
                                <div class="alert alert-info">
                                    <strong>Step 1.</strong> Select product categories you wish to upload discounts for.
                                    <br>
                                    <strong>Step 2.</strong> Make adjustments on the Excel file then re-upload
                                    <br>
                                    <small>
                                        (Percentage Discount) Column is should be edited
                                    </small>

                                    <br>
                                </div>
                                {!! Form::open([
                                    'route' => ['zones.custom_discount_download'],
                                    'method' => 'POST',
                                    'files' => true,
                                    'id' => 'zonepriceform'
                                ]) !!}

                                @csrf

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <input type="checkbox" id="markall"><span class="ml-2">Select All</span>
                                    </div>

                                    <input type="hidden" name="zoneid" value="{{$zone->id}}">
                                    @foreach ($categories as $value)
                                        <div class="col-md-3">
                                            <input type="checkbox" name="categories[]" class="p_mark_all"
                                                value="{{ $value->id }}"><span class="ml-2">{{ $value->name }}</span>
                                        </div>
                                    @endforeach

                                    <div class="col-12 mt-3">
                                        <div>
                                             <button type='submit' class ='btn btn-primary btn-sm multiple-submits' @click="busBatchProcess('{{route('zones.custom_discount_download')}}','zonepriceform')">Download</button>
                                        </div>

                                         <busjob_exports></busjob_exports>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="tab-pane" id="branches">

                                <div class="form-group">

                                    <div class="alert alert-info">
                                        <strong>Step 1.</strong> Upload Adjusted Excel File Here.
                                        <br>
                                    </div>

                                     <busjobimports url="{{route('zones.custom_discount_update', ['zone'=>$zone])}}"></busjobimports>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
@endsection
