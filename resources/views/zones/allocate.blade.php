@extends('layouts.base')

@section('title')
    Allocate Counties
@endsection

@php
    $theme_manager->enqueue_files(['select2', 'selectpicker', 'multiselect'], ['select2', 'selectpicker', 'multiselect']);
@endphp

@section('content')
    @include('partials.page-title')
    <div class="card mt-3">
        <div class="card-header">
            <div class="row">
                <div class="col-7"><b>Allocate Counties to {{ $zone->name }}</b></div>
                <div class="col-3 offset-2">
                    <a class="btn btn-info btn-sm float-right" href="{{ route('zones.index') }}"><span
                            class="fa fa-arrow-left mr-2"></span> Back</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            {!! Form::open(['route' => 'zones.assign', 'method' => 'POST', 'class' => 'prevent-multiple-submits']) !!}
            <input type="hidden" name="zone_id" value="{{ $zone->id }}">

            <div class="form-group mx-0">
                <strong>Select Counties to allocate:</strong>

                <div class="row">
                    <div class="col-md-12">
                        <input type="checkbox" id="markall"><span class="ml-2">Select All</span>
                    </div>
                    @foreach ($counties as $county)
                        @if (!is_null($county->zoneAllocation) && $county->zoneAllocation->zone_id == $zone->id)
                            <label for="checkbox" class="col-md-2">
                                <input type="checkbox" class="county_id" name="county_id[]" value="{{ $county->id }}"
                                    checked>
                                <span>{{ $county->name }}</span>
                                <br>
                            </label>
                        @endif
                        @if (is_null($county->zoneAllocation))
                            <label for="checkbox" class="col-md-2">
                                <input type="checkbox" class="county_id" name="county_id[]" value="{{ $county->id }}">
                                <span>{{ $county->name }}</span>
                                <br>
                            </label>
                        @endif
                    @endforeach

                </div>
            </div>
            <div class="form-group row mx-0">
                <div class="col-md-6 center">
                    {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits']) }}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection



@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $("#markall").click(function() {
                $(".county_id").prop("checked", this.checked);
            });

        });
    </script>
@endsection
