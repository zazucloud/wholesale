@extends('layouts.base')
@php
    $theme_manager->enqueue_files(['fontawesome-icons', 'select2', 'jqvmaps-css', 'magnific-popup'], ['countup', 'moment', 'charts-js', 'sparkline', 'select2', 'jqvmaps-js', 'jqvmaps-usa', 'magnific-popup', 'custom']);
@endphp
@section('title')
    Dashboard
@endsection

@section('content')
    @include('partials.page-title')

    <div class="row">
        <div class="col-lg-12">

            {{-- @component('components.loginTest')

            @endcomponent --}}
            <div class="widget-list row">

                @can('orders-view_pending')
                    <div class="widget-holder col-6 col-md-4">
                        <div class="widget-bg">
                            <div class="widget-body bg-success text-inverse px-4 pt-3 pb-4 radius-3">
                                <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                                    <header class="align-self-center">
                                        <a href="#" class="bg-grey fs-30 text-muted"><i
                                                class="linearicons-cart icon-lg"></i></a>
                                    </header>
                                    <section class="mt-0">
                                        <h6 class="icon-box-title my-0">
                                            <span class="counter">
                                                {{ number_format($pending_orders_count, 0) }}
                                            </span>
                                        </h6>
                                        <p class="mb-0"><b>Pending Orders</b></p>
                                    </section>
                                </div><!-- /.icon-box -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.widget-bg -->
                    </div><!-- /.widget-holder -->
                @endcan

                @can('production-list')
                    <div class="widget-holder col-6 col-md-4">
                        <div class="widget-bg">
                            <div class="widget-body bg-secondary text-inverse px-4 pt-3 pb-4 radius-3">
                                <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                                    <header class="align-self-center">
                                        <a href="#" class="bg-grey fs-30 text-muted"><i
                                                class="linearicons-clipboard-text icon-lg"></i></a>
                                    </header>
                                    <section class="mt-0">
                                        <h6 class="icon-box-title my-0">
                                            <span class="counter">
                                                {{ number_format($pending_production_requests_count, 0) }}
                                            </span>
                                        </h6>
                                        <p class="mb-0">Pending Productions</p>
                                    </section>
                                </div><!-- /.icon-box -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.widget-bg -->
                    </div><!-- /.widget-holder -->
                @endcan

                @can('wholesaler-list')
                    <div class="widget-holder col-6 col-md-4">
                        <div class="widget-bg">
                            <div class="widget-body bg-danger text-inverse px-4 pt-3 pb-4 radius-3">
                                <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                                    <header class="align-self-center">
                                        <a href="#" class="bg-grey fs-30 text-muted"><i
                                                class="linearicons-apartment icon-lg"></i></a>
                                    </header>
                                    <section class="mt-0">
                                        <h6 class="icon-box-title my-0">
                                            <span class="counter">
                                                {{ $wholesaler_count }}
                                            </span>
                                        </h6>
                                        <p class="mb-0">
                                            Wholesaler
                                        </p>
                                    </section>
                                </div><!-- /.icon-box -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.widget-bg -->
                    </div><!-- /.widget-holder -->
                @endcan

            </div><!-- /.widget-list -->
        </div><!-- /.col-lg-4 -->
    </div>

    @can('orders-view_pending')
        <div class="widget-list row">
            <div class="widget-holder widget-full-height col-12 col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <div class="widget-title">
                            <h5><b>Pending Orders</b></h5>
                            <br>
                            <small class="ml-0 fs-13">Showing Last 5 Orders</small>
                        </div>


                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="widget-traffic-sources">
                            @if (count($recent_pending_orders))
                                <ul class="list-unstyled mr-b-0 d-flex flex-column justify-content-center">
                                    @foreach ($recent_pending_orders as $key => $recent_pending_order)
                                        <li>
                                            <span class="flex-1">{{ $recent_pending_order->clientable->business_name }}
                                                <br>
                                                Total:{{ currency($recent_pending_order->total) }}
                                            </span>
                                            <span
                                                class=" flex-1 text-mute ">{{ stamp_date_time_simple($recent_pending_order->created_at) }}</span>
                                            <small class="text-success fs-12">
                                                <a href="{{ route('orders.view', $recent_pending_order->id) }}" type="button"
                                                    class="btn btn-block btn-outline-primary ripple btn-sm">View
                                                    Order
                                                </a></small>
                                        </li>
                                    @endforeach

                                </ul>
                            @else
                                <div class="alert alert-info">


                                    <span>No Pending Orders</span>


                                </div>
                            @endif
                        </div>
                        <!-- /.widget-traffic-sources -->
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>


        </div>
    @endcan

@endsection

@section('custom-scripts')
    <script>
        $("#menu-dashboard").addClass('active');
    </script>
@endsection
