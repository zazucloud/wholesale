@extends('admin.wholesalers.profile_layout')
@section('bcontent')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('wholesalersAccounts',$wholesaler->slug) }}
    @endsection
@endsection
@section('profiletitle')Wholesaler Accounts @endsection

@section('profilebody')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            <h6>Login Accounts</h6>
                        </div>
                        <div class="col-6">
                            <button @click="openWhAccountModal(null,1)" class="btn btn-sm btn-primary pull-right"> <i class="fa fa-plus mr-2"></i> Add
                                Account</a>
                        </div>

                    </div>

                </div>

                <div class="card-body override_cbody">

                    <div class="spacious-container">
                        <table class="table table-hover center-text" id="invoicestable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Account Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Last Login</th>
                                    <th>Actions</th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accounts as $index => $account)

                                <tr>
                                    <th scope="row">{{ $index+1 }}</th>

                                    <td>{{$account->name}}</td>
                                    <td><span class="d-block">{{$account->email}}</span>
                                        <br>
                                        {{$account->status}}
                                    </td>
                                    <td>{{$account->phone_no}}</td>

                                    <td>

                                        @php
                                        $last_login = \App\Models\wholesale\WholesalerUser::find($account->id)->last_login;

                                        if($last_login==null){

                                        echo 'Never Logged In';

                                        }else{

                                        $last_login_time = $last_login->created_at;
                                        echo stamp_date_time($last_login_time);
                                        }

                                        @endphp


                                    </td>


                                    <td class="">
                                        <a href="{{route('wholesalers.account_manage',['wholesaler'=>$wholesaler->slug,'account'=>$account->email])}}" class="btn btn-primary btn-sm">Manage</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                {{-- <div style="padding-left:20px;">
                    {!! $accounts->appends(Request::except('page'))->render() !!}

                </div> --}}
            </div>
        </div>
    </div>
</div>
<whaccountmodal :wholesaler="{{json_encode($wholesaler)}}"></whaccountmodal>
@endsection
