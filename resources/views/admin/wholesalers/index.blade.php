@extends('layouts.base')

@section('title')
    WholeSalers
@endsection

@section('content')
    @include('partials.page-title')

    {{-- <a href="{{route('admin.wholesalers.create_account')}}" class="btn btn-primary btn-circle pull-right"> <i
    class="fa fa-plus"></i> Add Wholesaler</a> --}}


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">
                    <div class="card-header">
                        @include('admin.wholesalers.search.searchform')
                    </div>

                    <div class="card-body override_cbody">

                        <div class="spacious-container">
                            <table class="table table-hover center-text" id="invoicestable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Customer Code</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Physical Address</th>
                                        <th scope="col">Pin</th>
                                        <th scope="col">County</th>
                                        <th scope="col">Credit Mode</th>
                                        <th scope="col">Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($wholesalers as $index => $datum)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>
                                            <td>{{ $datum->business_name }}</td>
                                            <td>{{ $datum->registration_number }}</td>
                                            <td>{{ $datum->email }}</td>
                                            <td>{{ $datum->telephone }}</td>
                                            <td>{{ $datum->physical_address }}</td>
                                            <td>{{ $datum->pin }}</td>
                                            <td>{{ $datum->county->name ?? 'n/a' }}</td>
                                            <td>{{ $datum->credit_mode == 1 ? 'cash' : 'credit' }}</td>
                                            <td class="tw-flex">
                                                @can('wholesaler-edit')
                                                    <a href="{{ route('admin.wholesalers.show', ['wholesaler' => $datum]) }}"
                                                        class="btn btn-info btn-sm mr-1"><i
                                                            class="list-icon linearicons-eye"></i></a>
                                                @endcan

                                                @can('wholesaler-delete')
                                                    <form method="post"
                                                        action="{{ route('admin.wholesalers.delete', ['wholesaler' => $datum]) }}">
                                                        @csrf
                                                        @method('DELETE')

                                                        <button class="btn btn-sm btn-block btn-danger" type="submit"
                                                            onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                                            data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="list-icon linearicons-trash2"></i>
                                                        </button>
                                                    </form>
                                                @endcan

                                            </td>


                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div style="padding-left:20px;">
                        {!! $wholesalers->appends(Request::except('page'))->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
@endsection
