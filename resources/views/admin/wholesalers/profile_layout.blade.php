@extends('layouts.base')
@section('title')
    @yield('profiletitle')
@endsection

@yield('bcontent')

@section('content')
    @include('partials.page-title')
    <div class="row widget-holder">
        <div class="col-md-3 col-3">
            <!-- start profile info -->
            <div class="card">
                <div class="card-body px-1">
                    <div class="dropdown float-end">
                        <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <i class="mdi mdi-dots-horizontal"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item">Edit Profile</a>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item">Settings</a>
                        </div>
                    </div>

                    <div class="d-flex align-items-start flex-column text-center">
                        <img class="d-flex align-items-start rounded me-2 m-auto"
                            src="{{ asset('wholesaler_assets/default_avatar.png') }}" height="38">
                        <br>
                        <div class="w-100 ps-1">
                            <h6 class="mt-1 mb-0">{{ $wholesaler->business_name }}</h6>
                        </div>
                    </div>

                    <div class="list-group list-group-flush mt-2 font-15">
                        <a href="{{ route('admin.wholesalers.show', $wholesaler->slug) }}"
                            class="px-2 list-group-item list-group-item-action text-primary fw-semibold border-0"><i
                                class="list-icon mr-2 w30 fa fa-home font-16 me-1" aria-hidden="true"></i>
                            <span>Home</span></a>
                        <a href="{{ route('wholesalers.orders', $wholesaler->slug) }}"
                            class="px-2 list-group-item list-group-item-action border-0"><i
                                class="list-icon mr-2 w30 fa fa-arrow-down font-16 me-1"></i> <span>Orders</span></a>
                        <a href="{{ route('wholesalers.accounts', $wholesaler->slug) }}"
                            class="px-2 list-group-item list-group-item-action border-0"><i
                                class="list-icon mr-2 w30 fa fa-sign-in font-16 me-1"></i> <span>Accounts</span></a>
                        <a href="{{ route('wholesalers.payments', $wholesaler->slug) }}"
                            class="px-2 list-group-item list-group-item-action border-0"><i
                                class="list-icon mr-2 w30 fa fa-dollar font-16 me-1"></i> <span>Payments</span></a>
                        <a href="{{ route('wholesaler.staff_assignment', $wholesaler->slug) }}"
                            class="px-2 list-group-item list-group-item-action border-0"><i
                                class="list-icon mr-2 w30 fa fa-user-secret font-16 me-1"></i> <span>Staff
                                Assignment</span></a>
                        <a href="{{ route('wholesaler.catalog_visibility', $wholesaler->slug) }}"
                            class="px-2 list-group-item list-group-item-action border-0"><i
                                class="list-icon mr-2 w30 fa fa-eye font-16 me-1"></i> <span>Catalogue</span></a>
                        @if (auth()->user()->getRoleNames()[0] != 'Sales Representative')
                            @if (auth()->user()->hasPermissionTo('wholesaler-product_discount'))
                                <a href="{{ route('wholesaler.custom_discount', $wholesaler->slug) }}"
                                    class="px-2 list-group-item list-group-item-action border-0"><i
                                        class="list-icon mr-2 w30 fa fa-money font-16 me-1"></i> <span>Product
                                        Discounts</span></a>
                            @endif
                            @if (auth()->user()->hasPermissionTo('wholesaler-category_discount'))
                                <a href="{{ route('wholesaler.discount_control', $wholesaler->slug) }}"
                                    class="px-2 list-group-item list-group-item-action border-0"><i
                                        class="list-icon mr-2 w30 fa fa-wrench font-16 me-1"></i> <span>Category
                                        Discount</span></a>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
            <!-- end profile info -->

        </div>

        <div class="col-md-9 col-12">

            @yield('profilebody')
        </div>
    </div>

@endsection

@section('custom-scripts')
@endsection
