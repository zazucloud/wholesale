@extends('admin.wholesalers.profile_layout')
@section('bcontent')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('wholesalersCatalogue',$wholesaler->slug) }}
    @endsection
@endsection
@section('profiletitle')Catalogue Visibility @endsection

@section('profilebody')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">

                <div class="card-body override_cbody">

                    {!! Form::open(array('route' =>
                    ['wholesaler.catalog_visibility_update',$wholesaler->slug],'method'=>'POST','class'=>'prevent-multiple-submits')) !!}

                    @csrf
                    <input type="hidden" name="wholesaler_id" id="wholesaler_id" value="{{$wholesaler->id}}">
                    <input type="hidden" name="wholesaler_slug" id="wholesaler_slug" value="{{$wholesaler->slug}}">

                    @include('admin.wholesalers.fields.editcategoriesfields')

                    <div class="form-group row mx-0">
                        <div class="col-md-6 center">
                            {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm multiple-submits'] )  }}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
