@extends('admin.wholesalers.profile_layout')
@section('title') {{$wholesaler->business_name}} @endsection
@section('profile_page_title') Deleting Wholesaler @endsection

@section('profile_content')


    <div class="row">
        <div class="col-md-12">

            <h4>Confirmation</h4>
            <div class="portlet light ">
                <div class="portlet-body">

                    <form method="post" action="{{route('admin.wholesalers.delete',$wholesaler->slug)}}">
                        @csrf

                        <div class="alert alert-danger">
                            You're about to delete {{$wholesaler->business_name}}

                            <br>
                            Enter your password to confirm
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="password">Your Password</label>
                                <input required id="password" name="password" type="password" class="form-control"
                                       placeholder="Password"></div>

                            <div class="col-lg-12">
                                <button class="btn btn-danger"><i class="fa fa-send"></i> Proceed, Delete</button>

                            </div>

                        </div>

                    </form>

                </div>
            </div>

        </div>
    </div>



@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
        $("#wholesaler_delete").addClass('active');
    </script>
@endsection