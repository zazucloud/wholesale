@extends('admin.wholesalers.profile_layout')
@section('title') New Account @endsection
@section('profile_page_title') {{$wholesaler->business_name}} @endsection

@section('profile_content')


    <div class="row">


        <div class="col-md-12">
            <a href="{{route('wholesalers.accounts',['wholesaler'=>$wholesaler->slug])}}"
               class="btn btn-primary btn-circle pull-right"> <i
                        class="fa fa-chevron-left"></i> Back to Accounts Listing</a>

            <h4>New Account</h4>


            <div class="portlet light ">
                <div class="portlet-body">

                    <form method="post"
                          action="{{route('wholesalers.account_create',['wholesaler'=>$wholesaler->slug])}}"
                          enctype="multipart/form-data" class="prevent-multiple-submits">

                        @csrf

                        <input type="hidden" name="wholesaler" value="{{$wholesaler->slug}}">

                        <div class="row">

                            <div class="col-lg-7">


                                <div class="form-group">
                                    <label for="name">Account Name</label>
                                    <input required id="name" name="name" type="text" class="form-control"
                                           placeholder="Account Name" value="{{old('name')}}">
                                </div>


                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input required id="email" name="email" type="email" class="form-control"
                                           placeholder="Account Email Address" value="{{old('email')}}">
                                    <span class="text-muted">Account username</span>
                                </div>


                                <div class="form-group">
                                    <label for="phone_number">Phone Number</label>
                                    <input required id="phone_number" name="phone_number" type="text"
                                           class="form-control" value="{{old('phone_number')}}"
                                           placeholder="Phone Number">
                                </div>


                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">Active</option>
                                        <option value="5">Disabled</option>
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary multiple-submits"><i class="fa fa-send"></i> Submit
                                        </button>
                                    </div>
                                </div>


                            </div>

                        </div>


                    </form>

                </div>
            </div>

        </div>
    </div>



@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
        $("#wholesaler_menu_accounts").addClass('active');
    </script>
@endsection
