<div class="profile-sidebar">
    <div class="portlet light profile-sidebar-portlet ">
        <div class="profile-userpic">
            <img src="{{asset('wholesaler_assets/default_avatar.png')}}" class="img-responsive" alt="">
        </div>

        <div class="profile-usertitle">
            <div class="profile-usertitle-name"> {{$wholesaler->business_name}} </div>
            <div class="profile-usertitle-job"> {{$wholesaler->registration_number}} </div>
        </div>

        @include('admin.wholesalers.partial_menu')

    </div>
</div>
