@extends('admin.wholesalers.profile_layout')
@section('bcontent')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('wholesalersPdiscounts',$wholesaler->slug) }}
    @endsection
@endsection
@section('profiletitle') Wholesaler Custom Discounts @endsection

@section('profilebody')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">

                <div class="card-body override_cbody">

                    {!! Form::open(array('route' => ['wholesaler.custom_discount_update',$wholesaler->slug],'method'=>'POST','files' => true,'class'=>'prevent-multiple-submits')) !!}

                    @csrf
                    <div class="form-group">


                        <div class="alert alert-info">
                            <strong>Step 1.</strong> Begin by downloading the current state - <a href="{{route('wholesaler.custom_discount_download',$wholesaler->slug)}}">click here to
                                download</a>@if (!is_null($wholesaler->county->zoneAllocation)) or Download zone pricing <a href="{{route('zones.download_current_discount',['zone'=>$wholesaler->county->zoneAllocation->zone_id,'mode'=>1,'wholesaler'=>$wholesaler->id])}}" ><b>Here</b></a> @endif
                            <br>
                            <strong>Step 2.</strong> Make adjustments on the Excel file then re-upload
                            <br>
                            <br>
                            <small>
                                Only Column D (Discount) is editable
                            </small>

                            <br>
                            <small>For changing base price, use the inventory import feature</small>
                        </div>


                        <div>

                            <a title="Download Excel file" href="{{route('wholesaler.custom_discount_download',$wholesaler->slug)}}" type="button"
                                class="d-block btn btn-primary btn-sm pull-left"><i class="fa fa-download"></i> Download
                                Current State
                            </a>
                            <br>
                            <br>

                            <div class="d-block">
                                <label for="upload"> <i class="fa fa-file-excel-o"></i> Excel Upload</label>
                                <input id="upload" name="upload" required type="file" class="form-control"
                                    placeholder="Excel Upload..."></div>
                        </div>

                    </div>

                    <div class="form-group row mx-0">
                        <div>
                            {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm multiple-submits'] )  }}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
