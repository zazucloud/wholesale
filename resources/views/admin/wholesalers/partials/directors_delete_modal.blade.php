<div id="deleteWholesalerDirector" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Director</h4>
            </div>

            <form action="{{ route('director.destroy', 'directorDestroy') }}" method="POST">
                @method('delete')
                @csrf
                <div class="modal-body">

                    <input type="hidden" name="director_id" id="director_id">
                  Are you sure you want to delete this director.

                </div>

                 <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

            </form>

           
        </div>

    </div>
</div>
