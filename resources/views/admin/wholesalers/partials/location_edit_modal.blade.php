<div id="editLocation" class="modal fade" role="dialog">
    <div class="modal-dialog">


        <div class="modal-content">

            <form method="post" action="{{route('wholesalers.locations.update',['wholesaler'=>$wholesaler->slug])}}">
                @method('patch')

                <input type="hidden" name="location_id" id="location_id_edit" value="">
                <input type="hidden" id="coordinate_edit" name="coordinate" value="">
                <input type="hidden" id="longitude_edit" name="longitude" value="">
                <input type="hidden" id="latitude_edit" name="latitude" value="">
                <input type="hidden" name="wholesaler_id" value="{{$wholesaler->id}}">
                @csrf

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Location Edit</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="location_label_edit">Location Label:</label>
                        <input placeholder="eg. Head Office" name="label" type="text" class="form-control"
                               id="location_label_edit">
                    </div>


                    <div class="form-group">
                        <label for="locality_edit">Locality:</label>
                        <input placeholder="eg. Athi-River Area" name="locality" type="text" class="form-control"
                               id="locality_edit">
                    </div>


                    <div class="row">

                        <div class="col-lg-12 center-block">
                            <span class="title">Drag Pin to Pick Location <span class="red" style="display: none;" id="not_mapped_label">-- The location is not yet mapped</span></span>
                            <div align="center" id="location_selector_edit" style="width: 100%; height: 300px;"></div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-chevron-left"></i> Close
                    </button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                </div>

            </form>
        </div>

    </div>
</div>
