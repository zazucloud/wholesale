<div id="editFinancialInfo" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Financial Information For {{ $wholesaler->business_name }}</h4>
            </div>

            <form action="{{ route('financial_info', 'wholesaler_finance') }}" method="POST">
                @method('patch')
                @csrf
                <div class="modal-body">

                    <input type="hidden" name="wholesaler_id" id="wholesaler_id">
                    <div class="form-group">
                        <label for="bank_account_no">Account Number:</label>
                        <input placeholder="Account Number" name="bank_account_no" type="text" class="form-control"
                            id="bank_account_no">
                    </div>

                    <div class="form-group">
                        <label for="financial_controller">Financial Controller:</label>
                        <input placeholder="Financial Controller" name="financial_controller" type="text" class="form-control"
                            id="financial_controller">
                    </div>

                    <div class="form-group">
                        <label for="financial_contact_person">Financial Contact Person:</label>
                        <input placeholder="Financial Contact Person" name="financial_contact_person" type="text" class="form-control"
                            id="financial_contact_person">
                    </div>

                    <div class="form-group">
                        <label for="financial_contact_phone">Financial Contact Person Phone:</label>
                        <input placeholder="Financial Contact Person Phone" name="financial_contact_phone" type="text" class="form-control"
                            id="financial_contact_phone">
                    </div>

                    {{-- <div class="form-group">
                        <label for="location_label_edit">Bank Branch:</label>
                        <input placeholder="eg. Head Office" name="label" type="text" class="form-control"
                            id="location_label_edit">
                    </div>

                    <div class="form-group">
                        <label for="location_label_edit">Bank :</label>
                        <input placeholder="eg. Head Office" name="label" type="text" class="form-control"
                            id="location_label_edit">
                    </div> --}}
                </div>

                 <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

            </form>

           
        </div>

    </div>
</div>
