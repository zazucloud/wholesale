<div id="addwholesalerDirector" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Directors To{{ $wholesaler->business_name }}</h4>
            </div>

            <form action="{{ route('director.store') }}" method="POST" class="prevent-multiple-submits">
                @csrf
                <div class="modal-body">

                    <input type="hidden" name="wholesaler_id" value="{{ $wholesaler->id }}">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input placeholder="name" name="name" type="text" class="form-control"
                            >
                    </div>

                    <div class="form-group">
                        <label for="email">email:</label>
                        <input placeholder="email" name="email" type="text" class="form-control"
                            >
                    </div>

                    <div class="form-group">
                        <label for="Phone Number">Phone Number:</label>
                        <input placeholder="Phone Number" name="phone" type="text" class="form-control"
                            >
                    </div>

                </div>

                 <div class="modal-footer">
                <button type="submit" class="btn btn-primary multiple-submits" >Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

            </form>


        </div>

    </div>
</div>
