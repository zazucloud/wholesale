@extends('admin.wholesalers.profile_layout')
@section('bcontent')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('wholesalersCdiscounts',$wholesaler->slug) }}
    @endsection
@endsection
@section('profiletitle') Discount Control @endsection

@section('profilebody')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">

                <div class="card-body override_cbody">

                    {!! Form::open(array('route' => null,'method'=>'POST','class'=>'prevent-multiple-submits')) !!}

                    @csrf
                    <input type="hidden" name="wholesaler_id" id="wholesaler_id" value="{{$wholesaler->id}}">
                    <input type="hidden" name="wholesaler_slug" id="wholesaler_slug" value="{{$wholesaler->slug}}">

                    <div class="col-xs-12 col-sm-12 col-md-12 m-0 p-0">
                        <div class="form-group">

                            <strong class="mb-3">Discount Control</strong><br>
                            <strong>Manage discount rates for {{$wholesaler->business_name}}</strong>
                            <br />
                            <br />
                            @if(count($parent_categories)>0)
                            <ul class="list-group">
                                @foreach($parent_categories as $category)
                                <li class="list-group-item">


                                    <div class="row">
                                        <div class="form-group col-4">
                                            <label>{{$category->name}}</label>
                                            <div class="input-group">
                                                {{ Form::number('discount_rates['.$category->id.']',(($category->discount->first()!==null)?$category->discount->first()->discount_rate:0),
                                            array('class' => 'form-control','id'=>$category->id,'title'=>'Discount
                                            rate for '.$category->name,"placeholder"=>"Discount Rate",
                                            'aria-describedby'=>"sizing-addon1",'step'=>'.01')) }}

                                                <div class="input-group-append">
                                                    <div class="input-group-text">%
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($category->childinventory as $child)
                                        <div class="form-group col-4">
                                            <label>{{$child->name}}</label>
                                            <div class="input-group">
                                                {{ Form::number('discount_rates['.$child->id.']', (($child->discount->first()!==null)?$child->discount->first()->discount_rate:0),
                                            array('class' => 'form-control','id'=>$child->id,'title'=>'Discount
                                            rate for '.$child->name,"placeholder"=>"Discount Rate",
                                            'aria-describedby'=>"sizing-addon1",'step'=>'.01')) }}
                                                <div class="input-group-append">
                                                    <div class="input-group-text">%
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach


                                    </div>

                                </li>

                                @endforeach
                            </ul>
                            @else
                            <div class="alert alert-danger">
                                Categories need to be added/configured first before managing catalogue visibility
                            </div>
                            @endif




                        </div>
                    </div>

                    <div class="form-group row mx-0">
                        <div class="col-md-6 center">
                            {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm multiple-submits'] )  }}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
