@extends('admin.wholesalers.profile_layout')
@section('profiletitle')Manage Staff Assignments @endsection

@section('profilebody')



<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">

                        </div>
                        <div class="col-6">
                            <a href="{{route('wholesaler.staff_assignment',['wholesaler'=>$wholesaler->slug])}}"
                                class="btn btn-info pull-right btn-sm"> <i class="fa fa-chevron-left mr-2"></i> Back to
                                Staff Assignment List</a>
                        </div>

                    </div>

                </div>

                <div class="card-body override_cbody">
                    <div class="alert alert-info">
                        Select staff who can manage {{$wholesaler->business_name}}
                    </div>

                    {!! Form::open(array('route' =>
                    ['wholesaler.staff_assignment_process',$wholesaler->slug],'method'=>'POST','class'=>'prevent-multiple-submits')) !!}

                    @csrf

                    <ul class="list-group">
                        @foreach($staff as $item)
                        <li class="list-group-item">
                            <div class="row">

                                <label
                                    class="col-12">{{ Form::checkbox('staff[]', $item->id, ((in_array($item->id,$wholesaler_staff))?true:false), array('class' => 'name')) }}

                                    {{$item->name}}</label>

                            </div>

                        </li>

                        @endforeach
                    </ul>
                    <div class="form-group row mx-0 mt-3">
                        <div class="col-md-6 center">
                            {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits'] )  }}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
