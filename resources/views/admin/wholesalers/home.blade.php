@extends('admin.wholesalers.profile_layout')

@section('bcontent')
@section('breadcrumbs')
    {{ Breadcrumbs::render('wholesalersHome', $wholesaler->slug) }}
@endsection
@endsection


@section('profiletitle')
Wholesaler Profile
@endsection

@section('profilebody')
<div class="row mb-3">
    <div class="col-12 col-md-6">
        <div class="widget-bg">
            <div class="widget-body px-4 pt-3 pb-4">
                <div class="icon-box icon-box-centered flex-1 my-0 p-0 radius-3">
                    <header class="align-self-center"><a href="#" class="bg-grey fs-30 text-muted"><i
                                class="lnr lnr-users icon-lg"></i></a>
                    </header>
                    <section class="mt-0">
                        <h6 class="icon-box-title my-0"><span class="counter" id="counter-3">
                                {{ $last_three_months_orders }}</span></h6>
                        <p class="mb-0">Orders in the last 3 months</p>
                    </section>
                </div>
                <!-- /.icon-box -->
            </div>
            <!-- /.widget-body -->
        </div>
        <!-- /.widget-bg -->
    </div>

    <div class="col-12 col-md-6">
        <div class="widget-bg">
            <div class="widget-body px-4 pt-3 pb-4">
                <div class="icon-box icon-box-centered flex-1 my-0 p-0 radius-3">
                    <header class="align-self-center"><a href="#" class="bg-grey fs-30 text-muted"><i
                                class="lnr lnr-users icon-lg"></i></a>
                    </header>
                    <section class="mt-0">
                        <h6 class="icon-box-title my-0"><span class="counter"
                                id="counter-3">Ksh.{{ amount($lastordervalue) }}</span></h6>
                        <p class="mb-0">Value of Last Order</p>
                    </section>
                </div>
                <!-- /.icon-box -->
            </div>
            <!-- /.widget-body -->
        </div>
        <!-- /.widget-bg -->
    </div>


</div>
<div class="card">
    <div class="card-body p-0">
        <span class="caption-subject font-blue-madison bold uppercase mx-3 my-3"></span>
        <h5 class="box-title"><b class="mx-3 my-2">Customer Information</b></h5>
        <div class="tabs tabs-bordered mx-3 my-2">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a href="#profile-tab" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2 active">
                        <i class="mdi mdi-pencil-box-multiple font-18 d-md-none d-block"></i>
                        <span class="d-none d-md-block">Profile</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#branches" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                        <i class="mdi mdi-image font-18 d-md-none d-block"></i>
                        <span class="d-none d-md-block"> Branches </span>
                    </a>
                </li>
                {{-- <li class="nav-item">
                        <a href="#financial" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2 ">
                            <i class="mdi mdi-book-open-variant font-18 d-md-none d-block"></i>
                            <span class="d-none d-md-block"> Financial </span>
                        </a>
                    </li> --}}
                <li class="nav-item">
                    <a href="#directors" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                        <i class="mdi mdi-image font-18 d-md-none d-block"></i>
                        <span class="d-none d-md-block"> Directors </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#trade" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                        <i class="mdi mdi-book-open-variant font-18 d-md-none d-block"></i>
                        <span class="d-none d-md-block"> Trade References </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#accounts" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                        <i class="mdi mdi-book-open-variant font-18 d-md-none d-block"></i>
                        <span class="d-none d-md-block"> Account Details </span>
                    </a>
                </li>
                @if (auth()->user()->hasPermissionTo('wholesaler-credit_info'))
                    <li class="nav-item">
                        <a href="#credit" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                            <i class="mdi mdi-book-open-variant font-18 d-md-none d-block"></i>
                            <span class="d-none d-md-block"> Credit Information </span>
                        </a>
                    </li>
                @endif
            </ul> <!-- end nav-->
        </div>
        <div class="tab-content">



            <div class="tab-pane active" id="profile-tab">
                <div align="right" class="mt-3">
                    <a href="{{ route('wholesalers.edit_basic_info', ['wholesaler' => $wholesaler->slug]) }}"
                        class="mt-5 text-info">
                        <i class="fa fa-edit"></i> Edit Basic Details</a>
                </div>
                <div class="row">

                    <div class="col-lg-6">

                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Business Name:</b></span>
                            <span>{{ $wholesaler->business_name }}</span>
                        </div>


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Postal Address:</b></span>
                            <span>{{ $wholesaler->postal_address }}</span>
                        </div>


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Physical Address:</b></span>
                            <span>{{ $wholesaler->physical_address }}</span>
                        </div>


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Telephone:</b></span>
                            <span>{{ $wholesaler->telephone }}</span>
                        </div>


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Email:</b></span>
                            <span>{{ $wholesaler->email }}</span>
                        </div>


                    </div>


                    <div class="col-lg-6">


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Ownership Type:</b></span>
                            <span>{{ $wholesaler->ownership_text($wholesaler->ownership_type, $wholesaler->ownership_type_extra) }}</span>
                        </div>

                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Registration Number:</b></span>
                            <span>{{ $wholesaler->registration_number }}</span>
                        </div>


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Registration Date:</b></span>
                            <span>{{ $wholesaler->registration_date }}</span>
                        </div>


                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>Nature of Business:</b></span>
                            <span>{{ $wholesaler->business_nature }}</span>
                        </div>

                        <div class="margin-top-20 profile-data">
                            <span class="data_label"><b>PIN:</b></span>
                            <span>{{ $wholesaler->pin }}</span>
                        </div>


                    </div>


                </div>
            </div>

            <div class="tab-pane" id="branches">
                <div class="card mt-2">
                    <div class="card-header row mx-0">
                        <div class="col-6">

                        </div>
                        <div class="col-6">
                            <button @click="openWhLocationModal(null,1)" class="btn btn-sm btn-primary pull-right"> <i
                                    class="fa fa-plus mr-2"></i> Add
                                Location</button>
                        </div>

                    </div>

                    <div class="card-body override_cbody">

                        <div class="spacious-container">
                            <table class="table table-hover center-text" id="invoicestable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Locality</th>
                                        <th scope="col">Orders</th>
                                        <th scope="col">Map</th>
                                        <th scope="col"></th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($locations as $index => $location)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>

                                            <td>{{ $location->label }}</td>
                                            <td>{{ $location->locality }}</td>
                                            <td>
                                                <form
                                                    action="{{ route('wholesalers.orders', ['wholesaler' => $wholesaler->slug]) }}"
                                                    method="GET">
                                                    <input type="hidden" name="location"
                                                        value="{{ $location->id }}">
                                                    <button class="badge bg-soft-info text-info btn">
                                                        {{ $location->counter }}
                                                    </button>

                                                </form>
                                            </td>
                                            <td>
                                                @if ($location->coordinate === null)
                                                    <button class="btn dark"><i class="fa fa-map"></i> Not
                                                        Mapped
                                                    </button>
                                                @else
                                                    <a href="#" class="btn btn-primary"><i
                                                            class="fa fa-map-marker"></i>
                                                        See on
                                                        Map
                                                    </a>
                                                @endif

                                            </td>
                                            <td>
                                                <button type="button"
                                                    @click="openWhLocationModal({{ $location }},2)"
                                                    class="btn btn-sm edit_location">
                                                    <i class="fa fa-edit"></i> Edit
                                                </button>
                                            </td>


                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
            </div>
            {{-- <div class="tab-pane" id="financial">

                    <div class="row">

                        <div class="col-lg-6">

                            <div class="margin-top-20 profile-data">
                                <span class="data_label">Bank</span>
                                <span class="uppercase">{{ \App\Models\Bank::name($wholesaler->bank_id) }}</span>
                            </div>


                            <div class="margin-top-20 profile-data">
                                <span class="data_label">Bank Branch</span>
                                <span
                                    class="uppercase">{{ \App\Models\BankBranch::name($wholesaler->bank_branch_id) }}</span>

                            </div>

                            <div class="margin-top-20 profile-data">
                                <span class="data_label">Account Number</span>
                                <span>{{ $wholesaler->bank_account_no }}</span>
                            </div>


                        </div>

                        <div class="col-lg-6">

                            <div class="margin-top-20 profile-data">
                                <span class="data_label">Financial Controller</span>
                                <span>{{ $wholesaler->financial_controller }}</span>
                            </div>

                            <div class="margin-top-20 profile-data">
                                <span class="data_label">Financial Contact Person</span>
                                <span>{{ $wholesaler->financial_contact_person }}</span>
                            </div>

                            <div class="margin-top-20 profile-data">
                                <span class="data_label">Financial Contact Person Phone</span>
                                <span>{{ $wholesaler->financial_contact_phone }}</span>
                            </div>

                        </div>

                    </div>
                </div> --}}
            <div class="tab-pane" id="directors">
                <div class="row">

                    {{-- <div class="col-lg-12">
                        <h1 class="page-title">
                            <button type="button" class="btn btn-primary btn-circle pull-right"
                                data-toggle="modal" data-target="#addwholesalerDirector"> <i
                                    class="fa fa-plus"></i>
                                Add Director
                            </button>
                            <br>
                        </h1>
                    </div> --}}

                    <div class="col-lg-12">

                        <div class="card mt-2">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-6">

                                    </div>
                                    <div class="col-6">
                                        <button @click="openWhDirectorsModal(null,1)"
                                            class="btn btn-sm btn-primary pull-right"> <i class="fa fa-plus mr-2"></i>
                                            Add
                                            Directors</button>
                                    </div>

                                </div>

                            </div>

                            <div class="card-body override_cbody">

                                <div class="spacious-container">
                                    <table class="table table-hover center-text" id="invoicestable">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th>Name</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($directors as $index => $director)
                                                <tr>
                                                    <td>{{ $index + 1 }}</td>

                                                    <td>{{ $director->name }}</td>

                                                    <td>{{ $director->phone }}</td>

                                                    <td>{{ $director->email }}</td>

                                                    <td class="tw-flex">
                                                        <button class="btn btn-primary btn-sm mr-2"
                                                            @click="openWhDirectorsModal({{ $director }},2)">

                                                            <i data-toggle="tooltip" data-placement="top"
                                                                title="Edit" class="fa fa-edit"></i>
                                                        </button>

                                                        <form
                                                            action="{{ route('director.destroy', 'directorDestroy') }}"
                                                            method="POST">
                                                            @csrf
                                                            <input type="hidden" name="_METHOD" value="DELETE">
                                                            <button class="btn btn-danger btn-sm"
                                                                onclick="return confirm('Are you sure you a director from your system ?')">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </td>


                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>


                    </div>
                    <div class="col-lg-6"></div>

                </div>
            </div>
            <div class="tab-pane" id="trade">
                <div class="col-lg-6"></div>
                <div class="col-lg-6"></div>
            </div>
            <div class="tab-pane" id="accounts">

                <div class="row">

                    <div class="col-12">
                        <div class="margin-top-20 profile-data">
                            <span class="data_label">Account Username</span>
                            {{-- <span>{{ \App\Models\User::account_username($wholesaler->user_id) }}</span> --}}
                        </div>

                        <div class="margin-top-20 profile-data">
                            <span class="data_label">Last Activity</span>
                            {{-- <span>{{ optional(\App\Models\User::find($wholesaler->user_id))->last_active() }}</span> --}}
                        </div>


                    </div>




                </div>


                <div class="row">
                    <br>
                    <br>
                </div>


                <div class="row">

                    <div class="col-lg-12">


                        <h6 class="profile-desc-title">Recent Account Activity</h6>

                        @if (!is_null($activity_log))
                            <div class="card mt-2">
                                <div class="card-header">
                                </div>

                                <div class="card-body override_cbody">

                                    <div class="spacious-container">



                                        <table class="table table-hover center-text" id="invoicestable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <td>Activity</td>
                                                    <td>IP Address</td>
                                                    <td>Timestamp</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($activity_log as $index => $activity)
                                                    <tr>
                                                        <td>{{ $index + 1 }}</td>

                                                        <td>{{ $activity->description }}</td>

                                                        <td>{{ json_decode($activity->properties)->ip }}</td>

                                                        <td>{{ stamp_date_time($activity->created_at) }}</td>


                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>

                                </div>

                            </div>
                        @else
                            <div class="alert alert-info">

                                No Activity Has Been Logged

                            </div>
                        @endif

                    </div>


                </div>
            </div>
            @if (auth()->user()->hasPermissionTo('wholesaler-credit_info'))
                <div class="tab-pane" id="credit">
                    <div class="row">
                        <div class="col-6">
                        <p><b>Credit Amount:</b> Ksh.{{ amount($wholesaler->credit_amount ?? 0) }}</p>
                    </div>
                    <div class="col-6">
                        <button @click="openCredit()" class="btn btn-sm btn-primary">
                            Edit Credit Information</button>
                    </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<wholesalerlocationmodal :wholesaler="{{ json_encode($wholesaler) }}"></wholesalerlocationmodal>
<directorsmodal :wholesaler="{{ json_encode($wholesaler) }}"></directorsmodal>
<creditmodal :wholesaler="{{ json_encode($wholesaler) }}"></creditmodal>
@endsection
