@extends('admin.wholesalers.profile_layout')

@section('bcontent')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('wholesalersPayments',$wholesaler->slug) }}
    @endsection
@endsection

@section('profiletitle')Wholesaler Payments @endsection

@section('profilebody')


<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">
                <div class="card-header">

                </div>

                <div class="card-body override_cbody">

                    <div class="spacious-container">
                        <table class="table table-hover center-text" id="invoicestable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Actions</th>
                                    <th>Channel</th>
                                    <th>Amount</th>
                                    <th>Reference</th>
                                    <th>Created By</th>
                                    <th>Timestamp</th>
                                    <th></th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $index => $payment)

                                <tr>
                                    <th scope="row">{{ $index+1 }}</th>

                                    <td><a href="#">{{$payment->receipt_no}}</a></td>

                                    <td>{{$payment->channel_name}}</td>
                                    <td>{{amount($payment->amount)}}</td>
                                    <td>{{$payment->reference}}</td>
                                    <th>{{$payment->creator->name}}</th>
                                    <td>{{stamp_date_time($payment->created_at)}}</td>
                                    <td>
                                        <a href="#" class="btn btn-primary"><i class="fa fa-newspaper-o"></i> View</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                {{-- <div style="padding-left:20px;">
                    {!! $accounts->appends(Request::except('page'))->render() !!}

                </div> --}}
            </div>
        </div>
    </div>
</div>



@endsection
