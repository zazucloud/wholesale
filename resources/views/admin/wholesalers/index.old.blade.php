@extends('admin.layout')
@section('title') Wholesalers @endsection

@section('page-content')

    <h1 class="page-title"> Wholesalers
        <small></small>

        @can('create_wholesaler_accounts')
        <a href="{{route('admin.wholesalers.create_account')}}" class="btn btn-primary btn-circle pull-right"> <i
                    class="fa fa-plus"></i> Add Wholesaler</a>
        @endcan



    </h1>
    <!-- END PAGE HEADER-->
    <div class="m-heading-1 border-yellow-lemon m-bordered">
        <h3>Showing Wholesalers</h3>
        <p>Default page description - relevant information relating to the current page or process</p>
    </div>
    <div class="row">
        <div class="col-md-12">


            @if(!count($wholesalers))

                <div class="alert alert-info">
                    No wholesalers found
                </div>

            @else


                <div class="portlet light ">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Registration Number</th>
                                <th>Account Created</th>
                                <th>Account Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($wholesalers as $wholesaler)

                                <tr>
                                    <td>{{$wholesaler->business_name}}</td>
                                    <td>{{$wholesaler->registration_number}}</td>
                                    <td>{{stamp_date_clean($wholesaler->created_at)}}</td>
                                    <td>

                                        @if($wholesaler->status==1)
                                            <span class="label label-success">Active</span>
                                        @elseif($wholesaler->status==5)
                                            <span class="label label-success">Disabled</span>
                                        @elseif($wholesaler->status==9)
                                            <span class="label label-success">Banned</span>
                                        @endif


                                    </td>
                                    <td>
                                        <a href="{{route('admin.wholesalers.show',$wholesaler->slug)}}"
                                           class="btn btn-primary"><i class="fa fa-newspaper-o"></i> View Profile
                                        </a>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>

                        {{$wholesalers->links()}}

                    </div>
                </div>

            @endif

        </div>
    </div>

@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
    </script>
@endsection