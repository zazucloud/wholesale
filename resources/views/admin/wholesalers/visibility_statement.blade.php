@if((count($visible_categories))===(count($categories)))

    <div class="alert alert-success">
        All categories/items are visible to {{$wholesaler->business_name}}
    </div>

@elseif((count($visible_categories))===0)

<div class="alert alert-danger">
    No categories/items are visible to {{$wholesaler->business_name}}
</div>


@else
    <br>
    <div class="alert alert-info">
        {{$wholesaler->business_name}} can view item in the following categories:
        <div class="bold">{{\App\Models\InventoryCategory::name_multiple($visible_categories)}}</div>
    </div>


@endif