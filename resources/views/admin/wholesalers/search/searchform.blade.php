<div class="row">
    {!! Form::open(['route' => null, 'method' => 'get', 'class' => 'row col-sm-10']) !!}
    <div class="col-sm-10">
        <div class="">
            <label>Search</label>
            {!! Form::text('search', null, [
                'class' => 'form-control form-control-inputs',
                'required' => true,
                'placeholder' => 'Search By Name,Customer Code,Physical Addres, Pin,Phone number or Email Address',
            ]) !!}
        </div>
    </div>
    <div class="col-sm-2  mt-4 pt-2">
        <div>
            {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

        </div>
    </div>
    {!! Form::close() !!}
    {{-- @can('drivers-create') --}}

    <div class="col-md-2 col-12 mt-4 pt-2">
        <a href="{{ route('admin.wholesalers.create_account') }}" class="btn btn-primary btn-sm pull-right"> <i
            class="fa fa-plus mr-2"></i> New Wholesaler</a>
    </div>
    {{-- @else
    <button class="btn btn-default col-sm-2 disonabled">
        <span class="mdc-fab__label"> <i class="fe-user-plus"></i> <span class="mdc-fab__label"> Add
                Driver</span></span>
    </button>
    @endcan --}}
</div>
