

<table>
	<thead style="text-align: center;">
		<tr>

			<th><b style="margin:5px;">Category</b></th>
			<th><b style="margin:5px;">SKU</b></th>
			<th><b style="margin:5px;">Item Name</b></th>
			<th><b style="margin:5px;">Base Price</b></th>
			<th><b style="margin:5px;">Regional Price</b></th>
			<th><b style="margin:5px;">Rcd_id</b></th>

		</tr>
	</thead>
	<tbody>
		@foreach($results AS $value)
		<tr>
			<td style="font-size: 8px;text-align:right;">{{$value['category']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['sku']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['itemname']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['basePrice']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['regionPrice']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['rcd_id']}}</td>

		</tr>

		@endforeach
	</tbody>
	<tr></tr>
	<tr></tr>
</table>
