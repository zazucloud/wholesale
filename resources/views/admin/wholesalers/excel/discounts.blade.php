

<table>
	<thead style="text-align: center;">
		<tr>

			<th style="word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Category</b></th>
			<th style="word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">SKU</b></th>
			<th style="word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Item Name</b></th>
			<th style="word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Discount</b></th>
			<th style="word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Base Price</b></th>
			<th style="word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Rcd_id</b></th>

		</tr>
	</thead>
	<tbody>
		@foreach($results AS $value)
		<tr>
			<td style="font-size: 8px;text-align:right;">{{$value['category']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['sku']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['itemname']}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['percentage']}}</td>
			<td style="font-size: 8px;text-align:right;">Ksh.{{number_format($value['basePrice'])}}</td>
			<td style="font-size: 8px;text-align:right;">{{$value['rcd_id']}}</td>

		</tr>

		@endforeach
	</tbody>
	<tr></tr>
	<tr></tr>
</table>
