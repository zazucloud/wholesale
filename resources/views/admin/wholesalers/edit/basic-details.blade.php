@extends('layouts.base')
@section('title')
    Edit Wholesaler Details
@endsection

@php
    $theme_manager->enqueue_files(['select2', 'selectpicker', 'multiselect'], ['select2', 'selectpicker', 'multiselect']);
@endphp

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">

                        {!! Form::open([
                            'url' => route('wholesalers.update_basic_info', $wholesaler->slug),
                            'files' => true,
                            'class' => 'prevent-multiple-submits',
                        ]) !!}

                        @csrf

                        <div class="form-group">
                            <label for="name">Wholesaler Account Name</label>
                            <input value="{{ old('name', $wholesaler->business_name) }}" required id="name" name="name"
                                type="text" class="form-control" placeholder="Wholesaler's Name">
                        </div>


                        <div class="form-group">
                            <label for="ownership_type">Type Of Ownership</label>
                            <select name="ownership_type" id="ownership_type" class="form-control">
                                <option @if ($wholesaler->ownership_type == 1) selected="selected" @endif value="1">Limited
                                    Company
                                </option>
                                <option @if ($wholesaler->ownership_type == 2) selected="selected" @endif value="2">
                                    Partnership
                                </option>
                                <option @if ($wholesaler->ownership_type == 3) selected="selected" @endif value="3">Limited
                                    Partnership
                                </option>
                                <option @if ($wholesaler->ownership_type == 4) selected="selected" @endif value="4">Sole
                                    Proprietorship
                                </option>
                                <option @if ($wholesaler->ownership_type == 5) selected="selected" @endif value="5">Other
                                </option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="registration_number">Customer Code</label>
                            <input required id="registration_number" name="registration_number" type="text"
                                class="form-control" value="{{ old('name', $wholesaler->registration_number) }}"
                                placeholder="Customer Code">
                        </div>

                        <div class="form-group">
                            <label>County</label>
                            <select class="m-b-10 form-control select2-hidden-accessible" name="county_id"
                                value="{{ $wholesaler->county_id }} " data-toggle="select2" tabindex="-1"
                                aria-hidden="true" required>
                                <option> Select County</option>
                                @foreach ($counties as $county)
                                    <option value="{{ $county->id }}"
                                        {{ $wholesaler->county_id == $county->id ? 'selected="selected"' : '' }}>
                                        {{ $county->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="physical_address">Physical Address</label>
                            <input required id="physical_address" name="physical_address" type="text"
                                class="form-control" value="{{ old('physical_address', $wholesaler->physical_address) }}"
                                placeholder="Physical Address">
                        </div>


                        <div class="form-group">
                            <label for="postal_address">Postal Address</label>
                            <input id="postal_address" name="postal_address" type="text" class="form-control"
                                value="{{ old('postal_address', $wholesaler->postal_address) }}"
                                placeholder="Postal Address">
                        </div>


                        <div class="form-group">
                            <label for="pin">PIN Number</label>
                            <input required id="pin" name="pin" type="text" class="form-control"
                                value="{{ old('pin', $wholesaler->pin) }}" placeholder="PIN Number">
                        </div>


                        <div class="form-group">
                            <label for="contact_person">Contact Person</label>
                            <input spellcheck="false" required id="contact_person" name="contact_person" type="text"
                                class="form-control" value="{{ old('contact_name', $wholesaler->contact_name) }}"
                                placeholder="Contact Person">
                        </div>


                        <div class="form-group">
                            <label for="contact_person_position">Contact Person Position</label>
                            <input required id="contact_person_position" name="contact_person_position" type="text"
                                class="form-control" value="{{ old('contact_position', $wholesaler->contact_position) }}"
                                placeholder="Position of contact person">

                            <span class="text-muted">Designation of contact person</span>
                        </div>


                        <div class="form-group">
                            <label for="telephone">Phone Number</label>
                            <input required id="telephone" name="telephone" type="text" class="form-control"
                                value="{{ old('telephone', $wholesaler->telephone) }}" placeholder="Phone Number">

                        </div>


                        <div class="form-group">
                            <label for="email_address">Email Address</label>
                            <small><em>(Will serve as account username)</em></small>
                            <input spellcheck="false" required id="email_address" name="email_address" type="email"
                                class="form-control" value="{{ old('email_address', $wholesaler->email) }}"
                                placeholder="Email Address">
                        </div>

                        <div class="form-group row mx-0">
                            <div class="col-sm-12">
                                <label>Choose If Wholesaler Is Either A Credit or Cash Client</label>

                                <div class="form-check">
                                    <input type="radio" name="credit_mode" id="credit_mode1" class="form-check-input"
                                        value="0" {{ $wholesaler->credit_mode == 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="credit_mode1">
                                        Credit Client
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" name="credit_mode" id="credit_mode2" class="form-check-input"
                                        value="1" {{ $wholesaler->credit_mode == 1 ? 'checked' : '' }}>

                                    <label class="form-check-label" for="credit_mode2">
                                        Cash Client
                                    </label>
                                </div>
                            </div>
                        </div>

                        {{-- <h5 class="vrs-title col-md-12"><strong>Contact Person's</strong></h5> --}}
                        {{-- <contacts :getinputs="{{ json_encode($client->contacts) }}"></contacts> --}}

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary multiple-submits">Save</button>

                        </div>

                        {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
    </script>
@endsection
