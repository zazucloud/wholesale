<br>
<div class="alert alert-info">
    @if(count($given_discounts)==0)

        No discounts offered to {{$wholesaler->business_name}}

    @else

        {{$wholesaler->business_name}} receives the following discounts:
        <div class="bold">{{\App\Models\WholesalerCategoryDiscount::discounts_narration($given_discounts)}}</div>

        <strong style="margin-top: 10px; display: block; font-weight: normal;">NB: Discounts are only effective on categories visible to the wholesaler</strong>
    @endif
</div>