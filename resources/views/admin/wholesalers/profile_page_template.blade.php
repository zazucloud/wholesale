@extends('admin.wholesalers.profile_layout')
@section('title')  {{$wholesaler->business_name}} @endsection
@section('profile_page_title') Wholesaler Profile @endsection

@section('profile_content')

@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
        $("#wholesaler_menu_").addClass('active');
    </script>
@endsection