<div class="profile-usermenu">
    <ul class="nav">
        <li id="wholesaler_menu_overview">
            <a href="{{ route('admin.wholesalers.show', $wholesaler->slug) }}">
                <i class="fa fa-home"></i> Home </a>
        </li>
        <li id="wholesaler_menu_orders">
            <a href="{{ route('wholesalers.orders', $wholesaler->slug) }}">
                <i class="fa fa-arrow-down"></i> Orders </a>
        </li>

        <li id="wholesaler_menu_accounts">
            <a href="{{ route('wholesalers.accounts', $wholesaler->slug) }}">
                <i class="fa fa-sign-in"></i> Accounts </a>
        </li>


        <li id="wholesaler_menu_payments">
            <a href="{{ route('wholesalers.payments', $wholesaler->slug) }}">
                <i class="fa fa-diamond"></i> Payments </a>
        </li>

        <li id="wholesaler_menu_staff_assignment">
            <a href="{{ route('wholesaler.staff_assignment', $wholesaler->slug) }}">
                <i class="fa fa-user-secret"></i> Staff Assignment</a>
        </li>

        <li class="hide-for-now" id="wholesaler_menu_activity_logs">
            <a href="">
                <i class="fa fa-list"></i> Activity Logs </a>
        </li>

        @if (auth()->user()->can('view_wholesaler_catalog_visibility') ||
            auth()->user()->can('manage_wholesaler_catalog_visibility'))
            <li id="wholesaler_menu_catalog_visibility">
                <a href="{{ route('wholesaler.catalog_visibility', $wholesaler->slug) }}">
                    <i class="fa fa-eye"></i> Catalogue Visibility </a>
            </li>
        @endcan

        @if (auth()->user()->can('view_wholesaler_discounts') ||
            auth()->user()->can('manage_wholesaler_discounts'))

            <li id="wholesaler_menu_custom_discounts">
                <a href="{{ route('wholesaler.custom_discount', $wholesaler->slug) }}">
                    <i class="fa fa-money"></i> Custom Discounts</a>
            </li>


        @endcan
        <li id="wholesaler_menu_discount_control">
            <a href="{{ route('wholesaler.discount_control', $wholesaler->slug) }}">
                <i class="fa fa-wrench"></i> Discount Configuration</a>
        </li>


        @if (auth()->user()->can('delete_wholesalers'))
            <li id="wholesaler_delete">
                <a href="{{ route('admin.wholesalers.delete', $wholesaler->slug) }}">
                    <i class="fa fa-trash"></i> Delete Wholesaler </a>
            </li>
        @endcan

</ul>
</div>
