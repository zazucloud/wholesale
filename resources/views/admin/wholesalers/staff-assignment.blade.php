@extends('admin.wholesalers.profile_layout')

@section('bcontent')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('wholesalersStaff',$wholesaler->slug) }}
    @endsection
@endsection
@section('profiletitle')Staff Assignments @endsection

@section('profilebody')


<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            <h6>Showing staff Assigned</h6>
                        </div>
                        <div class="col-6">
                            <a href="{{route('wholesaler.staff_assignment_edit',['wholesaler'=>$wholesaler->slug])}}"
                                class="btn btn-info btn-sm pull-right"> <i
                                         class="fa fa-check-square-o mr-2"></i> Manage Account Selection</a>
                        </div>

                    </div>

                </div>

                <div class="card-body override_cbody">

                    <div class="spacious-container">
                        <table class="table table-hover center-text" id="invoicestable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Accounts</th>
                                    <th>Action</th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach($wholesaler_staff as $index => $item)

                                <tr>
                                    <th scope="row">{{ $index+1 }}</th>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{((!is_null($item->wholesalers))?$item->wholesalers()->count():0)}}</td>
                                        <td>
                                            <a href="#"
                                               class="btn btn-primary btn-xs"> View Profile
                                            </a>
                                        </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <div style="padding-left:20px;">
                    {!! $wholesaler_staff->appends(Request::except('page'))->render() !!}

                </div>
            </div>
        </div>
    </div>
</div>



@endsection
