<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">

        <strong class="mb-3">Catalog Visibility Control</strong>

        <br />
        <br />
        <div class="col-md-12 tw-flex" style=" padding-left: 1.3rem; ">
            <input type="checkbox" id="markall"><span class="ml-2">Select All</span>
        </div>
        @if (count($categories) > 0)
            <ul class="list-group">
                @foreach ($parent_categories as $category)
                    <li class="list-group-item">

                        <div class="row">

                            <label
                                class="col-4 tw-flex">{{ Form::checkbox('categories[]', $category->id, in_array($category->id, $visible_categories) ? true : false, ['class' => 'name p_mark_all mr-2']) }}

                                {{ $category->name }}</label>

                            @foreach ($category->childinventory as $child)
                                <label
                                    class="col-4 tw-flex">{{ Form::checkbox('categories[]', $child->id, in_array($child->id, $visible_categories) ? true : false, ['class' => 'name p_mark_all mr-2']) }}

                                    {{ $child->name }}</label>
                            @endforeach


                        </div>

                    </li>
                @endforeach
            </ul>
        @else
            <div class="alert alert-danger">
                Categories need to be added/configured first before managing catalogue visibility
            </div>
        @endif




    </div>
</div>
