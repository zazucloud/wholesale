<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>WholeSaler Name</label>
        {!! Form::text('name', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => "Wholesaler's Name",
        ]) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>County</label>
        {!! Form::select(
            'county_id',
            $counties,
            [],
            [
                'class' => 'form-control',
                'placeholder' => 'Select County',
                'required' => true,
                'data-toggle' => 'select2',
            ],
        ) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Ownership</label>

        {!! Form::select(
            'ownership_type',
            [
                '' => 'Choose Type Of Ownership',

                '1' => 'Limited Company',
                '2' => 'Partnership',
                '3' => 'Limited Partnership',
                '4' => 'Sole Proprietorship',
                '5' => 'Other',
            ],
            null,
            ['class' => 'custom-select', 'id' => 'ownership_type', 'required' => true],
        ) !!}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Customer Code</label>
        {!! Form::text('registration_number', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Customer Code',
        ]) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Physical Address</label>
        {!! Form::text('physical_address', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Physical Address',
        ]) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Pin Number</label>
        {!! Form::text('pin', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'PIN Number']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Contact Person Name</label>
        {!! Form::text('contact_name', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Contact Person',
        ]) !!}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Designation</label>
        {!! Form::text('contact_person_position', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Contact Person Position',
        ]) !!}
        <small><em>Designation of contact person</em></small>
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Phone Number</label>
        {!! Form::text('telephone', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Phone Number',
        ]) !!}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Email Address</label>
        {!! Form::email('email_address', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Email Address',
        ]) !!}
        <small><em>(Will serve as account username)</em></small>
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Choose If Wholesaler Is Either A Credit or Cash Client</label>

        <div class="form-check">
            {!! Form::radio('credit_mode', 0, ['class' => 'form-check-input', 'id' => 'credit_mode1']) !!}
            <label class="form-check-label" for="credit_mode1">
                Credit Client
            </label>
        </div>
        <div class="form-check">
            {!! Form::radio('credit_mode', 1, ['class' => 'form-check-input', 'id' => 'credit_mode2']) !!}

            <label class="form-check-label" for="credit_mode2">
                Cash Client
            </label>
        </div>
    </div>
</div>

<div class="alert alert-info mx-3">
    Default account credentials will automatically be emailed to the wholesaler
</div>
