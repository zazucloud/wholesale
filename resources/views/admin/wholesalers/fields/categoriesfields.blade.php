<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">

        <strong class="mb-3">Catalog Visibility Control</strong>

        <br />
        @if(count($categories)>0)
        <ul class="list-group">
            @foreach($parent_categories as $category)
            <li class="list-group-item">

               
                    <div class="row">

                        <label
                            class="col-4">{{ Form::checkbox('categories[]', $category->id, false, array('class' => 'name')) }}

                            {{$category->name}}</label>
                        
                        @foreach($category->childinventory as $child)
                        <label
                            class="col-4">{{ Form::checkbox('categories[]', $child->id, false, array('class' => 'name')) }}

                            {{$child->name}}</label>
                        @endforeach


                    </div>
                
            </li>

            @endforeach
        </ul>
        @else
        <div class="alert alert-danger">
            Categories need to be added/configured first before managing catalogue visibility
        </div>
        @endif

        
       

    </div>
</div>