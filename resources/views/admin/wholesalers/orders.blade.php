@extends('admin.wholesalers.profile_layout')

@section('bcontent')
@section('breadcrumbs')
    {{ Breadcrumbs::render('wholesalersOrders', $wholesaler->slug) }}
@endsection
@endsection

@section('profiletitle')
Wholesaler Orders
@endsection

@section('profilebody')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">
                <div class="card-header">
                </div>

                <div class="card-body override_cbody">

                    <div class="spacious-container">
                        <table class="table table-hover center-text" id="invoicestable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>

                                    <th scope="col">OrderNo</th>
                                    <th scope="col">Order Total</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Branch</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Actions</th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $index => $order)
                                    <tr>
                                        <th scope="row">{{ $index + 1 }}</th>

                                        <th>#{{ $order->face_id }}</th>
                                        <td>{{ number_format($order->total) }}</td>
                                        <td>{{ $order->status_text }}</td>
                                        <td>{{ !is_null($order->location) ? $order->location->label : 'N/A' }}</td>

                                        <td>{{ stamp_date_time($order->created_at) }}<br>
                                            <small>{{ ago($order->created_at) }}</small>
                                        </td>

                                        <td>
                                            <a href="{{ route('orders.view', ['order' => $order->id]) }}"
                                                class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                        </td>


                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <div style="padding-left:20px;">
                    {!! $orders->appends(Request::except('page'))->render() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
