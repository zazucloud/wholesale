@extends('admin.wholesalers.profile_layout')
@section('profiletitle'){{$wholesaler->business_name}} @endsection

@section('profilebody')

    <div class="row">


        <div class="col-md-12">



            <div class="card ">
               <div class="mt-3 mr-2"> <a href="{{route('wholesalers.accounts',['wholesaler'=>$wholesaler->slug])}}"
                class="btn btn-info btn-sm pull-right"> <i
                         class="fa fa-arrow-left mr-2"></i> Back to Accounts Listing</a></div>
                <div class="card-body">

                    <form method="post"
                          action="{{route('wholesalers.account_update',['wholesaler'=>$wholesaler->slug])}}"
                          enctype="multipart/form-data" class="prevent-multiple-submits">

                        @csrf

                        <input type="hidden" name="account" value="{{$account->email}}">

                        <div class="row">

                            <div class="col-lg-12">


                                <div class="form-group">
                                    <label for="name">Account Name</label>
                                    <input required id="name" name="name" type="text" class="form-control"
                                           placeholder="Account Name" value="{{$account->name}}">
                                </div>


                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input required id="email" name="email" type="email" class="form-control"
                                           placeholder="Account Email Address" value="{{$account->email}}">
                                    <span class="text-muted">Account username</span>
                                </div>


                                <div class="form-group">
                                    <label for="phone_number">Phone Number</label>
                                    <input required id="phone_number" name="phone_number" type="text"
                                           class="form-control" value="{{$account->phone_no}}"
                                           placeholder="Phone Number">
                                </div>


                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1">Active</option>
                                        <option @if($account->status==1) selected @endif value="5">Disabled</option>
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary multiple-submits"><i class="fa fa-send"></i> Submit
                                        </button>
                                    </div>
                                </div>


                            </div>

                        </div>


                    </form>

                </div>
            </div>

        </div>
    </div>



@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
        $("#wholesaler_menu_accounts").addClass('active');
    </script>
@endsection
