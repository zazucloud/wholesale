@extends('admin.layout')
@section('title') Create Wholesaler @endsection
@section('custom-head')
    <link href="{{asset('assets/global/plugins/jquery-steps/jquery.steps.css?v=3'.time())}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/css/select2.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('page-content')

    <h1 class="page-title"> Create Wholesaler
        <small></small>


        <a href="{{route('admin.wholesalers.index')}}" class="btn btn-primary btn-circle pull-right"> <i
                    class="fa fa-chevron-left"></i> Back to Wholesalers Listing</a>

    </h1>
    <!-- END PAGE HEADER-->
    <div class="m-heading-1 border-yellow-lemon m-bordered">
        <h3>Adding a new wholesaler</h3>
        <p>Default page description - relevant information relating to the current page or process</p>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="portlet light ">
                <div class="portlet-body">


                    <div class="content-page ml-0">
                        <div class="clearfix"></div>
                        <div class="content registration-form m-t-50">
                            <div class="row">
                                <div class="col-sm-12">

                                    <div class="card-box">
                                        {!! Form::open(['id' => 'wizard-vertical','url' => route('admin.wholesalers.store'), 'files' => true,'class'=>'']) !!}

                                        {!! Form::token() !!}

                                        <h3>Business Details</h3>
                                        <section>
                                            <div class="col-sm-12">
                                                <label class="control-label">Registered Business Name</label>
                                                <input class="form-control input-lg" name="business_name"
                                                       type="text">
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="control-label">Postal Address</label>
                                                <input class="form-control input-lg" name="postal_address"
                                                       type="text">
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="control-label">Physical Address</label>
                                                <input class="form-control input-lg" name="physical_address"
                                                       type="text">
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="control-label">Telephone</label>
                                                <input class="form-control input-lg" name="telephone"
                                                       type="tel">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Email</label>
                                                <input class="form-control input-lg" name="email" type="email">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Tax P.I.N</label>
                                                <input class="form-control input-lg" name="pin" type="text">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Contact Name</label>
                                                <input class="form-control input-lg" name="contact_name"
                                                       type="text">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Title/Position</label>
                                                <input class="form-control input-lg" name="contact_position"
                                                       type="text">
                                            </div>


                                            <div class="col-sm-12">
                                                <label class="control-label">Name of Financial
                                                    Controller</label>
                                                <input class="form-control input-lg" name="financial_controller"
                                                       type="text">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Type Of Ownership</label>
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <select name="ownership_type" id="ownership_type"
                                                                class="form-control input-lg">
                                                            <option value="">--Select--</option>
                                                            <option value="2">Partnership</option>
                                                            <option value="1">Limited Partnership</option>
                                                            <option value="3">Sole Proprietorship</option>
                                                            <option value="0">Other</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-7" id="type_other_container">
                                                        <input placeholder="Other(Specify)"
                                                               class="form-control input-lg"
                                                               name="type_other" id="type_other" type="text">
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Registration Number</label>
                                                <input class="form-control input-lg" name="registration_number"
                                                       type="text">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Date Of Registration</label>
                                                <input class="form-control input-lg" name="registration_date"
                                                       type="date">
                                            </div>

                                            <div class="col-sm-12">
                                                <label class="control-label">Nature Of Business</label>
                                                <input class="form-control input-lg" name="business_nature"
                                                       type="text">
                                            </div>

                                            <br>
                                            <br>

                                        </section>
                                        <h3>Directors/Partners Details</h3>
                                        <section>
                                            <div class="row">
                                                <div class="col-sm-12">

                                                    <h4>List of Directors</h4>

                                                    <table class="table">
                                                        <thead>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            Full Name
                                                        </th>
                                                        <th>
                                                            Mobile/Home Number
                                                        </th>
                                                        </thead>
                                                        <tbody id="directors_container">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-12 text-left">
                                                    <button type="button"
                                                            class="btn btn-default waves-effect waves-light"
                                                            data-toggle="modal"
                                                            data-target="#add-partners-modal">
                                                        <span class="fa fa-plus"></span> Add Director
                                                    </button>
                                                </div>
                                            </div>
                                        </section>
                                        <h3>Financial Details</h3>
                                        <section>

                                            <div class="row">

                                                <div class="col-lg-6">
                                                    {!! Form::label('bank', 'Bank', ['class' => '']) !!}
                                                    <br>
                                                    {!! Form::select('bank',$banks, null,['id'=>'bank','class'=>'form-control select2']) !!}
                                                </div>


                                                <div class="col-lg-6">

                                                    {!! Form::label('branch', 'Bank Branch', ['class' => '']) !!}
                                                    <br>

                                                    <select class="form-control select2" name="branch" id="branch">
                                                        <option value="0">Select Branch</option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <br>
                                                <div class="col-sm-7">
                                                    <label class="control-label">Account No.</label>
                                                    <input class="form-control input-lg" name="bank_account_number"
                                                           type="text">
                                                </div>


                                            </div>

                                            <div class="row">
                                                <br>
                                                <br>

                                                <div class="col-sm-12">
                                                    <label class="control-label">Contact Person</label>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <input placeholder="Name" class="form-control input-lg"
                                                                   name="financial_contact_person"
                                                                   type="text">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input placeholder="Telephone"
                                                                   class="form-control input-lg"
                                                                   name="financial_contact_phone"
                                                                   type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </section>
                                        <h3>Trade References</h3>
                                        <section>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table">
                                                        <thead>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            Name
                                                        </th>
                                                        <th>
                                                            Postal Address
                                                        </th>
                                                        <th>
                                                            Telephone Number
                                                        </th>
                                                        <th>
                                                            Contact Name
                                                        </th>
                                                        </thead>
                                                        <tbody id="trade_references_container">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-12 text-left">
                                                    <button type="button"
                                                            class="btn btn-default waves-effect waves-light"
                                                            data-toggle="modal"
                                                            data-target="#trade-reference-modal">
                                                        <span class="fa fa-plus"></span> Add Trade Reference
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row m-t-30">
                                                <div class="col-sm-3">
                                                    <label class="control-label">Estimated total value Of goods
                                                        required Per
                                                        annum</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input name="annual_item_value" id="annual_item_value"
                                                           class="form-control input-lg" type="text">
                                                </div>
                                            </div>
                                            <div class="row m-t-30">
                                                <div class="col-sm-3">
                                                    <label class="control-label">Amount of credit
                                                        required</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input name="credit_amount" id="credit_amount"
                                                           class="form-control input-lg" type="text">
                                                </div>
                                            </div>
                                        </section>


                                        <h3>Account Details</h3>
                                        <section>

                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label class="control-label">Login Email</label>
                                                    <input class="form-control input-lg" name="username"
                                                           type="text">


                                                    <br>

                                                    <div class="alert alert-info">
                                                        Account credentials will automatically be emailed to the new
                                                        account holder
                                                    </div>

                                                </div>
                                            </div>


                                            <br>

                                            <div class="row">

                                                <div class="col-sm-6">

                                                    {!! Form::label('account_manager', 'Account Manager', ['class' => '']) !!}
                                                    <br>
                                                    {!! Form::select('account_manager',$staff, null,['id'=>'account_manager','class'=>'form-control select2']) !!}

                                                </div>
                                            </div>


                                        </section>


                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="trade-reference-modal" class="modal fade" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel"
                         aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Add Trade Reference</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <input id="trade_reference_name" name="trade_reference_name" type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">P.O. Box</label>
                                                <input id="trade_reference_postal" name="trade_reference_postal" type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Town</label>
                                                <input id="trade_reference_town" name="trade_reference_town"  type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Telephone Number</label>
                                                <input id="trade_reference_phone" name="trade_reference_phone"  type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Contact Name</label>
                                                <input id="trade_reference_contact_name" name="trade_reference_contact_name" type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="close_trade_reference" type="button" class="btn btn-secondary waves-effect"
                                            data-dismiss="modal">Close
                                    </button>
                                    <button id="save_trade_reference" type="button" class="btn btn-info waves-effect waves-light"><i class="fa fa-send"></i> Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="add-partners-modal" class="modal fade" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel"
                         aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Add Director/Partner</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <input name="director_name" id="director_name" spellcheck="false"
                                                       type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label">Telephone Number</label>
                                                <input name="director_phone" id="director_phone" type="text"
                                                       class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="close_director_modal" type="button" class="btn btn-secondary waves-effect"
                                            data-dismiss="modal">Close
                                    </button>
                                    <button id="save_director_contact" type="button"
                                            class="btn btn-info waves-effect waves-light"> <i class="fa fa-send"></i> Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>

        <input value="" type="hidden" name="director_name[]"><input value="" type="hidden" name="director_phone[]">

    </div>
    </div>

@endsection

@section('custom-scripts')
    <script>
        $("#menu-wholesalers").addClass('active');
    </script>

    <script src="{{asset('assets/global/plugins/jquery-steps/jquery.steps.min.js')}}"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>

    <script src="{{asset('assets/global/scripts/jquery.wizard-init.js?v=1')}}"></script>
    <script src="{{asset('assets/global/scripts/select2.min.js')}}"></script>

    <script>
        $(".select2").select2({width: '90%'});



        $("#save_trade_reference").on('click', function () {

            let trade_reference_name_el = $("#trade_reference_name");

            let trade_reference_postal_el = $("#trade_reference_postal");

            let trade_reference_town_el = $("#trade_reference_town");

            let trade_reference_phone_el = $("#trade_reference_phone");

            let trade_reference_contact_name_el = $("#trade_reference_contact_name");

            let trade_reference_name = trade_reference_name_el.val();

            let trade_reference_postal = trade_reference_postal_el.val();

            let trade_reference_town = trade_reference_town_el.val();
            let trade_reference_phone = trade_reference_phone_el.val();
            let trade_reference_contact_name = trade_reference_contact_name_el.val();

            let added_trade_reference = '<tr><td></td><td>'+trade_reference_name+'</td><td>'+trade_reference_postal+'</td><td>'+trade_reference_phone+'</td><td>'+trade_reference_phone+'</td><input value="'+trade_reference_contact_name+'" type="hidden" name="director_name[]"><input value="'+director_phone+'" type="hidden" name="director_phone[]"></tr>';

            $("#trade_references_container").append(added_trade_reference);

            $("#close_trade_reference").trigger('click');

        });



        $("#save_director_contact").on('click', function () {

            let director_name_el = $("#director_name");
            let director_phone_el = $("#director_phone");

            let director_name = director_name_el.val();
            let director_phone = director_phone_el.val();

            let added_director_html = '<tr><td></td><td>'+director_name+'</td><td>'+director_phone+'</td><input value="'+director_name+'" type="hidden" name="director_name[]"><input value="'+director_phone+'" type="hidden" name="director_phone[]"></tr>';

            $("#directors_container").append(added_director_html);

            director_name_el.val('');
            director_phone_el.val('');

            $("#close_director_modal").trigger('click');

        });

        $("#bank").on('change', function () {

            fetch_bank_branches();
        });

        $("#ownership_type").on('change', function () {

            other_ownership_type_control();
        });


        function other_ownership_type_control() {

            var ownership_type = $("#ownership_type").val();
            var other_type_container = $("#type_other_container");
            var other_type = $("#type_other");

            if (ownership_type === 0) {
                other_type_container.show();
            } else {
                other_type_container.hide();

            }

            other_type.val('');
        }

        function fetch_bank_branches() {

            var selected = $("#bank").find(":selected");
            var bank_id = selected.val();

            $.post("{{route('admin.ajax_bank_branches')}}", {
                    bank_id: bank_id
                },
                function (data) {
                    $('#branch').html(data);
                });
        }


        fetch_bank_branches();

        other_ownership_type_control();

    </script>

@endsection
