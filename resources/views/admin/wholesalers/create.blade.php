@extends('layouts.base')
@section('title')
    New Wholesaler
@endsection

@php
    $theme_manager->enqueue_files(['select2', 'selectpicker', 'multiselect'], ['select2', 'selectpicker', 'multiselect']);
@endphp

@section('content')
    @include('partials.page-title')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">

                            </div>
                            <div class="offset-2 col-2">

                                <a class="btn btn-sm btn-info" href="{{ route('admin.wholesalers.index') }}">
                                    <span class="fa fa-arrow-left mr-2"></span> Go Back
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="card-body">
                        {!! Form::open(['route' => null, 'method' => 'POST', 'class' => 'prevent-multiple-submits']) !!}

                        @csrf
                        @include('admin.wholesalers.fields.inputfields')
                        @include('admin.wholesalers.fields.categoriesfields')
                        <div class="alert alert-info mt-3 mx-3">
                            <span class=" ">
                                <strong>Note:</strong> Wholesaler Category Discounts and other details can be configured
                                from the wholesaler's profile
                            </span>
                        </div>

                        <h5 class="vrs-title col-md-12"><strong>Contact Person's</strong></h5>
                        <contacts></contacts>

                        <div class="form-group row mx-0">
                            <div class="col-md-6 center">
                                {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits']) }}
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function numberOnly(id) {
            var element = document.getElementById(id);
            var regex = /[^0-9]/gi;
            element.value = element.value.replace(regex, "");
        }
    </script>

    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
@endsection
