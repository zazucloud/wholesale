@extends('layouts.base')

@section('title') {{ $attribute->label }} - Configuration @endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8 col-12">
                        <h6> Showing Options for:
                            <strong>{{ $attribute->label }} </strong>
                        </h6>
                    </div>
                    <div class="col-md-2 col-12">
                        <button data-target="#addattributesmodal" data-toggle="modal"
                            class="btn btn-success btn-sm text-white"><i class="fa fa-plus mr-2"></i> New
                            Option</button>
                    </div>
                    <div class="col-md-2 col-12">

                        <a href="{{ route('admin.config.inventory_attributes') }}"
                            class="btn btn-info btn-sm text-white float-right"> <i class="fa fa-arrow-left mr-2"></i>Go Back</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if ($options->count() == 0)
                    <div class="alert alert-info">
                        No available options for {{ $attribute->label }} - <a type="button" data-toggle="modal"
                            data-target="#addAttributeOptionAdd" href="#">consider
                            adding</a>
                    </div>
                @else
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                        <thead>
                            <tr>
                                <th width="30%">Option</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($options as $option)
                                <tr>
                                    <td>{{ $option->option }}</td>
                                    <td>
                                        <a title="Disable {{ $option->option }}" href=""> <i class="fa fa-trash"></i>
                                            Disable
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="spacious-container">
                    {{ $options->links() }}
                </div>

                @endif

            </div>
        </div>
    </div>

    <div id="addattributesmodal" class="modal  fade bs-modal-lg" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">
                        Add new attribute option for: {{ $attribute->label }}
                    </h5>
                </div>

                <div class="modal-body">
                    <form action="{{ route('admin.config.inv-att.store') }}" class="prevent-multiple-submits">
                        <div class="modal-content">
                            @csrf
                            @method('POST')
                            <div class="form-group">
                                <input name="attribute_id" required type="hidden" value="{{ $attribute->id }}">
                            </div>

                            <div class="form-group">
                                <label for="option" class="control-label">Option Value</label>
                                <input required placeholder="Option Value" id="option" class="form-control" name="option"
                                    type="text">

                            </div>

                        </div><!-- /.modal-content -->
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success btn-sm multiple-submits" value="Submit">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
@endsection
