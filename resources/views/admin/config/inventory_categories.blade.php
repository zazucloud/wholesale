@extends('layouts.base')

@section('title')
    Inventory Category
@endsection

@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection


@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                @include('admin.config.search.inventory_category')
            </div>
            <div class="card-body">
                <div class="spacious-container">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Brand</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($categories as $index => $category)
                            <tr>
                                <td> {{ $index + 1 }}</td>
                                <td class="align-middle">
                                    @if (is_null($category->getFirstMedia('categorylogos')))
                                        N/A
                                    @else
                                        <img src="{{ $category->getFirstMedia('categorylogos')->getFullUrl() }}"
                                            alt="{{ $category->name }}" class="tw-rounded-full tw-h-10 tw-w-10">
                                    @endif

                                </td>
                                <td>
                                    <label class="badge badge-info">
                                        {{ $category->brand->name }}
                                    </label>
                                </td>
                                <td>{{ $category->name }}</td>
                                <td class="tw-flex">
                                    @can('categories-edit')
                                        <a @click="openInventoryConfigModal({{ $category }},2)"
                                            class="btn btn-sm btn-warning text-white mr-2" data-toggle="tooltip"
                                            data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    @endcan

                                    @can('categories-delete')
                                        <a @click="openInventoryConfigModal({{ $category }},3)"
                                            class="btn btn-sm btn-danger text-white" data-toggle="tooltip" data-placement="top"
                                            title="Delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            </div>
            <div style="padding-left:20px;">
                {!! $categories->appends(Request::except('page'))->render() !!}
            </div>
        </div>

        <inventorycategorymodal :parent="{{ json_encode($parent) }}"></inventorycategorymodal>
    </div>
@endsection

@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="select2"]').select2();
        });
    </script>
@endsection
