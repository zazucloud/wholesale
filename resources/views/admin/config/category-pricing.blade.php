@extends('layouts.base')

@section('title') {{ $category->name }} - Pricing @endsection

@section('content')
    @include('partials.page-title')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">

                    <div class="card-body override_cbody">

                        {!! Form::open(array('route' => ['admin.config.category_pricing_process',$category->id],'method'=>'POST','files' => true,'class'=>'prevent-multiple-submits')) !!}

                        @csrf
                        <div class="form-group">


                            <div class="alert alert-info">
                                <p>For bulk price change;</p>
                                <strong>Step 1.</strong> <a href="{{route('admin.config.category-pricing-download',$category->id)}}"><b>Click here to download </b> an Excel sheet of current price list</a>
                                <br>
                                <strong>Step 2.</strong> Attach the updated file below and submit

                                <br>
                                <br>
                                <small>
                                    Only contain wholesalers with visibility to the respective category will be listed in the excel sheet
                                </small>
                            </div>


                            <div>

                                <a title="Download Excel file" href="{{route('admin.config.category-pricing-download',$category->id)}}" type="button"
                                    class="d-block btn btn-primary btn-sm pull-left"><i class="fa fa-download"></i> Download
                                    Current State
                                </a>
                                <br>
                                <br>

                                <div class="d-block">
                                    <label for="upload"> <i class="fa fa-file-excel-o"></i> Excel Upload</label>
                                    <input id="upload" name="upload" required type="file" class="form-control"
                                        placeholder="Excel Upload..."></div>
                            </div>

                        </div>

                        <div class="form-group row mx-0">
                            <div>
                                {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits'] )  }}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
