@extends('layouts.base')

@section('title') Inventory Bulk Import @endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">

                    <div class="col-8">


                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-info">
                    <a href="{{ route('admin.config.inventory_template') }}">Click here to
                        download Inventory Import template</a> <br>
                    To bulk import Inventory, upload a compatible Excel file
                </div>
                @if (auth()->user()->hasPermissionTo('inventory_config-uploading'))
                <form method="post" action="{{ route('admin.config.custom_import_Inventory') }}"
                    enctype="multipart/form-data" class="prevent-multiple-submits">
                    @csrf
                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <label for="upload">Upload Excel File</label>
                            <input id="upload" name="csv_file" required type="file" class="form-control"
                                placeholder="Excel Upload...">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">

                           <div > <input type="checkbox" name="header"
                               ><span class="ml-2">File contains headers</span></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <input type="submit" class="btn btn-success btn-sm multiple-submits" value="Parse Excel File" style="font-size: 15px;">
                        </div>
                    </div>

                </form>
                @endif
            </div>
        </div>
    </div>
@endsection
