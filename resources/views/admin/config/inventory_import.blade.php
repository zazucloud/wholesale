@extends('layouts.base')

@section('title') Inventory Bulk Import @endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-8"></div>
                    <div class="col-4">
                        @can('inventory-export')
                            <a href="{{ route('admin.config.inventory_template') }}"
                                class="btn btn-success float-right btn-sm">Download Template</a>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-info">
                    <a href="{{ route('admin.config.inventory_template') }}">Click here to
                        download Inventory Import template</a> <br>
                    To bulk import inventory, upload a compatible Excel file
                </div>
                <form method="POST" action="{{ route('admin.config.inventory_import_process') }}"
                    enctype="multipart/form-data" class="prevent-multiple-submits">
                    @csrf

                    <div class="row">
                        <div class="form-group col-6">
                            <label for="upload"> <i class="fa fa-file-excel-o"></i> Excel Upload</label>
                            <input id="upload" name="uploadInventory" required type="file" class="form-control"
                                placeholder="Excel Upload...">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <input type="submit" class="btn btn-success btn-sm multiple-submits" value="Upload">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
