@extends('layouts.base')

@section('title')
    Warehouse
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">

        <div class="card">
            <div class="card-header">
                @include('admin.config.search.warehouses')
            </div>
            <div class="card-body">
                <div class="spacious-container">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Warehouse</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($warehouses as $index => $warehouse)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $warehouse->name }}</td>
                                <td>
                                    @can('warehouse-edit')
                                        <a @click="openWarehouseConfigModal({{ $warehouse }},2)" data-toggle="tooltip"
                                            data-placement="top" title="Edit">
                                            <i class="list-icon linearicons-pencil4 text-info"></i>
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                <div style="padding-left:20px;">
                    {!! $warehouses->appends(Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
    <warehouseconfigmodal></warehouseconfigmodal>
@endsection
