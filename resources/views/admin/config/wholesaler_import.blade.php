@extends('layouts.base')

@section('title') Wholesaler Bulk Import @endsection

@section('content')
    @include('partials.page-title')

    {{-- <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">

                    <div class="col-8">
                        <a href="{{ route('wholesaler_import_template') }}" class="btn btn-outline-success">
                            Download Excel Template
                        </a>


                    </div>
                    <div class="col-4">
                        @can('wholesaler-export')
                            <a href="{{ route('admin.config.wholesaler_template') }}"
                                class="btn btn-success float-right btn-sm">Download Template</a>
                        @endcan

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-info">
                    <a href="{{ route('admin.config.wholesaler_template') }}">Click here to
                        download wholesalers Import template</a> <br>
                    To bulk import wholesalers, upload a compatible Excel file
                </div>
                <form method="post" action="{{ route('admin.config.wholesaler_import_process') }}"
                    enctype="multipart/form-data" class="prevent-multiple-submits">
                    @csrf
                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <label for="upload">Upload Excel File</label>
                            <input id="upload" name="uploadWholesaler" required type="file" class="form-control"
                                placeholder="Excel Upload...">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <input type="submit" class="btn btn-success btn-sm multiple-submits" value="Upload">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div> --}}



{{-- Custom import --}}

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">

                    <div class="col-8">
                        {{-- <a href="{{ route('wholesaler_import_template') }}" class="btn btn-outline-success">
                            Download Excel Template
                        </a> --}}


                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-info">
                    <a href="{{ route('admin.config.wholesaler_template') }}">Click here to
                        download wholesalers Import template</a> <br>
                    To bulk import wholesalers, upload a compatible Excel file
                </div>
                @if (auth()->user()->hasPermissionTo('wholesaler_config-uploading'))
                <form method="post" action="{{ route('admin.config.wholesaler_custom_import_parse') }}"
                    enctype="multipart/form-data" class="prevent-multiple-submits">
                    @csrf
                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <label for="upload">Upload Excel File</label>
                            <input id="upload" name="csv_file" required type="file" class="form-control"
                                placeholder="Excel Upload...">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">

                           <div > <input type="checkbox" name="header"
                               ><span class="ml-2">File contains headers</span></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <input type="submit" class="btn btn-success btn-sm multiple-submits" value="Parse Excel File" style="font-size: 15px;">
                        </div>
                    </div>

                </form>
                @endif
            </div>
        </div>
    </div>
@endsection
