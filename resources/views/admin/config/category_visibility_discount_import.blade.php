@extends('layouts.base')

@section('title') Category Visibility
    and Discounting Import
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <a href="{{ url()->previous() }}" class="btn btn-info btn-sm float-right">Back</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.config.category_visibility_discount_process') }}"
                    enctype="multipart/form-data" class="prevent-multiple-submits">
                    @csrf
                    <div class="form-group ">
                        <div class="alert alert-info">
                            <p>
                                <strong>Step 1.</strong> Begin by downloading the current state - <a
                                    href="{{ route('admin.config.wholesaler_category_discount_download') }}">click
                                    here to download</a>
                            </p>
                            <p>
                                <strong>Step 2.</strong> Make adjustments on the Excel file then re-upload
                            </p>
                            <p>
                                <small>
                                    Only Column C (visibility) and D (Percentage Discount) are editable
                                </small>
                                <br>
                                <small>
                                    For Column C have either Visible or Hidden
                                </small>
                                <br>
                                <small>
                                    For Column D input the percentage discount
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <a title="Download Excel file"
                            href="{{ route('admin.config.wholesaler_category_discount_download') }}"
                            class="d-block btn btn-primary btn-sm tw-w-min">
                            Download Current State
                        </a>
                    </div>
                    <div class=" form-group">
                        <label for="upload"> <i class="fa fa-file-excel-o"></i> Excel Upload</label>
                        <input id="upload" name="upload" required type="file" class="form-control"
                            placeholder="Excel Upload...">
                    </div>
                    <div class="form-group">
                        <br>
                        <button class="btn btn-primary multiple-submits">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
