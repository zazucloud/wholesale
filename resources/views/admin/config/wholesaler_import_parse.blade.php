@extends('layouts.base')

@section('title')
    Wholesaler Bulk Import
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">

        <div class="card">
            <div class="card-body override_cbody">
                <div class="spacious-container">
                    <form class="form-horizontal" method="POST"
                    action="{{ route('admin.config.wholesaler_custom_import_process') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file->id }}" />
                    <input type="hidden" name="filename" value="{{ $filename }}" />

                    <table class="table">
                        @if ($csv_data_file->csv_header == 1)
                            <tr>
                                @foreach ($csv_header_fields as $csv_header_field)
                                    <th>{{ $csv_header_field }}</th>
                                @endforeach
                            </tr>
                        @endif
                        @foreach ($csv_data as $row)
                            <tr>
                                @foreach ($row as $key => $value)
                                    <td>{{ $value }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        <tr>
                            @foreach (array_values($csv_data[0]) as $key => $value)
                                <td>
                                    <select name="fields[{{ $key }}]">
                                        @foreach (config('app.wholesaler_db_fields') as $db_field)
                                            
                                            <option value="{{ $db_field }}" >{{ $db_field }}
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                            @endforeach
                        </tr>
                    </table>

                    <button type="submit" class="btn btn-primary">
                        Import Data
                    </button>
                </form>
                </div>
            </div>
        </div>
    </div>
    <warehouseconfigmodal></warehouseconfigmodal>
@endsection
