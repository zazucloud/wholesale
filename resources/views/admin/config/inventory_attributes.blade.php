@extends('layouts.base')

@section('title')
    Inventory Attributes
@endsection

@section('content')
    @include('partials.page-title')


    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        @can('attributes-create')
                            <button data-target="#attributesmodal" data-toggle="modal"
                                class="btn btn-success btn-sm float-right"><i class="fa fa-plus mr-2"></i> New
                                Attribute</button>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover" width="100%">
                    <thead>
                        <tr>
                            <th>Attribute</th>
                            <th>Configuration</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($inventory_attributes as $inventory_attribute)
                            <tr>
                                <td>{{ $inventory_attribute->label }}</td>
                                <td>
                                    @if ($inventory_attribute->type == 'fixed')
                                        {{ \App\Models\InventoryAttribute::narrate_options($inventory_attribute->id) }}
                                    @elseif($inventory_attribute->type == 'flexible')
                                        <span>*Flexible Attribute</span>
                                    @endif
                                </td>
                                <td>
                                    @can('attributes-edit')
                                        <a href="{{ route('admin.config.inv-att.manage', $inventory_attribute->slug) }}"
                                            data-toggle="tooltip" data-placement="top" title="Manage"
                                            class="btn btn-info btn-sm">
                                            <i class="list-icon linearicons-wrench"></i>
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="attributesmodal" class="modal  fade bs-modal-lg" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Add New Attribute</h5>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.config.inventory_attributes.store') }}" method="POST"
                        class="prevent-multiple-submits">
                        @csrf
                        <div class="form-group">
                            <label for="attribute_name" class="control-label">Attribute Name</label>
                            <input required placeholder="Attribute Name" id="attribute_name" class="form-control"
                                name="attribute_name" type="text">
                        </div>

                        {{-- <div class="form-group">
                            <label>Attribute Type</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="attribute_type" id="attribute_type_flexible" value="flexible"
                                        checked> Flexible
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="attribute_type" id="attribute_type_fixed" value="fixed">
                                    Fixed Options
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <span class="alert alert-info "> With flexible type a custom value is added while
                                adding an item while for fixed options, a preset option is selected.
                            </span>
                        </div> --}}
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success btn-sm multiple-submits" value="Submit">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>

                        </div>
                    </form>
                </div>



            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
