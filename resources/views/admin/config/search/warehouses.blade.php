 <div class="row">
     <div class="col-md-10 col-12">
         {!! Form::open(['route' => 'warehouses.search', 'method' => 'get', 'class' => 'row']) !!}
         <div class="col-sm-10">
             <div class="">
                <label>Search</label>
                 {!! Form::text('search', null, [
                     'class' => 'form-control form-control-inputs',
                     'required' => true,
                     'placeholder' => 'Search By Warehouse Name',
                 ]) !!}
             </div>
         </div>
         <div class="col-sm-2 mt-4 pt-2">
             <div>
                 {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn  btn-info btn-sm']) }}

             </div>
         </div>
         {!! Form::close() !!}
     </div>
     <div class="col-md-2 col-12 mt-md-4 pt-md-2">
         @can('warehouse-create')
             <button @click="openWarehouseConfigModal(null,1)" class="btn btn-success float-right btn-sm">
                 <i class="fa fa-plus mr-2"></i> New Warehouse
             </button>
         @endcan

     </div>
 </div>
