<div class="row">
    <div class="col-md-10 col-12">
        {!! Form::open(['route' => 'brands.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-sm-10">
            <label>Search</label>
            <div class="">
                {!! Form::text('search', null, [
                    'class' => 'form-control form-control-inputs',
                    'required' => true,
                    'placeholder' => 'Search By Brand Name',
                ]) !!}
            </div>
        </div>
        <div class="col-sm-2 mt-4 pt-2">
            <div>
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn  btn-info btn-sm']) }}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2 col-12 mt-md-4 pt-md-2">
        @can('brands-create')
            <button @click="openBrandConfigModal(null,1)" class="btn btn-success btn-sm float-right">
                <i class="fa fa-plus mr-2"></i> New Brand
            </button>
        @endcan

    </div>
</div>
