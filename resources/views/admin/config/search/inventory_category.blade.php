<div class="row">
    <div class="col-md-10 col-12 ">
        {!! Form::open(['route' => 'categories.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-sm-6">
            <div class="">
                <label for="">Search</label>
                {!! Form::text('search', null, [
                    'class' => 'form-control form-control-inputs',
                    'placeholder' => 'Search By Name',
                ]) !!}
            </div>
        </div>
        <div class="col-sm-4">
            <div class="">
                <label for="">Selet Brand</label>
                {!! Form::select(
                    'brand_id',
                    $brands,
                    [],
                    ['placeholder' => 'Select Brand', 'class' => 'form-control', 'id' => 'select_client', 'data-toggle' => 'select2'],
                ) !!}
            </div>
        </div>
        <div class="col-sm-2 mt-4 pt-2">
            <div >
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn  btn-info btn-sm']) }}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2 col-12 mt-md-4 pt-md-2">
        @can('categories-create')
            <button class="btn btn-success float-right btn-sm" @click="openInventoryConfigModal(null,1)">
                <i class="fa fa-plus mr-2"></i> Add Category
            </button>
        @endcan
    </div>
</div>
