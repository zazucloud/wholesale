@extends('layouts.base')

@section('title')
    Brand Configurations
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                @include('admin.config.search.brands')
            </div>

            <div class="card-body">
                <div class="spacious-container">

                    <table class="table table-hover center-text">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th scope="col">Image</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Narration</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($brands as $index => $brand)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>
                                        @if (!is_null($brand->getFirstMedia('brandlogos')))
                                            <img class="img-responsive" height="50" width="75"
                                                src="{{ $brand->getFirstMedia('brandlogos')->getUrl() }}"
                                                alt="{{ $brand->name }}">
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                    <td>{{ $brand->name }}</td>
                                    <td>{{ ellipsis($brand->narration, 170) }}
                                    </td>
                                    <td class="tw-flex">
                                        @can('brands-edit')
                                            <a @click="openBrandConfigModal({{ $brand }},2)" data-toggle="tooltip"
                                                data-placement="top" title="Edit"
                                                class="btn btn-warning btn-sm text-white mr-1">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('brands-delete')
                                            <form method="post" action="{{ route('admin.config.brand.delete', $brand->id) }}">
                                                @csrf
                                                @method('DELETE')

                                                <button class="btn btn-sm btn-block btn-danger" type="submit"
                                                    onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                                    data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="list-icon linearicons-trash2"></i>
                                                </button>
                                            </form>
                                        @endcan


                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                        <div style="padding-left:20px;">
                            {!! $brands->appends(Request::except('page'))->render() !!}

                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <brandconfigmodal></brandconfigmodal>
@endsection
