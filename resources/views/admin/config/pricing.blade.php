@extends('layouts.base')

@section('title')
    Pricing
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">

                    <div class="card-body override_cbody">
                        <div class="form-group">
                            <div class="alert alert-info">
                                <strong>Step 1.</strong> Begin by downloading the current state - <a href="#"
                                    @click="busBatchProcess('{{route('admin.config.inventory_download_pricing')}}')">click here to
                                    download</a>
                                <br>
                                <strong>Step 2.</strong> Make adjustments on the Excel file then re-upload
                                <br>
                                <br>
                            </div>


                            <div>
                                @can('pricing-downloading')
                                    <a title="Download Excel file" href="#" @click="busBatchProcess('{{route('admin.config.inventory_download_pricing')}}')" type="button"
                                        class="d-block btn btn-primary btn-sm pull-left"><i class="fa fa-download"></i> Download
                                        Inventory Pricing
                                    </a>
                                @endcan

                                <br>
                                <br>

                                <busjobimports url="{{route('admin.config.inventroy_pricing_update')}}"></busjobimports>

                                <busjob_exports></busjob_exports>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
@endsection
