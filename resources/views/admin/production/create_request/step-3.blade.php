@extends('admin.production.wizard')
@section('wizardtitle')
New Production Request
@endsection
@section('wizardcontent')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <div class="col-4 tw-flex tw-justify-end">
                            <a class="btn btn-sm btn-info" href="{{route('admin.production.creation_step_2',['production_request'=>$request_tag])}}">
                                <span class="fa fa-arrow-left mr-2"></span>  Back to Step 2 - Add more items
                            </a>
                        </div>

                    </div>
                </div>

                <div class="card-body">
                    <h5 class="box-title"><b class="mx-3 my-2"><i class="fa fa-database"></i> Production Request Confirmation -
                            {{$request_tag}}</b></h5>
                            <div class="spacious-container">
                                <table class="table table-hover center-text">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">SKU</th>
                                            <th scope="col">Item</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Details</th>
                                            <th scope="col">Delivery Date</th>
                                            <th scope="col">Priority</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
        
                                        @foreach($production_items as $index => $items)
                                        <tr>
                                            <th scope="row">{{ $index+1 }}</th>
        
                                            <td>{{$items->sku}}</td>
                                            <td>{{$items->name}}</td>
                                            <td>{{$items->requested_quantity}}</td>
                                            <td>{{$items->requested_quantity}}</td>
                                            <td>{{stamp_date_clean($items->item_delivery_date)}}
                                                <br>
                                            </td>
                                            <td>{{$items->item_priority}}</td>
                                           
                                        </tr>
        
        
                                        @endforeach
        
        
                                    </tbody>
                                </table>
                            </div>

                    {!! Form::open(array('route' => ['admin.production.push_request'],'method'=>'POST')) !!}

                    @csrf
                    <input type="hidden" name="request_tag" value="{{$request_tag}}">

                      <div class="form-group row mx-0">
                        <div class="col-md-6 center">
                            {{ Form::button('Submit Production Request', ['type' => 'submit', 'class' => 'btn btn-primary'] )  }}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection