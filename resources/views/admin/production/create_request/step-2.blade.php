@extends('admin.production.wizard')
@section('wizardtitle')
New Production Request
@endsection
@section('wizardcss')
<link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('wizardcontent')
<h5 class="box-title"><b class="mx-3 my-2"><i class="fa fa-database"></i> Production Request -
        {{$request_tag}}</b></h5>

{!! Form::open(array('route' => ['admin.production.add_item'],'method'=>'POST')) !!}

@csrf
<input type="hidden" name="request_tag" value="{{$request_tag}}">

<inventoryproductiondropdown :priorityoptions="{{json_encode($priority_select_options)}}"
    :items="{{json_encode($production_items)}}"></inventoryproductiondropdown>

<div class="form-group row mx-0">
    <div class="col-md-6 center">
        {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits'] )  }}
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('wizardscripts')
<script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script>
    $(document).ready(function(){
    $('[data-toggle="select2"]').select2();

    $('.test').select2({
        placeholder: 'inventory...',
        allowClear: true,
        ajax: {
            url: '/admin/inventory/ajax/getinventory/dropdown',
            dataType: 'json',
            delay: 300,
            processResults: function (data) {
                return {
                    results:  $.map(data.results, function (item) {
                        return {
                            text: item.name+' ('+item.sku+')',
                            id: item.id
                        }
                    }),
                    pagination: {more: data.pagination.more}
                };
            },
            data: function(params) {
                return {
                    term: params.term || '',
                    page: params.page || 1
                }
            },

            cache: true
        }
  });
});
</script>
@endsection
