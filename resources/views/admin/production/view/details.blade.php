<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Request
                    Details</span>

                <div class="row mb-2">
                    <div class="col-4 pl-4">Request Tag:</div>
                    <div class="col-8"><b># {{ $production_request->request_tag }}</b></div>
                </div>
                @if ($production_request->if_stock_update==1)
                <div class="row mb-2">
                    <div class="col-4 pl-4">Request Type:</div>
                    <div class="col-8"><b>{{(($production_request->if_stock_update==1)?'Stock Update':'Normal')}}</b></div>
                </div>
                @elseif ($production_request->clientable_type == 'App\Models\Wholesale\Wholesaler')
                <div class="row mb-2">
                    <div class="col-4 pl-4">Wholesaler:</div>
                    <div class="col-8"><b>{{$production_request->clientable->business_name}}</b></div>
                </div>
                @else
                <div class="row mb-2">
                    <div class="col-4 pl-4">Customer:</div>
                    <div class="col-8"><b>{{$production_request->clientable->business_name}}</b></div>
                </div>
                @endif

                <div class="row mb-2">
                    <div class="col-4 pl-4">Created:</div>
                    <div class="col-8"><b>{{ date('d-M-Y', strtotime($production_request->created_at)) }}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Delivery Date:</div>
                    <div class="col-8"><b>{{ date('d-M-Y', strtotime($production_request->delivery_date)) }}</b></div>
                </div>

                @if ($production_request->notes != null)
                    <div class="row mb-2">
                        <div class="col-4 pl-4">Request Notes:</div>
                        <div class="col-8">{{ $production_request->notes }}</div>
                    </div>
                @endif
                <div class="row mb-2">
                    <div class="col-4 pl-4">Current Status:</div>
                    <div class="col-8">
                        @if ($production_request->status == 1)
                            <div class="badge bg-soft-danger text-danger">Pending</div>
                        @endif

                        @if ($production_request->status == 2)
                            <div class="badge bg-soft-info text-info">In Production</div>
                        @endif

                        @if ($production_request->status == 3)
                            <div class="badge bg-soft-success text-success">Complete</div>
                        @endif

                        @if ($production_request->status == 4)
                            <div class="badge bg-soft-success text-success">Delivered</div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Manage Request Status</span>
                @if ($production_request->status != 4)

                    <div class="row mb-2">
                        <div class="col-5">{{ str_plural('Day', $date_difference) }} to Delivery:</div>
                        <div class="col-7"><b><span class="badge bg-soft-info text-info">{{ $date_difference }}</span>
                                days</b></div>
                    </div>
                @endif
                <div class="row mb-2">
                    <div class="col-5">Deliverable Count:</div>
                    <div class="col-7"><b>{{ number_format(count($items)) }} item(s)</b></div>
                </div>

                @if ($pending_items_count == 0)

                    <i class="fa fa-check"></i> All items have been marked as done

                @else
                    <div class="row mb-2">
                        <div class="col-5">Pending:</div>
                        <div class="col-7"><b>{{ $pending_items_count }} item(s)</b></div>
                    </div>
                @endif

                <div class="row mb-2">
                    <div class="col-5">Update Status to:</div>
                    <div class="col-7">
                        <form action="{{ route('production_request.status_update') }}" method="post">
                            {{ csrf_field() }}

                            <input type="hidden" name="request_id" value="{{ $production_request->id }}">
                            <input type="hidden" name="request_tag" value="{{ $production_request->request_tag }}">

                            <div class="row ">
                                <div class="col-md-9">
                                    <select class="form-control" name="status_update" title="Update request status">
                                        <option value="">Select Status
                                        </option>
                                        <option value="1">Pending
                                        </option>
                                        <option value="2">In Production
                                        </option>
                                        <option value="3">Complete
                                        </option>
                                        <option value="4">Delivered
                                        </option>
                                    </select>
                                </div>




                                <div class="col-md-3 pl-0">
                                    @if (auth()->user()->hasPermissionTo('production-edit'))

                                        <button class="btn btn-primary mt-1"><i class="fa fa-send"></i>
                                        </button>
                                    @else
                                        <button class="btntable disonabled btn btn-dark mt-1">
                                            <i class="text-white fa fa-send"></i>
                                        </button>
                                    @endif
                                </div>

                            </div>


                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Request
                    Items</span>

                <table class="table table-hover center-text">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th> SKU</th>
                            <th class="tw-w-36"> Item</th>
                            <th> Quantity</th>
                            <th class="tw-w-36"> Details</th>
                            <th> DeliveryDate</th>
                            <th> Status</th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $index => $item)

                            <tr>
                                <th scope="row">{{ $index + 1 }}</th>
                                <td>{{ $item->sku }}</td>
                                <td>{{ wordwrap($item->name, 25) }}</td>
                                <td>{{ number_format($item->requested_quantity) }}</td>

                                <td>
                                    <div class="row">

                                        @if ($item->item_notes != null)

                                            <div class="col-lg-12">

                                                <span class="bold">Notes:</span> {{ $item->item_notes }}

                                            </div>
                                        @endif

                                        @if ($item->item_delivery_date != $production_request->delivery_date)

                                            <div class="col-lg-12">
                                                <p>
                                                    <span class="bold">Delivery Date:</span>
                                                    ({{ \App\Models\ProductionRequest::request_priority_label($production_request->priority) }}
                                                    Priority)
                                                </p>

                                            </div>
                                        @endif


                                    </div>

            </div>


            <td> {{ stamp_date_clean($item->item_delivery_date) }}

                <br>
                ({{ \App\Models\ProductionRequest::request_priority_label($production_request->priority) }}
                Priority)


            </td>


            <td>

                <select data-item_name="{{ $item->name }}" data-item_id="{{ $item->item_id }}"
                    class="form-control status_control" name="status[{{ $item->item_id }}]"
                    title="Manage status for {{ $item->name }}"
                    @change="updateStatusOfProductionItem($event,{{ json_encode($item) }})">
                    <option @if ($item->production_status == 1) selected @endif value="1">Pending
                    </option>
                    <option @if ($item->production_status == 2) selected @endif value="2">Done
                    </option>
                    <option @if ($item->production_status == 3) selected @endif value="3">Delivered
                    </option>
                </select>

            </td>


            </tr>
            @endforeach
            </tbody>
            </table>


        </div>

    </div>
</div>


</div>
