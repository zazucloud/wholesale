<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Order
                    Details</span>

                <div class="row mb-2">
                    <div class="col-4 pl-4">Contact Person:</div>
                    <div class="col-8"><b>{{$sales_person->name ?? 'N/A'}}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Phone Number:</div>
                    <div class="col-8"><b>{{$sales_person->phone_number ?? 'N/A'}}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Email:</div>
                    <div class="col-8"><b>{{$sales_person->email ?? 'N/A'}}</b></div>
                </div>
            </div>

        </div>
    </div>
</div>