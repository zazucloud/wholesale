@extends('layouts.base')
@section('title')
@yield('wizardtitle')
@endsection
@section('css')
@yield('wizardcss')

@endsection
@section('content')
@include('partials.page-title')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <div class="col-4 tw-flex tw-justify-end">
                            <a class="btn btn-sm btn-info" href="{{route('admin.production.index')}}">
                                <span class="fa fa-arrow-left mr-2"></span> Go Back
                            </a>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    @yield('wizardcontent')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('extrascripts')
@yield('wizardscripts')
@endsection
