@extends('layouts.base')
@section('title')
View Production Request
@endsection
@section('css')
<link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
@include('partials.page-title')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <div class="col-4 tw-flex tw-justify-end">
                            <a class="btn btn-sm btn-info" href="{{route('admin.production.index')}}">
                                <span class="fa fa-arrow-left mr-2"></span> Go Back
                            </a>
                        </div>

                    </div>
                </div>

                <div class="card-body">
                    <div class="row mx-0 mb-3">
                        <div class="col-md-6">
                            {{-- <b>Production Request View # {{$order->face_id}}</b> --}}
                        </div>
                        <div class="col-md-6 tw-flex text-right tw-justify-end">
                            {{-- <a href="{{route('orders.invoice_download',['order'=>$order->id])}}"
                                class="btn btn-info btn-sm mr-2"><i class="fa fa-download" aria-hidden="true"></i></a>

                            <form action="{{ route('orders.cancel',['order'=>$order->id]) }}" method="POST">
                                @csrf
                                <input type="hidden" name="_METHOD" value="DELETE">
                                <button class="btn btn-danger btn-sm"
                                    onclick="return confirm('Are you sure you want to delete order from your system ?')">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form> --}}
                        </div>
                    </div>
                    <div class="tabs tabs-bordered mx-3 my-2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#details" data-toggle="tab" aria-expanded="true"
                                    class="nav-link px-3 py-2 active">
                                    <i class="mdi mdi-pencil-box-multiple font-18 d-md-none d-block"></i>
                                    <span class="d-none d-md-block">Production Request Details</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#information" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2 ">
                                    <i class="mdi mdi-book-open-variant font-18 d-md-none d-block"></i>
                                    <span class="d-none d-md-block"> Contact Information </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#comments" data-toggle="tab" aria-expanded="true" class="nav-link px-3 py-2">
                                    <i class="mdi mdi-image font-18 d-md-none d-block"></i>
                                    <span class="d-none d-md-block"> Comments </span>
                                </a>
                            </li>


                        </ul> <!-- end nav-->
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            @include('admin.production.view.details')
                        </div>
                        <div class="tab-pane" id="information">
                            @include('admin.production.view.contactinfo')
                        </div>
                        <div class="tab-pane" id="comments">
                            @include('admin.production.view.comments')
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<comment finalroute="{{route('production_request.update.comment')}}"></comment>

@endsection
@section('extrascripts')
<script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>

@endsection
