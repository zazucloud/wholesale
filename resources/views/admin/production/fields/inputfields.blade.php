<div class="form-group row mx-0 mb-1" id="customerinfotype">
    <label class="col-3"><input name="type" type="radio" value="wholesaler" class="name" checked>

        wholesaler</label>
    <label class="col-3"><input name="type" type="radio" value="retailer" class="name">

        Customer</label>
</div>
<div class="form-group row" id="customerinforow">

    <div class="form-group col-12 col-md-6">
        <div class="col-sm-12">
            <label>Customer</label>
            {{ Form::select('customer', $wholesalers, null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'wholesalers', 'data-toggle' => 'select2', 'placeholder' => 'Select Customer']) }}

        </div>
    </div>
    <div class="form-group col-12 col-md-6">
        <div class="col-sm-12">
            <label>Sales Person</label>
            {{ Form::select('sales_person', [], null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'staff', 'data-toggle' => 'select2', 'placeholder' => 'Select Staff']) }}

        </div>
    </div>
</div>

<div class="form-group row" id="retailerinforow">

    <div class="form-group col-12">
        <div class="col-sm-12">
            <label>Customer</label>
            {{ Form::select('retailer', $retailers, null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'wholesalers', 'data-toggle' => 'select2', 'placeholder' => 'Select Customer']) }}

        </div>
    </div>
</div>

<inventoryproductiondropdown :priorityoptions="{{ json_encode($priority_select_options) }}"
    :items="{{ json_encode($production_items) }}"></inventoryproductiondropdown>


<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Delivery Date</label>
        {!! Form::date('delivery_date', null, [
            'class' => 'form-control',
            'required' => 'required',
            'placeholder' => 'Delivery Date',
        ]) !!}


    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Priority Level</label>

        {!! Form::select(
            'priority_level',
            [
                '' => 'Choose Priority Level',
                '1' => 'Lowest',
                '2' => 'Normal',
                '3' => 'Highest',
            ],
            null,
            ['class' => 'custom-select', 'id' => 'priority_level', 'required' => true],
        ) !!}
    </div>
</div>
