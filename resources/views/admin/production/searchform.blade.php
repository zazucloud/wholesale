<div>
    {!! Form::open(['route' => null, 'method' => 'get', 'class' => 'row']) !!}
    <div class="col-sm-11 row">
    <div class="col-sm-4 ">
        <div class="form-group">
            <label>Search</label>

            {!! Form::text('search', null, [
                'class' => 'form-control form-control-inputs',
                'name'=>'request_tag',
                'placeholder' => 'Search By Request Tag Or Order No',
            ]) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Select User</label>
            <select class=" form-control test" name="createdby"></select>
        </div>
    </div>
    <div class="col-sm-4">

        <div class="form-group">
        <label>Created</label>

            {!! Form::date('date', null, [
                'class' => 'form-control form-control-inputs',
                'name'=>'date',
                'placeholder' => 'Search By Order Tag',
            ]) !!}
        </div>
    </div>
    </div>
    <div class="col-sm-1">
        <div class="float-right mt-4 pt-2">

            {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

        </div>
    </div>
    {!! Form::close() !!}
</div>
<hr/>
