@extends('admin.production.tabslayout')

@section('production_page_title') Draft Production Requests @endsection

@section('requests_page_content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">
                <div class="card-body override_cbody">
                    @include('admin.production.searchform')

                    <div class="spacious-container">
                        <table class="table table-hover center-text">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Request Tag</th>
                                    <th scope="col">Created</th>
                                    <th scope="col">Created By</th>
                                    <th scope="col">Delivery Date</th>
                                    <th scope="col">Contact Person</th>
                                    <th scope="col">Priority</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($draft_production_requests as $index => $production_request)
                                <tr>
                                    <th scope="row">{{ $index+1 }}</th>

                                    <td>{{$production_request->request_tag}}</td>
                                    <td>{{stamp_date_time_simple($production_request->updated_at)}}</td>
                                    <td>{{$production_request->createdBy->name?? 'N/A'}}</td>
                                    <td>{{stamp_date_clean($production_request->delivery_date)}}
                                        <br>
                                    </td>
                                    <td>{{$production_request->staff->name ?? 'N/A'}}</td>
                                    <td>{{ production_priority_level($production_request->priority )}}</td>

                                    <td><a href="{{route('admin.production.creation_step_2', ['production_request'=> $production_request->request_tag])}}"
                                            class="btn btn-info btn-sm"><i class="fa fa-arrow-right"></i></a>
                                    </td>
                                </tr>


                                @endforeach


                            </tbody>
                        </table>
                    </div>

                </div>
                <div style="padding-left:20px;">
                    {!! $draft_production_requests->appends(Request::except('page'))->render() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
