@extends('admin.production.wizard')
@section('wizardtitle')
    New Production Request
@endsection
@section('wizardcss')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('wizardcontent')
    {!! Form::open(['route' => ['admin.production.store_step_1'], 'method' => 'POST', 'class' => 'prevent-multiple-submits']) !!}

    @csrf
    <label class="col-12 mb-4"><input name="ifstockupdate" type="checkbox" id="ifstockupdate" value="1"
            class="name">

        Stock Update (Only select if production request is for stock update and not tied to a wholesaler)</label>
    @include('admin.production.fields.inputfields')
    <div class="form-group row mx-0">
        <div class="col-md-6 center">
            {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits']) }}
        </div>
    </div>
    {!! Form::close() !!}
@endsection
@section('wizardscripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#retailerinforow").hide();

            $('[data-toggle="select2"]').select2();

            $('input[type=radio][name=type]').change(function() {
                if (this.value == 'wholesaler') {
                    $("#customerinforow").show();
                    $("#retailerinforow").hide();
                } else if (this.value == 'retailer') {
                    $("#customerinforow").hide();
                    $("#retailerinforow").show();
                }
            });

            $('#wholesalers').on("change", function() {
                var id = $(this).val();
                axios
                    .get("/admin/production/ajax/staff/specific?id=" + id)
                    .then(resp => {
                        var data = resp.data.html;
                        $('#staff').html(data);
                    })
                    .catch(error => {
                        this.error = error.response.data.message;
                    });

            });

            $('#ifstockupdate').on("change", function() {
                if (this.checked) {
                    $("#customerinforow").hide();
                    $("#customerinfotype").hide();
                } else {
                    $("#customerinforow").show();
                    $("#customerinfotype").show();
                }
            });

            $('.test').select2({
                placeholder: 'inventory...',
                allowClear: true,
                ajax: {
                    url: '/admin/inventory/ajax/getinventory/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.name + ' (' + item.sku + ')',
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });

        });
    </script>
@endsection
