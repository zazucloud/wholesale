@extends('layouts.base')

@section('title') Production Requests @endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="card mt-3">
            <div class="row m-0">
                <h5 class="box-title ml-3 mt-3 col-12 col-md-6"><b>@yield('production_page_title')</b></h5>

                <div class="col-12 col-md-6 mr-3 mt-3 mb-2">
                    @if (auth()->user()->hasPermissionTo('production-create'))

                        <a href="{{ route('admin.production.create') }}" class="btn btn-primary btn-sm pull-right mr-2">
                            <i class="fa fa-plus mr-2"></i> New Production Request</a>
                    @else
                        <button class="btntable disonabled btn btn-dark btn-sm pull-right mr-2">
                            <i class="text-white fa fa-plus mr-2"></i>New Production Request
                        </button>
                    @endif
                </div>
            </div>
            <div class="card-body override_cbody pt-0 spacious-container">

                <div class="tabs tabs-vertical">
                    <ul class="nav nav-tabs flex-column">
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('admin.production.index') ? 'active' : '' }}"
                                href="{{ route('admin.production.index') }}">All Requests</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('admin.production.pending') ? 'active' : '' }}"
                                href="{{ route('admin.production.pending') }}">Pending</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('admin.production.inproduction') ? 'active' : '' }}"
                                href="{{ route('admin.production.inproduction') }}">In Production</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('admin.production.completed') ? 'active' : '' }}"
                                href="{{ route('admin.production.completed') }}">Completed</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('admin.production.delivered') ? 'active' : '' }}"
                                href="{{ route('admin.production.delivered') }}">Delivered</a>
                        </li>

                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('admin.production.drafts') ? 'active' : '' }}"
                                href="{{ route('admin.production.drafts') }}">Drafts</a>
                        </li>

                    </ul>
                    <!-- /.nav-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane active">
                            @yield('requests_page_content')
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </div>
    </div>


@endsection

@section('extrascripts')
<script>
        $('#mailOrder').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var order_id = button.data('qid')
            var customer_email = button.data('mail')

            var modal = $(this)
            modal.find('.modal-body #order_id').val(order_id)
            modal.find('.modal-body #customer_email').val(customer_email)
        })
    </script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('.test').select2({
                placeholder: 'Created By...',
                allowClear: true,
                ajax: {
                    url: '/admin/users/ajax/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });
        });
    </script>
@endsection

