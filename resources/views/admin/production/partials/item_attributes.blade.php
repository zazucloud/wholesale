@if(count($item_attributes)>0)
    @foreach($item_attributes as $item_attribute)

        <?php
        $attribute = \App\Models\InventoryAttribute::find($item_attribute);
        $attribute_label = $attribute->label;
        $attribute_type = $attribute->type;
        ?>
        <div class="row">
            <div class="col-lg-10">

                @if($attribute->type=='fixed')

                    <?php
                    $options = \App\Models\InventoryAttribute::find($attribute->id)->options()->get();
                    ?>

                    <div>
                        <label for="attribute_{{$attribute->id}}">{{ucwords($attribute->label)}}</label>
                        <br>
                        <select class="form-control select2" name="attribute[{{$attribute->id}}]" id="attribute_{{$attribute->id}}">
                            @foreach($options as $option)
                                <option value="{{$option->id}}">{{$option->option}}</option>
                            @endforeach
                        </select>
                    </div>


                @elseif($attribute->type=='flexible')
                    <div class="row">
                        <div class="form-group container">
                            <div class="col-md-10">
                                <input title="Enter {{$attribute->label}} Value"
                                       type="text"
                                       class="form-control"
                                       name="flexible_attribute_option[{{$attribute->id}}]"
                                       placeholder=" {{$attribute->label}}">
                            </div>
                        </div>
                    </div>

                @endif
                <br>
            </div>
        </div>


    @endforeach
    <script>
        $(".select2").select2();
    </script>
@endif