@extends('admin.production.tabslayout')

@section('production_page_title')All Production Requests @endsection

@section('requests_page_content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card mt-2">

                <div class="card-body override_cbody">
                    @include('admin.production.searchform')
                    <div class="spacious-container">
                        <table class="table table-hover center-text">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Request Tag</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Order</th>
                                    <th scope="col">Created</th>
                                    <th scope="col">Created By</th>
                                    <th scope="col">Delivery Date</th>
                                    <th scope="col">Contact Person</th>
                                    <th scope="col">Priority</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($production_requests as $index => $production_request)
                                <tr>
                                    <th scope="row">{{ ++$i }}</th>

                                    <td>{{$production_request->request_tag}}</td>
                                    <td>{{(($production_request->if_stock_update==1)?'Stock Update':'Normal')}}</td>
                                    <td>
                                        @if ($production_request->if_stock_update!==1 && !is_null($production_request->order_id) && !is_null($production_request->order))
                                           <a href="{{ route('orders.view', ['order' => $production_request->order->id]) }}"
                                        class="text-info mr-2">{{$production_request->order->order_tag}}</a>
                                            @else
                                            N/A
                                        @endif
                                    </td>
                                    <td>{{stamp_date_time_simple($production_request->created_at)}}</td>
                                    <td>{{$production_request->createdBy->name ?? 'n/a'}}</td>
                                    <td>{{stamp_date_clean($production_request->delivery_date)}}
                                        <br>
                                    </td>
                                    <td>{{$production_request->staff->name ?? 'N/A'}}</td>
                                    <td>{{ production_priority_level($production_request->priority )}}</td>
                                    <td><a href="{{route('admin.productionrequest.view', ['production_request'=> $production_request->request_tag])}}"
                                            class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>


                                @endforeach


                            </tbody>
                        </table>
                    </div>

                </div>
                <div style="padding-left:20px;">
                    {!! $production_requests->appends(Request::except('page'))->render() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
