@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Roles Management
@endsection
<div class="card mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-md-7 col-0">

            </div>
            <div class="col-md-3 col-12">
                @can('roles-create')
                    <a class="btn btn-success btn-rounded ripple float-right" href="{{ route('roles.create') }}">
                        <span>Create New Role</span>
                    </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="spacious-container">
            <table class="table table-hover center-text">

                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>

                @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $role->name }}</td>
                        <td class="tw-flex">

                            <a href="{{ route('roles.show', $role->id) }}"
                                class="btn btn-sm btn-block btn-rounded btn-outline-info" data-toggle="tooltip"
                                data-placement="top" title="View">
                                <i class="list-icon linearicons-eye"></i>
                            </a>

                            @can('roles-edit')
                                <a href="{{ route('roles.edit', $role->id) }}"
                                    class="btn btn-sm btn-block btn-rounded btn-outline-warning" data-toggle="tooltip"
                                    data-placement="top" title="Edit">
                                    <i class="list-icon linearicons-pencil5"></i>
                                </a>
                            @endcan

                            @can('roles-delete')
                                <form method="post" action="{{ route('roles.destroy', $role->id) }}">
                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-sm btn-block btn-rounded btn-outline-danger btntable"
                                        type="submit"
                                        onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                        data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="list-icon linearicons-trash2"></i>
                                    </button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $roles->render() !!}
        </div>
    </div>
</div>


@endsection
