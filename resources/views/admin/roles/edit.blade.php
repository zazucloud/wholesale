@extends('layouts.base')


@section('title')
    Edit Role
@endsection
@section('content')
    @include('partials.page-title')

    <div class="card mt-3">
        <div class="card-body">
            <div class="row">

                <div class="col-lg-12 margin-tb">
                    <div class="float-right">

                        <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>

                    </div>

                </div>

            </div>


            @if (count($errors) > 0)

                <div class="alert alert-danger">

                    <strong>Whoops!</strong> There were some problems with your input.<br><br>

                    <ul>

                        @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                        @endforeach

                    </ul>

                </div>

            @endif


            {!! Form::model($role, ['method' => 'PATCH', 'route' => ['roles.update', $role->id],'class'=>'prevent-multiple-submits']) !!}

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">

                        <strong>Name:</strong>

                        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}

                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">

                        <strong>Permissions</strong>

                        <br />
                        <ul class="list-group">
                            @foreach ($permission as $key => $value)
                                <li class="list-group-item">

                                    <div class="row">
                                        <label class="col-2">{{ $key }}</label>
                                        <div class="col-10 row">
                                            @foreach ($value as $item)
                                                <label
                                                    class="col-2">{{ Form::checkbox('permission[]', $item->id, in_array($item->id, $rolePermissions) ? true : false, ['class' => 'name']) }}

                                                    {{ $item->extracted }}</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>

                            @endforeach
                        </ul>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                    <button type="submit" class="btn btn-primary multiple-submits">Submit</button>

                </div>

            </div>

            {!! Form::close() !!}
        </div>
    </div>


@endsection
