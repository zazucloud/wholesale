@extends('layouts.base')

@section('title')
    Customers
@endsection

@section('content')
    @include('partials.page-title')


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">
                    <div class="card-header">
                        @include('admin.retailers.search.searchform')
                    </div>

                    <div class="card-body override_cbody">

                        <div class="spacious-container">
                            <table class="table table-hover center-text" id="invoicestable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>

                                        <th scope="col">Name</th>
                                        <th scope="col">Kra Pin</th>
                                        <th scope="col">Email Address</th>
                                        <th scope="col">Phone Number</th>
                                        <th scope="col">Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($retailers as $index => $datum)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>

                                            <td>{{ $datum->business_name }}</td>
                                            <td>{{ $datum->kra_pin }}</td>
                                            <td>{{ $datum->email }}</td>
                                            <td>{{ $datum->telephone }}</td>
                                            <td class="d-flex">

                                                <a href="{{ route('admin.retailers.edit', ['id' => $datum->id]) }}"
                                                    class="btn btn-warning btn-sm mr-2">

                                                    <i data-toggle="tooltip" data-placement="top" title="Edit"
                                                        class="fa fa-edit"></i>
                                                </a>

                                                <form action="{{ route('admin.retailers.destroy', ['id' => $datum]) }}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="btn btn-danger btn-sm"
                                                        onclick="return confirm('Are you sure you Customer from your system ?')">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div style="padding-left:20px;">
                        {!! $retailers->appends(Request::except('page'))->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
@endsection
