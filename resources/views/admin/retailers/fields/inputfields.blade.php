<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Customer Name</label>
        {!! Form::text('business_name', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => "Retailer's Name",
        ]) !!}

    </div>
</div>



<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Phone Number</label>
        {!! Form::text('telephone', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Phone Number',
        ]) !!}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Email Address</label>
        {!! Form::email('email', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'Email Address',
        ]) !!}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>KRA PIN</label>
        {!! Form::text('kra_pin', null, [
            'class' => 'form-control',
            'required' => true,
            'placeholder' => 'KRA PIN',
        ]) !!}
    </div>
</div>
<div class="form-group row mx-0">
    <div class="col-md-6">
        <div class="form-group row">

            <div class="col-sm-12">
                <label>location Address</label>
                <gmap-autocomplete class="form-control" placeholder="Enter place or drag the marker"
                    @place_changed="getlonglat">
                </gmap-autocomplete>
                <input type="hidden" name="longitude" :value="long" />
                <input type="hidden" name="latitude" :value="lat" />
                <input type="hidden" name="address" :value="address" />
                <small>setlocation: <strong>@{{ address }}</strong></small>
                <br>
                <small>latitude: <strong>@{{ lat }}</strong></small>
                <small>longitude: <strong>@{{ long }}</strong></small>
            </div>
            <br>



        </div>
    </div>
    <div class="col-sm-6" style="height: 300px;">
        <br>

        <google-map :center="finposition" :zoom="14" map-type-id="terrain"
            style="width: 100%; height:calc(100% - 40px);" :fullscreenControl="true">
            <google-marker :position="finposition" :clickable="true" :draggable="true"
                @dragend="getdraggedlonglat" />
        </google-map>

    </div>
</div>
