@extends('layouts.base')
@section('title')
Edit Customer
@endsection
@section('content')
@include('partials.page-title')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <div class="offset-2 col-2">

                            <a class="btn btn-sm btn-info" href="{{route('admin.retailers.index')}}">
                                <span class="fa fa-arrow-left mr-2"></span> Go Back
                            </a>
                        </div>

                    </div>
                </div>

                <div class="card-body">
                    {{-- {!! Form::open(array('route' => ['admin.retailers.update'],'method'=>'POST','class'=>'prevent-multiple-submits')) !!} --}}
                    {!! Form::model($retailer, ['method' => 'PATCH','route' => ['admin.retailers.update', 'id'=>$retailer->id],'class'=>'prevent-multiple-submits']) !!}

                    @csrf
                    @include('admin.retailers.fields.inputfields')

                    <div class="form-group row mx-0">
                        <div class="col-md-6 center">
                            {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits'] )  }}
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('extrascripts')
<script>
    window.getaddress="{{$retailer->address}}";
    window.getlat="{{$retailer->latitude}}";
    window.getlong="{{$retailer->longitude}}";
</script>
<script>
    function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}
</script>

@endsection
