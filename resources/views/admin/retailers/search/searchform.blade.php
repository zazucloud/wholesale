<div class="row">
    <div class="col-sm-10">
        {!! Form::open(['route' => 'retailers.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-sm-10">
            <div class="">
            <label>Search</label>
                {!! Form::text('search', null, [
                    'class' => 'form-control form-control-inputs',
                    'required' => true,
                    'placeholder' => 'Search By Name,ID or Phone number',
                ]) !!}
            </div>
        </div>
        <div class="col-sm-2  mt-4 pt-2">
            <div>
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-sm-2  mt-4 pt-2">
        <a href="{{ route('admin.retailers.create') }}" class="btn btn-primary pull-right btn-sm"> <i class="fa fa-plus mr-2"></i>
            New Customer</a>
    </div>
</div>
