@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Template Management
@endsection

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8"></div>
            <div class="col-4">
                <a href="{{ route('templates.index') }}" class="btn btn-info btn-sm float-right">
                    <i class="fa fa-arrow-left mr-2"></i>  Go Back
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {!! Form::open(['route' => 'templates.store', 'method' => 'POST', 'files' => true,'class'=>'prevent-multiple-submits']) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Template Name:</strong>
                    {!! Form::text('name', null, ['placeholder' => 'Template Name', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Template Header:</strong>
                    <input type="file" id="template_header" class="form-control" name="template_header" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Template Footer:</strong>
                    <input type="file" id="template_footer" class="form-control" name="template_footer" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm multiple-submits">Submit</button>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
