@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Edit Template Details
@endsection

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8"></div>
            <div class="col-4">
                <a href="{{ route('templates.index') }}" class="btn btn-info btn-sm float-right">
                    <i class="fa fa-arrow-left mr-2"></i> Go Back
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">

        {!! Form::model($template, ['method' => 'PATCH', 'route' => ['templates.update', $template->id], 'files' => true,'class'=>'prevent-multiple-submits']) !!}

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Template Header:</strong>
                    @forelse ($template->getMedia('templateHeader') as $item)
                        <img class="img-responsive " src="{{ $item->getUrl() }}" alt="">
                        <input type="file" id="template_header" class="form-control" name="template_header" />

                    @empty

                        <input type="file" id="template_header" class="form-control" name="template_header" />

                    @endforelse
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Template Footer:</strong>
                    @forelse ($template->getMedia('templateFooter') as $item)
                        <img class="img-responsive " src="{{ $item->getUrl() }}" alt="">
                        <input type="file" id="template_footer" class="form-control" name="template_footer" />

                    @empty

                        <input type="file" id="template_footer" class="form-control" name="template_footer" />

                    @endforelse
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm multiple-submits">Submit</button>
            </div>

        </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
