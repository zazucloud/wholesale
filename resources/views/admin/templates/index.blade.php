@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Template Management
@endsection

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8"></div>
            <div class="col-4">
                @can('templates-create')
                    <a href="{{ route('templates.create') }}" class="btn btn-success float-right btn-sm">
                        <i class="fa fa-plus mr-2"></i>New Template
                    </a>
                @endcan
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover center-text">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Header</th>
                <th>Footer</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($templates as $key => $template)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $template->name }}</td>
                        <td>
                            @if (!is_null($template->getFirstMedia('templateHeader')))
                                <img class="img-responsive" height="50" width="75"
                                    src="{{ $template->getFirstMedia('templateHeader')->getUrl() }}"
                                    alt="{{ $template->name }}">
                            @else
                                <img class="img-responsive" height="50" width="75" src="" alt="">
                            @endif
                        </td>
                        <td>
                            @if (!is_null($template->getFirstMedia('templateFooter')))
                                <img class="img-responsive" height="50" width="75"
                                    src="{{ $template->getFirstMedia('templateFooter')->getUrl() }}"
                                    alt="{{ $template->name }}">
                            @else
                                <img class="img-responsive" height="50" width="75" src="" alt="">
                            @endif
                        </td>
                        <td class="tw-flex">
                            @can('templates-edit')
                                <a href="{{ route('templates.edit', $template->id) }}" data-toggle="tooltip"
                                    data-placement="top" title="Edit" class="btn btn-warning text-white btn-sm mr-2">
                                    <i class="fa fa-edit"></i>
                                </a>
                            @endcan

                            @can('templates-delete')
                                <form method="post" action="{{ route('templates.destroy', $template->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger text-white btn-sm mr-2"
                                        onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                        data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
