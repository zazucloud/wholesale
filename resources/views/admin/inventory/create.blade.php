@extends('layouts.base')

@section('title')New Inventory Item @endsection
@section('css')
<link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
    @include('partials.page-title')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-10">

                            </div>
                            <div class="col-2">
                                <a class="btn btn-sm btn-info float-right mr-2" href="{{ route('admin.inventory.index') }}">
                                    <span class="fa fa-arrow-left mr-2"></span> Go Back
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="card-body">


                        {!! Form::open(['route' => ['admin.inventory.store'], 'method' => 'POST', 'files' => true,'class'=>'prevent-multiple-submits']) !!}

                        @csrf
                        @include('admin.inventory.form')

                        <div class="form-group row mx-0">
                            <div class="col-md-6 center">
                                {{ Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm multiple-submits']) }}
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('extrascripts')
<script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('[data-toggle="select2"]').select2();
    });

</script>
@endsection
