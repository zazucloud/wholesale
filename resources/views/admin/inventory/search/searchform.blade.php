<div class="row col-sm-12">
    {!! Form::open(['route' => null, 'method' => 'get', 'class' => 'row col-sm-10']) !!}
    <div class="col-sm-2">
        <div class="">
            <label>Warehouse</label>
            {{ Form::select('warehouse_id', $warehouses, null, ['class' => 'table-group-action-input form-control input-medium', 'placeholder' => 'Select Warehouse', 'data-toggle' => 'select2']) }}

        </div>
    </div>
    <div class="col-sm-3">
        <div class="">
            <label>Search</label>
            {!! Form::text('search_name', null, [
                'class' => 'form-control form-control-inputs',
                // 'required' => true,
                'placeholder' => 'Search By Name,SKU or Item Number',
            ]) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="">
            <label>Color</label>
            {!! Form::text('color', null, [
                'class' => 'form-control form-control-inputs',
                // 'required' => true,
                'placeholder' => 'Search By Color',
            ]) !!}
        </div>
    </div>

    <label for="checkbox" class="col-sm-2 mt-5 ">
        <input type="checkbox" name="checkImage" class=""> <span>check Image </span>
    </label>
    <div class="col-sm-2 mt-4 pt-2">
        <div>
            {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

        </div>
    </div>
    {!! Form::close() !!}


    <div class="col-sm-2 mt-4">
        <form method="POST" action="{{ route('inventory.export.no_images') }}" class="float-right">
            @csrf

            <input type="hidden" class="form-control" name="search_name" value="{{ $toreturn['search_name'] ?? '' }}">
            <input type="hidden" class="form-control" name="color" value="{{ $toreturn['color'] ?? '' }}">
            <input type="hidden" class="form-control" name="warehouse_id"
                value="{{ $toreturn['warehouse_id'] ?? '' }}">
            <input type="hidden" class="form-control" name="checkImage" value="{{ $toreturn['checkImage'] ?? '' }}">
            <button type="submit" class="btn btn-sm btn-primary float-right mt-2">
                Export To Excel
            </button>
        </form>

    </div>

    {{-- <div class="row col-md-2 col-12">
        <div class="col-sm-6 col-12 mt-4 pt-2">
            <a href="{{route('zones.download_current_discount',['zone'=>$zone->id])}}" class="btn btn-sm btn-primary">Export</a>
        </div>

        <div class="col-sm-6 col-12 mt-4 pt-2">
            <button @click="openZoneModal()" class="btn btn-sm btn-info">Import</a>
        </div>
    </div> --}}

</div>
