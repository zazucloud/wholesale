@extends('layouts.base')

@section('title')
    Inventory
@endsection

@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @include('partials.page-title')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">
                    <div class="card-header">
                        @include('admin.inventory.search.searchform')
                    </div>

                    <div class="card-body override_cbody">

                        <div class="spacious-container">
                            <table class="table table-hover center-text" id="invoicestable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>Images</th>
                                        <th>Gallery</th>
                                        <th>ViewGallery</th>
                                        <th>Warehouse</th>
                                        <th>SKU</th>
                                        <th>ItemNumber</th>
                                        <th>Name</th>
                                        <th>Color</th>
                                        <th>Stock Available</th>

                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($inventory as $index => $datum)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>
                                            <td>
                                                @foreach ($datum->getMedia('inventory') as $item)
                                                    <img src="{{ $item->getUrl() }}" alt=""
                                                        class="tw-rounded-full tw-h-10 tw-w-10">
                                                @endforeach
                                            </td>
                                            <td>
                                                @if (is_null($datum->getFirstMedia('inventorygallery')))
                                                    <div>
                                                        <img src="{{ asset('images/notavail.png') }}"
                                                            class="tw-rounded-full tw-h-10 tw-w-10">
                                                    </div>
                                                @else
                                                        <div>
                                                            <img src="{{ $datum->getFirstMedia('inventorygallery')->getUrl() }}" class="tw-rounded-full tw-h-10 tw-w-10">
                                                        </div>

                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('inventory.inventory_view_gallery', ['inventory' => $datum->id]) }}"
                                                    class="ml-3 btn-sm btn btn-info">
                                                    <i data-toggle="tooltip" data-placement="top" title="View Gallery"
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                            <th>
                                                @if (!empty($datum->warehouse_id))
                                                    <span class="p-1 font-weight-bolder text-success">
                                                        {{ strtoupper($datum->warehouse->name) }}
                                                    </span>
                                                @else
                                                    {{ 'N/A' }}
                                                @endif
                                            </th>


                                            <td>{{ $datum->sku }}</td>
                                            <td>{{ $datum->itemnumber }}</td>
                                            <td>{{ $datum->name }}</td>
                                            <td>{{ $datum->color }}</td>
                                            <td>{{ $datum->quantity }}</td>
                                            <td class="d-flex">

                                                <a href="{{ route('inventory.edit', ['inventory' => $datum]) }}"
                                                    class="btn btn-warning btn-sm mr-2">

                                                    <i data-toggle="tooltip" data-placement="top" title="Edit"
                                                        class="fa fa-edit"></i>
                                                </a>

                                                <form action="{{ route('inventory.delete', ['inventory' => $datum]) }}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="_METHOD" value="DELETE">
                                                    <button class="btn btn-danger btn-sm"
                                                        onclick="return confirm('Are you sure you a Inventory Item from your system ?')">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                            </td>


                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div style="padding-left:20px;">
                        {!! $inventory->appends(Request::except('page'))->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
@endsection

@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="select2"]').select2();
        });
    </script>
@endsection
