@extends('admin.layout')
@section('title') {{$inventory->name}} - View @endsection

<link rel="stylesheet" href="{{asset('assets/pages/css/profile.css?v=22')}}">

@section('page-content')
    <style>
        .profile-data .data_label {
            margin-right: 5px;
            font-weight: bold;
        }

        .profile-data .data_label:after {
            content: ":"
        }
    </style>

    <h1 class="page-title"> Item Profile
        <small>{{$inventory->name}}</small>


        <a href="{{route('admin.inventory.index')}}" class="btn btn-primary btn-circle pull-right"> <i
                    class="fa fa-chevron-left"></i> Back to Listing </a>


    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">

                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="{{asset('media/box.png')}}" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{$inventory->name}} </div>
                        <div class="profile-usertitle-job">SKU: {{$inventory->sku}} </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->


                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a href="">
                                    <i class="icon-home"></i> Details </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="icon-arrow-down"></i> Orders </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-area-chart"></i> Reports</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
                <!-- END PORTLET MAIN -->

            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->

            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">

                        <!-- BEGIN PORTLET -->
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md col-lg-12">

                                    <span>Details</span>

                                    <a href="{{route('inventory.edit',$inventory->sku)}}"
                                       class="btn btn-outline-info pull-right"> <i
                                                class="fa fa-chevron-left"></i> Edit Details </a>

                                </div>

                            </div>


                            <div class="portlet-body">

                                <div class="tab-content">

                                    <h5>Brand: {{$inventory->brand}}</h5>


                                </div>

                            </div>


                        </div>
                        <!-- END PORTLET -->
                    </div>
                </div>


                <div class="row">


                </div>
            </div>

        </div>
    </div>



@endsection

@section('custom-scripts')
    <script>
        $("#menu-staff").addClass('active');
    </script>
@endsection