@extends('layouts.base')

@section('title') Inventory Product Gallery @endsection

@section('content')

    @include('partials.page-title')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card mt-2">

                    <div class="card-body">
                        <div class="row">

                            @if (is_null($inventory->getFirstMedia('inventorygallery')))

                            <div class="col-12 tw-flex">
                                <img src="{{ asset("images/notavail.png") }}" class="tw-w-44 m-auto">
                            </div>
                            @else
                                @foreach ($inventory->getMedia('inventorygallery') as $item)
                                    <div class="col-4">
                                        <img src="{{ $item->getUrl() }}" class="tw-w-100">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('custom-scripts')

@endsection
