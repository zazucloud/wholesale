<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Brand</label>
        {{ Form::select('brand', $brands, $current_brand, ['class' => 'table-group-action-input form-control input-medium', 'data-toggle' => 'select2']) }}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Warehouse</label>
        {{ Form::select('warehouse', $warehouses, null, ['class' => 'table-group-action-input form-control input-medium', 'data-toggle' => 'select2']) }}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>SKU</label>
        {{ Form::text('sku', null, ['class' => 'form-control', 'required' => 'required']) }}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Name</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) }}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Description</label>
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-6">
        <label>Inventory Image</label>
        <input type="file" name="image" class='form-control' required>

    </div>
    <div class="col-sm-6 tw-justify-center pt-3 tw-flex">
        @if (isset($inventory) && !is_null($inventory->getFirstMedia('inventory')))
            <img src="{{ $inventory->getFirstMedia('inventory')->getUrl() }}" alt=""
                class="img-thumbnail tw-h-16 tw-w-16 mr-3">
        @endif
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-6">
        <label>Inventory Gallery (You can select multiple images)</label>
        <input type="file" name="image_gallery[]" class='form-control' multiple required>
    </div>

    <div class="col-sm-6 tw-justify-center pt-3 tw-flex">
        @if (isset($inventory) && !is_null($inventory->getFirstMedia('inventorygallery')))
            @foreach ($inventory->getMedia('inventorygallery') as $item)
                <img src="{{ $item->getUrl() }}" class="img-thumbnail tw-h-16 tw-w-16 mr-3">
            @endforeach
        @endif
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">

        <strong class="mb-3">Categories</strong>

        <br />
        @if (count($parent) > 0)
            <ul class="list-group">
                @foreach ($parent as $category)
                    <li class="list-group-item">


                        <div class="row">

                            <label
                                class="col-4">{{ Form::checkbox('categories[]', $category->id, in_array($category->id, $selected_categories) ? true : false, ['class' => 'name']) }}

                                {{ $category->name }}</label>

                            @foreach ($category->childinventory as $child)
                                <label
                                    class="col-4">{{ Form::checkbox('categories[]', $child->id, in_array($child->id, $selected_categories) ? true : false, ['class' => 'name']) }}

                                    {{ $child->name }}</label>
                            @endforeach


                        </div>

                    </li>
                @endforeach
            </ul>
        @else
            <div class="alert alert-danger">
                Categories need to be added/configured first.
            </div>
        @endif
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Regular Price</label>
        {!! Form::number('price', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Price']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Sales Price</label>
        {!! Form::number('crossed_price', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        <strong class="mb-3">Attributes</strong>
        <div class="card  mt-2">
            <div class="card-body">
                <div class="tabs tabs-bordered">
                    <ul class="nav nav-tabs">

                        <li class="nav-item">
                            <a href="#global-attributes" data-toggle="tab" aria-expanded="true"
                                class="nav-link px-3 py-2 active">
                                Global Attributes
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#custom-attributes" data-toggle="tab" aria-expanded="true"
                                class="nav-link px-3 py-2">
                                Custom Attributes
                            </a>
                        </li>
                        
                    </ul>
                    <!-- /.nav-tabs -->
                    <div class="tab-content px-0">
                        <div class="tab-pane active" id="global-attributes">
                            <attributes :inventoryattributes="{{ json_encode($attributes) }}"
                                :selected_attributes="{{ json_encode($selected_fixed_attributes) }}"></attributes>
                        </div>
                        <div class="tab-pane" id="custom-attributes">
                            <customattributes
                                :selected_custom_attributes="{{ json_encode($selected_flexible_attributes) }}">
                            </customattributes>
                        </div>

                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </div>

        <br>
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Available Quantity</label>
        {!! Form::number('quantity', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Low Stock Threshold Quantity</label>
        {!! Form::number('threshold', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Out Of Stock Threshold Quantity</label>
        {!! Form::number('outofstock', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Minimum Stock Threshold Quantity</label>
        {!! Form::number('minimumstock', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Choose if item is either a Production or Trade Item</label>

        <div class="form-check">
            {!! Form::radio('productionitem', 0, ['class' => 'form-check-input', 'id' => 'productionitem1']) !!}
            <label class="form-check-label" for="productionitem1">
                Trade Item
            </label>
        </div>
        <div class="form-check">
            {!! Form::radio('productionitem', 1, ['class' => 'form-check-input', 'id' => 'productionitem2']) !!}

            <label class="form-check-label" for="productionitem2">
                Production Item
            </label>
        </div>
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Status</label>

        {!! Form::select(
            'visibility',
            [
                '1' => 'Published',
                '0' => 'Not Published',
            ],
            null,
            ['class' => 'custom-select', 'id' => 'visibility', 'required' => true],
        ) !!}
    </div>
</div>

<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Is Featured</label>

        {!! Form::select(
            'is_featured',
            [
                '0' => 'Not Featured',
                '1' => 'Featured',
            ],
            null,
            ['class' => 'custom-select', 'id' => 'featured', 'required' => true],
        ) !!}
    </div>
</div>
