<div class="row">
    <div class="col-md-10 col-12">
        {!! Form::open(['route' => 'manufacturers.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-md-10 col-12">
            <label for="">Search</label>
            <div class="">
                {!! Form::text('search', null, [
                    'class' => 'form-control form-control-inputs',
                    'required' => true,
                    'placeholder' => 'Search By Name,Email,Phone',
                ]) !!}
            </div>
        </div>
        <div class="col-md-2 mt-4 pt-md-2 col-12">
            <div>
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn  btn-info btn-sm']) }}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2 col-12 mt-4 pt-2">
        <button @click="openManufacturerModal(null,1)" class="btn btn-primary btn-sm float-right">
            <i class="fa fa-plus mr-2"></i> New Manufacturer</button>
    </div>
</div>
