@extends('layouts.base')

@section('title')
    Manufacturers
@endsection

@section('content')
    @include('partials.page-title')
    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                @include('admin.manufacturers.search.manufacturers')
            </div>
            <div class="card-body">
                <div class="spacious-container">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th scope="col">Image</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($manufacturers as $index => $manufacturer)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>
                                        @if (!is_null($manufacturer->getFirstMedia('manulogos')))
                                            <img class="img-responsive" height="50" width="75"
                                                src="{{ $manufacturer->getFirstMedia('manulogos')->getUrl() }}"
                                                alt="{{ $manufacturer->name }}">
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                    <td>{{ $manufacturer->name }}</td>
                                    <td>{{ $manufacturer->email }}</td>
                                    <td>{{ $manufacturer->phone }}</td>

                                    <td>
                                        @if ($manufacturer->status == 1)
                                            Active
                                        @elseif($manufacturer->status == 5)
                                            Disabled
                                        @elseif($manufacturer->status == 9)
                                            Terminated
                                        @endif
                                    </td>
                                    <td class="tw-flex">
                                        @can('manufacturers-edit')
                                            <a @click="openManufacturerModal({{ $manufacturer }},2)" data-toggle="tooltip"
                                                data-placement="top" title="Edit"
                                                class="btn btn-warning btn-sm text-white mr-1">
                                                <i class="list-icon linearicons-pencil3"></i>
                                            </a>
                                        @endcan

                                        @can('manufacturers-config')
                                            <a href="{{ route('admin.manufacturers.production_list', $manufacturer->id) }}"
                                                data-toggle="tooltip" data-placement="top" title="Manage"
                                                class="btn btn-info btn-sm mr-1">
                                                <i class="list-icon linearicons-hammer-wrench"></i>
                                            </a>
                                        @endcan

                                        @can('manufacturers-delete')
                                            <form method="post"
                                                action="{{ route('admin.manufacturers.delete', $manufacturer->id) }}">
                                                @csrf
                                                @method('DELETE')

                                                <button class="btn btn-sm btn-block btn-danger" type="submit"
                                                    onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                                    data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="list-icon linearicons-trash2"></i>
                                                </button>
                                            </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        {{ $manufacturers->links() }}

                    </table>
                </div>
            </div>
        </div>
    </div>


    <manufacturersmodal :manufacturer="{{ json_encode($manufacturers) }}"></manufacturersmodal>
@endsection
