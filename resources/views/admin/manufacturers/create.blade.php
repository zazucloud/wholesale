@extends('layouts.base')

@section('title')
    Manufacturer
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container mt-3">
        <div class="card">



            <div class="card-header">
                <div class="row">
                    <div class="col-12">
                        <h4>
                            Adding a Manufacturer
                            <a href="{{ route('payments.index') }}" class="btn btn-outline-primary btn-sm float-right">
                                Back to
                                Manufacturer Listing</a>
                        </h4>
                        <p>
                            Default page description - relevant information relating to the current page or process</p>
                        {{-- </div> --}}
                        {{-- <div class="col-4"> --}}

                    </div>
                </div>
            </div>


            <div class="card-body">



                {!! Form::open(['url' => route('admin.manufacturers.store'), 'files' => true,'class'=>'prevent-multiple-submits']) !!}

                {!! Form::token() !!}

                <div title="Enter manufacturers name" class="form-group">
                    <label for="name">Name</label>
                    <input required id="name" spellcheck="false" name="name" type="text" class="form-control"
                        placeholder="Manufacturer's Name">
                </div>


                <div class="form-group">
                    <label for="phone_number">Phone Number</label>
                    <input required id="phone" name="phone" type="text" class="form-control" placeholder="Phone Number">
                </div>

                <div class="form-group">
                    <label for="email_address">Email Address</label>
                    <small><em>(Will serve as account username)</em></small>
                    <input required id="email" spellcheck="false" name="email" type="email" class="form-control"
                        placeholder="Email Address">
                </div>


                <div class="form-group">
                    <label class="control-label" for="logo">Logo</label>
                    <div>
                        <input type="file" id="logo" name="logo">
                        <p class="help-block"> Select manufacturer's logo (optional) </p>
                    </div>
                </div>


                <div class="form-group">
                    <label for="notes" class="control-label">Notes:</label>
                    <br>
                    <div class="">
                        <textarea rows="5" id="notes" class="form-control" name="notes"
                            placeholder="Notes on manufacturer (optional)"></textarea>
                    </div>
                </div>



                <div class="form-group">

                    <button type="submit" class="btn btn-primary multiple-submits"><i class="fa fa-send"></i> Submit Details</button>

                </div>

                {!! Form::close() !!}



            </div>






        </div>
    </div>

@endsection

@section('script-bottom')
    <script>
        $("#menu-manufactures").addClass('active');

    </script>
@endsection
