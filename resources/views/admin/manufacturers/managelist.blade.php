@extends('layouts.base')

@section('title')
    Manage Production List
@endsection

@section('css')
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @include('partials.page-title')
    <div class="container-fluid mt-3">
        <div class="card">
            <div class="row px-4 py-3">
                <div class="col-8">
                   <strong>{{ strtoupper($manufacturer->name) }}</strong>
                </div>
                <div class="col-4">
                    <a href="{{ route('admin.manufacturers.index') }}" class="btn btn-info float-right btn-sm"><span
                            class="fa fa-arrow-left mr-2"></span>Go Back</a>
                </div>
            </div>
            <div class="card-body">
                <manufacturersproducionlist :categories="{{ json_encode($categories) }}"
                    :manufacturer="{{ $manufacturer }}" :active_items="{{ json_encode($active_items) }}">
                </manufacturersproducionlist>
            </div>
        </div>
    </div>


@endsection

@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
@endsection
