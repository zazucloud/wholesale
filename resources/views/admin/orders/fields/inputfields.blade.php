<div class="form-group row mx-0">
    <label class="col-3 mb-3"><input name="type" type="radio" value="wholesaler" class="name" checked>

        wholesaler</label>
    <label class="col-3 mb-3"><input name="type" type="radio" value="retailer" class="name">

        Customer</label>
</div>
<div class="form-group row" id="wholesaler">
    <div class="form-group col-12 col-md-4">
        <div class="col-sm-12">
            <label>Client</label>
            {{-- {{ Form::select('wholesaler', $wholesalers, null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'clients', 'data-toggle' => 'select2', 'placeholder' => 'Select Client']) }} --}}
            <select class="form-control" id="clients" name="wholesaler" data-toggle="select3">
                <option value="">Select Client</option>
                @foreach ($wholesalers as $wholesaler)
                    <option value="{{ $wholesaler->id }}">{{ $wholesaler->business_name }}  -  {{ $wholesaler->registration_number }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group col-12 col-md-4">
        <div class="col-sm-12">
            <label>Mail Template</label>
            <select class="form-control" id="l13" name="wholesaler_template_id" data-toggle="select2">
                <option value="">Select Email Template</option>
                @foreach ($templates as $template)
                    <option value="{{ $template->id }}">{{ $template->name }}</option>
                @endforeach
            </select>

        </div>
    </div>
    <div class="form-group col-12 col-md-4">
        <div class="col-sm-12">
            <label>Delivery Locations</label>
            {{ Form::select('locations', [], null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'locations', 'data-toggle' => 'select2', 'placeholder' => 'Select Client First']) }}

        </div>
    </div>
</div>

<div class="form-group row" id="retailers">
    <div class="form-group col-12 col-md-6">
        <div class="col-sm-12">
            <label>Client</label>
            {{ Form::select('retailers', $retailers, null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'customer', 'data-toggle' => 'select2', 'placeholder' => 'Select Client']) }}
        </div>
    </div>
    <div class="form-group col-12 col-md-6">
        <div class="col-sm-12">
            <label>Mail Template</label>
            <select class="form-control" id="l13" name="retailer_template_id" data-toggle="select2">
                <option value="">Select Email Template</option>
                @foreach ($templates as $template)
                    <option value="{{ $template->id }}">{{ $template->name }}</option>
                @endforeach
            </select>

        </div>
    </div>
</div>
<inventorydropdown></inventorydropdown>
{{-- <div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Comments</label>
        {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 3]) !!}

    </div>
</div> --}}


<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Delivery Date</label>
        {!! Form::date('delivery_date', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Delivery Date']) !!}


    </div>
</div>
<div class="form-group row mx-0">
    <div class="col-sm-12">
        <label>Attachment eg. LPO</label><br>
        {!! Form::file('attachment', null, ['class' => 'form-control']) !!}
    </div>
</div>
