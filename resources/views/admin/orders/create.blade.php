@extends('layouts.base')
@section('title')
    New Order
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
    @include('partials.page-title')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-10">

                            </div>
                            <div class="col-2">
                                <a class="btn btn-sm btn-info float-right mr-3" href="{{ route('orders.index') }}">
                                    <span class="fa fa-arrow-left mr-2"></span> Go Back
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="card-body">
                        {!! Form::open(['route' => ['orders.store'], 'method' => 'POST', 'files' => true, 'id' => 'productionRequestForm', 'class' => 'prevent-multiple-submits']) !!}

                        @csrf
                        @include('admin.orders.fields.inputfields')
                        <div class="form-group row mx-0">
                            <div class="col-md-6 center">
                                <button type="submit" class='btn btn-primary multiple-submits'
                                    @click='formSubmittionCheck("productionRequestForm")'>Submit</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#retailers").hide();

            $('[data-toggle="select2"]').select2();
             $('[data-toggle="select3"]').select2();

            $('input[type=radio][name=type]').change(function() {
                if (this.value == 'wholesaler') {
                    $("#wholesaler").show();
                    $("#retailers").hide();
                } else if (this.value == 'retailer') {
                    $("#wholesaler").hide();
                    $("#retailers").show();
                }
            });

            $('.test').select2({
                placeholder: 'inventory...',
                allowClear: true,
                ajax: {
                    url: '/admin/inventory/ajax/getinventory/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.name + ' (' + item.sku + ')',
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });
        });
    </script>
@endsection
