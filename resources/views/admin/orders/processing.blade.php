@extends('admin.orders.tabslayout')

@section('order_page_title')
    Processing Orders
@endsection

@section('order_page_content')
    <div class="spacious-container">
        <div class="card">

            <div class="card-body">
                @include('admin.orders.searchform')

                <table class="table table-hover center-text">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">OrderNo.</th>
                            <th scope="col">Type</th>
                            <th scope="col">Name</th>
                            <th scope="col">Total</th>
                            <th scope="col">Status</th>
                            <th scope="col">CreatedAt</th>
                            <th scope="col">Actions</th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $index => $order)
                            <tr>
                                <th scope="row">{{ $index + 1 }}</th>
                                <td>{{ $order->face_id }}</td>
                                <td>
                                    @if ($order->clientable_type == 'App\Models\Retailer')
                                        <span class="badge bg-soft-success">Customer</span>
                                    @else
                                        <span class="badge bg-soft-primary">Wholesaler</span>
                                    @endif

                                </td>
                                <td>
                                    {{ $order->clientable->business_name }}
                                </td>
                                <td>{{ number_format($order->total) }}</td>
                                <td>
                                    <span
                                        class="badge bg-soft-{{ App\Models\Order::$order_stages_colors[$order->stage] }} text-{{ App\Models\Order::$order_stages_colors[$order->stage] }}">{{ App\Models\Order::$order_stages[$order->stage] }}</span>


                                </td>

                                <td>{{ stamp_date_time($order->created_at) }}<br>
                                    <small>{{ ago($order->created_at) }}</small>
                                </td>

                                <td class="tw-flex">
                                    <a href="{{ route('orders.view', ['order' => $order->id]) }}"
                                        class="btn btn-info btn-sm mr-2"><i class="fa fa-eye"></i></a>

                                    <a href="{{ route('orders.invoice_download', ['order' => $order->id]) }}"
                                        class="btn btn-info btn-sm mr-2"><i class="fa fa-download"
                                            aria-hidden="true"></i></a>

                                    <button class="btn btn-info btn-sm mr-2" data-toggle="modal" data-target="#mailOrder"
                                        data-qid="{{ $order->id }}" data-mail="{{ $order->clientable->email }}"><i
                                            class="fa fa-envelope-open"></i></button>
                                    @if (auth()->user()->hasPermissionTo('orders-delete'))
                                        <form action="{{ route('orders.cancel', ['order' => $order->id]) }}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_METHOD" value="DELETE">
                                            <button class="btn btn-danger btn-sm"
                                                onclick="return confirm('Are you sure you want to delete order from your system ?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        @else
                                            <button class="btntable disonabled btn btn-sm btn-dark mr-2">
                                                <i class="text-white fa fa-trash"></i>
                                            </button>
                                    @endif
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div style="padding-left:20px;">
        {!! $orders->appends(Request::except('page'))->render() !!}
    </div>
@endsection
