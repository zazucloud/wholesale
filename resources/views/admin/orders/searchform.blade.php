<div>
    {!! Form::open(['route' => null, 'method' => 'get', 'class' => 'row']) !!}
    <div class="col-sm-11 row">
        <div class="col-sm-2">
            <label>Select Client Type</label>
            <div>
                <label class="mb-0" style="display: inline-flex"> <input name="type" type="radio" value="wholesaler"
                        class="name form-control mr-2" checked> wholesaler</label>

            </div>
            <div>
                <label class="mb-0" style="display: inline-flex"><input name="type" type="radio" value="retailer"
                        class="name form-control mr-2">Customer</label>

            </div>
        </div>

        <div class="col-sm-3" id="wholesaler">
            <div class="form-group ">
                <label>Select Client</label>
                <select class=" form-control wholesaler_select" name="wholesaler" id="clients_wholesale"></select>

            </div>
        </div>

        <div class="col-sm-3" id="retailers">
            <div class="form-group ">
                <label>Select Client</label>
                <select class=" form-control retailer_select" name="retailer" id="clients"></select>

            </div>
        </div>
        <div class="col-sm-3 ">
            <div class="form-group">
                <label>Search</label>

                {!! Form::text('search', null, [
                    'class' => 'form-control form-control-inputs',
                    'name' => 'ordertag',
                    'placeholder' => 'Search By Order Tag',
                ]) !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Select User</label>
                <select class=" form-control test" name="contactperson"></select>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Status</label>
                <select class="form-control form-control-inputs" name="status">
                    <option value="">Select Status
                    </option>
                    <option value="1">Pending
                    </option>
                    <option value="2">Approved
                    </option>
                    <option value="3">Partially shipped
                    </option>
                    <option value="4">Fully shipped
                    </option>
                    <option value="5">Paid
                    </option>
                </select>
            </div>
        </div>
        <div class="col-sm-3">

            <div class="form-group">
                <label>Created</label>

                {!! Form::date('date', null, [
                    'class' => 'form-control form-control-inputs',
                    'name' => 'date',
                    'placeholder' => 'Search By Order Tag',
                ]) !!}
            </div>
        </div>

    </div>
    <div class="col-sm-1">
        <div class="float-right mt-4 pt-2">

            {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

        </div>
    </div>
    {!! Form::close() !!}
</div>
<hr />
