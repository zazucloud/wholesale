<div class="row">
    <div class="col-md-6 col-12">
        <div class="chatbox-body scrollbar-enabled scroll-to-bottom ps ps--active-y">
            @foreach($comments as $comment)
            <div class="chatbox-messages py-0">
                <div class="message reply media">
                    <div class="media-body">
                        <p
                         class="mxwidth"
                          {{-- @click="openComment({{$comment}})" --}}
                          >{{$comment->comment}}</p>
                        <small class="text-muted">{{$comment->user->name}}
                            -{{stamp_date_time($comment->created_at)}}
                        </small>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    </div>

    <div class="col-md-6 col-12">

        <form method="post" action="{{route('orders.comment.store')}}">

            {{csrf_field()}}

            <input type="hidden" name="order_id" value="{{$order->id}}">
            <label class="bold" for="comment">Your Comment</label>
            <br>
            <textarea required name="comment" id="comment" class="form-control" rows="5"></textarea>
            <br>
            <button type="submit" class="btn btn-primary center-block"><i class="fa fa-send"></i> Submit Comment
            </button>

        </form>
    </div>
</div>

