<div class="Timeline">

    <svg height="5" width="100">
        <line x1="0" y1="0" x2="200" y2="0" style="stroke:#004165;stroke-width:5" />
        Sorry, your browser does not support inline SVG.
    </svg>

    <div class="event1">

        <div class="event1Bubble">
            <div class="eventTime">
                <div class="DayDigit">{{ \Carbon\Carbon::parse($order->created_at)->format('d') }}</div>
                <div class="Day">
                    {{ \Carbon\Carbon::parse($order->created_at)->format('l') }}
                    <div class="MonthYear">{{ \Carbon\Carbon::parse($order->created_at)->format('F Y') }}</div>
                </div>
            </div>
            <div class="eventTitle">Order Made</div>
        </div>
        <div class="eventAuthor">by {{ $order->createdBy->name ?? 'N/A' }}</div>
        <svg height="20" width="20">
            <circle cx="10" cy="11" r="5" fill="#004165" />
        </svg>
        <div class="time">{{ \Carbon\Carbon::parse($order->created_at)->format('H:i A') }}</div>

    </div>
    @if (!is_null($approved))
        <svg height="5" width="200">
            <line x1="0" y1="0" x2="300" y2="0" style="stroke:#004165;stroke-width:5" />
            Sorry, your browser does not support inline SVG.
        </svg>

        <div class="event2">

            <div class="event2Bubble">
                <div class="eventTime">
                    <div class="DayDigit">{{ \Carbon\Carbon::parse($approved->created_at)->format('d') }}</div>
                    <div class="Day">
                        {{ \Carbon\Carbon::parse($approved->created_at)->format('l') }}
                        <div class="MonthYear">{{ \Carbon\Carbon::parse($approved->created_at)->format('F Y') }}</div>
                    </div>
                </div>
                <div class="eventTitle">Order Approved</div>
            </div>
            <div class="event2Author">by {{ $approved->createdBy->name ?? 'N/A' }}</div>
            <svg height="20" width="20">
                <circle cx="10" cy="11" r="5" fill="#004165" />
            </svg>
            <div class="time2">{{ \Carbon\Carbon::parse($approved->created_at)->format('H:i A') }}</div>
        </div>
    @endif


    @if (!is_null($partially_shipped))
        <svg height="5" width="200">
            <line x1="0" y1="0" x2="200" y2="0" style="stroke:#004165;stroke-width:5" />
            Sorry, your browser does not support inline SVG.
        </svg>
        <div class="event3 futureGray ">
            <div
                class="{{ $timeline_count == 4 ? 'event1Bubble' : ($timeline_count == 3 && is_null($fully_shipped) ? 'event1Bubble' : 'event2Bubble') }}">
                <div class="eventTime">
                    <div class="DayDigit">{{ \Carbon\Carbon::parse($partially_shipped->created_at)->format('d') }}</div>
                    <div class="Day">
                        {{ \Carbon\Carbon::parse($partially_shipped->created_at)->format('l') }}
                        <div class="MonthYear">{{ \Carbon\Carbon::parse($partially_shipped->created_at)->format('F Y') }}</div>
                    </div>
                </div>
                <div class="eventTitle">Partially shipped</div>
            </div>
            <div class="{{ $timeline_count == 4 ? 'eventAuthor' : ($timeline_count == 3 && is_null($fully_shipped) ? 'eventAuthor' : 'event2Author') }}">by {{ $partially_shipped->createdBy->name ?? 'N/A' }}</div>
            <svg height="20" width="20">
                <circle cx="10" cy="11" r="5" fill="#004165" />
            </svg>
            <div class="{{ $timeline_count == 4 ? 'time' : ($timeline_count == 3 && is_null($fully_shipped) ? 'time' : 'time2') }}">{{ \Carbon\Carbon::parse($partially_shipped->created_at)->format('H:i A') }}</div>

        </div>
    @endif

    @if (!is_null($fully_shipped))
        <svg height="5" width="200">
            <line x1="0" y1="0" x2="300" y2="0" style="stroke:#004165;stroke-width:5" />
            Sorry, your browser does not support inline SVG.
        </svg>
        <div class="event4 futureGray ">
            <div
                class="{{ $timeline_count == 4 ? 'event2Bubble' : ($timeline_count == 3 ? 'event1Bubble' : 'event2Bubble') }}">
                <div class="eventTime">
                    <div class="DayDigit">{{ \Carbon\Carbon::parse($fully_shipped->created_at)->format('d') }}</div>
                    <div class="Day">
                        {{ \Carbon\Carbon::parse($fully_shipped->created_at)->format('l') }}
                        <div class="MonthYear">{{ \Carbon\Carbon::parse($fully_shipped->created_at)->format('F Y') }}</div>
                    </div>
                </div>
                <div class="eventTitle">Fully shipped</div>
            </div>
            <div class="{{ $timeline_count == 4 ? 'event2Author' : ($timeline_count == 3 ? 'eventAuthor' : 'event2Author') }}">by {{ $fully_shipped->createdBy->name ?? 'N/A' }}</div>
            <svg height="20" width="20">
                <circle cx="10" cy="11" r="5" fill="#004165" />
            </svg>
            <div class="{{ $timeline_count == 4 ? 'time2' : ($timeline_count == 3 ? 'time' : 'time2') }}">{{ \Carbon\Carbon::parse($fully_shipped->created_at)->format('H:i A') }}</div>

        </div>
    @endif

    @if (!is_null($paid))
        <svg height="5" width="200">
            <line x1="0" y1="0" x2="200" y2="0" style="stroke:#004165;stroke-width:5" />
        </svg>

        <div class="event3 futureGray ">
            <div
                class="{{ $timeline_count == 4 ? 'event1Bubble' : ($timeline_count == 3 ? 'event2Bubble' : 'event1Bubble') }}">
                <div class="eventTime">
                    <div class="DayDigit">{{ \Carbon\Carbon::parse($paid->created_at)->format('d') }}</div>
                    <div class="Day">
                        {{ \Carbon\Carbon::parse($paid->created_at)->format('l') }}
                        <div class="MonthYear">{{ \Carbon\Carbon::parse($paid->created_at)->format('F Y') }}</div>
                    </div>
                </div>
                <div class="eventTitle">Payment Made</div>
            </div>
            <div class="{{ $timeline_count == 4 ? 'eventAuthor' : ($timeline_count == 3 ? 'event2Author' : 'eventAuthor') }}">by {{ $paid->createdBy->name ?? 'N/A' }}</div>
            <svg height="20" width="20">
                <circle cx="10" cy="11" r="5" fill="#004165" />
            </svg>
            <div class="{{ $timeline_count == 4 ? 'time' : ($timeline_count == 3 ? 'time2' : 'time') }}">{{ \Carbon\Carbon::parse($paid->created_at)->format('H:i A') }}</div>

        </div>
    @endif
</div>
