<div class="row">
    @if (!is_null($order->lporequest))
        <div class="col-12 col-md-12 mb-3">
            <div class="card">
                <div class="card-body">
                    {!! $order->lporequest !!}
                </div>
            </div>
        </div>
    @endif

    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Order
                    Details</span>

                <div class="row mb-2">
                    <div class="col-4 pl-4">Order Number:</div>
                    <div class="col-8"><b># {{ $order->face_id }}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Delivery Date:</div>
                    <div class="col-8"><b>{{ stamp_date($order->delivery_date) }}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Order Status:</div>
                    <div class="col-8"><span
                            class="badge bg-soft-{{ App\Models\Order::$order_stages_colors[$order->stage] }} text-{{ App\Models\Order::$order_stages_colors[$order->stage] }}">{{ App\Models\Order::$order_stages[$order->stage] }}</span>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Total:</div>
                    <div class="col-8"><b>{{ currency($order->total) }}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Order Creation:</div>
                    <div class="col-8"><b> By: {{ $user->name ?? 'N/A' }}

                            <br>
                            {{ stamp_date_time($order->created_at) }}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Update Status to:</div>
                    <div class="col-8">
                        <form action="{{ route('orders.change.status') }}" method="post">
                            {{ csrf_field() }}

                            <input type="hidden" name="order_id" value="{{ $order->id }}">

                            <div class="row ">
                                <div class="col-md-9">
                                    <select class="form-control" name="status_update" title="Update Order status">
                                        <option value="">Select Status
                                        </option>
                                        <option value="1" {{ $order->stage == 1 ? 'selected="selected"' : '' }}>
                                            Pending
                                        </option>
                                        <option value="2"{{ $order->stage == 2 ? 'selected="selected"' : '' }}>
                                            Approved
                                        </option>
                                        <option value="3"{{ $order->stage == 3 ? 'selected="selected"' : '' }}>
                                            Processing
                                        </option>
                                        <option value="4"{{ $order->stage == 4 ? 'selected="selected"' : '' }}>
                                            Processed
                                        </option>
                                        <option value="5"{{ $order->stage == 5 ? 'selected="selected"' : '' }}>

                                        </option>
                                        <option value="6"{{ $order->stage == 6 ? 'selected="selected"' : '' }}>
                                            Fully shipped
                                        </option>
                                        <option value="7"{{ $order->stage == 7 ? 'selected="selected"' : '' }}>
                                            Paid
                                        </option>
                                    </select>
                                </div>




                                <div class="col-md-3 pl-0">
                                    @if (auth()->user()->hasPermissionTo('orders-edit'))
                                        <button class="btn btn-primary mt-1"><i class="fa fa-send"></i>
                                        </button>
                                    @else
                                        <button class="btntable disonabled btn btn-dark mt-1">
                                            <i class="text-white fa fa-send"></i>
                                        </button>
                                    @endif
                                </div>

                            </div>


                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-danger text-danger tw-text-sm mb-3">Customer
                    Information</span>

                <div class="row mb-2">
                    <div class="col-4 pl-4">Phone Number:</div>
                    <div class="col-8"><b>{{ $order->clientable->telephone }}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4 pl-4">Email Address:</div>
                    <div class="col-8"><b>{{ $order->clientable->email }}</b></div>
                </div>

                @if ($order->clientable_type == 'App\Models\Wholesale\Wholesaler')
                    <div class="row mb-2">
                        <div class="col-4 pl-4">Registration Number:</div>
                        <div class="col-8">
                            <b>{{ $order->clientable->registration_number }}</b>
                        </div>
                    </div>
                @endif


            </div>

        </div>
    </div>

    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-body">

                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Order
                    Items</span>

                <table class="table table-hover center-text">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col" class="tw-w-40"> Item</th>
                            <th scope="col"> Color</th>
                            <th scope="col"> Status</th>
                            <th scope="col"> Quantity</th>
                            <th scope="col"> OriginalPrice</th>
                            <th scope="col"> Price</th>
                            <th scope="col"> Dsc(%)</th>
                            <th scope="col"> VAT</th>
                            <th scope="col"> Comment</th>
                            <th scope="col"> Total</th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order->items as $index => $item)
                            <tr>
                                <th scope="row">{{ $index + 1 }}</th>
                                <td>{{ optional($item->inventory)->name }}</td>
                                <td>{{ optional($item->inventory)->color }}</td>

                                <td>
                                    <span
                                        class="badge bg-soft-{{ App\Models\OrderProduct::$order_stages_colors[$item->status] }} text-{{ App\Models\OrderProduct::$order_stages_colors[$item->status] }}">{{ App\Models\OrderProduct::$order_stages[$item->status] }}</span>
                                </td>
                                <td> {{ number_format(optional($item)->quantity) }}</td>

                                <td> {{ currency(optional($item)->original_price) }}</td>
                                <td> {{ currency($item->price) }}</td>
                                <td> {{ $item->discount_percent }}</td>
                                <td> {{ $item->vat_percent }}</td>
                                <td @click="openComment({{ $item }})">{{ $item->note ? $item->note : 'N/A' }}
                                </td>
                                <td> {{ currency($item->price * $item->quantity) }}</td>


                            </tr>
                        @endforeach
                    </tbody>
                </table>


            </div>

        </div>
    </div>

    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-body">
                <span class="badge bg-soft-info text-info tw-text-sm mb-3">Delivery
                    Location</span>
                @if (is_null($delivery_location) && $order->clientable_type !== 'App\Models\Retailer')
                    <div class="alert alert-info">
                        Delivery Location Not Provided
                    </div>
                @elseif($order->clientable_type == 'App\Models\Retailer')
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-2">
                                        <div class="col-4 pl-4">Address:</div>
                                        <div class="col-8">
                                            <b>{{ $order->clientable->address }}</b>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-4 pl-4">Latitude:</div>
                                        <div class="col-8">
                                            <b>{{ $order->clientable->latitude }}</b>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-4 pl-4">Longitude:</div>
                                        <div class="col-8">
                                            <b>{{ $order->clientable->longitude }}</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7">
                            <gmap-map
                                :center="{ lat: {{ $order->clientable->latitude }}, lng: {{ $order->clientable->longitude }} }"
                                :zoom="14" map-type-id="terrain"
                                style="width: 100%; height: 300px;margin-top:10px">
                                <gmap-marker
                                    :position="{
                                        lat: {{ $order->clientable->latitude }},
                                        lng: {{ $order->clientable->longitude }}
                                    }" />
                                </gmap-marker>
                            </gmap-map>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-2">
                                        <div class="col-4 pl-4">Address:</div>
                                        <div class="col-8">
                                            <b>{{ $delivery_location->locality }}</b>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-4 pl-4">Latitude:</div>
                                        <div class="col-8">
                                            <b>{{ $delivery_location->latitude }}</b>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-4 pl-4">Longitude:</div>
                                        <div class="col-8">
                                            <b>{{ $delivery_location->longitude }}</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7">
                            <gmap-map
                                :center="{ lat: {{ $delivery_location->latitude }}, lng: {{ $delivery_location->longitude }} }"
                                :zoom="14" map-type-id="terrain"
                                style="width: 100%; height: 300px;margin-top:10px">
                                <gmap-marker
                                    :position="{
                                        lat: {{ $delivery_location->latitude }},
                                        lng: {{ $delivery_location->longitude }}
                                    }" />
                                </gmap-marker>
                            </gmap-map>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
