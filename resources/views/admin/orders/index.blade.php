@extends('admin.orders.tabslayout')

@section('order_page_title')
    All Orders
@endsection

@section('order_page_content')
    <div class="spacious-container">
        <div class="card">

            <div class="card-body">
                @include('admin.orders.searchform')

                <table class="table table-hover center-text">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">OrderNo.</th>
                            <th scope="col">Type</th>
                            <th scope="col">Name</th>
                            <th scope="col">Total</th>
                            <th scope="col">Status</th>
                            <th scope="col">CreatedAt</th>
                            <th scope="col">CreatedBy</th>
                            <th scope="col">Actions</th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $index => $order)
                            <tr>
                                <th scope="row">{{ ++$i }}</th>
                                <td>{{ $order->order_tag }}</td>
                                <td>
                                    @if ($order->clientable_type == 'App\Models\Retailer')
                                        <span class="badge bg-soft-success">Customer</span>
                                    @else
                                        <span class="badge bg-soft-primary">Wholesaler</span>
                                    @endif

                                </td>
                                <td>
                                    {{ $order->clientable->business_name }}
                                </td>
                                <td>{{ amount($order->total) }}</td>
                                <td>
                                    <span
                                        class="badge bg-soft-{{ App\Models\Order::$order_stages_colors[$order->stage] }} text-{{ App\Models\Order::$order_stages_colors[$order->stage] }}">{{ App\Models\Order::$order_stages[$order->stage] }}</span>


                                </td>

                                <td>{{ stamp_date_time($order->created_at) }}<br>
                                    <small>{{ ago($order->created_at) }}</small>
                                </td>

                                <td>
                                    {{ $order->createdBy->name ?? 'N/A' }}
                                </td>

                                <td class="tw-flex">
                                    <a href="{{ route('orders.view', ['order' => $order->id]) }}"
                                        class="btn btn-info btn-sm mr-2"><i class="fa fa-eye"></i></a>

                                    <a href="{{ route('orders.invoice_download', ['order' => $order->id]) }}"
                                        class="btn btn-info btn-sm mr-2"><i class="fa fa-download"
                                            aria-hidden="true"></i></a>

                                    <button class="btn btn-info btn-sm mr-2" data-toggle="modal" data-target="#mailOrder"
                                        data-qid="{{ $order->id }}" data-mail="{{ $order->clientable->email }}"><i
                                            class="fa fa-envelope-open"></i></button>
                                    @if (auth()->user()->hasPermissionTo('orders-delete'))
                                        <form action="{{ route('orders.cancel', ['order' => $order->id]) }}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_METHOD" value="DELETE">
                                            <button class="btn btn-danger btn-sm"
                                                onclick="return confirm('Are you sure you want to delete order from your system ?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        @else
                                            <button class="btntable disonabled btn btn-sm btn-dark mr-2">
                                                <i class="text-white fa fa-trash"></i>
                                            </button>
                                    @endif
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div style="padding-left:20px;">
        {!! $orders->appends(Request::except('page'))->render() !!}
    </div>
@endsection
@section('modalsection')
    <div class="modal fade" id="mailOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Order Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('order.mail') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="order_id" id="order_id">

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Customer Email :</label>
                            <div class="col-md-8">
                                <input type="email" name="customer_email" id="customer_email" class="form-control"
                                    required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l13">Select Template:</label>
                            <div class="col-md-8">
                                <select class="form-control" id="l13" name="template_id">
                                    <option value="">-- Select One --</option>
                                    @foreach ($templates as $template)
                                        <option value="{{ $template->id }}">{{ $template->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
<!-- Modal -->
