@extends('layouts.base')

@section('title')
    Orders
@endsection

@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="card mt-3">
            <div class="row m-0">
                <h5 class="box-title ml-3 mt-3 col-12 col-md-6"><b>@yield('order_page_title')</b></h5>

                <div class="col-12 col-md-6 mr-3 mt-3">
                    @if (auth()->user()->hasPermissionTo('orders-create'))
                        <a href="{{ route('orders.create') }}" class="btn btn-primary btn-sm pull-right"> <i
                                class="fa fa-plus mr-2"></i> New Order</a>
                    @else
                        <button class="btntable disonabled btn btn-dark btn-sm pull-right mr-2">
                            <i class="text-white fa fa-plus mr-2"></i>New Order
                        </button>
                    @endif
                </div>
            </div>
            <div class="card-body override_cbody pt-0 spacious-container">

                <div class="tabs tabs-vertical">
                    <ul class="nav nav-tabs flex-column">
                        <li class="nav-item"><a class="nav-link {{ request()->routeIs('orders.index') ? 'active' : '' }}"
                                href="{{ route('orders.index') }}">All Orders</a>
                        </li>
                        <li class="nav-item"><a class="nav-link {{ request()->routeIs('orders.pending') ? 'active' : '' }}"
                                href="{{ route('orders.pending') }}">Pending Orders</a>
                        </li>
                        <li class="nav-item"><a class="nav-link {{ request()->routeIs('orders.approved') ? 'active' : '' }}"
                                href="{{ route('orders.approved') }}">Approved</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('orders.processing') ? 'active' : '' }}"
                                href="{{ route('orders.processing') }}">Processing</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('orders.processed') ? 'active' : '' }}"
                                href="{{ route('orders.processed') }}">Processed</a>
                        </li>
                        <li class="nav-item"><a class="nav-link {{ request()->routeIs('orders.paid') ? 'active' : '' }}"
                                href="{{ route('orders.paid') }}">Partially shipped</a>
                        </li>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('orders.fully-shipped') ? 'active' : '' }}"
                                href="{{ route('orders.fully-shipped') }}">Fully Shipped</a>
                        <li class="nav-item"><a
                                class="nav-link {{ request()->routeIs('orders.partially-shipped') ? 'active' : '' }}"
                                href="{{ route('orders.partially-shipped') }}">Paid</a>
                        </li>

                    </ul>
                    <!-- /.nav-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane active">

                            @yield('order_page_content')
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </div>
    </div>

    @yield('modalsection')
@endsection


@section('extrascripts')
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $("#retailers").hide();


            $('input[type=radio][name=type]').change(function() {
                if (this.value == 'wholesaler') {
                    $("#wholesaler").show();
                    $("#retailers").hide();
                } else if (this.value == 'retailer') {
                    $("#wholesaler").hide();
                    $("#retailers").show();
                }
            });

        });
    </script>
    <script>
        $('#mailOrder').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var order_id = button.data('qid')
            var customer_email = button.data('mail')

            var modal = $(this)
            modal.find('.modal-body #order_id').val(order_id)
            modal.find('.modal-body #customer_email').val(customer_email)
        })
    </script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('.test').select2({
                placeholder: 'Created By...',
                allowClear: true,
                ajax: {
                    url: '/admin/users/ajax/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });

            $('.wholesaler_select').select2({
                placeholder: 'Select Wholesaler...',
                allowClear: true,
                ajax: {
                    url: '/wholesalers/ajax/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.business_name,
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });

            $('.retailer_select').select2({
                placeholder: 'Select Client...',
                allowClear: true,
                ajax: {
                    url: '/admin/retailers/ajax/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.business_name,
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });
        });
    </script>
@endsection
