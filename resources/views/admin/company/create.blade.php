@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Create Company
@endsection

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8"></div>
            <div class="col-4">
                <a href="{{ route('companies.index') }}" class="btn btn-outline-info">
                    <span>Go Back</span>
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">

        {!! Form::open(['route' => 'companies.store', 'method' => 'POST', 'files' => true,'class'=>'prevent-multiple-submits']) !!}

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Company Logo:</strong>
                    <input type="file" id="company_logo" class="form-control" name="company_logo" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Phone:</strong>
                    {!! Form::text('phone', null, ['placeholder' => 'Phone', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Footer Text:</strong>
                    {!! Form::text('footer_text', null, ['placeholder' => 'Footer Text', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>VAT:</strong>
                    {!! Form::text('vat', null, ['placeholder' => 'VAT', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Include VAT:</strong>
                    <div class="radiobox">
                        <label>
                            <input type="radio" name="vat_included" value="1">
                            <span class="label-text">Yes</span>
                        </label>
                    </div><!-- /.radiobox -->

                    <div class="radiobox">
                        <label>
                            <input type="radio" name="vat_included" value="0">
                            <span class="label-text">No</span>
                        </label>
                    </div><!-- /.radiobox -->

                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary multiple-submits">Submit</button>
            </div>
        </div>
        {!! Form::close() !!}
    @endsection
