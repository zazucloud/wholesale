@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Company Information
@endsection



<div class="card">
    {{-- <div class="card-header">
        <div class="row">
            <div class="col-8"></div>
            <div class="col-4">
                 <a href="{{ route('companies.index') }}" class="btn btn-outline-info float-right">
                    <span>Go Back</span>
                </a>
            </div>
        </div>
    </div> --}}
    <div class="card-body">
        {!! Form::model($company, ['method' => 'PATCH', 'route' => ['companies.update', $company->id ?? 0], 'files' => true,'class'=>'prevent-multiple-submits']) !!}

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Company Logo:</strong><br>
                    @if (is_null($company) || is_null($company->getFirstMedia('companylogos')))
                        <input type="file" id="company_logo" class="form-control mt-1" name="company_logo" />
                    @else
                        <img class="img-responsive tw-rounded mb-2"
                            src="{{ $company->getFirstMedia('companylogos')->getUrl() }}" height="250" alt="">
                        <input type="file" id="company_logo" class="form-control" name="company_logo" />
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Phone:</strong>
                    {!! Form::text('phone', null, ['placeholder' => 'Phone', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Footer Text:</strong>
                    {!! Form::text('footer_text', null, ['placeholder' => 'Footer Text', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>VAT Number:</strong>
                    {!! Form::text('vat_number', null, ['placeholder' => 'VAT NUMBER', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>VAT Percentage (%):</strong>
                    {!! Form::number('vat', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>include VAT:</strong>
                    <div class="radiobox">
                        <label>
                            <input type="radio" name="vat_included" value="1"
                                {{ !is_null($company) && $company->vat_included == '1' ? 'checked' : '' }}>
                            <span class="label-text">Yes</span>
                        </label>
                    </div><!-- /.radiobox -->

                    <div class="radiobox">
                        <label>
                            <input type="radio" name="vat_included" value="0"
                                {{ !is_null($company) && $company->vat_included == '0' ? 'checked' : '' }}>
                            <span class="label-text">No</span>
                        </label>
                    </div><!-- /.radiobox -->

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary multiple-submits">Submit</button>
                </div>

            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection
