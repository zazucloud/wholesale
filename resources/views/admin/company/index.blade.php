@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Company Management
@endsection

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8"></div>
            <div class="col-4">
                <a href="{{ route('companies.create') }}" class="btn btn-outline-success">
                    <span>Create Company</span>
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover center-text">
            <thead>
                <th>#</th>
                <th>Logo</th>
                <th>Name</th>
                <th>Email</th>
                <th>VAT</th>
                <th>VAT Included</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($company as $key => $company)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>
                            @foreach ($company->getMedia('companylogos') as $item)
                                <img class="img-responsive tw-rounded-full tw-h-14 tw-w-14"
                                    src="{{ $item->getUrl() }}" alt="{{ $company->name }}">
                            @endforeach
                        </td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->email }}</td>
                        <td>{{ $company->vat }}</td>
                        <td>
                            @if ($company->vat_included !== 1)
                                <span class="badge badge-danger m-2"> No</span>
                            @else
                                <span class="badge badge-success text m-2"> Yes</span>
                            @endif
                        </td>
                        <td class="row">
                            <a href="{{ route('companies.edit', $company->id) }}" data-toggle="tooltip"
                                data-placement="top" title="Edit">
                                <i class="list-icon linearicons-pencil text-info col-3"></i>
                            </a>
                            <form method="post" action="{{ route('companies.destroy', $company->id) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="tw-border-none tw-bg-transparent ml-2"
                                    onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                    data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="list-icon linearicons-trash2 text-danger"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
