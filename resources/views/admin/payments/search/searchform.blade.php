<div class="row">
    <div class="col-md-10">
        {!! Form::open(['route' => 'payments.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-sm-11 row">
            <div class="col-sm-5" style="padding-left:8px;padding-right:8px;">
                <label>Search</label>

                {!! Form::text('search', null, [
                    'class' => 'form-control form-control-inputs',
                    'placeholder' => 'Search By Receipt No. or Reference',
                ]) !!}
            </div>
            <div class="col-sm-3" style="padding-left:8px;padding-right:8px;">
                <label>Payment Channel</label>

                {!! Form::select(
                    'channel',
                    [
                        '5' => 'Airtel',
                        '1' => 'Bank Transfer',
                        '2' => 'Cheque',
                        '3' => 'Ipay',
                        '4' => 'Mpesa',
                        '6' => 'T-Kash',
                    ],
                    null,
                    ['class' => 'custom-select', 'id' => 'account_status', 'placeholder' => 'Payment Channel'],
                ) !!}
            </div>
            <div class="col-sm-4" style="padding-left:8px;padding-right:8px;">
                <label>Payment Channel</label>

                {!! Form::select(
                    'wholesaler_id',
                    $clients,
                    [],
                    ['placeholder' => 'Select Client', 'class' => 'form-control', 'id' => 'select_client', 'data-toggle' => 'select2'],
                ) !!}
            </div>
        </div>
        <div class="col-sm-1" style="padding-left:8px;padding-right:8px;">
            <div class="float-right mt-4 pt-2">
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info float-right btn-sm']) }}

            </div>
        </div>
        {!! Form::close() !!}

    </div>
    <div class="col-md-2 float-right mt-4 pt-2 d-flex">

        <div>
            <a href="{{ route('payments.create') }}" class="btn btn-success btn-sm"><i class="fa fa-plus mr-2"></i> New
                Payment</a>
        </div>

        <div class="btn-group dropstart ml-3">
            <a class="hambuger text-info dropdown-toggle btn-sm mr-1" type="button" id="dropdownMenuButton701"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0);">
                <i class="fa fa-ellipsis-v mr-2"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right py-1" aria-labelledby="dropdownMenuButton701">
                <a href="{{ route('payments.export.pdf') }}" class="dropdown-item bg-soft-warning text-danger">
                    <b>Export PDF</b></a>
                <a href="{{ route('payments.export.excel') }}" class="dropdown-item bg-soft-info text-info">
                    <b> Export CSV</b></a>
            </div>
        </div>



    </div>


</div>
