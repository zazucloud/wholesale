@extends('layouts.base')

@section('title')
    Create Payment Record
@endsection


@section('content')
    @include('partials.page-title')
    <div class="container mt-3">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <a href="{{ route('payments.index') }}" class="btn btn-info btn-sm float-right"> <i class="fa fa-arrow-left mr-2"></i> Back</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('payments.store') }}" class="prevent-multiple-submits" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" id="reference_input" name="reference_input">


                    <div class="row">
                        @if (count($wholesalers) > 0)
                            <div class="col-lg-6 col-md-6 form-group">
                                <label for="wholesaler">Client</label>
                                <select name="wholesaler" id="wholesaler" class="select2 form-control">
                                    <option value="0">Select Client</option>
                                    @foreach ($wholesalers as $wholesaler)
                                        <option value="{{ $wholesaler->id }}">
                                            {{ $wholesaler->business_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @else
                            <div class="alert alert-danger background-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled"></i>
                                </button>
                                <strong>No Customers Found!</strong> Unable to proceed with payment record
                                creation
                            </div>
                        @endif

                        <div class="form-group col-lg-6 col-md-6">
                            {!! Form::label('channel', 'Payment Channel') !!}
                            {!! Form::select('channel', $payment_channels, 2, ['id' => 'channel', 'class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="reference_container col-lg-6 col-md-6" id="reference_container_2"
                            data-reference="cheque_no">
                            <div class="form-group">
                                <label for="cheque_no">Cheque No</label>
                                <input id="cheque_no" name="cheque_no" type="text" class="form-control"
                                    placeholder="Cheque Number">
                            </div>
                        </div>

                        <div class="reference_container col-lg-6 col-md-6" id="reference_container_1"
                            data-reference="transaction_no">
                            <div class="form-group">
                                <label for="transaction_no">Transaction Number</label>
                                <input id="transaction_no" name="transaction_no" type="text" class="form-control"
                                    placeholder="Transaction Number">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 reference_container" id="reference_container_x"
                            data-reference="reference_no">
                            <div class="form-group">
                                <label for="reference_no">Reference No</label>
                                <input id="reference_no" name="reference_no" type="text" class="form-control"
                                    placeholder="Reference No">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input value="{{ old('amount') }}" required id="amount" name="amount" type="number"
                                    class="form-control" placeholder="Amount">
                                <small id="amount_in_words"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-5">
                            <div class="form-group" id="attachment_container">
                                <label for="attachment"> Attachment</label>
                                <input type="file" name="attachment" class="form-control" id="attachment">
                                <small id="attachment_description"></small>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="form-group">
                                <label for="comment">Comment
                                    <small>(Optional)</small>
                                </label>
                                <textarea class="form-control" id="comment" name="comment" cols="3"
                                    rows="3">{{ old('comment') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success btn-sm mr-2 multiple-submits"> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script>
        $("#menu-payments").addClass('active');

        $("#amount").on('keyup', function() {
            let amount = $(this).val();
            amount_to_words(amount);
        });

        $("#channel").on('change', function() {
            update_channel_reference();
        });

        update_channel_reference();

        function update_channel_reference() {

            let channel = $("#channel").val();

            $(".reference_container").hide();
            let target_element = $("#" + 'reference_container_' + channel);
            let reference_input = $("#reference_input");

            if (target_element.length==0) {
                target_element = $("#" + 'reference_container_x');
            }

            reference_input.val(target_element.data('reference'));

            console.log(target_element.data('reference'));

            if (target_element.length) {
                target_element.show();
            } else {
                $("#reference_container_x").show();
            }

            let attachment_description = $("#attachment_description");
            attachment_description.html('');

            let attachment_container = $("#attachment_container");
            attachment_container.hide();
            $("#attachment").attr('required', false);

            if (channel == 2) {
                attachment_description.html('Attach a scanned copy of the cheque');
                attachment_container.show();
                $("#attachment").attr('required', 'required');
            }

            if (channel == 1) {
                attachment_container.show();
                $("#attachment").attr('required', 'required');
                attachment_description.html('Attach a scanned copy of the transaction receipt of the bank transfer');
            }

        }

        function amount_to_words(number) {
            $.post("{{ route('payments.amount_to_words') }}", {
                    amount: number,
                    _token: $("input[name='_token']").val()
                },
                function(data) {
                    $('#amount_in_words').text(data);
                });
        }

    </script>
@endsection
