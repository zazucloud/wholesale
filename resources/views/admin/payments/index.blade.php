@extends('layouts.base')

@section('title')
    Payments
@endsection


@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @include('partials.page-title')
    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header">
                @include('admin.payments.search.searchform')
            </div>
            <div class="card-body">
                <div class="spacious-container">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ReceiptNo</th>
                                <th>Client</th>
                                <th>Payment Channel</th>
                                <th>Amount</th>
                                <th>Reference</th>
                                <th>Created By</th>
                                <th>Timestamp</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $key => $payment)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $payment->receipt_no }}</td>
                                    <td>
                                        <a
                                            href="{{ route('admin.wholesalers.show', $payment->wholesaler->slug) }}">{{ $payment->wholesaler->business_name }}</a>
                                    </td>
                                    <td>{{ $payment->channel_name }}</td>
                                    <td>{{ amount($payment->amount) }}</td>
                                    <td>{{ $payment->reference }}</td>
                                    <th>{{ $payment->creator->name }}</th>
                                    <td>{{ stamp_date_time($payment->created_at) }}</td>
                                    <td>
                                        <a href="{{ route('payments.show', $payment->receipt_no) }}"
                                            class="btn btn-info btn-sm">
                                            <i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $payments->links() }}
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script>
        $("#menu-payments").addClass('active');
    </script>

    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="select2"]').select2();
        });
    </script>
@endsection
