<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">

    <!-- App css -->
    {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> --}}

    @include('layouts.pdf.css')
</head>

<body>
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <!-- Logo & title -->
                    <div class="clearfix" style="margin-left: 0.75rem;margin-right: 0.75rem;">
                        <div style="width:10%;" class="float-left">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKkAAABxCAYAAACuuINtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpEOUFGNUFGNkI5MTExMUU1OEUzOUNCM0NFMjQ5Qzg5QSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpEOUFGNUFGN0I5MTExMUU1OEUzOUNCM0NFMjQ5Qzg5QSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjQ2QTQxMjU5QjkwODExRTU4RTM5Q0IzQ0UyNDlDODlBIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjQ2QTQxMjVBQjkwODExRTU4RTM5Q0IzQ0UyNDlDODlBIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Cko4TgAAOjhJREFUeNrsPQnYVVW1a9/hn/+feZBJZRBREEElNU1FTE0QCTUnzEqfr/RVlkOWVq9nPl+m2aCmllY4JyQqJWpOgKgIKiICyg/I+AMy/uOd9lvr7HXu3eecfc49dwC0OLC/+59xT2uvea8l5BJYCAAjsMTAfUj+Ffy3gD176PV/mg/VzhZs50P4e3nR34mon+/cMQWWrs1ATaXWcZEbBon1CWH/LfHv3HOOe/YFftN5XZ1L1fbuWEbh+XD87YvX+uGdAfj3fvhUPf5dgXXUcgvpjQ58phWfb8a/N2FZjRdX4sdW40ffxyfexmvb7Tr1NkpXe6SUgUNC7xFg9sSPxozAIHz+zjdhooh7xhaW8O6ePFS7aBK7FQ/oFfidBEAcsUWUurtbO1yD5fNYjsCaRuIvAqXohABTj0BRhXBRjbBTjdcr7A5qsERwU4XvVUkpuuLfCMhwELa0Fe+04Td2KQAVjfj7FpZ5WBaW0lgCzkxJ2EoWCcylAHsp7xWDncO9k9GeLGImErBr50B4/MWjoAmnuDJWdgDtiuUYLKOxlQchcA1BgDrQQlI+GCxoOOk+YUEE1Dj+iUsLGlzvfgHLWCwf4fmH+LsYn3+dAbcIIBV7FOMUB3CfdpKvWlsckAqchmgKkqlKePTVKGLSNFRXREDKktuEOBkOxjIYy7FYTiGyHkRa3SxDQQPgbfAALgSsrVj+ieV5LAuwLMPySVgg3bOAFxZbid3UHlHiN3cLn5yCjtYKWLGuH3SqiUAiJUsF0ArGnEdj+RoC3BkEsDZvmOMFhYO3tcl67prqsASh86/ZgRAuSLaxq84f27w0Xq/B+xPwzwl4fTH+TsXyD+JnsewMwarvJoGnENwiyjTxsmS8VhjZL0cdCFLzFo2DG//cE9LINEQjJQ8EAee9CBl/xcaO1wE0hymFQ3Cx79nAaMOaui59yH3uWZm7wdd4ArLfyj2PjxyK5Wb8+ylaRDqrUCgm/QDLa1jWYKkqaOJKwYalygvlYA/829CGZSiW47H0Lw9GxSmIpaC2KgntyQyS+WgpHzsEJ//7OPnHqfaJWA7Z6VhOk7QJqBhydUB1knBhWJFZFOnEqC4SYFoI9AotHDw9AK9eT2wIPnMX/v69UCAlZvdBLO+XhS341zhSLBAMygIplIhV4yl4cfap8LfZvaC+Kl1suzphuQjLOQqLykrTStXVQCjwOMlz9m/hAFL9HR0Tm/prY2gT1tbfy2Jr1cJe+HMGPkNagjH47t342xQWSJsZi27eB5uOYx2W9rIJkkijlq3uCq8v64D+3ePF8KKHYbkAyxQsfT7D4zoay3Duwx+wvBkGSKOBZN5+olBBIp+BIKyetRi2wK+t7uvpQFaiysPLFytM4fMfrx4K25uj0LlWFAqgMUXexU/xd5KzKcIjcTuFGjX4jvqkzmfmpCfhpeAOBT1hZDc599RrixxZfjdnSZBaW/ESCXyXgaW3hVuxvEFagVixg5zJVEIy0RXrYTWrCJLB0gzRaXye7QYixSTHNd/4vdy1tGNFqOclP5f72/ls1PNe7ts2zyVddeauC5GGeGxTYSqYYjUG2LTfPjYGFq5IQ6faglROpJc8Csvv8Z0RTmk8C3H8p/BRMYms9O4l6V4LlhdQdUHLawVyW8TsuhxD5jPI+Pzp+NMb3/0JaQBihQ2yYvIBOZ41K0bAjfcPs6wjpQqjfkJzuYR9YVCPmOpIZUiJDvCrK16Emq5I1RME7zFmRct4kF4UFymBWjxmBoI8x8n4/F34Xn/nPOewW47gSANvqv/q/KIw8J7SwVd6AdeLRZ2LRhgWjvTMRRaQc/OEpF/8NwFrYQIRAuhjs86GBcurIJ0RsGlHGiIRJ5nSB9y7mkySYs4o7dHsuGxxJrITTNbUQ2GttPRaFJH4TX8ZCyhow9jDd8LpJ8xQwFrIast3RFOwa9cBcOfUY6FxIzht9PmPMy31DcCBbkxoIvMmhCU1Mu54NasrdY6MVx8KDizq1rU6BS83NnUKV3Y7s30BYQ8jLmE5Cp++uDByX40i/7oqeHFREro1xKCuMqLUkXmBVAcyg4OAL5BCAUDq81wBQGofb63IwNZdKejXvRZOL7cmmcl8XfXH8PoHx0FbIgO1VaH5USKDN2I5tBh1ggTz+Nk8onmMg0fPe9vr0BKOeZd+atBpsdADi99Y9O6xsLNFWABaWyn0zxKj20sThz5LR4TpOUnt5NEDdQg0qUwUqiszxelo8wibO7b3g9ffGw1VFQIpUmjrEpkziUc7Ul+VDiAS3gl3kG6TGxWYrmWBr4V1wykuQhkGLJ64Cs9qhKHXbk8sjyUKcsp9N2XVnluM936Jp3+OhQJsQYJSFG5/bBCs25qGhhoHk09vX4plskFq+SwcMZ6M+7DcE5qEF8s0Y23vfzQcbn6kFrrUSeTpRZhF1BsH/Fas8nNmISjMGlGk1P7Lww8iU4MA0o5nSR6PtVhW4LUNeG0nvtIqVFsIODvhtd543h/v74/v1+P1Svwl/WyFCWId5D4LvG7h1WIBCE5X4d83Y3mEno75u7k4h0lYmEWZ7DIZx1O1WBm5fQ2z67f5EF3q8+dLzP6G1nUP2ZCe/ueaIRxMvNvPMtcGXsX8OWm9aH3paPzzntBqpWLt+cT3RjM4lsKjZfCRdLtiu6/F3yOMfLexjyJYQPWySR14bTa+OQv/fhv7sZj15LZnl3T10i4Rcu3D3yF48wR84iT8zgnYhiq3OipnhtUFLjd7YZ1vwp9r8e8Z9s1Y2IFlIqBYSNdCxEbQKop6Bkk4dXDeGRUePlJzATMMpvBIk8650hwkNP2cfc3BcwmdJFl/VJgsKKGYl9AYFZn6WBt0bWjBb3cPq2o6Ads9Bceksy6JZwFV2hK8bY836y0NAjb9vUxhK3gZz9aD8koif9BkAbSBsC45OJOFchpYDtTiC/j9KeREbRK67DkwaAfW4tlVoBxPkmGU+Z5JkA6HAY8i1DUwwr1CjDo5fZW5pdKswtlx3yQsgb8kqzFjDl2d9DycdkJkEax1vlfibfDmghNh2iv9oSIWit8dje2/Hhva1Sy0mCmGr1I9NxaLsExnq858LFtKZJnSDOBUPmKBhxydicKOt/lohx5XQ1B87X1WOU0H28e5ICAtVhQK+Z6Jj8+nTDd12OcB0ymEv1le7ved5b3hxUUJNoEG1kuo9lywvOcLGPIAco9HE95/CX/JO2r6buzpDmzCC/j7Ata3ANvzZVC+rP185vhdbPYv8PyvfkJDKJ5KyohF7oUwAZBwOQ949aEm1ZBOtmxy7jbfZfnb7KoTBoWxP/p3SJmgK6+lxwrixv4Wz1hmy1NtVRrqquOqVhE44KRumqBTJicV8fKnJmcQvk+X1uMJ2cTvwAvb96Bg+gzW/Qz+Xo1N+Tr+DsLzCm3+luPP/2HbHgmSbEM5CguRsci9lGZbrttzxgRoDgUx5Cx2On9p0qXmAFk6gMxtwnMArQjWtugWGpMC3Jaq9qLG4XRLIHFgRmkmVkrKdPDtMudxTCNAjkJXgnKF2yvaF2ziLxXGJFOnOFbJMrAOr5Or3nSZZzBKpuEms5mOwkxkzWRaM2Fck+ktaz7zIXWSLQzW8zqjDjr/KwyWGZdTrFn6FiFlfu9Qx1Io1ZP/QtRhUzcc52FbjnQzNEZeEzR5AZx9s0kpnn4bH38rDIAS9WjpkJDJCO277rpyna2qiCB/HZpjmo3lh/j0Ffg27VD+EZZZ+Vdswfo+0xxJH0uGCVNKl27MoTzWJD9pwKRerC2F023BSxI1dysDeRWiKA+BekP3o1hFtf9Ip+CvT58Fs+bXQl1lIKyQWuc8/D3AvfCcTbUXoMxqLjzCIYg3yQka/5iTp0JIYJOaW9OQzKBYPlbA4AFbQKYqPVuQdeQRiXfAEy/1hrcbE5ZKjb5TXx0J8uVo57ZsZp6bvJwSYchKWQ6v3VbmBWYzWyZ9zXPhVqv0RWyiwP5UxjNqhMghRGadTBpZ8T/C9UoHD7qvSv7ND+rhg7VJ6N05BhlzZ0jlNAbLYWzVCRzrPP0nCf5n+QCUxnVnWwZ6NETh9CMzkExF4fxxb0JDz4+UjUkG6IaxhZHMGTCwd2eoq0lDIingn+8g1KWlBbQ+85Vh6b8A3kcUMu/CB5EGecgII1Y1CT0mz5hgvtLFBviqYYRHeWwKTKC/E49FYM2mKKxeOQz23+8Dfe/UapCxOyGTKmyrCgJpQ420doFm/FcbYWiHTtTk8OGAmSz/76A8G/CHeMCZQU1KIvZMpDLQqUbAyaM74BvnPK5AyLI5sfeXDFjliRicdPRMOOnzfA/f29V2ISz4UEBHUlrbssvB1kfCqo5s9y9TcQpPbtcuqd0XPmyB1PSlwgMwNtB7danCJel62ZGclckWMsAlWDgXjV3Ifv/iogz86pGjoLW1Hlqa94edOwdDMtFDTZ6j83kK0bkWAgqR7aNPIXPjOAJWezwdpNyxmKRROrEcdQHuJKk6L89SLSCGtHnycQygrUyQLW4kjHtiShHrNsha+G+47CEYM1RCCr+RLpPsGQvF/gvFUAdJ9yYBymOW1NCn0/oj8pIzp4bANpsqK5g0bv5yf1s4XKucZlgz69FQE4V1WyV887aJ1vvbWyRceloLTDj1b+Hd99h95frfToEVTdICfp+DLHaHYov6egQkzaQIro11+sImXIbX3sILT7MVyDi1HdieCLbrhgtXQ+8eq6GmIlGuDTEW0/Pd8x6C2soL4NFXlZ8HOdKIsgBpCC8qWzIWXlYvYlImu3V51q/J4mRSKwmvTskv5pEwCkS6pgFcmNbIXnioCvmWEkncvEMZQMiHdsZrdfDGsguRLEYMpMc8eBmkQo1NGaBNDCIm/JT4tLdnLD5R4WZl9O0c0jUerppb8NovmOeT7vEgwWZrcxp54ih8/9y1cMigV9XSoO6V0bm7GsXHr5y8GEYP7QktbTVw+7R6ayzJbzYty4VJfY5dbcTDgOWml8mNJK3BSl35mQMMr63do/ETJowgmc/17pPRJz9nAPDyuF5zgvQ4M7g0CxGP5z7vva2MCwbaCKzfloFVm9Q2Ez8QdcVQsL5PExT3B1AbSE8K9tI3UwE+b8PTV/HP58HH9t6ayMAhA2Jw1nGb4IiRLylqkLSBs4y7DxIV0KvHIujVR8nuO1u+DI+/WoOUKAMN1YE8eR4gzRs/ScLnDk7AvCVxlAZR9MxtHaKu0ib/T9xfMejsTAfNNgoKcBD4xCXKxzLjZH2MPyuwrhbQNjUVMBaES/6Z9yH8YFVcVKFAMBD7RiqUKqWUlsTNbUBgoXYkguoNUHn1xPeGQvEBO6jue0FxlkYyv6MlDcP3z8C4k57VYoYEAidtMyY/4TqArHqNkBI5oZDv7XpzJxNqVlMVOBsJOOeM6dCemgzPzKu2ALW2sJ0I4W33QiThyimPQvsfLoSZ8zPQpU7f4Ea+f8KlQtJJlGkfTw794fPkaf6/+OcEJ+b0ke49GFQ8g3+TuqXJ8bzQtkoYPPRtPW0BwEyOHmSDRnpv+XZ2Z6AiafpFsGzi8Crzg4WgCxrMARoggNfjyWsI0QQqW60z068vqYyE7vVRqKpMKGVZcFu6KP4YTsNCgSb2B/JpVX2ynUgoWh5hbYrLsMWIvQlYM0qomjJpGkr7k2DqCzXWXrJYtNxAmmXLsX/J8kcOQ4COCevIKfyd7ntue7Rtz89CflQpM3f7cQOWy1RoRBnRuGKaQFLAfxHLX2jBQchgXHx0V5RE6s6/Do1DEBsg1KJ4K2hVEPX7/qRWmHDSk0FASpVScLMfYDnLwqDCWoT6pPdm7EoR+r7DFqObA3XEdDQDnHvK31BwnAi3PlHjQHLlBVKkDN+YOBs6130eZryewQpL3wBkco7128hnFM4ctuqiLUh51BsWz30OVnEG1lVnqIe3VcgeeINiG1EQ2b+5SW8AP0oT3y+nivYGz/VXu1nnS/B0jh+LQk9c/5VdcMIRzwXhd5pM8hf4MSgH9vo8AxRhPTtRlr5Yfg8qqEPgkJ581NPI258Bv0RhKolSlB33SoJ771HW5b1AIM3EoHff1dC7y1EoJUahvipSkh7Mrf7Jknnh3oHqDAHjNQH6GwiKWzhgAlIi8YP4++S1PhfLNPIvwvaca7EBSi+GLIGczIC6JCw/yvyfy+NL38QpNC0LuI0kFEZxoakfaeQNW9ozMHLwx1BVhyi03VeKH8cWqtF5hQFnN4hFIXfCbzOv+pi/9BGHyuoknHrM05BIn4VtEw4TqpNa5CorkESmLFKxf59P4OQR/WHpOrWl+d/giLJgZ9Mo4sEexfInPt/KfNtBfH4Yk8QlIb/fzQbSIo+VDCDO2UpLi9qNPUxCLJZkrtEIoARk1wUCaP5jBLNDS5lPTRn5xRSCXCQFE05+skSLUyBfGoMjD38JvnXWUmhPEjkpHlvp1pasyVKCw5Ekt7qEx0KV492koZSG4V0FqabUhaEoY79qRiubLOk+h4VbobAtGASkPZw+tc6dBzmiKB1txHstWNYa9erYgu4NAr538cPQvesS1oV61ZrEyuBnx+YdtvzhOSmW0yXUl0BEl2EdQcgScaByGRKbpkn3VQNV8WI2TMosD6mXHBmTPvu1pcc5GjQTqrtAkaDqY65MY3kH62/mpyii3n8y5iFHEIq0d6xGft8DbUuGyypkWgSdsdR6DBAi55ydJfkO1sd6gYLQfuI0o+YKbZqUHXEeDF8s+rm8ABhuMAn8zscysJxkLFLAKnFg7UMGvgq3Xd5oOSckwuqBBZhHknlRr/4THECpW7yyuz0DmNCC4/Jq7XCVdiz34/0mTXAhYeE3+ArxpV/HEteAcjqWFcI/1pG7xPXNi8LtxyPc/cn5yWK9G7FsMwC+WtjW4g4kmGMgG2wiABhFaHjqwRqC3QCkBapXo5UZGDxgHjTj9HWkZN54UNJn9u2iT0oOY2oMtQsDuX0A8pXQa8hcUtiA+fihO7C6pcyqkPmSMCnF1hzEfSQCdQvrSpP5xkMrldloyQGeWRqJ12HmExM/SvNBnkg7WvJ2+zBWgRUCjBBAgQmmDnZ8c68AqVQkn6LqnX18KwzoHkX+tBz+LjKflF0+pVLhRwLb8wqoSCeGMRTIF8rbQYUsJKfew0FtpDuaLTZB/a4qbruKxZOTvb7NjZ1JXhjSJwZfPn5njhCbh6UnlD9txgAIaT3cjZiUOh2HeHwrnD9xOvTvIRGjpkMFOwgWVsAbElvkbOg62dcFKT9+TC/BAlyoBYGSu5hIibjc3v98vkupneRWZkEoqO2vWa3zLZb+o2HmQZr4Hg8WlpqQ4N0W0pqQMHJgGs6eOAMikY58movSVr8wCmOV5QLSEqw0TM0S5DwhobYqankJRYR06UFF6N6aAIXtLx5ltsnLqVCMKsLzWfthTbdgOyYKZYFJY3vIgzLKETzIfj8Mf36Ez5ENfQs2ayQr6XuDMqWuZQV/m4FHTeXGTBijBXpJf1Z/HDPNI0Xv2bZLQPPmONRVJINIVyoUcRMF3W8vULuRZwJKpdLYlKsueAjOOFJakeh09zld0AlTdJOomweDoJ2hMnzJ8YEybCHe6htYTuRYSPSZFdgeMgU+gn/v0pblMGz7A9jOn+OzAzW/r+34HdIOtEtDbE43JjTz0F5tCI9VhS146YX2Gr2xDOAn950HlkznTzNbS1I7me9vM/HJxWPSMnAjFRUA55/2MtTXHAePvhKBynixqh8APdqJFyJ9TKO7N7gDmSsvxbq68ILZjC19TKp9TrWgLE5T8EYFk7hDmCdjcidb8bXnWZcqTfw33k9AIIV3Uw+H6YmSOtQYBacUCU55J5jC4zTn55sLECuE5eyyYe/zpC4Ff9ce6+HIoRutPERFBzxxYBmhxTbKbZnQt5rsoYNSG/bXNAlLsczCpmxhHSUJS48C++ozjqsjlRT3ZTkLUzsC6mh3q6a8i1d6VFh8idiJziZYISfnivwM3asMqMVI8gZJzurnUgi/d2EPASmxNfh/Z2tV0XJiadhQFFAKPnq4/AhSrglYwsLRNJ/3CaPMz8Oj7SxhUvcPkqQJo4p4MqjrFA9qThmVKNN5YQY/FQ9fyure5gzFWAzJ178jPd/Tt0voXkCFIFUpCw4y0OGKXj0Ey8lY/0qhshLbusrX8M/zNT8QGyOSMzM5b7zOZNV00PsUp6m/ewuqvqXGmL1O6SP7mHpFfhW0n371mjHQr9e7EI12mDAh+R1QZJPT8yrhgzO/0FWKlELeUB8HfSOZ6gobNg5kI0P+wG1lBdKspUQUmQxPujPemoKOObeUFJqstUAuQdoDri0g4lGvYOw1n6nRmTzJrvhWFoCNxqt/watXMDB0GNpCgLIJF1D/nDXNu2HRGPNTnfY3NZ7M1k3bM/C9uw+CWy+PwMADXvfD5+QT+n+g+Oz8Ur15DInnvhPyOdUgxK1bPxiuuecgy1IZi5i/rafmiZUtzYe2bbh40u10atYnM5f7xxlGw5RVJKygFvIgD6PHSTGv8cJ9EaC+hudfUa7a0IkTvBo3GisVluUxdTYob3a3cGRhUkfuAJ/t3+5Fy9eJbybT5vumZwibdmnYrjSiSaOrHrXkSUsQVA7MNQXCBDnf3MVA3uwLnZS5BsXLzvU7rA2OSUr0G3VtuXDsc1OjEStnlmU9ZlGxn9WdJzzZAV0ZgIUz18vuOki
                            /+QusrwHrO43bSBV2xvPO7q0cQLZ0EH8GlX/0WG37xwJwpSPUmk1kcrXuS+rwgAoIuMZ7bSme/vH4/vtekq+cqG+eehT81+RqGHDg25y+ywOoW3ghbWFDxKkhx4cEL+rvP1j1ZFimWF9NCl6bdxo8+1Y3a68/wWZFLCjkZw75fKZyhvptodjNcj4JNAsYw5AUTEm+yFZPmYWjfH8HAzOpXmaztE/mUArIdTi//2PwDy+zmSXiYseBnF1oL9K9bibP9gebtzQFFTMOhYu/JGDo4IV+++ypHw+BtanRYmVGMJamPU/1jC9aGfOTZoP2OtEmzJcDRfM4Auj8E2HqCz3g7cYk1FVHrF2jYalZ+YBU5LjI0si98OgCbauTCWnK3Q+k9jGbt2icgDWeACpjGynRW63YnwiI2La5tnIcr7/AqqUzeCLnBehCCa01KiFNVPrx2Xn4b3IUIYeXhSZppFfnGLy0OAWp9KFwxTktMKDXslxEfO/xOhdSbx3Let9e/N2t3FbSCqwLozta9uHRcO+T/WHVlhT07RaDTKbYLc17HUu6N55pVicN/bsTQFh/i+KXRBGq6pcDMYfzmFOAeoewE20DoTSMwm0GdsT415zANRZhAJZvsODSamr4fgioCxsz8IsHPwe3X7kGKuKt+dq0kVVKRQ/ux+vHwA//OBjaExnoWhctykm+CKfngOnTedICi5ec6eoWc7Zc4fK9LLQUw27Q5rHWjlxp6XCeU7GCH4gQmhBn2Yx9XSDtZK2GidETuwlPAGLZgIV45oP89N/0am1VBNZ+koEr75gMHR37cUKbcuOqmGVvW7byOLj6nqGW4zXFaZDFfw3KE1XKId2XCOlgyqJhiv4M5jTCBWDuXW3SSviVF2rJSN+Whglj4vDFo1dDJlHj0ZRFIir47O3TesD2VopWZ05q60Oyiad9Ftv1NWlIXac7QLuBlftCrj0UM4T2Kn0bfFLAU7gbCia2fmsGrrl7HFx7/nvQb9A7SjGWKCXUTlylgLKMwymYN+8UuO+Z/WBna5rTUopSQL5M0ofMBRArWk8KwpNPND9QFzcAaaY7Ez4noUsDBTEI9lijx5vbIjDuiFUw9JA5ubk0qHa3No+Hx1/uAo0bk1BbHYW6ylAhENuZn/wYP9Jf5O28nq06e5FA5Cxc3C/jNRSARLNJexKNqLF9d1USHpg5HHp1HwyD+7TB2M/PVGJgpkAQilIy36TlJvPEs5NhRxvA4hW1sAL7370hVrJvRawsAFpe7tQjJJhylTqlD1EgFkWMZ8XmlHD+yYugx4DF4RzLOAYntJC7ZJvvY+NPeQa/PwleXlRrLdolaxTJrohSorHAAd+CE/os/l4AQQ4fwVn6yHXwmxSPHv9+RoA3HZHdgm71MZj9QQaxXQwO7tcVOtV/AY4Y9ipkw/cSsGYcAYRdaBksAN3cNByWrOoL6XQM/vJCteUNV1+TtgLzZsrg/BPbDeAFJRjwHQKSKeCDKblYofxKhsO8dKmLwK7WWuhRmPcjTmEbqZ+qDCDSaim2W2Jw5kl/gzORQ1y78gj4wX2HWOkZW9olJJKSAdWMTbGv5HM6zgZSR0h1d0x/8CYcZqFzJP58jVVJS/0GiL5NcZnqq+OwcVsGfv7gALj18qOhc/1mSCZroaaqGRrqG70iAX9t27aDIR1pgidnD4f7nwNoqI1ADeLynp1jVjsyZXJOi5UZQkvkSZ2m0Nz+JZk1VJuUvwXa462tLv27R+BXV86GuppVhbrn2gkJ3MpuAlCKBXWF7XBD5K9vj0Xwh6ubIFqxFn76hykwb2kGutT6kn9aLv/E/q7Gng3M8edO5bYONXqyFleeUTLVdqj2+CjZNWCtqVTf/9Efh+A3ByN2lTD2MIBrvt6oxke4MCgu9J/+aQys3JSGyiipuNxb0MsqhvnKLkVK98XzpG58rGNMd5ucSRsK537J4lFXu0p9orCkMfWsP6x1Xa9lXaJjTEQkCVU1a616vjV5LnR97hh4dkHGckr2Ochl6Ql8fpCln5TO4MEeVYqeL1ULA48LnOZ2glS9uxZC+neStsLeabpwRRS+f9tFzlWQxQxIJbZkIJVSEls0Gto8TYaHa/BZXITiOggRPz9WDuWh/V4iFYOdKAFXV8aNMekLJf3O5FneSNLhBSwD9sUGp5P1iOEKdiD3c90xq8alvQhi0Ld/IxzQexRK1xXBS0vANKG2nAyQRrOF0AIDO3NjgTNfAbEM5+IvsSa3sYI+mJeJqvcr4xErPM97q/w1HzWVEcuJJTT1lNaWmu9imymuVi3DH1Glt/cMuceJ6NN9C4w7rBssaMxYM1lRpsD+AOUjITTeFH6mLVGSnkwWcJ0PtcO2vSMSxteAbPwzQJlg++ZVbgQfJPGTYwtJe2RjfyEf+bfHm/jo2irhm6ugANJOuxQo/QMlPJukXSfPMdIn/HcQoEbKBqTJChh84Jvws/+YCsP6CcuxgaTnsHuJnGQcHMnEcrtD3dtFpMMtLn8d0moTCQsH9MpA2XTEBam+RCizoKR0hyr4V9q7DqQh1io4Irxkefkcn0rm2TvwGlmlDgAIyDlVvoMwOLEt52FD7sEyyd5doTnOUEKCm7AM2f1ASgFTk2oMf/mdB+HgfhErgUDYXXg5kpHLaAdSOjfjaUDr3WIRxqoloC1JEasBfvath5CP2lWgTjAkjg0o0ai0MFSIZ7dgIf/TD9264KAcAfpWE0P2a1L234p/PwwqlmpdWWHASbBIkXUilnuwvvvx0uAAtP8lbCdt/97P1J7yN1Aq7uzaKU/DcYcIaNqeCuVJJ1wDrF3NDrIb4xaGRVFy2JaECWNQgDn7sfKFiDc116/gOp54wgz47lmtYYNpvIZdvM29A1ePnaXvfPXqmt27G7LjeSS+exezFJeDimBdroMEy/Ox9ulY/x+x/uOwzRE/BKPNJwK0laC3XzjpvlTlPhKobl22wyXj5yMjfhQ8PT8FvTrFgoRxxC+W65tPEF0Idm4Wotav1ZbwnlHbrS88MQJfOeU16NSQKtEEWKSgKWNQ1zmBvHszbGuusjLjkSk1QIVGFoO/Y7fvxe5flpWNtHDvTn2xc+U42CPNtErZ9vBaH7zfB/8eiOUs3uFJnk3LWbfaXABJp6C75IVFgtFQrGEQfpt8CIQ76IfbBqOFlEf2Q56K13+Hzzqk/lheK0axR4eAAf2WwTcmkAL7KJi7NGMFdCV9HAlULraMXN1msl6vmCzCFK9+p4NEYOfbUThq61ApBCceHYHLzpwFlbWfYC0Vij3Z40fKYol6dNkKk4/tBfOXq/CMkWB6RmNDu03Jr/Mk55xJLbIgeFK1B2s4svcP4ILkXxBw0k6ERq6XBKwdPC8JXiQxBkxCKp0I0PGXAJ3iPw32aBwK2zlBGtgJXB/xqe+WroIKxLrSiircs+dyuPGSj+CG+y60kgts2Cpg086MBahaBhNyCbsbHKmuvXt73FmUNa/3laC58ZKnEm1N6NM1Cp3rJHRDAnT1RQ/xtrq9BaA2nMZgQP+FcM1FC+Him6ZAMy6iyvxM10c40b8UimIco7b6O5MC22OusLJwRWoRZl20lK6o2XAwAZvrecKoLYzVBUvq9QY9MbjdKU1GA51XltK1XSSnWjubAZZi9y8v3sEkbCzTBGGKDNx8xVRLGXLPw+fBM/NjVspAwnLaoieF5eLS1FRqgBqqKakrcuNHtcI5Z05T6zKlCXjl5L9FEdgUefat2w+2qEkkRBwqJunPsWDRHfs4RM2121wsPBhVajpnd/K3YOY6e72O9K2evAVubQy4LIQG4HU6tDt3wGYtirlvTmL97lV7xunZVmgjrrv4tDnwlS8mYdnKkXDDn7pbbauuEJbEK0uIGp1iP0/S6/3kq6ugZ9c1Kh1h255XM4U5MpmIxY5QMI0C9I2kkiLPfQov2UXPyiIMplG/b8qsLVU69qUFR4Ux5CUQpuzZ3uwpWf844aSCnq97DTbE3twfC1xE5QZU5C6qa9cCWQSPPHge/PySYyASb4MHZh4Ib33UYTnkmqQrT65N1z3yWTx5ZAVMPqkRIpkqGLr/HMXIpJWw4uvFsyeFJpdg2aV+Ddw4pRvc98yBsGJj2lqoIQ5iaR7n4bgJh6KHez++n+LdI4mCMIyr0AQaYdSkuDGmqU6vwObNl+XMFQsuDJsTyvDSlj27fYTjmlLs9mh8F4wZ9ZzFeSQTNTB6SBcUqlKhnYR1KtHcFoUxw9bAyMPnKl1tSvF+CjhTnz40Cqr/I0fMhvg/DoTm9gz2PRoWm9L24am8BK/E/h/utznek/8qYED1DC/uzVRmmSD/ynUnnXPzSaZFoqWhJ4cdEiQe2Ev+pBzcP6Em7JjRs+CYI13MjigQoxEstuhqpVR5qYAo82IF1f/Dh7TCtuZq2LJLCZMhjzZs0h+Firt0KVibA52ug95t0NKgBiouppbX4UVnIfK9E6pu2uBHHmW/JU1DJL9afg8AbAKcGSc6oKDsFBbfmYQ9hTUzZeFyrdGNwVfPfgJOOjyB1KCooX4C5/oHOOFPUdx8N0k1KfhNwJNzOhBG9ioXahI8gY7dWVJASzfvTB6Xz2JmsXlppemRv8HfG1kVltfi9CkUOf6FDqn8Tts6SjL8LQLlM3qrWrLSE51QSjOrlNWx2r4AWSBzRy8UWd8HTa7Xtwe6QMV1ns0qIw0bCHMWcFC7Zcm34Nes9gKznjR3kJqDtjFQEICKfRBlE2grV1Hfsi1ppBrnjHsRaqu/AA+9RJ7tBZNfQsEUdeQeLO/g5P8HWPucTPKSNKis3K5+3t0OXqdrPXic8PhV+JN8p9CkCV078fT3eE55WVeAK/tpEJCSBaEXE9RowZKtCPGsidcTRfClxTpqFy7FEz9BThldyibtY9u79WyC/Xu2wLZdCKTd4sW6clPQBgp1s47ip+LkU+rIcSbBRAcYZzxYPdlu/khl3mAgbvWTVqfBoIC3U5RiCNTe/rngE2wiCEhrwWhV2HfsDvzcq9s2OP3IbpaTMelOS8iGuUgrZFaklOeH+i6svXM0cfvmsu43MMSQkEtgFahArHseQ5VdD1sg1i5v3Wn8PmGEc4tRSdFe9fZd+8EF/zPWioKXT9J3hhyQHj5PQ1xjEKN9GS+cCCpBGiUDq3SpezxqK5NqysQ2mNRI3gmxsCtR5M3MPs7CRylS4XKzEcIp8MVCk+y9DWThSXL5+5IPwEseJzaV7ugFVciTpjrKKq+SZ9Ob2MTOCCrkvHEBAsMYrIFMnRQ1jLYKRHLZ4ISD59I9qEwmUPdA5IIIC2tDtLB6Jlrw2ddABXKbic9sK2hqEZMSH9DnUwd4ezASWeh25n+OtiN/udhFRgF2mrYNh//58yhobMoEWqEKwKS6KojCrtTjlW54Thj2ePylLSojvDtR3WojP5WWmacFFXGPAHMOXiOybmfua3Vvww6DSdeqhlveLek9hmFKxU7lBOJCAqVIY90xnoCNBZP5ilQupx5+s3fDYqipHMURVsq+Slu5NDHZpdiiFM6cskSTs/FB2IbeCNK9hNoR2wMBhhxM4jaLkAMpieTbysRHAhvlN92AwtY6fH8lf3sTF6qruZRG0+BS3E0KrR2H4nw59x1qHGnyl4d6lMLS0BsiBS/OHQ+N6+sQa6azMLBlJxRifSr2ID1kIxf76MEA25ULZTWp5v5VMIxIVsUlWFW0i7HkJ6wK27A7BnfGPhjbg7AcS0FH636wYNlhEI93wNTnu8AHa5OIPXPJr+qqJFSWcadtAcdm8Al0trcxwL5jTx2RlBVM4eWFY+Dmx2pQSIpAVSwDfbrGPEJIxsfTRvybDRmxQPuAdE8eyNU9PWsS3Pv3GuhaJ6CEyJVZScNkDfoXAE3Hzz4g3V0EKp5y2unw0tRpZ8NT86qUrbzYCEG+GOezj2P9EibvA9JyAmYkpUQL5DtfmfMlePODLlBXrZymRCQDr71fCdtb0tDgE1y39Ek2Zyv5NAJwoHO2Z2RNyWqTARysyKMGsq+RniDj2jasv++nYcmneaE5T2vfjfk8b9cfSRWW0T0wuTZL5lFDOymbYyIOL8w5DetMwPRXu8OCFR1QWxnlyYgiwMrdBqD/0st/9cdjNGZdQEU8AX17va0mwHZxjKhJ2bBxFLQlKiEqMtlVYNpPTd8Z0Hs5xCq2OUjaxqaR0NpeDVHEKn5Or0EZNpKpCPTpvgFq6lartuFz6zeMgvZkrk0WfGYi0K9nI1RUb4Jkew9Ys2mQ474/fAqLFA/o/RaIaCa3KOwxQEDcsWMQbN7WA2J4X29nJN4Kq9f2h1ser7fin9ZWZaBft7j1d07ZDnsNQN2h3f2Cv30qgfSquw/KDjZtDOvbLQJ3XbXKA2A0WTc9OAJWZi0h0nfAyfb8v1+rhhGHvpzbrY3HLQ+PhKXr0lYsJj+A9I//AEgqM/DtM/eDiV9cnQ2b/XNsU2OTHZddvd/akYEfX9QJjj12Fry9aBT8dGpPy9wo9DbrgWGlDdxgpUG/44oE9NnvHSdGpWcqAKa/ciQ8+FLUioevky0bEOqrcm5smc8UxhSGv8wk2riRLrvF2pzJr5T1ICZMusgRfpL2wvfuHIFd7RJuuGglVCFmvfGBodYW5I3bVcCvSDb/JXi5fzbe9sJvVMSdNLFpW8aKDyVEANo0Qqnaj51O0x56AfVk3OPBUG1yfpOAo2enCFRXUnAIAZt3Suf2btDEauGO00/9j1pxOs/5QgtMHD/dUldfd9cU2ILfaU8Ia7FEI97oywK/E4k4E/WCwWyZb0E6G2pWQflRMT9g8G6Scy+wwoDJ9Lyf4BP0TWnEGo5pB3HW5Iv06MDWBFNQLwqbfcSgmBU6e/6HKcsCUhlX6fako6He3Zv0HYp1lJFOEZZiXkaFdOzZNu27MQGp/RgtkpRmFyMM6WyTAkiiCva+9uqKiKvNZiC120LvUvsH9Y7CkAFJSLRH4Y1l6jrVF48JPVitERD2AWn5gDTmkVnwKnmHE0l+b3XautalNhegP1ysVLAA2ks8ZGmpoiiIUVRhe1OgAn0Ac/UXrukhQKTQPJRGpnFjxNr2QPv561no2Sf47HG9if9Ksfm8zL/hrNiATiXnxb4PYPbGEdk3BPuOfUC679h3lIHc/yco9ywKiDU/xDvkJEtpsskti+Kvk9fMEVgovAMpbSht9iLw37NPcSwpmAG5ilEGONqHQ4G4KBswuYct5nYk+Zzqo5CCq7DMArOqvRe3iX7f4WKKTEY+kieC2vNDG+pIBCMv8fewvATK7SzMQe2lTW4U/pBShy+B3Ca9saC249AYUCKFDsP75BpJkZbJFW4u9zlMop56/j6NN+k4KOLAOm6Dnuue5mI4tyeVZ/7Jn3g5f/t4bgdtg1nj8w45yE9kYYP8RinsZi3PaXceexrPoOy65P5HKdaHcT9SPjoxQqKtFIvmOuS1GsiB1QukRiaMIvd+DyzPa/k+/r2ZK5qEgs1glTJQfDMASCmsHyXC+hjLW8znDUQ56OugtlFPVYBLiSot/8Yz8d54/O4mbtBzdqc0HrEPPnMh/o7Cumlr7wf4vA6k5At5Lj5/Gj5Hi+FATewkJcAS7vvzoLzr2/NIp/2x/JeUohfW067GgnY+CgLaC7Ceo/Ed6t+v8dvTDJoLmuhv4Zf74XO3MJAkgzUcVoDaKZKSIUh5iLZ9gxTabypAFc/iu4QkTsb7FMG5J56necM7xW9NE6uNo1inqpFRSrqLf/8MP0epLi5ngKPxuc0GVE2S74Q/5+Ppd1V75XV4lRY6pRq/BL9FQEfjv1IHUkN3CEjH4vfOwXdowVL0lQocT6l73AiwIkRvo5V0gLa6wxz0XD+tMvosbeonD+1BjF1u44F3O1FTAq1T+f2V/Ay1qBO/O5ixnW3IpMb35UGgchVjvnmGTh/A7/cApyGUvk3xA37KbafBewuUt3gFY+vhXOj+UyHGgCZyKPe1u1ZfnNuxPxc74MFiQ3sPYgrWHfIbbgnLEWBcwhRiPre/igH+VMaCy5mSbWMM26JhzIP5b+r/h1yngJwP6VrGgoRELudv3evCcidjuYzn72VuR4IpwiAuvSC/T0iU53k4n69i+DG91xzTdJBJP12Y60jySm+jzVZSVbgJP/EKropJlJ9Hqmxs93HFuhLqbO7IdnxuGnDEDWuFS4t0pTiAqOQ2ZWxyyZgDSaykwSRv8iYNw5ECwkq+iNeSGgkgoPkitvdX+IVKvIwTJ2jg78fn11oALICwEmU1HoQvPYuVtIQA0jRPdgOAoz76bbewkrQWwBfw+zfh1a+CikRtP0f9atPGXeaRG4bgAIxnDeejUiUPa8K/KdIyAqe4CvvYjN+bw6M9jYEohvepLkoz/ggjiX/gtWvUQrFaQOO+0eqPhBukgOFCAfQkZlcWcjs64zcuVotTIGshrybAZljg/lhYNeHtjzRpKWmMMswaUFTnl/B9K/W11E1b5KlRjLnKpUQWvO3nXbw6G2+dgg1HEi2etIFUO07Ce125QTPNfpC57MMmkovvXcSDer1zIbm9aaxfwnaT8R4BaBsD49MahifAIVJJ10mzuk3m729eZbxL0f55fOwaPL9F8l4fkUf57qqPsOUB0qKFViexvaJJaz8FhJjLyRPs8d4KzlxNBMw2b7yDeUn9sFmztVjD73DobwAVG/Tr2JKF3JxvYxPGMQYnluhtN0vnb0wQPnCkYm9Te7F/O7htBUr3filttMmQuWA+7+Lfs3iQR2tsBD1DiQRIABoi1LGIUXz2m35ZKYg0sFEIGXTZiN+JYUG+T17qfN34PpITaSe0esgWyPDdjLIWWcZVxOJyM5b1EJR6OcTwaDGYsL8wE8tLeE6RmYkHP43ZC7AAjrcQa7GQHIG+tJKiCWQgpA5+iYVMUIZYi8+kxfWJFmTZDjaXob6qb2S/n7HvacWO9YRURD6MJ/MEWIY8Iu8UR2AUlgnYx1osr+Dfv+Mty8YFasqfZSpS2jlZSAgX2g3+x+exIKzgFzFYelaITf4tINiIz/fGd8cyj9PEsd4nMy9DpPpVm9cRPlhEehaSXI/3HsJvE3Aeg49RumwU3GAeCy9CC2Rgv44CgCXMbMdbT6q2SZsn6sdScJQxa4QHbA3zkKHilpuEHGoIXqaF+D73nfa538AUYI6qT/gJZe7xTjJJJGHyRAWksgf1Hc9XsWbhNdBC1Disk6qCiD5pfsEd+EAMLG6jrCT4JRLWfoTniHnlaHyWxuVuxWqYZs/fLGoeJwuLErW7mpGWcPH9JJDOKLfT8womp8Rcn4XlnyyNd+dz2jb9LA9qoQfxl08wibsdC2HrH3Ndfl6gddpEr9L4bsJoFH7mVv6uHXp3C5PPxrBAGoRoua8phXks6fz7TGp3FGL8ArU1+Ff8ezSr5Y7i+42sBprFY70LSvf5J7XYA6DCLx7GpYM1L8+XEV46WIC6xOc+CdcLYk5oN6tdzKs9t2q01UiD+DjepaSrB7H0Rjq8w/DxofhYG2OSJjcWcq9oYZ70anzsCXxvKFLM65mEEmZdjO8nDG+m+LMRpWaDCAsSNsIhmpNkvq+7VBL5Bwy4eXlPIcyh0vm3hrHS05ZqDBDz00KV8j0LqAS2ze/bJj9bS+sg38A/x+MDRIYPxcc6s9bjUqxzPL73Q2ZrEgJCKBMhMEkx1ieGYx2X8jOzGAElgr7q59TmynJi/02sXAsLga022y1yWeeWsHTv73nk2xlXYnPtVpKV88vw2hhWmh9HSaQ4pOA8/FkeJJAEbSjDe3Y047/iq8PwUVIt/RC/8yjkouRnG8/hXNgTVBI2WIIDYeclIoxwBmkT8DmSZr+H3z+BgV2GE5AgKOyhZOmfwoffgt8egQ+cjuUCK1uxxLEKilvl9S6jGaTF/UcuRA5H43e/SloVLL2xPYSpn1S
                            SsywC8XuwGKUsupQ1K8RqfeAERv/58xM2XV5TuJAFCa7XMvY2Wxy0XEgymH8QeVc8v9eK12coNYY8ihyX8MHjuE9PgZbpzJSkwbTinPyxdYMEqN8oy4s4mA0JxFu2uoKzrmQWhFQvpD6ZxSG8SWW1Q5FGafvPfKJ4yZBB+wGM8ZFcVMZeLFTXNTjCxPaMwfM+ubH3Gj18AAypiGjVxoYw0BtsaSIKdSXe6cp64W2eMI0htDiGapOaS2IiBC8Oai0FsxvmAL/CkYZS/2ZEi6zWkY+VMWVV9k6baMfPPIKPrEOsNQzPx2MdA5jEz/Uzl+kCkzRJzs6e2+ZXSu+3QVoWFEr7J9IuZD8Xywx8M6KAQ/yEJVXOuWuFv5ZsBBjCwbmqs9FdbdUBmFOFan0GW52m5/3UlCE26boVz5exMj+uh/PWi+uoYIz5ZzaSHKrdQ8ARW4ls8nuEEDrsCTcoZPQFnK9eDVVawCT0Z80Jhu02ObMTmgRLHmLB1Hdr0CLSBacvsTqiGrzb7QjA7nSprYK2wK3kVU6T34XNjDPd+jnhVNpGrW9KGXHRk6hmpRCuep5i6fAatjTFXGq1LcyjjWKb9yWSLFgSXsDPb+dvUpqZcUo4sEypy0IITbovbsR1PRqg3nuKVXPXsTCZXw2ohM3RbAihYxgtPkodw+fE+09QOnDxEl7fGWDlAcWXh8akYUIh6/eoDsoP2hnr+ITbrsPRChYmO7jfaXxuMFsSG4OAlB6kTQ+kezvWx+Ob1Bt38vUUV9ih9I2+fSAMRhLoofg+rhQxNcCBI8MWiI6cXT5ryUgShsxaklzJsPD312QDxz+/SpiRoge7FsDblFAVn7sR75Fjxilc3AfyymIO1vC0G9sbgh5bljAKa4jVpHPjJDinCpnFjdoG6uNjSv0lL1NY2xB/S3oWPanFHiNdsyC+luz3ToG3hYWa39h+BwZWKqFRobz8otZPKRWJyQSQe9viRDrZY/DZY8wstnxLURRLlYXtkDvxnb6soQnEpIsZe7b5CAUNTFrto4ntusvZKUX6rLO5iM0XMnn6kHWo7T6rttnSiVnbBcQGZSa1HqDnPyZHFrzzsQ+Go2uPYFt74TOUiLWJzaz6wC8EcriQcCGenw22h5AQSWzIRh68hxW2F20hUAwBxVJ8fztj64wmOK60gFBYgbvShpkigCPSfQg2rT/2cwsEB4prZcmaWKWz8AvnKMwqiQJEWTL+B5t65/kgDVL4fyQUFl8XpK1wHbt4ntNuEu5CZFbwM6ls+Gl790x2e5FiD4glIzjayfLPRkq4K5X/QgKkqd2yGsen7f8FGAAZmo2YF9oYYAAAAABJRU5ErkJggg=="
                                height="50">

                        </div>
                        <div style="width:90%;" class="float-left">
                            <ul style="width:20%;list-style-type: none;" class="float-left">
                                <li>
                                    <p class="m-0" style="font-size: 11px;">tilecentre.com</p>
                                    <p class="m-0" style="font-size: 11px;">Pin: P051142421G</p>
                                    <p class="m-0" style="font-size: 11px;">VAT: 0122763N</p>
                                </li>
                            </ul>
                            <ul style="width:20%;list-style-type: none;" class="float-left">
                                <li>
                                    <p class="m-0" style="font-size: 11px;">Head Office</p>
                                    <p class="m-0" style="font-size: 11px;">Parkside Towers</p>
                                    <p class="m-0" style="font-size: 11px;">Mombasa Road</p>
                                    <p class="m-0" style="font-size: 11px;">P.O.Box 3115-00200</p>
                                    <p class="m-0" style="font-size: 11px;">Nairobi, Kenya</p>
                                </li>
                            </ul>
                            <ul style="width:20%;list-style-type: none;" class="float-left">
                                <li>
                                    <p class="m-0" style="font-size: 11px;">(020)3939000</p>
                                    <p class="m-0" style="font-size: 11px;">0722/073365700</p>
                                    <p class="m-0" style="font-size: 11px;">mail@tilecentre.com</p>
                                </li>
                            </ul>
                            <ul style="width:20%;list-style-type: none;" class="float-left">
                                <li>
                                    <p class="m-0" style="font-size: 11px;">Mombasa Branch</p>
                                    <p class="m-0" style="font-size: 11px;">Lecol Building</p>
                                    <p class="m-0" style="font-size: 11px;">Mbaraki Road</p>
                                    <p class="m-0" style="font-size: 11px;">P.O.Box 84935-80100</p>
                                    <p class="m-0" style="font-size: 11px;">Mombasa, Kenya</p>
                                </li>
                            </ul>
                            <ul style="width:20%;list-style-type: none;" class="float-left">
                                <li>
                                    <p class="m-0" style="font-size: 11px;">(041)2317444</p>
                                    <p class="m-0" style="font-size: 11px;">0726/0736963000</p>
                                    <p class="m-0" style="font-size: 11px;">mail@msa.tilecentre.com</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br>
                    <div>
                        <table class="tableinner">
                            <tr>
                                <td style="width:10%;"></td>
                                <td style="width:80%;">
                                    <h5 class="m-0 font-weight-bolder" style="font-size: 16px;" align="center">
                                        <span> COMPILED PAYMENTS REPORTS</span>
                                    </h5>
                                </td>
                                <td style="width:10%;"></td>
                            </tr>
                        </table>
                    </div>

                    <hr style="border: 1px solid grey;">
                    <div class="grid grid-cols-3">
                        <table class="table tbcust">
                            <tbody>
                                <tr>
                                    <table class="tableinner" style="margin-bottom:1rem;">
                                        <tr>
                                            <td style="width:100%;" colspan="2">
                                                <div>
                                                    <div class="table-responsive">
                                                        <table class="table mt-4 table-centered"
                                                            style="border-bottom: 2px solid grey;">

                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col" style="min-width: 100px;">
                                                                        ReceiptNo</th>
                                                                    <th scope="col" style="min-width: 100px;">Client
                                                                    </th>
                                                                    <th scope="col" style="min-width: 100px;">Payment
                                                                        Channel</th>
                                                                    <th scope="col" style="min-width: 100px;">Amount
                                                                    </th>
                                                                    <th scope="col" style="min-width: 100px;">
                                                                        Reference</th>
                                                                    <th scope="col" style="min-width: 100px;">
                                                                        Created By</th>
                                                                    <th scope="col" style="min-width: 100px;">
                                                                        Timestamp</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($payments as $key => $payment)
                                                                    <tr>
                                                                        <td>{{ $key + 1 }}</td>
                                                                        <td>{{ $payment->receipt_no }}</td>
                                                                        <td>
                                                                            {{ $payment->wholesaler->business_name }}
                                                                        </td>
                                                                        <td>{{ $payment->channel_name }}</td>
                                                                        <td>{{ amount($payment->amount) }}</td>
                                                                        <td>{{ $payment->reference }}</td>
                                                                        <th>{{ $payment->creator->name }}</th>
                                                                        <td>{{ stamp_date_time($payment->created_at) }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
