@extends('layouts.base')

@section('title')
    View Payment - {{ $payment->receipt_no }}
@endsection

@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <a href="{{ route('payments.index') }}" class="btn btn-info btn-sm float-right"><span
                                class="fa fa-arrow-left mr-2"></span> Back </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name mb-2"><b>Receipt No:</b> {{ $payment->receipt_no }} </div>
                        </div>
                        <div>
                            <span class="profile-desc-text"></span>

                            <div class="margin-top-20 profile-desc-link mb-2">
                                <span><b>Client:</b></span>
                                <a href="{{ route('admin.wholesalers.show', $payment->wholesaler->slug) }}"
                                    class="text-info">{{ $payment->wholesaler->business_name }}</a>
                            </div>

                            <div class="margin-top-20 profile-desc-link mb-2">
                                <span><b>Amount:</b></span>
                                <span>Ksh. {{ amount($payment->amount) }}</span>
                                <br>
                                <small>{{ convert_number_to_words(floatval($payment->amount), ' Kenyan Shillings') }}</small>
                            </div>
                            <div class="margin-top-20 profile-desc-link mb-2">
                                <span><b>Payment Channel:</b></span>
                                <span>{{ $payment->channel_name }}</span>
                            </div>
                            <div class="margin-top-20 profile-desc-link mb-2">
                                <span><b>Reference:</b></span>
                                <span>{{ $payment->reference }}</span>
                            </div>
                            <div class="margin-top-20 profile-desc-link mb-2">
                                <span><b>Created By:</b></span>
                                <span>{{ $payment->creator->name }}</span>
                            </div>

                            @if ($payment->comment)

                                <div class="margin-top-20 profile-desc-link mb-2">
                                    <span><b>Comment:</b></span>
                                    <span>{{ $payment->comment }}</span>
                                </div>

                            @endif

                            @if ($payment->attachment)
                                <div class="margin-top-20 profile-desc-link mb-2">
                                    <span><b>Attachment:</b></span>
                                    <span> <a target="_blank" class="text-info"
                                            href="{{ route('payments.attachment.view', $payment->receipt_no) }}"> View
                                            Attachment </a></span>
                                </div>
                            @endif

                        </div>
                        <div class="mt-3">
                            <small class="muted">Created: {{ stamp_date_time_simple($payment->created_at) }}
                                <br>({{ ago($payment->created_at) }})
                            </small>
                        </div>
                    </div>

                    <div class="col-md-8 col-12">

                        <div class="card">

                            <div class="card-body">
                                <h6>Cancel Payment</h6>

                                <div class="alert alert-info">
                                    Click the button below to
                                    proceed with cancelling this payment.
                                    <br>
                                    Upon cancelling, payment record will no longer be visible
                                </div>
                            </div>
                            <form method="post" action="{{ route('payments.cancel', $payment->receipt_no) }}">

                                @csrf
                                @method('patch')

                                <input type="hidden" name="receipt_no" value="{{ $payment->receipt_no }}">
                                <button class="btn btn-danger float-right mr-3 btn-sm mb-3">
                                    Cancel
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script>
        $("#menu-payments").addClass('active');
    </script>
@endsection
