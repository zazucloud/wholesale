<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from coderthemes.com/ubold/layouts/default/ecommerce-dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 May 2021 19:47:50 GMT -->

<head>
    <meta charset="utf-8" />
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/ecommerce/css/bootstrap-modern.min.css') }}" rel="stylesheet" type="text/css"
        id="bs-default-stylesheet" />
    <link href="{{ URL::asset('assets/ecommerce/css/app-modern.min.css') }}" rel="stylesheet" type="text/css"
        id="app-default-stylesheet" />
    {{-- <link href="{{ URL::asset('assets/ecommerce/css/bootstrap-modern-dark.min.css') }}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet"/>
    <link href="{{ URL::asset('assets/ecommerce/css/app-modern-dark.min.css') }}" rel="stylesheet" type="text/css" id="app-dark-stylesheet"/> --}}
    <link href="{{ URL::asset('assets/ecommerce/css/icons.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/ecommerce/css/main.css') }}" rel="stylesheet">


    <!-- App favicon -->
    <link rel="shortcut icon" href="https://coderthemes.com/ubold/layouts/assets/images/favicon.ico">

</head>

<!-- body start -->

<body class="loading"
    data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "light"}, "showRightSidebarOnPageLoad": true}'>

    <!-- Begin page -->
    <div id="wrapper">
        @if(session('success'))
        <customtoast message="{{ session()->get('success') }}"></customtoast>
        @endif
        @if($errors->all())
        <errortoast :message="{{ json_encode($errors->all()) }}"></errortoast>
        @endif
        <!-- Topbar Start -->
        @include('wholesaler.layouts.topbar')
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        @include('wholesaler.layouts.sidebar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
        <div class="content-page tw-mt-0">
            <div class="content mt-5">

                <!-- Start Content-->
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>


        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->



    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>







</body>
<script src="{{ URL::asset('js/core/jquery.min.js') }}"></script>


@stack('extra_script')


@yield('script')

<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ URL::asset('assets/ecommerce/js/vendor.min.js') }}"></script>
<script src="{{ URL::asset('assets/ecommerce/js/app.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

</html>
