@extends('layouts.base')
@php
$theme_manager->enqueue_files(
array(
'fontawesome-icons',
'select2',
'jqvmaps-css',
'magnific-popup'
),
array(
'countup',
'moment',
'charts-js',
'sparkline',
'select2',
'jqvmaps-js',
'jqvmaps-usa',
'magnific-popup',
'custom'
)
);
@endphp
@section('title') Dashboard @endsection

@section('content')
@include('partials.page-title')

<div class="row">
    <div class="col-lg-12">

         @component('components.loginTest')
                
            @endcomponent
        <div class="widget-list row">
            <div class="widget-holder col-6 col-md-3">
                <div class="widget-bg">
                    <div class="widget-body bg-success text-inverse px-4 pt-3 pb-4 radius-3">
                        <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                            <header class="align-self-center">
                                <a href="#" class="bg-grey fs-30 text-muted"><i
                                        class="linearicons-cart icon-lg"></i></a>
                            </header>
                            <section class="mt-0">
                                <h6 class="icon-box-title my-0">
                                    <span class="counter">
                                        10
                                        {{-- {{number_format($pending_orders_count,0)}} --}}
                                    </span>
                                </h6>
                                <p class="mb-0"><b>Wholesaler User Dashboard</b></p>
                            </section>
                        </div><!-- /.icon-box -->
                    </div><!-- /.widget-body -->
                </div><!-- /.widget-bg -->
            </div>
        </div>
    </div>
</div>


@endsection

@section('custom-scripts')
<script>
    $("#menu-dashboard").addClass('active');
</script>
@endsection