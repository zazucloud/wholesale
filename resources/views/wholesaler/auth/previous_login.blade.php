@extends('layouts.errors')

@section('title')
  Login
@endsection

@php
  $theme_manager->replace_body_class('body-bg-full profile-page')
@endphp

@section('content')
<br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5 mt-4">
            <div class="card bg-pattern" style="border:1px solid #e0e0e0;">

                <div class="card-body p-4">

                    <div class="text-center w-75 m-auto">
                        <p>WHOLESALER PORTAL</p>
                        <a href="/home">
                            <span><img src="images/titlelogo.png" alt="" height="55"></span>
                            {{-- <span><img src="images/verus.png" alt="" height="55"></span> --}}
                        </a>
                        <p class="text-muted mb-4 mt-3">Enter your email address and password Login.
                        </p>
                    </div>
@component('components.loginTest')
    
@endcomponent
                    <form method="POST" action="{{ route('wholesale_user.login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-8">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">
                                <span toggle="#password" class="list-icon linearicons-eye field-icon toggle-password1"></span>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-outline-success">
                                    {{ __('Login') }}
                                </button>
                                <br>
                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>

                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->
</div>
<!-- end container -->
@endsection
@section('script')
<script>
    $(".toggle-password1").click(function() {

$(this).toggleClass("linearicons-eye linearicons-eye-crossed");
var input = $($(this).attr("toggle"));
if (input.attr("type") == "password") {
  input.attr("type", "text");
} else {
  input.attr("type", "password");
}
});
</script>
@endsection
