 @extends('wholesaler.layouts.main')
 @section('content')

     <div class="row">
         <div class="col-12">
             <div class="page-title-box">
                 <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                         <li class="breadcrumb-item"><a href="javascript: void(0);">TOPMFG</a></li>
                         <li class="breadcrumb-item"><a href="javascript: void(0);">E-commerce</a>
                         </li>
                         <li class="breadcrumb-item active">Checkout Details</li>
                     </ol>
                 </div>
                 <h4 class="page-title">Order Details</h4>
             </div>
         </div>
     </div>


     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-body">
                     <div class="row">
                         <div class="col-lg-4">
                             <div class="nav nav-pills flex-column navtab-bg nav-pills-tab text-center" id="v-pills-tab"
                                 role="tablist" aria-orientation="vertical">

                                 <a class="nav-link mt-2 py-2 active" id="custom-v-pills-shipping-tab" data-bs-toggle="pill"
                                     href="#custom-v-pills-shipping" role="tab" aria-controls="custom-v-pills-shipping"
                                     aria-selected="false">
                                     <i class="mdi mdi-truck-fast d-block font-24"></i>
                                     Shipping Info</a>

                             </div>

                             <div class="border mt-4 rounded">
                                 <h4 class="header-title p-2 mb-0">Order Summary</h4>

                                 <div class="table-responsive">
                                     <table class="table table-centered table-nowrap mb-0">
                                         <tbody>
                                             @foreach ($cartItems as $cartItem)

                                                 <tr>
                                                     <td style="width: 90px;">
                                                         <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-1.png"
                                                             alt="product-img" title="product-img" class="rounded"
                                                             height="48">
                                                     </td>
                                                     <td>
                                                         <a href="ecommerce-product-detail.html"
                                                             class="text-body fw-semibold">{{ $cartItem->inventory->name }}</a>
                                                         <small class="d-block">{{ $cartItem->quantity }} x
                                                             ${{ $cartItem->price }}</small>
                                                     </td>

                                                     <td class="text-end">
                                                         ${{ $cartItem->total_price }}
                                                     </td>
                                                 </tr>
                                             @endforeach

                                             <tr class="text-end">
                                                 <td colspan="2">
                                                     <h6 class="m-0">Sub Total:</h6>
                                                 </td>
                                                 <td class="text-end">
                                                     $157
                                                 </td>
                                             </tr>
                                             <tr class="text-end">
                                                 <td colspan="2">
                                                     <h6 class="m-0">Shipping:</h6>
                                                 </td>
                                                 <td class="text-end">
                                                     FREE
                                                 </td>
                                             </tr>
                                             <tr class="text-end">
                                                 <td colspan="2">
                                                     <h5 class="m-0">Total:</h5>
                                                 </td>
                                                 <td class="text-end fw-semibold">
                                                     $157
                                                 </td>
                                             </tr>
                                         </tbody>
                                     </table>
                                 </div>
                                 <!-- end table-responsive -->
                             </div> <!-- end .border-->
                         </div> <!-- end col-->
                         <div class="col-lg-8">
                             <div class="tab-content p-3">
                                 <form action="{{ route('wholesale.products.completeOrder') }}" method="post">
                                     @csrf
                                     <div class="tab-pane fade active show" id="custom-v-pills-shipping" role="tabpanel"
                                         aria-labelledby="custom-v-pills-shipping-tab">
                                         <div>
                                             <h4 class="header-title">Saved Branch Locations</h4>
                                             <div class="row">
                                                 @foreach ($locations as $item)

                                                     <div class="col-md-12  mb-2">
                                                         <div class="border px-3 py-2 roundedmb-md-0">
                                                             <div class="form-check">
                                                                 <input type="radio" id="customRadio1" name="location"
                                                                     class="form-check-input" value="{{ $item->id }}">
                                                                 <label class="form-check-label font-16 fw-bold"
                                                                     for="customRadio1">{{ $item->label }}</label>
                                                             </div>
                                                             <h5 class="mt-3">{{ $item->locality }}</h5>

                                                             <p class="mb-2"><span class="fw-semibold me-2">Latitude:</span>
                                                                 {{ $item->latitude }}</p>
                                                             <p class="mb-2"><span
                                                                     class="fw-semibold me-2">Longitude:</span>
                                                                 {{ $item->longitude }}</p>

                                                         </div>
                                                     </div>
                                                 @endforeach
                                                 <div class="col-md-12 mb-3">
                                                     <label for="example-date" class="form-label">Delivery Date</label>
                                                     <input class="form-control" id="example-date" type="date"
                                                         name="delivery_date">
                                                 </div>
                                             </div>


                                             <div class="row mt-4">
                                                 <div class="col-sm-6">
                                                     <a href="{{ route('wholesale.products.index') }}"
                                                         class="btn btn-secondary">
                                                         <i class="mdi mdi-arrow-left"></i> Back to Shopping </a>
                                                 </div> <!-- end col -->
                                                 <div class="col-sm-6">
                                                     <div class="text-sm-end mt-2 mt-sm-0">
                                                         <button type="submit" class="btn btn-success">
                                                             <i class="mdi mdi-cash-multiple me-1"></i> Complete Order
                                                         </button>
                                                     </div>
                                                 </div> <!-- end col -->
                                             </div> <!-- end row -->
                                         </div>
                                     </div>
                                 </form>
                             </div>
                         </div> <!-- end col-->
                     </div> <!-- end row-->

                 </div>
             </div>
         </div>
     </div>


 @endsection
