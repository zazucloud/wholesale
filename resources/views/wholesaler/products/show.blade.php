 @extends('wholesaler.layouts.main')
 @section('content')

     <div class="row">
         <div class="col-12">
             <div class="page-title-box">
                 <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                         <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                         <li class="breadcrumb-item"><a href="javascript: void(0);">eCommerce</a>
                         </li>
                         <li class="breadcrumb-item active">Product Details</li>
                     </ol>
                 </div>
                 <h4 class="page-title">Product Details</h4>
             </div>
         </div>
     </div>





     <div class="row">
         <div class="col-12">
             <div class="card">
                 <div class="card-body">
                     <div class="row">
                         <div class="col-lg-5">

                             <div class="tab-content pt-0">
                                 <div class="tab-pane show active" id="product-1-item">
                                           @forelse ($product->getMedia('inventory') as $item)
                                            <img src="{{$item->getUrl()}}" alt="" class="img-fluid mx-auto d-block rounded">
                                    @empty
                                           <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-9.jpg"
                                         alt="" class="img-fluid mx-auto d-block rounded">  
                                        @endforelse
                                   
                                 </div>
                                 <div class="tab-pane" id="product-2-item">
                                     <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-10.jpg"
                                         alt="" class="img-fluid mx-auto d-block rounded">
                                 </div>
                                 <div class="tab-pane" id="product-3-item">
                                     <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-11.jpg"
                                         alt="" class="img-fluid mx-auto d-block rounded">
                                 </div>
                                 <div class="tab-pane" id="product-4-item">
                                     <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-12.jpg"
                                         alt="" class="img-fluid mx-auto d-block rounded">
                                 </div>
                             </div>

                             <ul class="nav nav-pills nav-justified">
                                 <li class="nav-item">
                                     <a href="#product-1-item" data-bs-toggle="tab" aria-expanded="false"
                                         class="nav-link product-thumb show active">
                                         <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-9.jpg"
                                             alt="" class="img-fluid mx-auto d-block rounded">
                                     </a>
                                 </li>
                                 <li class="nav-item">
                                     <a href="#product-2-item" data-bs-toggle="tab" aria-expanded="true"
                                         class="nav-link product-thumb">
                                         <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-10.jpg"
                                             alt="" class="img-fluid mx-auto d-block rounded">
                                     </a>
                                 </li>
                                 <li class="nav-item">
                                     <a href="#product-3-item" data-bs-toggle="tab" aria-expanded="false"
                                         class="nav-link product-thumb">
                                         <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-11.jpg"
                                             alt="" class="img-fluid mx-auto d-block rounded">
                                     </a>
                                 </li>
                                 <li class="nav-item">
                                     <a href="#product-4-item" data-bs-toggle="tab" aria-expanded="false"
                                         class="nav-link product-thumb">
                                         <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-12.jpg"
                                             alt="" class="img-fluid mx-auto d-block rounded">
                                     </a>
                                 </li>
                             </ul>
                         </div> <!-- end col -->
                         <div class="col-lg-7">
                             <div class="ps-xl-3 mt-3 mt-xl-0">
                                 {{-- <a href="#" class="text-primary">{{ $product->name }}</a> --}}
                                 <h4 class="mb-3">{{ $product->name }}</h4>

                                 <h6 class="text-danger text-uppercase">20 % Off</h6>
                                 <h4 class="mb-4">Price : <span class="text-muted me-2"><del>Kes
                                             .{{ $product->crossed_price }}</del></span> <b>Kes .{{ $product->price }}
                                         USD</b></h4>
                                 <h4><span class="badge bg-soft-success text-success mb-4">Instock</span></h4>
                                 <p class="text-muted mb-4">The languages only differ in their grammar, their pronunciation
                                     and their most common words. Everyone realizes why a new common language would be
                                     desirable: one could refuse to pay expensive translators.</p>
                                 <div class="row mb-3">
                                     <div class="col-md-6">
                                         <div>
                                             <p class="text-muted"><i
                                                     class="mdi mdi-checkbox-marked-circle-outline h6 text-primary me-2"></i>
                                                 Sed ut perspiciatis unde</p>
                                             <p class="text-muted"><i
                                                     class="mdi mdi-checkbox-marked-circle-outline h6 text-primary me-2"></i>
                                                 Nemo enim ipsam voluptatem</p>
                                             <p class="text-muted"><i
                                                     class="mdi mdi-checkbox-marked-circle-outline h6 text-primary me-2"></i>
                                                 Temporibus autem quibusdam et</p>
                                         </div>
                                     </div>
                                     <div class="col-md-6">
                                         <div>
                                             <p class="text-muted"><i
                                                     class="mdi mdi-checkbox-marked-circle-outline h6 text-primary me-2"></i>
                                                 Itaque earum rerum hic</p>
                                             <p class="text-muted"><i
                                                     class="mdi mdi-checkbox-marked-circle-outline h6 text-primary me-2"></i>
                                                 Donec quam felis ultricies nec</p>
                                         </div>
                                     </div>
                                 </div>
                                 <addtocart :product="{{json_encode($product)}}"></addtocart>

                         </div>
                     </div> <!-- end col -->
                 </div>
                 <!-- end row -->



             </div>
         </div> <!-- end card-->
     </div> <!-- end col-->
     </div>

 @endsection
