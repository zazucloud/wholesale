 @extends('wholesaler.layouts.main')
 @section('content')

     <div class="row">
         <div class="col-12">
             <div class="page-title-box">
                 <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                         <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                         <li class="breadcrumb-item"><a href="javascript: void(0);">eCommerce</a>
                         </li>
                         <li class="breadcrumb-item active">Shopping Cart</li>
                     </ol>
                 </div>
                 <h4 class="page-title">Shopping Cart</h4>
             </div>
         </div>
     </div>



     <div class="row">
         <div class="col-12">
             <div class="card">
                 <div class="card-body">
                     <div class="row">
                         <div class="col-lg-8">
                             <div class="table">
                                 <table class="table table-borderless table-nowrap table-centered mb-0">
                                     <thead class="table-light">
                                         <tr>
                                             <th>Product</th>
                                             <th>Price</th>
                                             <th>Quantity</th>
                                             <th>Total</th>
                                             <th style="width: 50px;">Action</th>
                                         </tr>
                                     </thead>
                                     <form action="{{ route('wholesale.products.cartQuantity') }}" method="POST">
                                         @csrf
                                         @method('PATCH')
                                         <tbody>
                                             @foreach ($cartItems as $key => $cartItem)


                                                 <tr>
                                                     <td>
                                                         <img src="https://coderthemes.com/ubold/layouts/assets/images/products/product-1.png"
                                                             alt="contact-img" title="contact-img" class="rounded me-3"
                                                             height="48">
                                                         <p class="m-0 d-inline-block align-middle font-16">
                                                             <a href="ecommerce-product-detail.html"
                                                                 class="text-reset font-family-secondary">{{ $cartItem->inventory->name }}</a>
                                                             <br>
                                                             <small class="me-2"><b>Size:</b> Large </small>
                                                             <small><b>Color:</b> Light Green
                                                             </small>
                                                         </p>
                                                     </td>
                                                     <td>
                                                         Kes. {{ $cartItem->price }}
                                                     </td>
                                                     <td>
                                                         <input type="number" name="quantity[]" min="1"
                                                             value="{{ $cartItem->quantity }}" class="form-control"
                                                             placeholder="Qty" style="width: 90px;">
                                                         <input type="hidden" name="cartitemid[]"
                                                             value="{{ $cartItem->id }}">
                                                         <input type="hidden" name="price[]"
                                                             value="{{ $cartItem->price }}">
                                                     </td>
                                                     <td>
                                                         Kes. {{ $cartItem->total_price }}
                                                     </td>
                                                     <td>
                                                         <a  class="btn btn-danger" data-bs-toggle="modal"
                                                             data-bs-target="#delete_cartItem"
                                                             data-mycid="{{ $cartItem->id }}">del</a>
                                                         {{-- <a href="javascript:void(0);" class="action-icon"> <i class="mdi mdi-delete"></i></a> --}}
                                                     </td>
                                                 </tr>
                                             @endforeach
                                         </tbody>
                                 </table>
                             </div> <!-- end table-responsive-->

                             <!-- Add note input-->
                             {{-- <div class="mt-3">
                                                    <label for="example-textarea" class="form-label">Add a Note:</label>
                                                    <textarea class="form-control" id="example-textarea" rows="3" placeholder="Write some note.."></textarea>
                                                </div> --}}

                             <!-- action buttons-->
                             <div class="row mt-4">
                                 <div class="col-sm-6">
                                     <a href="{{ route('wholesale.products.index') }}"
                                         class="btn text-muted d-none d-sm-inline-block btn-link fw-semibold">
                                         <i class="mdi mdi-arrow-left"></i> Continue Shopping </a>
                                 </div> <!-- end col -->
                                 <div class="col-sm-6">
                                     <div class="text-sm-end">
                                         <a href="{{ route('wholesale.products.checkout') }}" class="btn btn-danger"><i class="mdi mdi-cart-plus me-1"></i> Checkout </a>
                                         {{-- <button type="submit" class="btn btn-danger">checkout</button> --}}
                                     </div>
                                 </div> <!-- end col -->
                             </div>
                             </form> <!-- end row-->
                         </div>
                         <!-- end col -->

                         <div class="col-lg-4">
                             <div class="border p-3 mt-4 mt-lg-0 rounded">
                                 <h4 class="header-title mb-3">Order Summary</h4>

                                 <div class="table-responsive">
                                     <table class="table mb-0">
                                         <tbody>
                                             <tr>
                                                 <td>Grand Total :</td>
                                                 <td>Kes .{{ $grandTotal }}</td>
                                             </tr>
                                             <tr>
                                                 <td>Discount : </td>
                                                 <td>-$157.11</td>
                                             </tr>
                                             <tr>
                                                 <td>Shipping Charge :</td>
                                                 <td>$25</td>
                                             </tr>
                                             <tr>
                                                 <td>Estimated Tax : </td>
                                                 <td>$19.22</td>
                                             </tr>
                                             <tr>
                                                 <th>Total :</th>
                                                 <th>$1458.3</th>
                                             </tr>
                                         </tbody>
                                     </table>
                                 </div>
                                 <!-- end table-responsive -->
                             </div>

                             <div class="alert alert-warning mt-3" role="alert">
                                 Use coupon code <strong>UBTF25</strong> and get 25% discount !
                             </div>

                             <div class="input-group mt-3">
                                 <input type="text" class="form-control form-control-light" placeholder="Coupon code"
                                     aria-label="Recipient's username">
                                 <button class="btn input-group-text btn-light" type="button">Apply</button>
                             </div>

                         </div> <!-- end col -->

                     </div> <!-- end row -->
                 </div> <!-- end card-body-->
             </div> <!-- end card-->
         </div> <!-- end col -->
     </div>




     <div class="modal fade" id="delete_cartItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Remove Confirmation </h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                     </button>
                 </div>
                 <div class="modal-body">
                     <form action="{{ route('wholesale.products.delete', 'cartItem') }}" method="POST">
                         {{ method_field('delete') }}
                         @csrf
                         <input type="hidden" name="cart_item_id" id="cart_item_id">
                            Are you sure you want to remove this item from the cart?
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                     <button type="Submit" class="btn btn-danger">Remove</button>
                 </div>
                 </form>
             </div>
         </div>
     </div>


  @push('extra_script')
  <script>
    $('#delete_cartItem').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)
    var cart_item_id = button.data('mycid')


    var modal = $(this)
    modal.find('.modal-body #cart_item_id').val(cart_item_id)

  })
    </script>
  @endpush
 @endsection
