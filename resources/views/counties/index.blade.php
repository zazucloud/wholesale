@extends('layouts.base')

@section('title')
    Counties
@endsection

@section('content')
    @include('partials.page-title')

    <div class="card mt-3">
        <div class="card-header">
            @include('counties.search')
        </div>
        <div class="card-body">
            <div class="spacious-container">
            <table class="table table-hover center-text">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($counties as $key => $county)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $county->name }}</td>
                            <td class="tw-flex">

                                <a href="{{ route('counties.edit', $county) }}" data-toggle="tooltip" data-placement="top"
                                    title="Edit" class="btn btn-sm btn-warning text-white mr-2">
                                    <i class="fa fa-edit"></i>
                                </a>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['counties.destroy', $county], 'style' => 'display:inline']) !!}
                                <button type="submit" class="btn btn-sm btn-danger text-white mr-2"
                                    onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                    data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
            {!! $counties->appends(Request::except('page'))->render() !!}
        </div>
    </div>
@endsection
