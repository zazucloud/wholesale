<div class="row">

    <div class="col-12 col-md-10">
        {!! Form::open(['route' => 'counties.search', 'method' => 'get', 'class' => 'row']) !!}
        <div class="col-md-10 col-12">
            <div class="">
                <label for="">Search</label>
                {!! Form::text('search', null, [
                    'class' => 'form-control py-2',
                    'required' => true,
                    'placeholder' => 'Search By Name',
                ]) !!}
            </div>
        </div>
        <div class="col-md-2 col-12 mt-md-4 pt-2">
            <div>
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm ripple']) }}

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2 col-12 mt-md-4 pt-md-2">
        <a class="btn btn-success btn-sm ripple  float-right" href="{{ route('counties.create') }}">
            <span><i class="fa fa-plus mr-2"></i> Create New</span>
        </a>
    </div>


</div>
