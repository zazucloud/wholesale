@extends('layouts.base')

@section('title')
    Create County
@endsection

@php
$theme_manager->enqueue_files(['select2', 'selectpicker', 'multiselect'], ['select2', 'selectpicker', 'multiselect']);
@endphp

@section('content')
    @include('partials.page-title')
    <div class="card mt-3">
        <div class="card-header">
            <div class="row">
                <div class="col-7"></div>
                <div class="col-3 offset-2">
                    <a class="btn btn-info btn-sm float-right" href="{{ route('counties.index') }}"><span
                            class="fa fa-arrow-left mr-2"></span> Back</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            {!! Form::open(['route' => 'counties.store', 'method' => 'POST', 'class' => 'prevent-multiple-submits']) !!}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-sm multiple-submits">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection



@section('script')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
@endsection
