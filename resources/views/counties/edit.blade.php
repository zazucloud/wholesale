@extends('layouts.base')

@section('title')
    Edit County
@endsection

@section('content')
    @include('partials.page-title')
    <div class="card mt-3">
        <div class="card-header">
            <div class="row">
                <div class="col-7"></div>
                <div class="col-3 offset-2">
                    <a class="btn btn-info btn-sm float-right" href="{{ route('zones.index') }}"><span
                            class="fa fa-arrow-left mr-2"></span> Back</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            {!! Form::model($county, [
                'method' => 'PATCH',
                'route' => ['counties.update', $county->id],
                'class' => 'prevent-multiple-submits',
            ]) !!}

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-sm multiple-submits">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
