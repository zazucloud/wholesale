<div class="row ">
    {{-- <div class="row col-sm-12">
        <div class="row col-sm-2">
        <a class="btn btn-success  float-right" href="{{ route('admin.inquiry.create') }}">
            Create Quotation
        </a>
    </div>
    </div> --}}

    {!! Form::open(['route' => 'quotation.search', 'method' => 'get', 'class' => 'row col-sm-10']) !!}

    {{-- <div class="col-sm-10 row"> --}}


    <div class="col-sm-2">
        <label>Select Client Type</label>
        <div>
            <label class="mb-0" style="display: inline-flex"> <input name="type" type="radio" value="wholesaler"
                    class="name form-control mr-2" checked> wholesaler</label>

        </div>
        <div>
            <label class="mb-0" style="display: inline-flex"><input name="type" type="radio" value="retailer"
                    class="name form-control mr-2">Customer</label>

        </div>
    </div>

    <div class="col-sm-2" id="wholesaler">
        <div class="form-group ">
            <label>Select Client</label>
            {{ Form::select('wholesaler', $wholesaler, null, ['class' => ' form-control select2', 'id' => 'clients_wholesale', 'placeholder' => 'Select Client']) }}

        </div>
    </div>

    <div class="col-sm-2" id="retailers">
        <div class="form-group ">
            <label>Select Client</label>
            {{ Form::select('retailer', $retailers, null, ['class' => 'form-control select2', 'id' => 'clients', 'placeholder' => 'Select Client']) }}

        </div>
    </div>


    <div class="col-sm-2">
        <div class="form-group">
            <label>Search</label>

            {!! Form::text('search', null, [
                'class' => 'form-control form-control-inputs',
                'name' => 'inquiryno',
                'placeholder' => 'Search Quotation number',
            ]) !!}
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label>Select User</label>
            <select class=" form-control test" name="contactperson"></select>
        </div>
    </div>


    <div class="col-sm-2">

        <div class="form-group">
            <label>Created</label>

            {!! Form::date('date', null, [
                'class' => 'form-control form-control-inputs',
                'name' => 'date',
                'placeholder' => 'Search By Order Tag',
            ]) !!}
        </div>
    </div>
    {{-- </div> --}}
    <div class="col-sm-1">
        <div class="float-right mt-4 pt-2">

            {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-info btn-sm']) }}

        </div>
    </div>
    {!! Form::close() !!}

    <div class="col-sm-2">
        <div class="mt-4 pt-2">
            <a class="btn btn-success btn-sm float-right" href="{{ route('admin.inquiry.create') }}">
                Create Quotation
            </a>
        </div>
    </div>

</div>
