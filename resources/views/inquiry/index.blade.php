@extends('layouts.base')

@section('title')
    Quotations
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">


                <div class="card mt-2">
                    <div class="card-header">
                        @include('inquiry.search.searchform')
                    </div>
                    <div class="card-body">
                        <div class="spacious-container">
                            <table class="table table-hover center-text">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Quotation no.</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">total</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Created By</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($inquiries as $index => $datum)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>
                                            <td>
                                                {{ $datum->inquiry_no }}
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.inquiry.show', $datum->id) }}"
                                                    style="color:black;">
                                                    @if ($datum->clientable_type == 'App\Models\Retailer')
                                                        <span class="badge bg-soft-success">Customer</span>
                                                    @else
                                                        <span class="badge bg-soft-primary">Wholesaler</span>
                                                    @endif
                                                </a>
                                            </td>

                                            <td>
                                                <a href="{{ route('admin.inquiry.show', $datum->id) }}"
                                                    style="color:black;">
                                                    {{ $datum->clientable->business_name ?? '' }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.inquiry.show', $datum->id) }}"
                                                    style="color:black;">
                                                    Ksh. {{ amount($datum->total_price) }}
                                                </a>
                                            </td>

                                            <td>

                                                @if ($datum->expired)
                                                    <span class="badge badge-warning">
                                                        expired
                                                    </span>
                                                @else
                                                    <span class="badge badge-success">
                                                        active
                                                    </span>
                                                @endif

                                            </td>

                                            <td>{{ stamp_date_time($datum->created_at) }}<br>
                                                <small>{{ ago($datum->created_at) }}</small>
                                            </td>
                                            <td>
                                                {{ $datum->createdBy->name ?? '' }}
                                            </td>

                                            <td class="tw-flex">
                                                <a href="{{ route('admin.inquiry.edit', ['id' => $datum->id]) }}"
                                                    class="btn btn-warning btn-sm mr-2">

                                                    <i data-toggle="tooltip" data-placement="top" title="Edit"
                                                        class="fa fa-edit"></i>
                                                </a>
                                                @if (auth()->user()->hasPermissionTo('quotation-download'))
                                                    @if ($datum->mailed == 1)
                                                        <a href="{{ route('quotation.download', ['inquiry' => $datum->id]) }}"
                                                            class="btn btn-info btn-sm mr-2"><i class="fa fa-download"
                                                                aria-hidden="true" data-toggle="tooltip"
                                                                data-placement="top" title="Download pdf"></i></a>
                                                    @endif
                                                @else
                                                    <button class="btn btn-default btn-sm mr-2" disabled>
                                                        <i class="fa fa-download" aria-hidden="true" data-toggle="tooltip"
                                                            data-placement="top" title="Download pdf"></i>
                                                    </button>
                                                @endif





                                                @if (auth()->user()->hasPermissionTo('quotation-email'))
                                                    <button class="btn btn-secondary btn-sm mr-2" data-toggle="modal"
                                                        data-target="#mailQuote" data-qid="{{ $datum->id }}"
                                                        data-mail="{{ $datum->clientable->email ?? '' }}">
                                                        <i class="fa fa-envelope" aria-hidden="true" data-toggle="tooltip"
                                                            data-placement="top" title="Send Mail"></i>
                                                    </button>
                                                @else
                                                    <button class="btn btn-default btn-sm mr-2" disabled>
                                                        <i class="fa fa-envelope" aria-hidden="true" data-toggle="tooltip"
                                                            data-placement="top" title="Send Mail"></i>
                                                    </button>
                                                @endif

                                                @if (auth()->user()->hasPermissionTo('quotation-expired'))
                                                    <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                                        data-target="#expireQuote" data-qid="{{ $datum->id }}"
                                                        data-myreason="{{ $datum->reason }}">
                                                        <i class="fa fa-stop-circle" aria-hidden="true"
                                                            data-toggle="tooltip" data-placement="top" title="Expired"></i>
                                                    </button>
                                                @else
                                                    <button class="btn btn-default btn-sm mr-2" disabled>
                                                        <i class="fa fa-stop-circle" aria-hidden="true"
                                                            data-toggle="tooltip" data-placement="top" title="Expired"></i>
                                                    </button>
                                                @endif

                                                @if (auth()->user()->hasPermissionTo('quotation-order_transfer'))
                                                    @if ($datum->transfered == null && $datum->expired == null)
                                                        <button class="btn btn-success btn-sm mr-2" data-toggle="modal"
                                                            data-target="#transferQuote" data-myid="{{ $datum->id }}"
                                                            data-wid="{{ $datum->clientable->id ?? '' }}"
                                                            data-tid="{{ $datum->template_id }}"
                                                            data-price="{{ $datum->quotations->sum('total_price') }}">

                                                            <i class="fa fa-exchange" aria-hidden="true"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="transfer to Order"></i>
                                                        </button>
                                                    @endif
                                                @else
                                                    <button class="btn btn-default btn-sm mr-2" disabled>
                                                        <i class="fa fa-exchange" aria-hidden="true" data-toggle="tooltip"
                                                            data-placement="top" title="transfer to Order"></i>
                                                    </button>
                                                @endif

                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="padding-left:20px;">
                    {!! $inquiries->appends(Request::except('page'))->render() !!}

                </div>
            </div>
        </div>
    </div>





    <!-- Modal -->
    <div class="modal fade" id="mailQuote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Quotation Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('quotation.mail', 'quote') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="inquiry_id" id="inquiry_id">

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">Customer Email :</label>
                            <div class="col-md-8">
                                <input type="email" name="customer_email" id="customer_email" class="form-control"
                                    required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label" for="l13">Select Template:</label>
                            <div class="col-md-8">
                                <select class="form-control" id="l13" name="template_id">
                                    <option value="">-- Select One --</option>
                                    @foreach ($templates as $template)
                                        <option value="{{ $template->id }}">{{ $template->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal expireQuote -->
    <div class="modal fade" id="expireQuote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Expired Quotation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('expired.quotation', 'expiry') }}" method="POST">
                    @method('PATCH')
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="inquiry_id" id="inquiry">


                        <p style=" font-size: 16px;">Are you sure you this quotation in expired ?</p>

                        <div class="form-group">
                            <label>Reason</label>
                            <textarea class="form-control" name="reason" id="reason" rows="3" required></textarea>
                        </div>


                    </div> <button class="btn btn-default btn-sm mr-2" disabled>
                        <i class="fa fa-stop-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top"
                            title="Expired"></i>
                    </button>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-primary">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal transferQuote -->
    <div class="modal fade" id="transferQuote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Transfer Quotation To An Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('quotation.order') }}" method="POST" id="qutationorderform"
                    class="prevent-multiple-submits">
                    <div class="modal-body">
                        @csrf
                        {{-- <input type="hidden" name="inquiry_id" id="inquiry_idd"> --}}
                        <input type="hidden" name="inquiry_id" id="inquiry_idd">
                        <input type="hidden" name="wholesaler_id" id="wholesaler_id">
                        <input type="hidden" name="template_id" id="template_id">
                        <input type="hidden" name="total" id="total">






                        <div class="form-group">
                            <label>Delivery Date</label>
                            <input type="date" name="delivery_date" class="form-control" required>
                        </div>

                        {{-- <div class="form-group">
                            <label>ATTACHMENT EG. LPO</label>
                            <input type="file" name="attachment" class="form-control" required >
                        </div> --}}

                        <div class="form-group">
                            <label>Comment</label>
                            <textarea class="form-control" name="comment" rows="3" required></textarea>
                        </div>




                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary multiple-submits"
                            @click='formSubmittionCheck("qutationorderform")'>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $("#retailers").hide();


            $('input[type=radio][name=type]').change(function() {
                if (this.value == 'wholesaler') {
                    $("#wholesaler").show();
                    $("#retailers").hide();
                } else if (this.value == 'retailer') {
                    $("#wholesaler").hide();
                    $("#retailers").show();
                }
            });

        });
    </script>


    <script>
        $('#mailQuote').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var inquiry_id = button.data('qid')
            var customer_email = button.data('mail')

            var modal = $(this)
            modal.find('.modal-body #inquiry_id').val(inquiry_id)
            modal.find('.modal-body #customer_email').val(customer_email)
        })
    </script>

    <script>
        $('#expireQuote').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var inquiry = button.data('qid')
            var reason = button.data('myreason')

            var modal = $(this)
            modal.find('.modal-body #inquiry').val(inquiry)
            modal.find('.modal-body #reason').val(reason)
        })
    </script>

    <script>
        $(document).ready(function() {

            $('.test').select2({
                placeholder: 'Created By...',
                allowClear: true,
                ajax: {
                    url: '/admin/users/ajax/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });
        });
    </script>

    <script>
        $('#transferQuote').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var inquiry_id = button.data('myid')
            var wholesaler_id = button.data('wid')
            var template_id = button.data('tid')
            var total_price = button.data('price')
            // var comment = button.data('comment')

            var modal = $(this)
            modal.find('.modal-body #inquiry_idd').val(inquiry_id)
            modal.find('.modal-body #wholesaler_id').val(wholesaler_id)
            modal.find('.modal-body #template_id').val(template_id)
            modal.find('.modal-body #total').val(total_price)

            $.ajax({
                url: "/admin/quotations/retrieve/" + inquiry_id,
                type: "get",
                success: function(response) {
                    for (let index = 0; index < response.items.length; index++) {
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'inventory_id[]',
                            value: response.items[index].inventory_id
                        }).appendTo('#qutationorderform');

                        $('<input>').attr({
                            type: 'hidden',
                            name: 'inventory_quantity[]',
                            value: response.items[index].quantity
                        }).appendTo('#qutationorderform');
                    }
                },

                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });

            // modal.finc('.modal-body #comment').val(comment)
        })
    </script>
@endsection
