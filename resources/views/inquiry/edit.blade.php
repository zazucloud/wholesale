@extends('layouts.base')

@section('title')Edit Quotation @endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
    @include('partials.page-title')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">

                            </div>
                            <div class="offset-2 col-2">
                                <a class="btn btn-rounded btn-outline-primary" href="{{ route('admin.inquiry.index') }}">
                                    <span class="fas fa-arrow-left"></span> Go Back
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="card-body">
                        {!! Form::model($inquiry, ['route' => 'admin.inquiry.update', 'class' => 'prevent-multiple-submits', 'method' => 'post']) !!}

                        @csrf
                        <input type="hidden" name="id" value="{{ $inquiry->id }}">

                        <div class="form-group row mx-0">
                            <label class="col-3 mb-3">
                                <input name="type" type="radio" value="wholesaler"
                                    class="name" id="ws"
                                    {{ $inquiry->clientable_type == 'App\Models\Wholesale\Wholesaler' ? 'checked' : '' }}>

                                wholesaler</label>
                            <label class="col-3 mb-3"><input name="type" type="radio" value="retailer"
                                    class="name" id="rt"
                                    {{ $inquiry->clientable_type == 'App\Models\Retailer' ? 'checked' : '' }}>

                                    Customer</label>
                        </div>

                        {{-- <div class="form-group row">
                            <div class="form-group col-12 col-md-12">
                                <div class="col-sm-12">
                                    <label>Client</label>
                                    {{ Form::select('wholesaler_id', $wholesalers, null, ['class' => 'table-group-action-input form-control input-medium', 'id' => 'clients', 'data-toggle' => 'select2', 'placeholder' => 'Select Client']) }}

                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group row" id="wholesaler">
                            <div class="form-group col-12 col-md-12">
                                <div class="col-sm-12">
                                    <label class="form-control-label">Client</label>
                                    <select class="m-b-10 form-control select2-hidden-accessible" name="wholesaler"
                                        data-toggle="select2" tabindex="-1" aria-hidden="true" >
                                        {{-- <option ></option> --}}
                                        @foreach ($wholesalers as $wholesaler)
                                            <option
                                                value="{{ $wholesaler->id  }}" {{ $inquiry->clientable->id == $wholesaler->id ? 'selected="selected"' : '' }}>
                                                {{ $wholesaler->business_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="retailers">
                            <div class="form-group col-12 col-md-12">
                                <div class="col-sm-12">
                                    <label class="form-control-label">Client</label>
                                    <select class="m-b-10 form-control select2-hidden-accessible" name="retailers"
                                        data-toggle="select2" tabindex="-1" aria-hidden="true" >
                                        {{-- <option ></option> --}}
                                        @foreach ($retailers as $retailer)
                                            <option
                                                value="{{ $retailer->id }}" {{ $inquiry->clientable->id == $retailer->id ? 'selected="selected"' : '' }}>
                                                {{ $retailer->business_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <b class="mx-3">Additional Quotation Details</b>
                        <div class="form-group row" id="retailers">
                            <div class="form-group col-12 col-md-4">
                                <div class="col-sm-12">
                                    <label>Customer Reference</label>
                                    {{ Form::textarea('customer_reference', null, ['class' => 'form-control py-2', 'rows'=>1]) }}

                                </div>
                            </div>

                            <div class="form-group col-12 col-md-4">
                                <div class="col-sm-12">
                                    <label>Notes</label>
                                    {{ Form::textarea('note', null, ['class' => 'form-control py-2', 'rows'=>1]) }}

                                </div>
                            </div>

                            <div class="form-group col-12 col-md-4">
                                <div class="col-sm-12">
                                    <label>Validity</label>
                                    {{ Form::textarea('validity', null, ['class' => 'form-control py-2', 'rows'=>1]) }}

                                </div>
                            </div>

                            <div class="form-group col-12 col-md-4">
                                <div class="col-sm-12">
                                    <label>Terms</label>
                                    {{ Form::textarea('terms', null, ['class' => 'form-control py-2', 'rows'=>1]) }}

                                </div>
                            </div>

                            <div class="form-group col-12 col-md-4">
                                <div class="col-sm-12">
                                    <label>Delivery</label>
                                    {{ Form::textarea('delivery', null, ['class' => 'form-control py-2', 'rows'=>1]) }}

                                </div>
                            </div>
                        </div>

                        <inventoryquotationdropdown :items="{{ json_encode($inquiry->quotations) }}">
                        </inventoryquotationdropdown>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label>Select Template:</label>
                                {{ Form::select('template_id', $templates, null, ['class' => 'form-control', 'id' => 'l13', 'placeholder' => 'Select One']) }}

                            </div>
                        </div>

                        <div class="form-group row mx-0">
                            <div class="col-md-6 center">
                                {{ Form::button('Update', ['type' => 'submit', 'class' => 'btn btn-primary multiple-submits']) }}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>



                </div>
            </div>
        </div>
    </div>

@endsection

@section('extrascripts')
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            // $("#retailers").hide();

            if ($("#ws").is(':checked')) {
                $("#wholesaler").show();
                $("#retailers").hide();
            } else if ($("#rt").is(':checked')) {
                $("#wholesaler").hide();
                $("#retailers").show();
            }

            $('[data-toggle="select2"]').select2();

            $('input[type=radio][name=type]').change(function() {
                if (this.value == 'wholesaler') {
                    $("#wholesaler").show();
                    $("#retailers").hide();
                } else if (this.value == 'retailer') {
                    $("#wholesaler").hide();
                    $("#retailers").show();
                }
            });

            $('.test').select2({
                placeholder: 'inventory...',
                allowClear: true,
                ajax: {
                    url: '/admin/inventory/ajax/getinventory/dropdown',
                    dataType: 'json',
                    delay: 300,
                    processResults: function(data) {
                        return {
                            results: $.map(data.results, function(item) {
                                return {
                                    text: item.name + ' (' + item.sku + ')',
                                    id: item.id
                                }
                            }),
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },

                    cache: true
                }
            });
        });
    </script>
@endsection
