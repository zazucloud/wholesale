@extends('layouts.base')

@section('title') Quotation @endsection

@section('content')
    @include('partials.page-title')



    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">



                <div class="card mt-3">

                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">

                            </div>
                            <div class="col-4 tw-flex tw-justify-end">
                                <a class="btn btn-sm btn-info" href="{{route('admin.inquiry.index')}}">
                                    <span class="fa fa-arrow-left mr-2"></span> Go Back
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="card-body"><span class="badge bg-soft-info text-info tw-text-sm mb-3">Quoted
                            Items</span>
                        <table class="table table-hover center-text">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" class="tw-w-40"> Item</th>
                                    <th scope="col"> Quantity</th>
                                    <th scope="col"> OriginalPrice</th>
                                    <th scope="col"> Price</th>
                                    <th scope="col"> Dsc(%)</th>
                                    <th scope="col"> VAT</th>
                                    <th scope="col"> Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($inquiry->quotations as $key => $quotation)
                                <tr>
                                    <th scope="row">{{ ++$key }}</th>
                                    <td>{{ $quotation->inventory->name }} </td>
                                    <td>{{ $quotation->quantity }}</td>
                                    <td>{{currency($quotation->price)}}  </td>
                                    <td>  {{ currency($quotation->disc_price) }}</td>
                                    <td>{{ $quotation->discount_percent }}</td>
                                    <td> {{ $quotation->vat_percent }}</td>
                                    <td> {{ currency((vat_amount($quotation->disc_price, $quotation->vat_percent)+$quotation->disc_price)*$quotation->quantity) }}</td>

                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection

@section('extrascripts')

@endsection
