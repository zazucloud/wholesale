<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">

    <!-- App css -->
    {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> --}}
    <!-- Latest compiled and minified CSS -->
    {{-- <link href=" {{ asset('assets/css/icons.min.css ') }} " rel="stylesheet" type="text/css" />

    <link href=" {{ asset('assets/css/app.min.css ') }} " rel="stylesheet" type="text/css" />
    <link href=" {{ asset('css/app.css ') }} " rel="stylesheet" type="text/css" />
    <link href=" {{ asset('css/minibar.min.css ') }}  " rel="stylesheet" type="text/css" /> --}}

    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}

    <style>
        /*! CSS Used from: http://127.0.0.1:8000/assets/css/app.min.css */
        table {
            page-break-inside: auto !important;
            border-collapse: collapse !important;
        }

        tr {
            page-break-inside: avoid !important;
            page-break-after: auto !important;
        }

        thead {
            display: table-header-group !important;
        }

        tfoot {
            display: table-footer-group !important;
        }

        th {
            text-align: inherit !important;
        }

        html {
            position: relative;
            min-height: 100%;
        }

        body {
            padding-bottom: 60px;
        }

        .tbcust>tbody>tr>td {
            border: none !important;
        }

        .text-center {
            text-align: center;
        }

        @media (max-width:767.98px) {
            body {
                overflow-x: hidden;
                padding-bottom: 80px;
            }
        }

        @media print {
            body {
                padding: 0;
                margin: 0;
            }
        }

        /*! CSS Used from: http://127.0.0.1:8000/css/app.css */
        body {
            background-color: #fff !important;
        }

        .card-box {
            background-color: #fff;
            padding: 1.5rem;
            box-shadow: 0 0.75rem 6rem rgba(56, 65, 74,
                    0.03);
            margin-bottom: 24px;
            border-radius: 0.25rem;
        }

        :root {
            --blue: #3490dc;
            --indigo: #6574cd;
            --purple: #9561e2;
            --pink: #f66d9b;
            --red: #e3342f;
            --orange: #f6993f;
            --yellow: #ffed4a;
            --green: #38c172;
            --teal: #4dc0b5;
            --cyan: #6cb2eb;
            --white: #fff;
            --gray: #6c757d;
            --gray-dark: #343a40;
            --primary: #3490dc;
            --secondary: #6c757d;
            --success: #38c172;
            --info: #6cb2eb;
            --warning: #ffed4a;
            --danger: #e3342f;
            --light: #f8f9fa;
            --dark: #343a40;
            --breakpoint-xs: 0;
            --breakpoint-sm: 576px;
            --breakpoint-md: 768px;
            --breakpoint-lg: 992px;
            --breakpoint-xl: 1200px;
            --font-family-sans-serif: "Nunito",
                sans-serif;
            --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New",
                monospace;
        }

        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0,
                    0);
        }

        body {
            margin: 0;
            font-family: "Nunito",
                sans-serif;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }

        h3,
        h4,
        h6 {
            margin-top: 0;
            margin-bottom: 0.5rem;
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
        }

        abbr[title] {
            text-decoration: underline;
            -webkit-text-decoration: underline dotted;
            text-decoration: underline dotted;
            cursor: help;
            border-bottom: 0;
            -webkit-text-decoration-skip-ink: none;
            text-decoration-skip-ink: none;
        }

        address {
            margin-bottom: 1rem;
            font-style: normal;
            line-height: inherit;
        }

        b,
        strong {
            font-weight: bolder;
        }

        small {
            font-size: 80%;
        }

        img {
            vertical-align: middle;
            border-style: none;
        }

        table {
            border-collapse: collapse;
        }

        th {
            text-align: inherit;
        }

        h3,
        h4,
        h6 {
            margin-bottom: 0.5rem;
            font-weight: 500;
            line-height: 1.2;
        }

        h3 {
            font-size: 1.575rem;
        }

        h4 {
            font-size: 1.35rem;
        }

        h6 {
            font-size: 0.9rem;
        }

        small {
            font-size: 80%;
            font-weight: 400;
        }

        .container-fluid {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .row {
            display: -webkit-box;
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        .col-12 {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }

        .col-12 {
            -webkit-box-flex: 0;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .offset-md-3 {
            margin-left: 20.6666666667%;
        }

        .offset-md-8 {
            margin-left: 80.6666666667%;
        }

        @media (min-width: 768px) {
            .offset-md-2 {
                margin-left: 30.6666666667%;
            }
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }

        .tableinner {
            width: 100%;
            color: #212529;
        }

        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
            border-top: 2px solid grey;
        }

        .badge {
            display: inline-block;
            padding: 0.25em 0.4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25rem;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out;
        }

        @media (prefers-reduced-motion: reduce) {
            .badge {
                -webkit-transition: none;
                transition: none;
            }
        }

        .badge:empty {
            display: none;
        }

        .badge-danger {
            color: #fff;
            background-color: #e3342f;
        }

        .clearfix::after {
            display: block;
            clear: both;
            content: "";
        }

        .float-left {
            float: left !important;
        }

        .float-right {
            float: right !important;
        }

        .m-0 {
            margin: 0 !important;
        }

        .mt-3 {
            margin-top: 1rem !important;
        }

        .mt-4 {
            margin-top: 1.5rem !important;
        }

        .pt-5 {
            padding-top: 3rem !important;
        }

        .text-right {
            text-align: right !important;
        }

        .font-weight-bolder {
            font-weight: bolder !important;
        }

        .text-muted {
            color: #6c757d !important;
        }

        @media print {

            *,
            *::before,
            *::after {
                text-shadow: none !important;
                box-shadow: none !important;
            }

            abbr[title]::after {
                content: " ("attr(title) ")";
            }

            thead {
                display: table-header-group;
            }

            tr,
            img {
                page-break-inside: avoid;
            }

            p,
            h3 {
                orphans: 3;
                widows: 3;
            }

            h3 {
                page-break-after: avoid;
            }

            body {
                min-width: 992px !important;
            }

            .badge {
                border: 1px solid #000;
            }

            .table {
                border-collapse: collapse !important;
            }

            .table td,
            .table th {
                background-color: #fff !important;
            }

        }

        /*! CSS Used fontfaces */
        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIOOaBXso.woff2) format('woff2');
            unicode-range: U+0460-052F,
                U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIMeaBXso.woff2) format('woff2');
            unicode-range: U+0400-045F,
                U+0490-0491, U+04B0-04B1, U+2116;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIOuaBXso.woff2) format('woff2');
            unicode-range: U+0102-0103,
                U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIO-aBXso.woff2) format('woff2');
            unicode-range: U+0100-024F,
                U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofINeaB.woff2) format('woff2');
            unicode-range: U+0000-00FF,
                U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212,
                U+2215, U+FEFF, U+FFFD;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito SemiBold'),
                local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofA6sKUbOvISTs.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito SemiBold'),
                local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofA6sKUZevISTs.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito SemiBold'),
                local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofA6sKUbuvISTs.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9,
                U+20AB;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito SemiBold'),
                local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofA6sKUb-vISTs.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F,
                U+A720-A7FF;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito SemiBold'),
                local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofA6sKUYevI.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F,
                U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Bold'), local('Nunito-Bold'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAjsOUbOvISTs.woff2) format('woff2');
            unicode-range: U+0460-052F,
                U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Bold'), local('Nunito-Bold'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAjsOUZevISTs.woff2) format('woff2');
            unicode-range: U+0400-045F,
                U+0490-0491, U+04B0-04B1, U+2116;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Bold'), local('Nunito-Bold'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAjsOUbuvISTs.woff2) format('woff2');
            unicode-range: U+0102-0103,
                U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Bold'), local('Nunito-Bold'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAjsOUb-vISTs.woff2) format('woff2');
            unicode-range: U+0100-024F,
                U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Bold'), local('Nunito-Bold'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAjsOUYevI.woff2) format('woff2');
            unicode-range: U+0000-00FF,
                U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212,
                U+2215, U+FEFF, U+FFFD;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 900;
            src: local('Nunito Black'), local('Nunito-Black'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAtsGUbOvISTs.woff2) format('woff2');
            unicode-range: U+0460-052F,
                U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 900;
            src: local('Nunito Black'), local('Nunito-Black'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAtsGUZevISTs.woff2) format('woff2');
            unicode-range: U+0400-045F,
                U+0490-0491, U+04B0-04B1, U+2116;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 900;
            src: local('Nunito Black'), local('Nunito-Black'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAtsGUbuvISTs.woff2) format('woff2');
            unicode-range: U+0102-0103,
                U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 900;
            src: local('Nunito Black'), local('Nunito-Black'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAtsGUb-vISTs.woff2) format('woff2');
            unicode-range: U+0100-024F,
                U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 900;
            src: local('Nunito Black'), local('Nunito-Black'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXW3I6Li01BKofAtsGUYevI.woff2) format('woff2');
            unicode-range: U+0000-00FF,
                U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212,
                U+2215, U+FEFF, U+FFFD;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIOOaBXso.woff2) format('woff2');
            unicode-range: U+0460-052F,
                U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIMeaBXso.woff2) format('woff2');
            unicode-range: U+0400-045F,
                U+0490-0491, U+04B0-04B1, U+2116;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIOuaBXso.woff2) format('woff2');
            unicode-range: U+0102-0103,
                U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofIO-aBXso.woff2) format('woff2');
            unicode-range: U+0100-024F,
                U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        @font-face {
            font-family: 'Nunito';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Regular'), local('Nunito-Regular'),
                url(https://fonts.gstatic.com/s/nunito/v12/XRXV3I6Li01BKofINeaB.woff2) format('woff2');
            unicode-range: U+0000-00FF,
                U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212,
                U+2215, U+FEFF, U+FFFD;
        }
    </style>
</head>


<body>
    <div class="mt-3 container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <!-- Logo & title -->

                    <br>
                    <div>
                        <table class="tableinner">
                            <tr>
                                <td style="width:33.333333%;">

                                    <h5 class="m-0 font-weight-bolder" style="font-size: 16px;">Order Sheet</h5>

                                </td>
                                <td style="width:33.333333%;">
                                    <p class="m-b-5" style="text-align: center;"><strong>Order No: </strong> <span>
                                            <b>{{ $order->face_id ?? '' }}</b> </span>
                                    </p>

                                </td>
                                <td style="width:33.333333%;">
                                    <p class="float-right m-b-5"><strong>SM: </strong> <span>
                                            <b>{{ wordwrap($order->createdBy->smcode ?? 'N/A', 25) }}</b> </span>
                                    </p>

                                </td>
                            </tr>
                        </table>
                    </div>

                    <hr style="border: 1px solid grey;">
                    <div class="grid grid-cols-3">
                        <table class="table tbcust">
                            <tbody>
                                <tr>
                                    <table class="tableinner" style="margin-bottom:1rem;">
                                        <tr>
                                            <td style="width:100%;">
                                                <div>
                                                    <div>
                                                        <p>
                                                        <div class="">
                                                            <p class="m-b-5"><strong
                                                                    style="margin-right: 10px;">Customer Code
                                                                    :
                                                                </strong> <span>
                                                                    {{ $order->clientable->registration_number ?? 'N/A' }}</span>
                                                            </p>
                                                            <p class="m-b-5"><strong>Customer : </strong> <span>
                                                                    {{ wordwrap($order->clientable->business_name, 25) }}
                                                                </span></p>
                                                            <p class="m-b-5"><strong style="margin-right: 10px;">Phone
                                                                    :
                                                                </strong>
                                                                <span><span>{{ $order->clientable->telephone ?? 'N/A' }}</span></span>
                                                            </p>
                                                            <p class="m-b-5"><strong style="margin-right: 10px;">Email
                                                                    :
                                                                </strong> <span>
                                                                    {{ $order->clientable->email ?? 'N/A' }}</span>
                                                            </p>

                                                        </div>
                                                        </p>
                                                        <br>

                                                    </div>

                                                </div>
                                            </td>
                                            <td style="width:0%;">



                                            </td>
                                        </tr>
                                    </table>
                                </tr>
                                <tr>
                                    <td style="width:100%;" colspan="2">
                                        <div class="">
                                            <div class="table-responsive">
                                                <table class="table mt-4 table-centered"
                                                    style="border-bottom: 2px solid grey;">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Item</th>
                                                            <th style="width: 12%">Qty</th>
                                                            <th style="width: 12%">Base Price</th>
                                                            <th style="width: 12%">Dsc %</th>
                                                            <th style="width: 12%">Price</th>
                                                            <th style="width: 12%">VAT</th>
                                                            <th style="width: 12%">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($order->items as $key => $item)
                                                            @php
                                                                $item_vat_amount = $item->price / ($item->vat_percent / 100 + 1);
                                                                $vat_amounts[] = $item_vat_amount;

                                                                // Less tax
                                                                $net_item_totals[] = $item_vat_amount * $item->quantity;
                                                            @endphp
                                                            <tr>
                                                                <td>{{ $key + 1 }}</td>
                                                                <td>
                                                                    <b>
                                                                        {{ $item->inventory->sku }}
                                                                    </b>
                                                                    <div>
                                                                        {{ $item->inventory->name }}
                                                                    </div>
                                                                </td>
                                                                <td> {{ number_format($item->quantity) }}</td>

                                                                <td>Ksh.{{ amount($item->original_price) }}</td>
                                                                <td>{{ $item->discount_percent }}</td>
                                                                <td>Ksh.{{ amount($item->price) }}</td>
                                                                <td>{{ $item->vat_percent }}</td>
                                                                <td> Ksh.{{ amount($item->item_total) }}</td>
                                                            </tr>
                                                            @if (!is_null($item->note))
                                                                <tr>
                                                                    <td colspan="7"
                                                                        style=" white-space: nowrap; padding:5px 15px;">
                                                                        <b>Comment : </b>{{ $item->note }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach


                                                        <tr>
                                                            <td colspan="7">
                                                                <table style="border: none;">
                                                                    {{-- <thead>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                    </thead> --}}
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="2"
                                                                                style="border: 2px solid;padding:6px;">
                                                                                <div class="">
                                                                                    <div>
                                                                                        <p><b>Sub Total:</b> <span>
                                                                                                &nbsp;&nbsp;&nbsp;
                                                                                                Ksh.{{ amount(array_sum($net_item_totals)) }}</span>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </td>
                                                                            <td colspan="2"
                                                                                style="border: 2px solid;padding:6px;">
                                                                                <div class="">
                                                                                    <div>
                                                                                        <p><b>Total VAT:</b> <span>
                                                                                                &nbsp;&nbsp;&nbsp;
                                                                                                Ksh.{{ amount($order->total - array_sum($net_item_totals)) }}</span>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </td>
                                                                            <td colspan="2"
                                                                                style="border: 2px solid;padding:6px;">
                                                                                <div class="">
                                                                                    <div>
                                                                                        <p><b>Invoice Total:</b> <span>
                                                                                                &nbsp;&nbsp;&nbsp;
                                                                                                <b>Ksh.{{ amount($order->total) }}</b></span>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> <!-- end table-responsive -->
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width:100%;">
                                        <div class="">
                                            <div class="clearfix">
                                                <table class="tableinner">
                                                    <tr>
                                                        <td style="width:100%;" class="text-center">
                                                            <p class="text-muted">
                                                                Generated: {{ stamp_date_time_now_full() }} <br>
                                                                End of Document
                                                            </p>
                                                        </td>

                                                    </tr>
                                                </table>

                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                        <!-- end col -->
                        <!-- end col -->
                    </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container -->
</body>

</html>
