<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<table>
    <tr>
        <td>Wholesaler Name</td>
        <td>Business Registration Number </td>
        @foreach($inventory_list as $inventory)
            <td>{{ $inventory->sku }}</td>
        @endforeach
    </tr>

    @foreach($wholesalers as $wholesaler)
        <tr class="this_class">
            <td>{{$wholesaler->business_name}}</td>
            <td>{{$wholesaler->registration_number}}</td>
            @foreach($inventory_list as $inventory)
                <td>{{ item_price_via_ids($wholesaler->id,$inventory->id)}}</td>
            @endforeach
        </tr>
    @endforeach
</table>
</html>