<table>
    <thead style="text-align: center;">
        <tr>

            <th style="width: 11px;word-wrap: break-word; height:50px;font-size:8px;"><b
                    style="margin:5px;">Wholesaler</b></th>
            <th style="width: 11px;word-wrap: break-word; height:50px;font-size:8px;"><b
                    style="margin:5px;">Category</b></th>
            <th style="width: 11px;word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Category
                    Visibility</b></th>
            <th style="width: 11px;word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Percentage
                    Discount (%)</b></th>
            <th style="width: 11px;word-wrap: break-word; height:50px;font-size:8px;"><b style="margin:5px;">Rcd_id</b>
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach ($results as $value)
            <tr>
                <td style="font-size: 8px;text-align:right;">{{ $value['wholersaler'] }}</td>
                <td style="font-size: 8px;text-align:right;">{{ $value['category'] }}</td>
                <td style="font-size: 8px;text-align:right;">{{ $value['category_visibility'] }}</td>
                <td style="font-size: 8px;text-align:right;">{{ $value['percentage'] }}</td>
                <td style="font-size: 8px;text-align:right;">{{ $value['rcd_id'] }}</td>

            </tr>

        @endforeach
    </tbody>
    <tr></tr>
    <tr></tr>
</table>
