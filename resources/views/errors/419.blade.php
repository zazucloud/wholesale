@extends('layouts.errors')

@section('title')
  Error 419
@endsection

@php
  $theme_manager->replace_body_class('body-bg-full error-page error-404')
@endphp

@section('content')
	<body{{ $theme_manager->body_classes() }} style="background-image: url({{asset('assets/img/site-bg.jpg')}});">
		<div id="wrapper" class="wrapper">
			<div class="content-wrapper">

				<main class="main-wrapper">
					<h1 class="color-white mb-0">419</h1>

					<h3 class="mr-b-5 color-white mt-0">Session Has Expired !</h3>
				</main>

			</div><!-- .content-wrapper -->
		</div><!-- .wrapper -->

	</body>
</html>
@endsection
