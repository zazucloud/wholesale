@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    Roles Management
@endsection
<div class="card mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-7">

            </div>
            <div class="col-3 offset-2">
                <a class="btn btn-success btn-sm ripple" href="{{ route('roles.create') }}">
                    <span><i class="fa fa-plus mr-2"></i> Create New</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card-body">

        <table class="table table-hover center-text">

            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
            </tr>

            @foreach ($roles as $key => $role)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $role->name }}</td>
                    <td class="d-flex">
                        <a href="{{ route('roles.show', $role->id) }}" data-toggle="tooltip" data-placement="top"
                            title="View" class="btn btn-info btn-sm mr-2">
                            <i class="fa fa-eye"></i>
                        </a>

                        @can('roles-edit')
                            <a href="{{ route('roles.edit', $role->id) }}" data-toggle="tooltip" data-placement="top"
                                title="Edit" class="btn btn-warning btn-sm mr-2">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endcan

                        @can('roles-delete')
                            <form method="post" action="{{ route('roles.destroy', $role->id) }}">
                                @csrf
                                @method('DELETE')

                                <a type="submit" onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                    data-toggle="tooltip" data-placement="top" title="Delete"
                                    class="btn btn-danger btn-sm mr-2 text-white">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        @endcan

                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

{!! $roles->appends(Request::except('page'))->render() !!}

@endsection
