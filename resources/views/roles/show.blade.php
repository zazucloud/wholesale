@extends('layouts.base')

@section('title')
    Permissions
@endsection

@section('content')

    @include('partials.page-title')
    <br>

    <div class="container">

        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h6> Show Roles</h6>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-info btn-sm" href="{{ route('roles.index') }}"><span
                                        class="fa fa-arrow-left mr-2"></span> Go Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                                <p class="text-info">
                                    <strong>Name:</strong> {{ strtoupper($role->name) }}
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">

                                <strong>Permissions</strong>

                                <br />
                                <ul class="list-group">
                                    @foreach ($permission as $key => $value)
                                        <li class="list-group-item">

                                            <div class="row">
                                                <label class="col-4">{{ $key }}</label>
                                                <div class="col-8 row">
                                                    @foreach ($value as $item)
                                                        <label class="col-4">

                                                            {{ $item->extracted }}</label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card-body -->
            </div>
            <!-- end card -->
        </div>
    </div>

@endsection
