@extends('layouts.base')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10 mt-5">
            <div class="card">
                <div class="card-header"><strong>No Permissions</strong></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-6 center-text">
                            <img src="{{asset('images/shield.png')}}" alt="" style="width:50%;">

                        </div>
                        <div class="col-6">
                            <ol>
                                <li>
                                    <strong>
                                        You Are Unauthorized to View or Perform Operations.
                                    </strong>
                                </li>
                                <li>
                                    <strong>
                                        Login As Required User.
                                    </strong>
                                </li>
                                <li>
                                    <strong>
                                        Contact System Administrator For Any Queries
                                    </strong>
                                </li>
                            </ol>
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-sm btn-info" href="{{ URL::previous() }}">
        <span class="fa fa-arrow-left mr-2"></span>
    </a>
</div>
@endsection
