@extends('layouts.settings')

@section('settingscontent')
@section('settingstitle')
    User Management
@endsection

<br>

<div class="container">

    <div class="card">

        <div class="card-header">
            <div class="row">
                <div class="col-lg-12 margin-tb">

                    @include('users.search')

                </div>
            </div>
        </div>

        <div class="card-body">
            <br>
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <br>
            <div class="spacious-container">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Sales Code</th>
                            <th>Roles</th>
                            <th>Action</th>
                        </tr>

                        @foreach ($data as $key => $user)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->smcode ?? 'N/A' }}</td>
                                <td>
                                    @if (!empty($user->getRoleNames()))
                                        @foreach ($user->getRoleNames() as $v)
                                            <label class="badge badge-success">{{ $v }}</label>
                                        @endforeach
                                    @endif
                                </td>

                                <td class="tw-flex">

                                    @if (auth()->user()->hasPermissionTo('users-edit'))
                                        <a href="{{ route('users.edit', $user->id) }}" data-toggle="tooltip"
                                            data-placement="top" title="Edit"
                                            class="btn btn-sm btn-warning text-white mr-2">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    @else
                                        <button class="btntable disonabled btn btn-sm btn-dark mr-2">
                                            <i class="text-white fa fa-edit"></i>
                                        </button>
                                    @endif

                                    @if (auth()->user()->hasPermissionTo('users-delete'))
                                        <form method="post" action="{{ route('users.destroy', $user->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger text-white mr-2"
                                                onclick="return confirm('Are you sure You Want to Delete This Record?')"
                                                data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    @else
                                        <button class="btntable disonabled btn btn-sm btn-dark mr-2">
                                            <i class="text-white fa fa-trash"></i>
                                        </button>
                                    @endif

                                    @if (auth()->user()->hasPermissionTo('users-disable'))
                                        @if ($user->disabled == 0)
                                            <form method="post" action="{{ route('users.disable') }}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                <button type="submit" class="btn btn-sm btn-info text-white mr-2"
                                                    onclick="return confirm('Are you sure You Want to Disable This Account?')"
                                                    data-toggle="tooltip" data-placement="top" title="Disable">

                                                    <i class="fa fa-ban"></i>
                                                </button>
                                            </form>
                                        @else
                                            <form method="post" action="{{ route('users.enable') }}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                <button type="submit" class="btn btn-sm btn-info text-white mr-2"
                                                    onclick="return confirm('Are you sure You Want to Enable This Account?')"
                                                    data-toggle="tooltip" data-placement="top" title="Enable">
                                                    <i class="fa fa-shield"></i>
                                                </button>
                                            </form>
                                        @endif
                                    @else
                                        <button class="btntable disonabled btn btn-sm btn-dark mr-2">
                                            <i class="text-white fa fa-shield"></i>
                                        </button>
                                    @endif
                                </td>

                            </tr>
                    </tbody>
                    @endforeach
                </table>

                {!! $data->appends(Request::except('page'))->render() !!}
            </div>
        </div>

    </div>

</div>



@endsection
