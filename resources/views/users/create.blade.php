@extends('layouts.base')
@section('title')
    Add New User
@endsection
@section('content')
    @include('partials.page-title')
    <br>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-8">
                        <div class="pull-left">
                            <h6>Create New User</h6>
                        </div>

                    </div>
                    <div class="col-4">
                        <a class="btn btn-info btn-sm float-right" href="{{ route('users.index') }}"><span
                                class="fa fa-arrow-left mr-2"></span> Back</a>
                    </div>
                </div>


            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'class' => 'prevent-multiple-submits']) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Email:</strong>
                            {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Phone:</strong>
                            {!! Form::text('phone', null, ['placeholder' => 'Phone', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Smode:</strong>
                            {!! Form::text('smcode', null, ['placeholder' => 'Enter Sales Code', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Role:</strong>
                            {!! Form::select('roles[]', $roles, [], ['class' => 'form-control', 'multiple']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-sm multiple-submits">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>





    </div>
@endsection
