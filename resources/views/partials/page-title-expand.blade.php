<!-- Page Title Area -->
<div class="page-title-expand">
  <div class="{{ $theme_manager->get_container_class() }}">
    <div class="row">
      <div>
        <h4 class="my-1">Hi, welcome back!</h4>
        <p class="mb-0">Last login was 23 hours ago. <a href="#">View details</a></p>
      </div>

      <div class="flex-1"></div>

      <div class="d-none d-xl-inline-flex align-items-center w-50 mr-r-40">
        <div class="icon-box icon-box-side flex-1 mr-3 my-0">
          <header class="mx-3">
            <a href="#" class="bg-grey fs-30 text-muted"><i class="lnr lnr-rocket icon-lg"></i></a>
          </header>
          <section>
            <h6 class="icon-box-title mb-0">4 Cores</h6>
            <p class="mb-0">CPU Power</p>
          </section>
        </div><!-- /.icon-box -->

        <div class="icon-box icon-box-side flex-1 mr-3 my-0">
          <header class="mx-3">
            <a href="#" class="bg-grey fs-30 text-muted"><i class="lnr lnr-layers icon-lg"></i></a>
          </header>
          <section>
            <h6 class="icon-box-title mb-0">16 GB</h6>
            <p class="mb-0">Ram Space</p>
          </section>
        </div><!-- /.icon-box -->

        <div class="icon-box icon-box-side flex-1 mr-3 my-0">
          <header class="mx-3">
            <a href="#" class="bg-grey fs-30 text-muted"><i class="lnr lnr-database icon-lg"></i></a>
          </header>
          <section>
            <h6 class="icon-box-title mb-0">120 GB</h6>
            <p class="mb-0">Disc Space</p>
          </section>
        </div><!-- /.icon-box -->
      </div><!-- /.d-sm-inline-flex -->

      <div class="d-inline-flex align-items-center mt-3 mt-sm-0">
        <button class="btn btn-outline-default px-4 pd-tb-10">Create Report</button>
      </div><!-- /.d-inline-flex -->
    </div><!-- /.row -->
  </div><!-- /.{{ $theme_manager->get_container_class() }} -->
</div><!-- /.page-title -->
