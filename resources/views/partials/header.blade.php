<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{{ asset('assets/css/pace.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{-- <link rel="shortcut icon" href="{{ asset('images/verus.png') }}" weight="20px;" height="20px;"> --}}
    <link rel="shortcut icon" href="{{ asset('images/zazufavicon.png') }}" weight="20px;" height="20px;">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>
    @yield('css')
    <!-- CSS -->
    @include('partials.styles')

    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    @yield('bscripts')
</head>

<body data-header-skin="{{ $theme_manager->get_header_skin() }}"
    data-sidebar-skin="{{ $theme_manager->get_sidebar_skin() }}"
    data-navbar-brand-skin="{{ $theme_manager->get_navbar_brand_skin() }}"
    data-sidebar-state="{{ $theme_manager->get_sidebar_state() }}" {{ $theme_manager->body_classes() }}
    oncontextmenu="return {{((config('app.inspecthtml')=='yes')?'true':'false')}}"
    >
    <div id="wrapper" class="wrapper">
