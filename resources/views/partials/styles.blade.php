@section('styles')
  @foreach($theme_manager->styles() as $priority)
    @foreach($priority as $style)
      <link rel="stylesheet" href="{{ $style }}">
    @endforeach
  @endforeach
@show
