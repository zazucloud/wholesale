<!-- SIDEBAR -->
<aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">

    <!-- Sidebar Menu -->
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">

            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="list-icon linearicons-home"></i>
                    <span class="hide-menu">
                        Dashboard
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.wholesalers.index') }}">
                    <i class="list-icon linearicons-users2"></i>
                    <span class="hide-menu">
                        WholeSalers
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.retailers.index') }}">
                    <i class="list-icon linearicons-users2"></i>
                    <span class="hide-menu">
                        Customers
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.inventory.index') }}">
                    <i class="list-icon linearicons-box"></i>
                    <span class="hide-menu">
                        Inventory
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.inquiry.index') }}">
                    <i class="list-icon linearicons-receipt"></i>
                    <span class="hide-menu">
                        Quotation
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('payments.index') }}">
                    <i class="list-icon linearicons-cash-dollar"></i>
                    <span class="hide-menu">
                        Payments
                    </span>
                </a>
            </li>
            <li class="menu-item-has-children">
                <a href="javascript:void(0);">
                    <i class="list-icon linearicons-cart"></i>
                    <span class="hide-menu">
                        Orders
                    </span>
                </a>

                <ul class="list-unstyled sub-menu">
                    <li><a href="{{ route('orders.create') }}">Create Order</a></li>
                    <li><a href="{{ route('orders.index') }}">All Orders</a></li>
                    <li><a href="{{ route('orders.pending') }}">Pending</a></li>
                    <li><a href="{{ route('orders.approved') }}">Approved</a></li>
                    <li><a href="{{ route('orders.processing') }}">Processing</a></li>
                    <li><a href="{{ route('orders.processed') }}">Processed</a></li>
                    <li><a href="{{ route('orders.paid') }}">Partly Shipped</a></li>
                    <li><a href="{{ route('orders.fully-shipped') }}">Fully Shipped</a></li>
                    <li><a href="{{ route('orders.partially-shipped') }}">Paid</a></li>
                </ul>
            </li>
            <li class="menu-item-has-children">
                <a href="javascript:void(0);">
                    <i class="list-icon linearicons-database"></i>
                    <span class="hide-menu">
                        Production
                    </span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li><a href="{{ route('admin.production.create') }}">New Request</a></li>
                    <li><a href="{{ route('admin.production.index') }}">All Requests</a></li>
                    <li><a href="{{ route('admin.production.pending') }}">Pending</a></li>
                    <li><a href="{{ route('admin.production.inproduction') }}">In Production</a></li>
                    <li><a href="{{ route('admin.production.completed') }}">Completed</a></li>
                    <li><a href="{{ route('admin.production.delivered') }}">Delivered</a></li>
                    <li><a href="{{ route('admin.production.drafts') }}">Drafts</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route('admin.manufacturers.index') }}">
                    <i class="list-icon linearicons-factory2"></i>
                    <span class="hide-menu">
                        Manufacturers
                    </span>
                </a>
            </li>
            <li class="menu-item-has-children">
                <a href="javascript:void(0);">
                    <i class="list-icon linearicons-hammer-wrench"></i>
                    <span class="hide-menu">
                        Configurations
                    </span>
                </a>

                <ul class="list-unstyled sub-menu">
                    <li><a href="{{ route('admin.config.brands') }}">Brands</a></li>
                    <li><a href="{{ route('admin.config.pricing') }}">Pricing</a></li>
                    <li><a href="{{ route('admin.config.warehouses') }}">Warehouses</a></li>
                    <li><a href="{{ route('admin.config.inventory_import') }}">Inventory Import</a></li>
                    <li><a href="{{ route('admin.config.wholesaler_import') }}">Wholesaler Import</a></li>
                    <li><a href="{{ route('admin.config.inventory_attributes') }}">Inventory Attributes</a></li>
                    <li><a href="{{ route('admin.config.inventory_categories') }}">Inventory Categories</a></li>
                    {{-- <li><a href="{{ route('admin.config.category_visibility_discount_import') }}">Discount Import</a></li> --}}
                </ul>
            </li>

            <li class="menu-item-has-children">
                <a href="javascript:void(0);">
                    <i class="list-icon linearicons-layers mr-2"></i>
                    <span class="hide-menu">
                        Zoning
                    </span>
                </a>

                <ul class="list-unstyled sub-menu">
                    <li><a href="{{ route('zones.index') }}">Zones</a></li>
                    <li><a href="{{ route('counties.index') }}">Counties</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route('config.users') }}">
                    <i class="list-icon linearicons-cog text-white"></i>
                    <span class="hide-menu text-white">Settings</span>
                </a>
            </li>

        </ul><!-- /.side-menu -->
    </nav><!-- /.sidebar-nav -->

</aside><!-- /.site-sidebar -->
