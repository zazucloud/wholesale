<!-- Page Title Area -->
<div class="page-title" style="margin-right: 0;">
    <div class="{{ $theme_manager->get_container_class() }}">
        <div class="row">
            <div class="page-title-left">
                <h6 class="page-title-heading mr-0 mr-r-5">@yield('title')</h6>
                <p class="page-title-description mr-0 d-none d-md-inline-block">@yield('subtitle')</p>
            </div><!-- /.page-title-left -->

            <div class="page-title-right d-none d-sm-inline-flex align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('inquiry.index') }}">REPORT</a></li>
                    <li class="breadcrumb-item active">@yield('title')</li>
                </ol>
            </div><!-- /.page-title-right -->
        </div><!-- /.row -->
    </div><!-- /.{{ $theme_manager->get_container_class() }} -->
</div><!-- /.page-title -->
