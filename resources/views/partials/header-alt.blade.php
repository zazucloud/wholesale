<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('images/zazufavicon.png') }}" weight="20px;" height="20px;">
    {{-- <link rel="shortcut icon" href="{{ asset('images/verus.png') }}" weight="20px;" height="20px;"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>@yield('title')</title>

	<!-- CSS -->
  @include('partials/styles')

</head>
<body style="min-height: 100vh;">
