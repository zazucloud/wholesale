<!-- SIDEBAR -->
<aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">

    <!-- Sidebar Menu -->
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
            <li>
                <a href="{{ route('customerhome',["dispatchid"=>$dispatchid,"token"=>$token]) }}">
                    <i class="list-icon linearicons-home"></i>
                    <span class="hide-menu">
                       Invoice Items
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('customer.track',["dispatchid"=>$dispatchid,"token"=>$token]) }}">
                    <i class="list-icon linearicons-list"></i>
                    <span class="hide-menu">
                        Track Items
                    </span>
                </a>
            </li>
        </ul><!-- /.side-menu -->
    </nav><!-- /.sidebar-nav -->

</aside><!-- /.site-sidebar -->
