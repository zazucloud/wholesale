<!-- FOOTER -->
<footer class="footer text-center" style="position: relative">
    <div class="{{ $theme_manager->get_container_class() }}">
        <span> Copyright © 2020 - Tile & Carpet Centre Ltd. Deliveries. All rights reserved.</span>
    </div><!-- /.{{ $theme_manager->get_container_class() }} -->
</footer>

</div>
<!--/ #wrapper -->

<!-- Scripts -->
@section('scripts')
@foreach($theme_manager->scripts() as $priority)
@foreach($priority as $script)
<script src="{{ $script}}"></script>
@endforeach
@endforeach
@yield('extrascripts')
<script>
     $(document).ready(function () {
    $(".spacious-container").floatingScroll();
});
</script>

</body>

</html>
