<!-- FOOTER -->
<footer class="footer text-center">
    <div class="{{ $theme_manager->get_container_class() }}">
        <span>Copyright © {{date("Y")}} - Tile & Carpet Centre Ltd. All rights reserved.</span>
    </div><!-- /.{{ $theme_manager->get_container_class() }} -->
</footer>

</div>
<!--/ #wrapper -->

<!-- Scripts -->
<script>
    document.onkeydown = function(e) {
        console.log('{{ config('app.inspecthtml') }}' === 'no');
        if ('{{ config('app.inspecthtml') }}' === 'no') {
            if (e.keyCode == 123 || e.keyCode == 91 || e.keyCode == 93) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
            if (e.keyCode == 18 && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
            if (e.keyCode == 18 && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'H'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'A'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)) {
                return false;
            }
        }
    }
</script>
@section('scripts')
@foreach($theme_manager->scripts() as $priority)
@foreach($priority as $script)
<script src="{{ $script}}"></script>
@endforeach
@endforeach
@yield('extrascripts')
@show
</body>
<script src="{{ URL::asset('js/core/core.js')}}"></script>
</html>
