<!-- HEADER & TOP NAVIGATION -->
<nav class="navbar">
    <div class="container-fluid px-0 align-items-stretch">

        <!-- Logo Area -->
        <div class="navbar-header">
            <a href="{{ url('') }}" class="navbar-brand">
                <span class="logo-expand">
                    <img height="35" alt="" src="{{ $theme_manager->get_logo_expand() }}" />
                    <p style="font-size: 1.25rem;margin-left: 10px;float: right;">Delivery Note</p>
                </span>
                <img height="40" class="logo-collapse" alt="" src="{{ $theme_manager->get_logo_collapse() }}" />
            </a>
        </div><!-- /.navbar-header -->

        <!-- Left Menu & Sidebar Toggle -->
        <ul class="nav navbar-nav">
            <li class="sidebar-toggle dropdown">
                <a href="javascript:void(0)" class="ripple">
                    <span>
                        <i class="list-icon linearicons-menu"></i>
                    </span>
                </a>
            </li>
        </ul><!-- /.navbar-left -->

        <!-- Search Form -->
        <form class="navbar-search d-none d-sm-block" role="search">
            <i class="list-icon lnr lnr-magnifier"></i>
            <input type="search" class="search-query" placeholder="Search anything...">
            <a href="javascript:void(0);" class="remove-focus">
                <i class="lnr lnr-cross"></i>
            </a>
        </form><!-- /.navbar-search -->

        <div class="spacer"></div>

        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple"
                    data-toggle="dropdown">
                    Tracking Portal
                </a>
            </li><!-- /.dropdown -->
        </ul><!-- /.navbar-nav -->

    </div><!-- /.container-fluid -->

</nav><!-- /.navbar -->
