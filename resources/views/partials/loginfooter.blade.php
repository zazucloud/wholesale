<!-- FOOTER -->
<footer class="footer text-center" style="position: sticky; top:100%;">
    <div class="{{ $theme_manager->get_container_class() }}">
        <span>Copyright © 2020 - Tile & Carpet Centre Ltd. All rights reserved. <br>Powered By <a
                href="https://www.zazu.cloud/" target="_blank">Zazu Cloud</a> </span>
        {{-- <span>Powered By <a
                href="https://www.verus.africa/">Verus Africa Limited</a> </span> --}}
    </div><!-- /.{{ $theme_manager->get_container_class() }} -->
</footer>

</div>
<!--/ #wrapper -->

<!-- Scripts -->

<script src="{{ asset('js/core/jquery.min.js ') }} "></script>
<script src="{{ asset('js/app.js ') }} "></script>
@yield('script')

</body>

</html>
