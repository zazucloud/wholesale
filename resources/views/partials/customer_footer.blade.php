<!-- FOOTER -->
<footer class="footer text-center">
    <div class="{{ $theme_manager->get_container_class() }}">
        <span>Copyright © 2020 - Tile & Carpet Centre Ltd. Deliveries. All rights reserved.</span>
    </div><!-- /.{{ $theme_manager->get_container_class() }} -->
</footer>

</div>
<!--/ #wrapper -->

<!-- Scripts -->
@section('scripts')
@foreach($theme_manager->scripts() as $priority)
@foreach($priority as $script)
<script src="{{ $script}}"></script>
@endforeach
@endforeach

@yield('extrascripts')
@show
<script>
    $(".sidebar-nav a").filter(function () {
return this.href == location.href.replace(/#.*/, "");
}).closest('li').addClass("current-page active");
</script>
</body>

</html>
