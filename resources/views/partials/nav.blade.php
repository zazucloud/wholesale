<!-- HEADER & TOP NAVIGATION -->
<nav class="navbar">
    <div class="container-fluid px-0 align-items-stretch">

        <!-- Logo Area -->
        <div class="navbar-header">
            <a href="{{ url('') }}" class="navbar-brand">
                <span class="logo-expand">
                    <img height="45" alt="" src="{{ $theme_manager->get_logo_expand() }}" />
                    {{-- <p style="font-size: 1.25rem;margin-left: 10px;float: right;">TOPMFG</p> --}}
                </span>
                <img height="45" class="logo-collapse" alt="" src="{{ $theme_manager->get_logo_collapse() }}" />
            </a>
        </div><!-- /.navbar-header -->

        <!-- Left Menu & Sidebar Toggle -->
        <ul class="nav navbar-nav">
            <li class="sidebar-toggle dropdown">
                <a href="javascript:void(0)" class="ripple">
                    <span>
                        <i class="list-icon linearicons-menu"></i>
                    </span>
                </a>
            </li>
        </ul><!-- /.navbar-left -->

        <!-- Search Form -->
        <form class="navbar-search d-none d-sm-block" role="search">
            <i class="list-icon lnr lnr-magnifier"></i>
            <input type="search" class="search-query" placeholder="Search anything...">
            <a href="javascript:void(0);" class="remove-focus">
                <i class="lnr lnr-cross"></i>
            </a>
        </form><!-- /.navbar-search -->

        <div class="spacer"></div>

        <!-- Right Menu -->
        <ul class="nav navbar-nav d-none d-lg-flex ml-2 ml-0-rtl">

            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <span>
                        <i class="list-icon linearicons-alarm"></i>
                        {{-- @if ($badgecount !== 0) --}}
                        <span class="button-pulse bg-danger">10</span>
                        {{-- @endif --}}

                    </span>
                    Messages
                </a>

                <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
                    <div class="card">
                        <header class="card-header d-flex justify-content-center align-items-center mb-0">
                            <i class="lnr lnr-envelope fs-15 mr-2"></i>
                            <span class="heading-font-family fw-400">
                                New Messages
                            </span>
                        </header>

                        <ul class="card-body list-unstyled dropdown-list-group" style="max-height: 40vh;">
                            {{-- @if ($history ?? '')
                            @foreach ($history as $item) --}}
                            <li>
                                <a href="#" class="media">
                                    <span class="d-flex thumb-xs2">
                                        <span class="list-icon linearicons-envelope rounded-circle bg-info"
                                            style="font-weight:bolder;"></span>

                                    </span>

                                    <span class="media-body">
                                        <span class="heading-font-family media-heading">#SIOw7382938</span>
                                        <br>
                                        <span class="heading-font-family media-heading">Test Message</span>
                                        <br>
                                        <span class="media-content">Lorem ipsum</span>
                                    </span>
                                </a>
                            </li>
                            {{-- @endforeach
                            @endif --}}
                        </ul><!-- /.dropdown-list-group -->

                        <footer class="card-footer text-center">
                            <a href="#" class="btn btn-link text-danger fs-12">See
                                all messages</a>
                        </footer>

                    </div><!-- /.card -->
                </div><!-- /.dropdown-menu -->
            </li><!-- /.dropdown -->
        </ul><!-- /.navbar-right -->

        <!-- User Image with Dropdown -->
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple"
                    data-toggle="dropdown">
                    {{ auth()->user()->name }}
                    <span style="margin-right: 1.5rem;"></span>
                    <span class="avatar thumb-xs">

                        <i class="list-icon linearicons-chevron-down" style="font-weight:bolder;"></i>
                    </span>
                </a>

                <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                    <div class="card">
                        <ul class="list-unstyled card-body">

                            <li>
                                <a href="#">
                                    <span>
                                        <span class="align-middle">Configurations</span>
                                    </span>
                                </a>
                            </li>

                            @if (Auth::guard('web')->check())
                                <li>
                                    <a class="dropdown-item" href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        <span>
                                            <span class="align-middle">Sign Out</span>
                                        </span>
                                    </a>

                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @else
                                <li>
                                    <a class="dropdown-item" href="{{ route('manufacturer_user.logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        <span>
                                            <span class="align-middle">Sign Out</span>
                                        </span>
                                    </a>

                                    <form id="logout-form" action="{{ route('manufacturer_user.logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endif

                        </ul><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.dropdown-card-profile -->
            </li><!-- /.dropdown -->
        </ul><!-- /.navbar-nav -->

    </div><!-- /.container-fluid -->

</nav><!-- /.navbar -->
