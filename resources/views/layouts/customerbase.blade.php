@include('partials.header')

@include('partials.customernav')

<div id="wrapper">
    @if(session('success'))
    <customtoast message="{{ session()->get('success') }}"></customtoast>
    @endif
    @if($errors->all())
    <errortoast :message="{{ json_encode($errors->all()) }}"></errortoast>
    @endif
    <div class="content-wrapper">
        @include('partials.customersidebar')
        <main class="main-wrapper clearfix">
            @yield('content')
        </main><!-- /.main-wrapper -->
    </div><!-- /.content-wrapper -->
</div>
@include('partials.customer_footer')
