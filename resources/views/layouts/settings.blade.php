@extends('layouts.base')

@section('title')
    MANAGE SETTINGS
@endsection

@section('content')
    @include('partials.page-title')

    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="{{ $theme_manager->get_container_class() }}">
        <div class="widget-list">
            <div class="row no-gutters">

                <div class="col-md-12 widget-holder widget-full-content">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">

                            <div class="mail-inbox row no-gutters">

                                <!-- Mail Sidebar -->
                                <div class="col-md-2 d-none d-md-flex flex-column mail-sidebar">
                                    <h6 class="page-title-heading ml-4" style="font-size: 1rem;">Navigation</h6>
                                    <div class="mail-list text-info">
                                        <div class="mail-list-item media">
                                            <a href="{{ route('config.users') }}" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-cog mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span class="mail-title headings-color d-block zi-3">Users</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div><!-- /.mail-inbox -->

                                    <div class="mail-list text-info">
                                        <div class="mail-list-item media">
                                            <a href="{{ route('roles.index') }}" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-shield mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span class="mail-title headings-color d-block zi-3">Roles</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div><!-- /.mail-inbox -->

                                    <div class="mail-list text-info">
                                        <div class="mail-list-item media">
                                            <a href="{{ route('company.edit') }}" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-library2 mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span class="mail-title headings-color d-block zi-3">Company</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div><!-- /.mail-inbox -->

                                    <div class="mail-list text-info">
                                        <div class="mail-list-item media">
                                            <a href="{{ route('templates.index') }}" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-document2 mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span
                                                            class="mail-title headings-color d-block zi-3">Templates</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div><!-- /.mail-inbox -->
                                    <h6 class="page-title-heading ml-4" style="font-size: 1rem;">Actions Icons</h6>

                                    <div class="mail-list text-info">
                                        <div class="mail-list-item media">
                                            <a href="#" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-eye mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span class="mail-title headings-color d-block zi-3">View</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div>
                                    <div class="mail-list text-warning">
                                        <div class="mail-list-item media">
                                            <a href="#" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-pencil5 mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span class="mail-title headings-color d-block zi-3">Edit</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div>
                                    <div class="mail-list text-danger">
                                        <div class="mail-list-item media">
                                            <a href="#" class="pos-absolute pos-0 zi-0">
                                            </a>

                                            <div class="checkbox checkbox-success bw-3 mail-select-checkbox">
                                                <i class="list-icon linearicons-trash2 mr-2"></i>
                                            </div><!-- /.checkbox -->
                                            <div class="media-body">
                                                <div class="d-flex">
                                                    <div class="headings-font-family">
                                                        <span class="mail-title headings-color d-block zi-3">Delete</span>
                                                    </div>
                                                </div><!-- /.d-flex -->
                                            </div><!-- /.media-body -->
                                        </div><!-- /.mail-list-item -->
                                    </div>

                                </div><!-- /.mail-sidebar -->

                                <div class="col-md-10 col-12 h-100 d-flex flex-column" style="padding:10px;">
                                    <div class="mail-inbox-header">
                                        <ul
                                            class="mail-inbox-categories list-unstyled list-inline headings-font-family fw-semibold mb-0">
                                            <li class="list-inline-item">@yield('settingstitle')</li>
                                        </ul>
                                        <div class="flex-1"></div>
                                    </div><!-- /.mail-inbox-header -->

                                    @yield('settingscontent')
                                </div><!-- /.col-lg-9 -->

                            </div><!-- /.mailbox -->

                        </div><!-- /.widget-body -->
                    </div><!-- /.widget-bg -->
                </div><!-- /.widget-holder -->

            </div><!-- /.row -->
        </div><!-- /.widget-list -->
    </div><!-- /.{{ $theme_manager->get_container_class() }} -->
@endsection
