@include('partials.header')

@include('partials.nav')

<div id="wrapper">
    @if(session('success'))
    <customtoast message="{{ session()->get('success') }}"></customtoast>
    @endif
    @if($errors->all())
    <errortoast :message="{{ json_encode($errors->all()) }}"></errortoast>
    @endif
    <div class="content-wrapper">
        @include('partials.sidebar')
        <main class="main-wrapper clearfix">

            @yield('content')
        </main><!-- /.main-wrapper -->
        {{-- @include('partials.right-sidebar') --}}
    </div><!-- /.content-wrapper -->
</div>
@include('partials.footer')
