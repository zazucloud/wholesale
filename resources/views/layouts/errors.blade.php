@include('partials.header-alt')

<div id="wrapper">
@if(session('success'))
    <customtoast message="{{ session()->get('success') }}"></customtoast>
    @endif
    @if($errors->all())
    <errortoast :message="{{ json_encode($errors->all()) }}"></errortoast>
    @endif
    @yield('content')
</div>
@include('partials.loginfooter')
