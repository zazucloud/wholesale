@include('partials.public-header-alt')

<div id="wrapper" style="min-height: calc(100vh - 80px);">
@if(session('success'))
    <customtoast message="{{ session()->get('success') }}"></customtoast>
    @endif
    @if($errors->all())
    <errortoast :message="{{ json_encode($errors->all()) }}"></errortoast>
    @endif
    @yield('content')
</div>
@include('partials.public-footer')
