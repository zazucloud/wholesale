import { defaultsDeep } from "lodash";
import Vue from "vue";
import * as VueGoogleMaps from "vue2-google-maps";

require("./bootstrap");
window.store = require("./store").default;

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyBI3HsY4H0p5Vd2C8jYu8ilIIA5lG2Wd5A", //old key AIzaSyAaVxESc63AM4ENscqiqYoyc4ednW4rqJo
        libraries: "places", // necessary for places input
    },
    installComponents: true,
});
Vue.component("contacts", require("./components/Contacts.vue").default);
Vue.component("google-map", VueGoogleMaps.Map);
Vue.component("google-marker", VueGoogleMaps.Marker);
Vue.component("customtoast", require("./components/CustomToast.vue").default);
Vue.component("errortoast", require("./components/ErrorToast.vue").default);
Vue.component("addtocart", require("./components/AddToCart.vue").default);
Vue.component("attributes", require("./components/Attributes.vue").default);
Vue.component("zonesmodal", require("./components/ZonesModal.vue").default);
Vue.component(
    "customattributes",
    require("./components/CustomAttributes.vue").default
);
Vue.component("comment", require("./components/CommentsModal.vue").default);
Vue.component("creditmodal", require("./components/CreditModal.vue").default);
Vue.component(
    "busjob_exports",
    require("./components/BusJobExports.vue").default
);
Vue.component(
    "wholesalerlocationmodal",
    require("./components/WholesalersLocationModal.vue").default
);
Vue.component(
    "directorsmodal",
    require("./components/DirectorsModal.vue").default
);
Vue.component(
    "whaccountmodal",
    require("./components/WholesalerAccountModal.vue").default
);
Vue.component(
    "manufacturersmodal",
    require("./components/ManufacturersModal.vue").default
);
Vue.component(
    "brandconfigmodal",
    require("./components/BrandConfigModal.vue").default
);
Vue.component(
    "inventorycategorymodal",
    require("./components/InventoryCategory.vue").default
);
Vue.component(
    "warehouseconfigmodal",
    require("./components/WarehouseConfig.vue").default
);

Vue.component("cartcounter", require("./components/CartCounter.vue").default);
Vue.component(
    "inventorydropdown",
    require("./components/InventoryDropdown.vue").default
);
Vue.component(
    "inventoryproductiondropdown",
    require("./components/InventoryProductionDropdown.vue").default
);

Vue.component(
    "inventoryquotationdropdown",
    require("./components/InventoryQuotationDropdown.vue").default
);

Vue.component(
    "manufacturersproducionlist",
    require("./components/ManufacturersProdList.vue").default
);
Vue.component(
    "discountuploadmodal",
    require("./components/ZoneDiscountUploadModal.vue").default
);

Vue.component(
    "busjobimports",
    require("./components/BusJobImports.vue").default
);

const app = new Vue({
    el: "#wrapper",
    store,
    data() {
        return {
            long: null,
            lat: null,
            address: null,
            finposition: { lat: -1.297024, lng: 36.819921 },
        };
    },
    methods: {
        getlonglat(places) {
            this.lat = places.geometry.location.lat();
            this.long = places.geometry.location.lng();
            this.address = places.formatted_address;
            this.finposition = { lat: this.lat, lng: this.long };
        },
        getdraggedlonglat(input) {
            const geocoder = new google.maps.Geocoder();
            const latlong = {
                lat: input.latLng.lat(),
                lng: input.latLng.lng(),
            };
            this.finposition = latlong;
            var that = this;
            geocoder.geocode({ location: latlong }, function (results, status) {
                if (status === "OK") {
                    that.lat = results[0].geometry.location.lat();
                    that.long = results[0].geometry.location.lng();
                    that.address = results[0].formatted_address;
                }
            });
        },
        openWhLocationModal(data, wh_route) {
            this.$store.commit("openLocationModal", { data, wh_route });
            $("body").addClass("modal-open");
        },

        openZoneModal() {
            this.$store.commit("openModal");
            $("body").addClass("modal-open");
        },
        openWhDirectorsModal(data, wh_route) {
            this.$store.commit("openDirectorsModal", { data, wh_route });
            $("body").addClass("modal-open");
        },
        openWhAccountModal(data, wh_route) {
            this.$store.commit("openWholesalerAccountModal", {
                data,
                wh_route,
            });
            $("body").addClass("modal-open");
        },
        openManufacturerModal(data, man_route) {
            this.$store.commit("openManufacturersModal", { data, man_route });
            $("body").addClass("modal-open");
        },
        openComment(data) {
            this.$store.commit("openCommentModal", data);
            $("body").addClass("modal-open");
        },
        openCredit() {
            this.$store.commit("openCreditModal");
            $("body").addClass("modal-open");
        },
        updateStatusOfProductionItem(event, item) {
            axios
                .post(
                    "/admin/production/production-request/request-item-update",
                    {
                        item_id: item.item_id,
                        status: event.target.value,
                    }
                )
                .then((resp) => {
                    if (
                        (resp.data.status =
                            "success" && resp.data.ifupdated == true)
                    ) {
                        $.toast({
                            position: "top-right",
                            text:
                                "Status for " +
                                item.name +
                                " has been updated successfully",
                            icon: "info",
                            hideAfter: 2000,
                        });
                    } else {
                        $.toast({
                            position: "top-right",
                            text:
                                "Status for " +
                                item.name +
                                " Cannot Be Updated to, It Has Already Passed That State ",
                            icon: "danger",
                            hideAfter: 2000,
                        });
                    }
                })
                .catch((error) => {
                    // this.error = error.response.data.message;
                });
        },

        busBatchProcess(url, formid = null) {
            this.$store.commit("bstatusUpdate");
            this.$store.commit("progressUpdate", true);
            this.$store.commit(
                "updateInitialmessage",
                "Please Wait for Export To Finish"
            );


            var myaxios = null;

            if (formid !== null) {
                $(document).on("submit", "#" + formid, function (e) {
                    e.preventDefault();

                    return false;
                });
                myaxios = axios.post(url, $("#" + formid).serialize());
            } else {
                myaxios = axios.post(url);
            }
 console.log(url);
            myaxios
                .then((resp) => {
                    this.$store.commit("updateBatchId", resp.data);
                    this.$store.commit(
                        "updateInitialmessage",
                        "Please Wait for Export To Finish"
                    );
                    $.toast({
                        position: "top-right",
                        text: resp.data.message,
                        icon: "success",
                        hideAfter: 2000,
                    });
                })
                .catch((error) => {
                    this.$store.commit("progressUpdate", false);
                    this.$store.commit("updateBatchId", null);
                    this.$store.commit("updateInitialmessage", null);

                    console.log(error)

                    $.toast({
                        position: "top-right",
                        text: error.response.data.message,
                        icon: "error",
                        hideAfter: 2000,
                    });
                });
        },

        openBrandConfigModal(data, brand_route) {
            this.$store.commit("openBrandConfigModal", { data, brand_route });
            $("body").addClass("modal-open");
        },
        openZonesModal(data, zone_route) {
            this.$store.commit("openZonesModal", { data, zone_route });
            $("body").addClass("modal-open");
        },
        openInventoryConfigModal(data, inv_route) {
            this.$store.commit("openInventoryConfigModal", { data, inv_route });
            $("body").addClass("modal-open");
        },
        openWarehouseConfigModal(data, whconf_route) {
            this.$store.commit("openWarehouseConfigModal", {
                data,
                whconf_route,
            });
            $("body").addClass("modal-open");
        },
        async formSubmittionCheck(id) {
            var that = this;
            $("#" + id).on("submit", function (e) {
                e.preventDefault();
            });
            var data = that.$store.state.inputs;
            data = {
                inventory_id: $("input[name='inventory_id[]']")
                    .map(function () {
                        return $(this).val();
                    })
                    .get(),
                inventory_quantity: $("input[name='inventory_quantity[]']")
                    .map(function () {
                        return $(this).val();
                    })
                    .get(),
            };
            const response = await axios
                .post("/admin/orders/check/stock", data)
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {});

            $("#generate_production").remove();
            $("#messagediv").remove();

            console.log(response);

            if (response.hadExcessProduction == true) {
                if (response.hadExcessTrade == true) {
                    alert(
                        "LPO needs to be raised for some of the items in The Order"
                    );
                }
                var message =
                    "Excess Quantity Detected. Generate Production Request For Order ?";

                var r = confirm(message);
                if (r == true) {
                    $("#" + id).append(
                        '<input type="hidden" name="generate_production" value="true" id="generate_production"/>'
                    );
                    $("form#" + id)
                        .unbind("submit")
                        .submit();
                } else {
                    var itemsmessage = "";
                    for (
                        let index = 0;
                        index < response.items.length;
                        index++
                    ) {
                        var no = index + 1;
                        itemsmessage +=
                            no +
                            ". " +
                            response.items[index].name +
                            "(" +
                            response.items[index].sku +
                            ")" +
                            " ->  Excess QTY: " +
                            response.items[index].leftover_quantity +
                            "<br>";
                    }
                    var finmessage =
                        "Please remove excess before submitting again.<br>" +
                        itemsmessage;
                    $("#" + id).append(
                        '<input type="hidden" name="generate_production" value="false" id="generate_production"/>'
                    );
                    $("#" + id).append(
                        '<div class="alert alert-danger" id="messagediv">' +
                            finmessage +
                            "</div>"
                    );
                    $(".multiple-submits")
                        .text("Submit")
                        .removeAttr("disabled");
                }
            } else if (
                response.hadExcessProduction == false &&
                response.hadExcessTrade == true
            ) {
                $("#" + id).append(
                    '<input type="hidden" name="generate_production" value="false" id="generate_production"/>'
                );
                alert(
                    "LPO needs to be raised for some of the items in The Order"
                );

                $("form#" + id)
                    .unbind("submit")
                    .submit();
            } else {
                $("#" + id).append(
                    '<input type="hidden" name="generate_production" value="false" id="generate_production"/>'
                );
                $("form#" + id)
                    .unbind("submit")
                    .submit();
            }
        },
    },
    mounted() {
        var that = this;
        $(document).ready(function () {
            if (window.getaddress) {
                that.address = window.getaddress;
            }
            if (window.county) {
                that.county = window.county;
            }
            if (window.getlat) {
                that.lat = parseFloat(window.getlat);
            }
            if (window.getlong) {
                that.long = parseFloat(window.getlong);
            }

            if (window.getlong && window.getlat) {
                that.finposition = { lat: that.lat, lng: that.long };
            }
        });
    },
});
