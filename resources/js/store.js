import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        locationmodalstate: false,
        wholesalerlocation: null,
        locationroute: 1,
        directorsmodalstate: false,
        directors: null,
        directorsroute: 1,
        accountmodalstate: false,
        account: null,
        accountroute: 1,
        manufacturersmodalstate: false,
        manufacturer: null,
        manufacturerroute: 1,
        brandconfigstate: false,
        brandconfig: null,
        brandconfigroute: 1,
        inventoryconfigstate: false,
        inventoryconfig: null,
        inventoryconfigroute: 1,
        cartcount: null,
        warehouseconfigstate: false,
        warehouseconfig: null,
        warehouseconfigroute: 1,
        inputs: [],
        zonesstate: false,
        zones: null,
        zonesroute: 1,
        comments_modal: false,
        comment: null,
        creditmodalstate: false,
        modalstate: false,
        BatchId: null,
        filename: null,
        BatchStartStatus: false,
        progressBarStatus: false,
        initial_message: null,
    },

    mutations: {
        openLocationModal(state, { data, wh_route }) {
            state.locationmodalstate = true;
            state.wholesalerlocation = data;
            state.locationroute = wh_route;
        },
        closeLocationModal(state) {
            state.locationmodalstate = false;
            state.wholesalerlocation = null;
            state.locationroute = 1;
        },
        openDirectorsModal(state, { data, wh_route }) {
            state.directorsmodalstate = true;
            state.directors = data;
            state.directorsroute = wh_route;
        },

        closeDirectorsModal(state) {
            state.directorsmodalstate = false;
            state.directors = null;
            state.directorsroute = 1;
        },
        openWholesalerAccountModal(state, { data, wh_route }) {
            state.accountmodalstate = true;
            state.account = data;
            state.accountroute = wh_route;
        },
        closeWholesalerAccountModal(state) {
            state.accountmodalstate = false;
            state.account = null;
            state.accountroute = 1;
        },
        openManufacturersModal(state, { data, man_route }) {
            state.manufacturersmodalstate = true;
            state.manufacturer = data;
            state.manufacturerroute = man_route;
        },
        closeManufacturersModal(state) {
            state.manufacturersmodalstate = false;
            state.manufacturer = null;
            state.manufacturerroute = 1;
        },
        openBrandConfigModal(state, { data, brand_route }) {
            state.brandconfigstate = true;
            state.brandconfig = data;
            state.brandconfigroute = brand_route;
        },
        closeBrandConfigModal(state) {
            state.brandconfigstate = false;
            state.brandconfig = null;
            state.brandconfigroute = 1;
        },
        openInventoryConfigModal(state, { data, inv_route }) {
            state.inventoryconfigstate = true;
            state.inventoryconfig = data;
            state.inventoryconfigroute = inv_route;
        },
        closeInventoryConfigModal(state) {
            state.inventoryconfigstate = false;
            state.inventoryconfig = null;
            state.inventoryconfigroute = 1;
        },
        updateCount(state, data) {
            state.cartcount = data;
        },
        openWarehouseConfigModal(state, { data, whconf_route }) {
            state.warehouseconfigstate = true;
            state.warehouseconfig = data;
            state.warehouseconfigroute = whconf_route;
        },
        closenWarehouseConfigModal(state) {
            state.warehouseconfigstate = false;
            state.warehouseconfig = null;
            state.warehouseconfigroute = 1;
        },
        openZonesModal(state, { data, zone_route }) {
            state.zonesstate = true;
            state.zones = data;
            state.zonesroute = zone_route;
        },
        closeZonesModal(state) {
            state.zonesstate = false;
            state.zones = null;
            state.zonesroute = 1;
        },
        openCommentModal(state, data) {
            state.comments_modal = true;
            state.comment = data;
        },
        closeCommentModal(state) {
            state.comments_modal = false;
            state.comment = null;
        },
        openCreditModal(state) {
            state.creditmodalstate = true;
        },
        closeCreditModal(state) {
            state.creditmodalstate = false;
        },

        openModal(state) {
            state.modalstate = true;
        },
        closeModal(state) {
            state.modalstate = false;
        },
        updateBatchId(state, data) {
            if (data !== null) {
                state.BatchId = data.batchid;
                state.filename = data.filename;
            } else {
                state.BatchId = null;
            }
        },
        bstatusUpdate(state) {
            state.BatchStartStatus = !state.BatchStartStatus;
        },
        progressUpdate(state, data) {
            state.progressBarStatus = data;
        },
        updateInitialmessage(state, data) {
            state.initial_message = data;
        },
    },
    actions: {
        queryCartItems(state) {
            axios
                .get("/wholesaler/get/cartdetails")
                .then((resp) => {
                    this.commit("updateCount", resp.data.counter);
                })
                .catch((error) => {
                    // this.error = error.response.data.message;
                });
        },
    },
});
