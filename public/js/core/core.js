$("#ltolabel").hide();
$("#ltorate").hide();
$("#methodtype").hide();

$(document).ready(function () {
    var date = new Date();
    var indate = new Date();
    indate.setDate(indate.getDate() + 1);

    var year = date.getFullYear();
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);
    var minDate = year + "-" + month + "-" + day;

    var year1 = indate.getFullYear();
    var month1 = ("0" + (indate.getMonth() + 1)).slice(-2);
    var day1 = ("0" + indate.getDate()).slice(-2);
    var minDate1 = year1 + "-" + month1 + "-" + day1;

    $(".datepicker").attr("min", minDate);
    $(".datepickerin").attr("min", minDate1);

    $(".spacious-container").floatingScroll();
});
$(function () {
    $(".prevent-multiple-submits").on("submit", function () {
        $(".multiple-submits")
            .text("Please Wait...")
            .attr("disabled", "disabled");
        return true;
    });
});
$("#clients").change(function () {
    $.ajax({
        url: "/wholesalers/ajax/getwholesaler/locations?id=" + $(this).val(),
        method: "GET",
        success: function (data) {
            $("#locations").html(data.html);
        },
    });
});

$("#methodtype").change(function () {
    var value = $("#methodtype").val();
    if (value == "0") {
        $("#ltorate").show();
        $("#ltolabel").html("Per Km (Ksh)");
        $("#ltolabel").show();

        $("#ltorate").attr("required", "");
        $("#isltoclosed").val("0");
    } else if (value == "1") {
        $("#ltorate").show();
        $("#ltolabel").html("Per Trip (Ksh)");
        $("#ltolabel").show();

        $("#ltorate").attr("required", "");
        $("#isltoclosed").val("0");
    }
});

$("#shcompany").change(function () {
    $.ajax({
        url: "thirdparties/specific?id=" + $(this).val(),
        method: "GET",
        success: function (data) {
            $("#shvehicle").html(data.html);
        },
    });
});

$(".sidebar-nav a")
    .filter(function () {
        return this.href == location.href.replace(/#.*/, "");
    })
    .closest("li")
    .addClass("current-page active");

$("input").on("input", function () {
    var pass = $("input[name=password]"),
        reps = $("input[name=password_confirmation]"),
        pass_cont = $("#password"),
        reps_cont = $("#password_confirmation");
    !$(this).is("[name=password]") ||
        $(function () {
            pass_cont.addClass(
                pass.val().length < 8 ? "has-error" : "has-success"
            );
        });
    !$(this).is("[name=password_confirmation]") ||
        $(function () {
            reps_cont
                .addClass(
                    reps.val() === pass.val() ? "has-success" : "has-error"
                )
                .removeClass(
                    reps.val() === pass.val() ? "has-error" : "has-success"
                );
        });
});

$(".toggle-password1").click(function () {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
$(".toggle-password2").click(function () {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
$("#markall").on("click", function () {
    $(".p_mark_all").prop("checked", this.checked);
});
