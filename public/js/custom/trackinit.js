$(document).ready(function() {
    // console.log("origin:" + origin);
    // console.log("dest:" + dest);
    // console.log("driver:" + driverloc);
    getCoordinater();
});

function getCoordinater() {
    $("#gmaps-route")
        .gmap3({
            zoom: 12,
            mapTypeControl: false,
            navigationControl: true,
            streetViewControl: false
        })
        .marker([
            driverloc.lat == null || driverloc.long == null
                ? {
                      address: origin,
                      icon:
                          "https://maps.google.com/mapfiles/marker_grey.png"
                  }
                : {
                      position: [driverloc.lat, driverloc.long],
                      icon: {
                          url:
                              "/images/lorry-512.png",
                          scaledSize: new google.maps.Size(50, 50)
                      }
                  },
            { position: [dest.lat, dest.long] }
        ])
        .route({
            origin:
                driverloc.lat == null || driverloc.long == null
                    ? origin
                    : [driverloc.lat, driverloc.long],
            destination: [dest.lat, dest.long],
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        })
        .directionsrenderer(function(results) {
            if (results) {
                return {
                    panel: "#box",
                    directions: results,
                    suppressMarkers: true
                };
            }
        });
}
