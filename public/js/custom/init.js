$("#getroutes").submit(function(e) {
    // Prevent form submission which refreshes page
    e.preventDefault();
    var origin = $("#origin").val();
    var dest = $("#destination").val();
    var getways = $("#waypoints").val();
    console.log(getways);

    getCoordinater(origin, dest, getways);
});

var mymap = new GMaps({
    el: "#gmaps-route",
    lat: -1.297024,
    lng: 36.819921,
    zoom: 14
});

function getCoordinater(origins, dest, getways) {
    var waypointsArray = [];
    //get waypoints

    for (var i = 0; i < getways.length; i++) {
        GMaps.geocode({
            address: getways[i],
            callback: function(results, status) {
                if (status == "OK") {
                    getlng = results[0].geometry.location;
                    var values = {
                        lat: parseFloat(getlng.lat()),
                        lng: parseFloat(getlng.lng())
                    };
                    waypointsArray.push({ location: values });
                    mymap.addMarker({
                        lat: getlng.lat(),
                        lng: getlng.lng()
                    });
                } else if (status == "ZERO_RESULTS") {
                    alert("Sorry, no results found");
                }
            }
        });
    }

    //end getwaypoints
    GMaps.geocode({
        address: origins,
        callback: function(results, status) {
            if (status == "OK") {
                latlng = results[0].geometry.location;
                mymap.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng(),
                    mouseover: function(e) {
                        //alert('Triggered');
                    }
                });

                GMaps.geocode({
                    address: dest,
                    callback: function(results, status) {
                        if (status == "OK") {
                            finlng = results[0].geometry.location;
                            mymap.addMarker({
                                lat: finlng.lat(),
                                lng: finlng.lng(),
                                mouseover: function(e) {
                                    //alert('Triggered');
                                }
                            });
                            mymap.setCenter(latlng.lat(), latlng.lng());
                            mymap.drawRoute({
                                origin: [latlng.lat(), latlng.lng()],
                                destination: [finlng.lat(), finlng.lng()],
                                waypoints: waypointsArray,
                                optimizeWaypoints: true,
                                provideRouteAlternatives: true,
                                travelMode: "driving",
                                strokeColor: "#4285F4",
                                strokeOpacity: 1,
                                strokeWeight: 6
                            });
                        } else if (status == "ZERO_RESULTS") {
                            alert("Sorry, no results found");
                        }
                    }
                });
                mymap.addMarker({
                    lat: -1.27197,
                    lng: 36.814328,
                    icon: {
                        url:
                            "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
                    },

                    mouseover: function(e) {
                        //alert('Triggered');
                    }
                });
            } else if (status == "ZERO_RESULTS") {
                alert("Sorry, no results found");
            }
        }
    });
}
//getCoordinater("3rd Parklands Avenue, Nairobi");

// GMaps.geolocate({
//   success: function(position) {
//       console.log(position.coords.latitude);
//     mymap.addMarker({
//         lat: position.coords.latitude,
//         lng: position.coords.longitude,
//         mouseover: function(e) {
//     //alert('Triggered');
//         }
//     });

//   },
//   error: function(error) {
//     alert('Geolocation failed: ' + error.message);
//   },
//   not_supported: function() {
//     alert("Your browser does not support geolocation");
//   },
//   always: function() {
//     alert("Always");
//   }

// });

// mymap.drawRoute({
//   origin: [48.8583701, 2.2944813],
//   destination: [47.8583701, 2.2944813],
//   travelMode: 'driving',
//   strokeColor: '#131540',
//   strokeOpacity: 0.6,
//   strokeWeight: 6
// });

// mymap.addMarker({
//   lat: 47.8583701,
//   lng: 2.2944813,
//   mouseover: function(e) {
//     //alert('Triggered');
//   }
// });
// GMaps.geocode({
//   address: 'Fort Granite Flats, Bishop Rd, Nairobi',
//   callback: function(results, status) {
//     if (status == 'OK') {
//       latlng = results[0].geometry.location;
//       mymap.setCenter(latlng.lat(), latlng.lng());
//       mymap.addMarker({
//   lat: latlng.lat(),
//   lng: latlng.lng(),
//   mouseover: function(e) {
//     //alert('Triggered');
//   }
// });
//     } else if (status == 'ZERO_RESULTS') {
//       alert('Sorry, no results found');
//     }
//   }
// });
