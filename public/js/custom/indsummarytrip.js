var markers = [];
$(document).ready(function () {
    getCoordinater();
});

function getCoordinater() {
    var path = [];

    for (let index = 0; index < points.length; index++) {
        path.push([points[index].latitude, points[index].longitude]);
        markers.push({
            position: [points[index].latitude, points[index].longitude],
            icon: "https://maps.google.com/mapfiles/marker_orange.png"
        });
    }
    runSnapToRoad(path);
}

function runSnapToRoad(path) {
    // console.log(path.join("|"));
    $.get(
        "https://roads.googleapis.com/v1/snapToRoads",
        {
            interpolate: true,
            key: "AIzaSyCG0a6Zjyy55yQ_KTPoTn_KikUAIH7pPx4",
            path: path.join("|")
        },
        function (data) {
            processSnapToRoadResponse(data);
        }
    );
}

function processSnapToRoadResponse(data) {
    snappedCoordinates = [];
    placeIdArray = [];
    for (var i = 0; i < data.snappedPoints.length; i++) {
        var latlng = new google.maps.LatLng(
            data.snappedPoints[i].location.latitude,
            data.snappedPoints[i].location.longitude
        );
        snappedCoordinates.push(latlng);
        placeIdArray.push(data.snappedPoints[i].placeId);
    }
    drawSnappedPolyline(snappedCoordinates);
}

// Draws the snapped polyline (after processing snap-to-road response).
function drawSnappedPolyline(snappedCoordinates) {
    $("#gmaps-route")
        .gmap3({
            zoom: 12,
            center: [-1.297024, 36.819921]
        })
        .marker(markers)
        .polyline({
            strokeColor: "#FF0000",
            strokeOpacity: 1.0,
            strokeWeight: 2,
            path: snappedCoordinates
        });

    // var snappedPolyline = new google.maps.Polyline({
    //     path: snappedCoordinates,
    //     strokeColor: "#add8e6",
    //     strokeWeight: 4,
    //     strokeOpacity: 0.9
    // });

    // snappedPolyline.setMap(map);
    // polylines.push(snappedPolyline);
}
