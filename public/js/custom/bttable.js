$(document).ready(function() {
    var $table = $("#demo-custom-toolbar"),
        $remove = $("#demo-delete-row");
    $table.on(
        "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
        function() {
            $remove.prop(
                "disabled",
                !$table.bootstrapTable("getSelections").length
            );
        }
    );
    $remove.click(function() {
        var ids = $.map($table.bootstrapTable("getSelections"), function(row) {
            return row.id;
        });
        $table.bootstrapTable("remove", {
            field: "id",
            values: ids
        });
        $remove.prop("disabled", true);
    });
});
$(window).on("load", function() {
    $('[data-toggle="table"]').show();
});
