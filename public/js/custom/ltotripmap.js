$(document).ready(function() {
    // console.log(window.ltopoints);
    getCoordinater();
});
function computeTotalDistance(result) {
    var totalDist = 0;
    var totalTime = 0;
    var myroute = result.routes[0];
    for (i = 0; i < myroute.legs.length; i++) {
        totalDist += myroute.legs[i].distance.value;
        totalTime += myroute.legs[i].duration.value;
    }
    totalDist = totalDist / 1000;
    console.log(totalDist);
    // document.getElementById("dvDistance").innerHTML = "total distance is: " + totalDist + " km<br>total time is: " + (totalTime / 60).toFixed(2) + " minutes";
}
function getCoordinater() {
    var waypoints = [];
    var ways = [];
    var data = [];
    var dest;
    var origin;
    if (window.ltopoints.length == 0) {
        $("#gmaps-route").gmap3({
            center: new google.maps.LatLng(-1.297024, 36.819921),
            zoom: 12
        });
        if(window.exceeded=="yes"){
            $("#totalkms").html("Points Have Exceeded Maxmimum Limit");
        }

    } else {
        for (let index = 0; index < window.ltopoints.length; index++) {
            if (index == 0) {
                origin = new google.maps.LatLng(
                    window.ltopoints[index].lat,
                    window.ltopoints[index].long
                );

                data.push({
                    position: [
                        window.ltopoints[index].lat,
                        window.ltopoints[index].long
                    ],
                    title: window.ltopoints[index].detail
                });
            } else if (index + 1 == window.ltopoints.length) {
                dest = new google.maps.LatLng(
                    window.ltopoints[index].lat,
                    window.ltopoints[index].long
                );
                data.push({
                    position: [
                        window.ltopoints[index].lat,
                        window.ltopoints[index].long
                    ],
                    title: window.ltopoints[index].detail
                });
                ways.push(
                    new google.maps.LatLng(
                        window.ltopoints[index].lat,
                        window.ltopoints[index].long
                    )
                );
            } else {
                waypoints.push({
                    location: new google.maps.LatLng(
                        window.ltopoints[index].lat,
                        window.ltopoints[index].long
                    ),
                    stopover: true
                });
                data.push({
                    position: [
                        window.ltopoints[index].lat,
                        window.ltopoints[index].long
                    ],
                    title: window.ltopoints[index].detail
                });
                ways.push(
                    new google.maps.LatLng(
                        window.ltopoints[index].lat,
                        window.ltopoints[index].long
                    )
                );
            }
        }

        $("#gmaps-route")
            .gmap3({
                center: origin,
                zoom: 12
            })
            .marker(data)
            .route({
                origin: origin,
                destination: dest,
                waypoints: waypoints,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            })
            .directionsrenderer(function(results) {
                if (results) {
                    var totalDistance = 0;
                    for (
                        let index = 0;
                        index < results.routes[0].legs.length;
                        index++
                    ) {
                        totalDistance +=
                            results.routes[0].legs[index].distance.value;
                    }
                    $("#totalkms").html(totalDistance / 1000+"Km");
                    // console.log(totalDistance);
                    return {
                        panel: "#box",
                        directions: results,
                        suppressMarkers: true
                    };
                }
            });
    }
}
