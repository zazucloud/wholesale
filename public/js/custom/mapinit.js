$(document).ready(function() {
    getCoordinater(addresses);
});

function getCoordinater(addresses) {
    var myOptions = {
        zoom: 12,
        center: new google.maps.LatLng(-1.297024, 36.819921)
    };
    var map = new google.maps.Map(
        document.getElementById("gmaps-route"),
        myOptions
    );
    var tmarkers=[];
    var iw = new google.maps.InfoWindow();
    // var oms = new OverlappingMarkerSpiderfier(map, {
    //     basicFormatEvents: true,
    //     // circleSpiralSwitchover:5,
    //     ignoreMapClick:true,
    //     // markersWontMove: true, 
    //     // markersWontHide: true,
    //     keepSpiderfied: true,
    //     spiralFootSeparation: 100,
    //     spiralLengthStart: 100,
    //     spiralLengthFactor: 100,
    //     circleFootSeparation: 100,
    //     // circleStartAngle: 3.14 / 12
    //      // allow the library to skip calculating advanced formatting information
    // });
    var getUrl = window.location;
    for (var i = 0, len = addresses.length; i < len; i++) {
        (function() {
            // make a closure over the marker and marker data
            var myLatlng = new google.maps.LatLng(
                parseFloat(addresses[i].latitude),
                parseFloat(addresses[i].longitude)
            );
            var baseUrl = getUrl .protocol + "//" + getUrl.host+'/invoice?invoicenum='+addresses[i].erp_invoicenum;
            var text =
            ' <a href="'+baseUrl+'">'+addresses[i].erp_invoicenum+'</a>';
            var marker = new google.maps.Marker({
                position: myLatlng,
                label: {
                    text: addresses[i].erp_invoicenum,
                    color: "black",
                    fontSize: "16px",
                    fontWeight: "bold"
                },
                title:addresses[i].erp_invoicenum
            }); 
            tmarkers.push(marker);
            // google.maps.event.addListener(marker, "spider_click", function(e) {
                
            //     iw.setContent(text);
            //     iw.open(map, marker);
            // });
            // oms.addMarker(marker); // adds the marker to the spiderfier _and_ the map
        })();
    }

    var cluster =new MarkerClusterer(map, tmarkers, {
        imagePath:
          "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
          zoomOnClick: false
  
      });

      google.maps.event.addListener(cluster, 'clusterclick', function(cluster) {

        var markers = cluster.getMarkers();
    
        var array = [];
        var num = 0;
    
        for (i = 0; i < markers.length; i++) {
    
          num++;
          var baseUrl = getUrl .protocol + "//" + getUrl.host+'/invoice?invoicenum='+markers[i].getTitle();
            // var text =
            // ' <a href="'+baseUrl+'" target="_blank">'+markers[i].getTitle()+'</a>';
            var text =
            ' <a href="'+baseUrl+'">'+markers[i].getTitle()+'</a>';
          array.push(text + '<br>');
        }
        iw.setContent(markers.length + " markers<br><br>" + array);
        iw.setPosition(cluster.getCenter());
        iw.open(map);
      });
}

// function getCoordinater(places) {
//     var data = [];
//     for (var i = 0; i < places.length; i++) {
//         data.push({
//             position: [places[i].latitude,places[i].longitude],
//             label: {
//                 text: places[i].erp_invoicenum,
//                 color: "black",
//                 fontSize: "16px",
//                 fontWeight: "bold"
//             },
//         });
//     }

//     $("#gmaps-route")
//         .gmap3({
//             center: [-1.297024, 36.819921],
//             zoom: 12
//         })
//         .marker(data)
//         .on('click', function (marker, event) {
//             var getUrl = window.location;
//             var baseUrl = getUrl .protocol + "//" + getUrl.host+'/invoice?invoicenum='+marker.label.text;
//             window.location.href=baseUrl;

//           })
//         ;
// }
