$(document).ready(function() {
    getCoordinater();
});

function getCoordinater() {
    var waypoints = [];
    var data = [];
    var dest;

    for (let index = 0; index < addresses.length; index++) {
        if (index == 0) {
            dest = new google.maps.LatLng(
                addresses[index].lat,
                addresses[index].long
            );

            data.push({
                position: [addresses[index].lat, addresses[index].long],
                title: addresses[index].partialsno
            });
        } else {
            waypoints.push({
                location: new google.maps.LatLng(
                    addresses[index].lat,
                    addresses[index].long
                ),
                stopover: true
            });
            data.push({
                position: [addresses[index].lat, addresses[index].long],
                title: addresses[index].partialsno
            });
        }
    }
    for (let index = 0; index < addlocations.length; index++) {
        if (index == 0 && dest == null) {
            dest = new google.maps.LatLng(
                addlocations[index].lat,
                addlocations[index].long
            );

            data.push({
                position: [addlocations[index].lat, addlocations[index].long],
                title: addlocations[index].name,
                icon: "https://maps.google.com/mapfiles/marker_orange.png"
            });
        } else {
            waypoints.push({
                location: new google.maps.LatLng(
                    addlocations[index].lat,
                    addlocations[index].long
                ),
                stopover: true
            });
            data.push({
                position: [addlocations[index].lat, addlocations[index].long],
                title: addlocations[index].name,
                icon: "https://maps.google.com/mapfiles/marker_orange.png"
            });
        }
    }

    if (driverloc.lat == null || driverloc.long == null) {
        data.push({
            position: origin,
            position: [origin.lat, origin.long],
            icon: "https://maps.google.com/mapfiles/marker_grey.png"
        });
    } else {
        data.push({
            position: [driverloc.lat, driverloc.long],
            icon: "https://maps.google.com/mapfiles/marker_green.png"
        });
    }

    console.log(waypoints);
    $("#gmaps-route")
        .gmap3({
            center:
                driverloc.lat == null || driverloc.long == null
                    ? [origin.lat, origin.long]
                    : [driverloc.lat, driverloc.long],
            zoom: 12
        })
        .marker(data)
        .route({
            origin:
                driverloc.lat == null || driverloc.long == null
                    ? [origin.lat, origin.long]
                    : [driverloc.lat, driverloc.long],
            destination: dest,
            waypoints: waypoints,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        })
        .directionsrenderer(function(results) {
            if (results) {
                return {
                    panel: "#box",
                    directions: results,
                    suppressMarkers: true
                };
            }
        });
}
